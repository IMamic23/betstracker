//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Oklade.DALEntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class MLBStartersStatsAll
    {
        public int ID { get; set; }
        public string PlayerName { get; set; }
        public string Team { get; set; }
        public Nullable<int> W { get; set; }
        public Nullable<int> L { get; set; }
        public string ERA { get; set; }
        public Nullable<int> G { get; set; }
        public Nullable<int> GS { get; set; }
        public Nullable<int> SV { get; set; }
        public Nullable<int> SVO { get; set; }
        public string IP { get; set; }
        public Nullable<int> H { get; set; }
        public Nullable<int> R { get; set; }
        public Nullable<int> ER { get; set; }
        public Nullable<int> HR { get; set; }
        public Nullable<int> BB { get; set; }
        public Nullable<int> SO { get; set; }
        public string AVG { get; set; }
        public string WHIP { get; set; }
        public string Venue { get; set; }
    }
}

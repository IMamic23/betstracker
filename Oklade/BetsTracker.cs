﻿using Microsoft.Win32;
using Oklade.BetsTrackerFolder;
using Oklade.DALEntityFramework;
using Oklade.SharedMethodsCollection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Oklade
{
    public partial class BetsTracker : Form
    {
        NBAMLBScores _nbamlbScores = new NBAMLBScores();
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private BetsTrackerActions _bta = new BetsTrackerActions();
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private SharedMethods _sharedmethods = new SharedMethods();
        TeamLogos teamLogos = new TeamLogos();
        private readonly List<string> _gameslist = new List<string>();
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private double[] _handicapDoubles = new double[5];
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private string[] _handicapStrings = new string[5];
        private string _homeaway;
        private string _homeaway1;
        private string _homeaway2;
        private string _timenoww2;
        private SqlDataAdapter _adapter;
        private SqlDataAdapter _adapter2;

        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private SqlConnection _connection;

        private DataSet _ds = new DataSet();
        private DataSet _ds2 = new DataSet();
        private BindingSource _bs1 = new BindingSource();
        private string _mlWins;
        private string _mlLosses;
        private string _atssUr;
        private string _leaguestring;
        private string _scoreFinal;
        private string _sql2;
        private string _sql3;
        private string _sql4;
        private string SqlActiveTeams;
        private string lastresult;
        private string connectionString;
        private string value;
        private string ID = "";
        private string ID2 = "";
        private string ID22;
        private string status;
        private string vrijednost;
        private string vrijednost2;
        private string vrijednost3;
        private string vrj;
        private string money;
        private string tb3;
        private string logo;
        private string IDKluba1;
        private string profit1;
        private string liga;
        private double profit;
        private double profit2;
        private double profit3;
        private int x2;
        private double tot;
        private double am1;
        private double am2;
        private double avg;
        private double suma;
        private string[] strr = new string[15];
        private string version;
        private int check1 = 0;
        private ToolTip t1 = new ToolTip();
        private string winlable;
        private string losslable;
        private string OverWin;
        private string OverLoss;
        private string UnderWin;
        private string UnderLoss;
        private string _strlratsr;
        private string strlrour;
        private string strlrteam;
        private string strlropp;
        private int nooflogins;
        private string timenoww;
        private string strlastresult;
        private int ID7;
        private double profit7;
        private double IO7;
        private double odds7;
        private int saveRow = 0;
        private string coversstring;
        private double dailybetstotal;
        private string dailystatscheck;
        private int j;
        private int i;
        private int k;
        private string _isDefaultGroup;
        int _daysBackFwd = 0;
        private string _date;
        //string _date = DateTime.Today.ToString("yyyy-MM-dd");

        public BetsTracker()
        {
            SplashFormOpenProgram.ShowSplashScreenOpen();
            InitializeComponent();

            System.Drawing.Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new System.Drawing.Point(workingArea.Right - Size.Width,
                                      workingArea.Bottom - Size.Height);

            radioButton1.Checked = true;
            _connection = new SqlConnection(connectionString);

            DateTime dtCheck = DateTime.Today;

            if (dtCheck.Month < 5 || dtCheck.Month > 10)
            {
                // NBAradioButton.Checked = true;
                BestBetsRB.Checked = true;
            }
            else
            {
                // MLBradiobtn.Checked = true;
                BestBetsRB.Checked = true;
            }
            var culture = new CultureInfo(ConfigurationManager.AppSettings["DefaultCulture"]);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            //// IS DEFAULT BETS GROUP////
            string getDefaultGroupSelected = @"SELECT BetsGroup from BetsGroups where isDefault = 'true'";
            _isDefaultGroup = _sharedmethods.GetStringSqlResponse(connectionString, getDefaultGroupSelected);
            _bta.SetDefaultGroup(_isDefaultGroup, NBAradioButton, NHLradioButton, MLBradioButton, BestBetsRB, isDefault);
            /////////////// END //////////////////

            Scores(_date);

            SplashFormOpenProgram.CloseForm();
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        private void Active(object sender, EventArgs e)
        {
            /// by Ivica Mamic
            ///////// ACTIVE TEAMS ////////////////

            SqlActiveTeams = "select ID,ImeKluba,Sport,League,Active from Klubovi";

            _connection.Open();
            try
            {
                var cmd223 = new SqlCommand(SqlActiveTeams, _connection);
                _adapter2 = new SqlDataAdapter(cmd223);
                _ds2.Reset();
                _adapter2.Fill(_ds2);
                AllBetsDGV.DataSource = _ds2.Tables[0];
                button9.Show();

                var chk = new DataGridViewCheckBoxColumn();
                chk.HeaderText = @"Active";
                chk.Name = "chk";
                AllBetsDGV.Columns.Add(chk);
                AllBetsDGV.Columns.Remove("active");
                AllBetsDGV.Columns[0].Width = 20;

                for (int intCount = 0; intCount < _ds2.Tables[0].Rows.Count; intCount++)
                {
                    if (_ds2.Tables[0].Rows[intCount][4].ToString() == "1")
                    {
                        AllBetsDGV.Rows[intCount].Cells["chk"].Value = true;
                    }
                    else if (_ds2.Tables[0].Rows[intCount][4].ToString() == "0")
                    {
                        AllBetsDGV.Rows[intCount].Cells["chk"].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            // }
        }

        private void Updateactive(object sender, EventArgs e)
        {
            ///////// UPDATE ACTIVE TEAMS ////////////////

            var cmdactive = new SqlCommandBuilder(_adapter2);

            var row = new DataGridViewRow();
            for (i = 0; i < AllBetsDGV.Rows.Count; i++)
            {
                row = AllBetsDGV.Rows[i];
                switch (Convert.ToBoolean(row.Cells["chk"].Value))
                {
                    case true:
                        _ds2.Tables[0].Rows[i][4] = "1";
                        break;
                    case false:
                        _ds2.Tables[0].Rows[i][4] = "0";
                        break;
                }
            }
            try
            {
                _adapter2.Update(_ds2);
                MessageBox.Show(@"Updated!", @"Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            _connection.Close();
            AllBetsDGV.Columns.Remove("chk");
            BetsTracker_Load(sender, e);
            TeamslistBox1.SelectedIndex = 0;
        }

        private void Sum(object sender, EventArgs e)
        {
            /// UKUPNI PROFIT ///

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string ukProfit = "SELECT SUM(PlusMinus) FROM bets;";
                var c1 = new SqlCommand(ukProfit, connection);
                var r1 = c1.ExecuteReader();
                try
                {
                    while (r1.Read())
                    {
                        money = (r1.GetSqlValue(0).ToString());
                    }

                    label7.Text = String.Format("${0:n}", money).Replace(".", ",");

                    double t6 = Convert.ToDouble(money);
                    label7.ForeColor = t6 > 0 ? Color.Green : Color.Black;
                    r1.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private static readonly DateTime expiration = new DateTime(2013, 10, 31);
        private double odds;
        private double IznosOklade;
        private double iznosOklade;
        private string query;
        private string profit4;

        public static DateTime GetFastestNISTDate()
        {
            var result = DateTime.MinValue;

            // Initialize the list of NIST time servers
            // http://tf.nist.gov/tf-cgi/servers.cgi
            string[] servers = new string[] {
        "nist1-ny.ustiming.org",
        "nist1-nj.ustiming.org",
        "nist1-pa.ustiming.org",
        "time-a.nist.gov",
        "time-b.nist.gov",
        "nist1.aol-va.symmetricom.com",
        "nist1.columbiacountyga.gov",
        "nist1-chi.ustiming.org",
        "nist.expertsmi.com",
        "nist.netservicesgroup.com"
};

            // Try 5 servers in random order to spread the load
            Random rnd = new Random();
            foreach (string server in servers.OrderBy(s => rnd.NextDouble()).Take(5))
            {
                try
                {
                    // Connect to the server (at port 13) and get the response
                    string serverResponse = string.Empty;
                    using (var reader = new StreamReader(new System.Net.Sockets.TcpClient(server, 13).GetStream()))
                    {
                        serverResponse = reader.ReadToEnd();
                    }

                    // If a response was received
                    if (!string.IsNullOrEmpty(serverResponse))
                    {
                        // Split the response string ("55596 11-02-14 13:54:11 00 0 0 478.1 UTC(NIST) *")
                        string[] tokens = serverResponse.Split(' ');

                        // Check the number of tokens
                        if (tokens.Length >= 6)
                        {
                            // Check the health status
                            string health = tokens[5];
                            if (health == "0")
                            {
                                // Get date and time parts from the server response
                                string[] dateParts = tokens[1].Split('-');
                                string[] timeParts = tokens[2].Split(':');

                                // Create a DateTime instance
                                DateTime utcDateTime = new DateTime(
                                    Convert.ToInt32(dateParts[0]) + 2000,
                                    Convert.ToInt32(dateParts[1]), Convert.ToInt32(dateParts[2]),
                                    Convert.ToInt32(timeParts[0]), Convert.ToInt32(timeParts[1]),
                                    Convert.ToInt32(timeParts[2]));

                                // Convert received (UTC) DateTime value to the local timezone
                                result = utcDateTime.ToLocalTime();

                                return result;
                                // Response successfully received; exit the loop
                            }
                        }
                    }
                }
                catch
                {
                    // Ignore exception and try the next server
                }
            }
            return result;
        }

        private void BetsTracker_Load(object sender, EventArgs e)
        {
            /// BETS TRACKER LOAD ///
            this.SuspendLayout();

            TeamLogosPicBox.Image = null;

            dateTimePicker1.CustomFormat = "dd.MM.yyyy.";
            resultsDates.CustomFormat = "yyyy-MM-dd";

            /// CHECK DEFAULT CHECKED ///
            isDefault.Checked = false;
            _bta.CheckDefaultChecked(_isDefaultGroup, NBAradioButton, NHLradioButton, MLBradioButton, BestBetsRB, isDefault);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                /// EXPIRATION DATE ///
                try
                {
                    string datumm = DateTime.Now.ToShortDateString();

                    DateTime date1;
                    DateTime date2;
                    date1 = DateTime.Now;
                    date2 = Convert.ToDateTime("30.12.2020.");
                    int result = DateTime.Compare(date1, date2);
                    TimeSpan t = date2 - date1;
                    double nrOfDays = t.TotalDays;
                    if (nrOfDays < 0)
                    {
                        MessageBox.Show(@"Your free trial has expired. Please register to continue using the application", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        System.Windows.Forms.Application.Exit();
                    }

                    label33.Text = "";
                    label8.Text = "";

                    /// NUMBER OF LOGINS SEC ///
                    ///
                    const string logins = "SELECT Logins from Users where ID = 1";
                    SqlCommand cmd27 = new SqlCommand(logins, connection);
                    SqlDataReader read27 = cmd27.ExecuteReader();
                    while (read27.Read())
                    {
                        nooflogins = (Convert.ToUInt16(read27["Logins"]));
                    }
                    read27.Close();

                    if (nooflogins > 2000)
                    {
                        MessageBox.Show("Your free trial has expired. Please register to continue using the application", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        System.Windows.Forms.Application.Exit();
                    }

                    nooflogins = nooflogins + 1;

                    string query778 = @"UPDATE Users set Logins = " + nooflogins + "";
                    SqlCommand cmd778 = new SqlCommand(query778, connection);

                    cmd778.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                ///////// TRIAL PERIOD EXPIRATION   /////////

                int period = 60; // trial period
                string keyName = "Software\\OklApp\\MyApp";
                long ticks = DateTime.Today.Ticks;

                RegistryKey rootKey = Registry.CurrentUser;
                RegistryKey regKey = rootKey.OpenSubKey(keyName);
                if (regKey == null) // first time app has been used
                {
                    regKey = rootKey.CreateSubKey(keyName);
                    long expiry = DateTime.Today.AddDays(period).Ticks;
                    regKey.SetValue("expiry", expiry, RegistryValueKind.QWord);
                    regKey.Close();
                }
                else
                {
                    long expiry = (long)regKey.GetValue("expiry");
                    regKey.Close();
                    long today = DateTime.Today.Ticks;
                    if (today > expiry)
                    {
                        MessageBox.Show("Your free trial has expired. Please register to continue using the application", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.Close();
                    }
                }
                //////////////////////////////////////////////////////////////////////

                Sum(sender, e);
                button9.Hide();
                leagueLogoPb.Hide();

                ProfitBox.ReadOnly = true;

                /// SET LABELS ///
                label17.Text = "";
                label18.Text = "";
                label19.Text = "";
                label20.Hide();
                label9.Hide();
                label16.Hide();

                label21.Hide();
                label22.Hide();
                label23.Hide();
                label24.Hide();
                label25.Hide();
                label26.Hide();
                label27.Hide();
                label28.Hide();
                label29.Hide();
                label30.Hide();
                label31.Hide();
                label32.Hide();

                if (NBAradioButton.Checked == true)
                {
                    _leaguestring = "NBA";
                    Leaguestats();
                }
                else if (NHLradioButton.Checked == true)
                {
                    _leaguestring = "NHL";
                    Leaguestats();
                }
                else if (MLBradioButton.Checked == true)
                {
                    _leaguestring = "MLB";
                    Leaguestats();
                }
                else if (BestBetsRB.Checked == true)
                {
                    _leaguestring = "BestBets";
                    Leaguestats();
                }
                else if (CasinoRB.Checked == true)
                {
                    _leaguestring = "Casino";
                    Leaguestats();
                }

                version = System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed ? System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString() : Assembly.GetExecutingAssembly().GetName().Version.ToString();

                versionLabel.Text = "Copyright © 2014 \n IMS-Studio  v" + version;

                string[] s2 = { "", "1,806", "1,813", "1,833", "1,84", "1,869", "1,877", "1,884", "1,892", "1,9", "1,909", "1,917", "1,925", "1,934", "1,952", "2,0" };
                OddsBox.DataSource = s2;

                if (MLBradioButton.Checked)
                {
                    liga = "MLB";
                    string[] s = { "", "U5", "O5", "U5,5", "O5,5", "U6", "O6", "U6,5", "O6,5", "U7", "O7", "U7,5", "O7,5", "U8", "O8", "U8,5", "O8,5", "U9", "O9", "U9,5", "O9,5", "U10", "O10", "U10,5", "U11" };
                    OfferBox.DataSource = s;
                }
                else if (NBAradioButton.Checked)
                {
                    liga = "NBA";
                    string[] s = { "", "U" };
                    OfferBox.DataSource = s;
                }
                else if (NHLradioButton.Checked == true)
                {
                    liga = "NHL";
                    string[] s = { "", "U4,5", "O4,5", "U5", "O5", "U5,5", "O5,5", "U6", "O6", "U6,5" };
                    OfferBox.DataSource = s;
                }
                else if (BestBetsRB.Checked == true)
                {
                    liga = "BestBets";
                    string[] s = { "", "U" };
                    OfferBox.DataSource = s;
                }
                else if (CasinoRB.Checked == true)
                {
                    liga = "Casino";
                    string[] s = { "", "black", "red", "odd", "even", "1-18", "19-36" };
                    OfferBox.DataSource = s;
                }

                // IME KLUBA U LISTBOX //
                _sql2 = "select ImeKluba from Klubovi where Active = 1 and league = '" + liga + "' order by League,ImeKluba";
                _sql3 = "select Value from Vrijednosti where Sport = 'NBA'";

                if (NBAradioButton.Checked == true)
                {
                    _sql3 = "select Value from Vrijednosti where Sport = 'NBA'";
                }
                if (BestBetsRB.Checked == true)
                {
                    _sql3 = "select Value from Vrijednosti where Sport = 'NBA'";
                }
                else if (NHLradioButton.Checked == true)
                {
                    _sql3 = "select Value from Vrijednosti where Sport = 'NHL'";
                }
                else if (CasinoRB.Checked == true)
                {
                    _sql3 = "select Value from Vrijednosti where Sport = 'Casino'";
                }
                try
                {
                    TeamslistBox1.Items.Clear();

                    DataSet myDataSet = new DataSet();

                    _adapter = new SqlDataAdapter(_sql2, connection);

                    _adapter.Fill(myDataSet, "ImeKluba");

                    DataTable myDataTable = myDataSet.Tables[0];

                    DataRow tempRow = null;

                    foreach (DataRow tempRow_Variable in myDataTable.Rows)
                    {
                        tempRow = tempRow_Variable;

                        TeamslistBox1.Items.Add((tempRow["ImeKluba"]));
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                try
                {
                    // VALUE U COMBOBOX //
                    NextBetBox.Items.Clear();

                    DataSet myDataSet2 = new DataSet();

                    _adapter2 = new SqlDataAdapter(_sql3, connection);

                    _adapter2.Fill(myDataSet2, "Value");

                    System.Data.DataTable myDataTable2 = myDataSet2.Tables[0];

                    DataRow tempRow2 = null;

                    foreach (DataRow tempRow2_Variable in myDataTable2.Rows)
                    {
                        tempRow2 = tempRow2_Variable;

                        NextBetBox.Items.Add((tempRow2["Value"]));
                    }

                    if (NBAradioButton.Checked)
                    {
                        leagueLogoPb.Show();
                        leagueLogoPb.Image = Oklade.Properties.Resources.nba_logo_png;
                    }
                    else if (MLBradioButton.Checked)
                    {
                        leagueLogoPb.Show();
                        leagueLogoPb.Image = Oklade.Properties.Resources.mlb2;
                    }
                    else if (NHLradioButton.Checked)
                    {
                        leagueLogoPb.Show();
                        leagueLogoPb.Image = Oklade.Properties.Resources.NHLLogo;
                    }
                    else if (BestBetsRB.Checked)
                    {
                        leagueLogoPb.Show();
                        leagueLogoPb.Image = Oklade.Properties.Resources.BestBets;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
              
            }
            this.ResumeLayout();
        }

        private void Leaguestats()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                try
                {
                    string query20 = "SELECT count(*) as OverWin FROM Bets,Klubovi where Bets.offer like '%O%' and Bets.statuswl = 'WIN' and Bets.IDKluba = Klubovi.ID and Klubovi.League = '" + _leaguestring + "'";
                    SqlCommand cmd20 = new SqlCommand(query20, connection);
                    SqlDataReader read20 = cmd20.ExecuteReader();
                    while (read20.Read())
                    {
                        OverWin = (read20["OverWin"].ToString());
                    }
                    read20.Close();
                    label27.Text = OverWin;
                    label27.ForeColor = Color.Green;
                    ////////////////////////////////////////////////////////////////////////////
                    string query21 = "SELECT count(*) as OverLoss FROM Bets,Klubovi where Bets.offer like '%O%' and Bets.statuswl = 'LOSS' and Bets.IDKluba = Klubovi.ID and Klubovi.League = '" + _leaguestring + "'";
                    SqlCommand cmd21 = new SqlCommand(query21, connection);
                    SqlDataReader read21 = cmd21.ExecuteReader();
                    while (read21.Read())
                    {
                        OverLoss = (read21["OverLoss"].ToString());
                    }
                    read21.Close();
                    label28.Text = OverLoss;
                    //    label28.ForeColor = Color.Red;

                    /////////////////////////////////////////////////////////
                    string query22 = "SELECT count(bets.offer) as UnderWin FROM Bets, Klubovi where Bets.offer like '%U%' and Bets.statuswl = 'WIN' and Bets.IDKluba = Klubovi.ID and Klubovi.League = '" + _leaguestring + "' ";
                    SqlCommand cmd22 = new SqlCommand(query22, connection);
                    SqlDataReader read22 = cmd22.ExecuteReader();
                    while (read22.Read())
                    {
                        UnderWin = (read22["UnderWin"].ToString());
                    }
                    read22.Close();
                    label30.Text = UnderWin;
                    label30.ForeColor = Color.Green;
                    /////////////////////////////////////////////////////////
                    string query23 = "SELECT count(bets.offer) as UnderLoss FROM Bets, Klubovi where Bets.offer like '%U%' and Bets.statuswl = 'LOSS' and Bets.IDKluba = Klubovi.ID and Klubovi.League = '" + _leaguestring + "'";
                    SqlCommand cmd23 = new SqlCommand(query23, connection);
                    SqlDataReader read23 = cmd23.ExecuteReader();
                    while (read23.Read())
                    {
                        UnderLoss = (read23["UnderLoss"].ToString());
                    }
                    read23.Close();
                    label29.Text = UnderLoss;
                    //   label29.ForeColor = Color.Red;
                    /////////////////////////////////////////////////////////

                    string queryWins = "SELECT Count(Bets.ID) as wins FROM Bets, Klubovi where Bets.Offer LIKE '%[+-]%' and Bets.StatusWL = 'WIN' and Bets.IDKluba = Klubovi.ID and Klubovi.League = '" + _leaguestring + "'";
                    SqlCommand cmdWins = new SqlCommand(queryWins, connection);
                    SqlDataReader readWins = cmdWins.ExecuteReader();
                    while (readWins.Read())
                    {
                        _mlWins = (readWins["wins"].ToString());
                    }
                    readWins.Close();

                    label32.Text = _mlWins;

                    string queryLosses = "SELECT Count(Bets.ID) as losses FROM Bets, Klubovi where Bets.Offer LIKE '%[+-]%' and Bets.StatusWL = 'LOSS' and Bets.IDKluba = Klubovi.ID and Klubovi.League = '" + _leaguestring + "'";
                    SqlCommand cmdLosses = new SqlCommand(queryLosses, connection);
                    SqlDataReader readLosses = cmdLosses.ExecuteReader();
                    while (readLosses.Read())
                    {
                        _mlLosses = (readLosses["losses"].ToString());
                    }
                    readLosses.Close();

                    label31.Text = _mlLosses;

                    /////////////////////////////////////////////////////////
                }
                catch (Exception ex3)
                {
                    MessageBox.Show(ex3.ToString());
                }
            }
        }

        private void Linkovi()
        {
            string link1 = "http://www.pinnaclesports.com/default.aspx";
            System.Diagnostics.Process.Start(link1);
            string link2 = "http://www.5dimes.eu/";
            System.Diagnostics.Process.Start(link2);
            string link3 = "http://www.bet365.com/home";
            System.Diagnostics.Process.Start(link3);
            string link4 = "http://scores.covers.com/basketball-scores-matchups.aspx";
            System.Diagnostics.Process.Start(link4);
            string link5 = "http://scores.covers.com/hockey-scores-matchups.aspx";
            System.Diagnostics.Process.Start(link5);
            string link6 = "http://scores.covers.com/baseball-scores-matchups.aspx";
            System.Diagnostics.Process.Start(link6);
        }

        private void btn_connect_Click(object sender, EventArgs e)
        {
        }

        private void pushBtn_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // PUSH BUTTON //
                connection.Open();
                Sum(sender, e);

                //string queryy = "Select TOP 1 ID from Bets where IDKluba = '" + ID + "' order by ID desc";
                if (TeamslistBox1.SelectedIndex == -1)
                {
                    try
                    {
                        if (status == "")
                        {
                            double profit = Convert.ToDouble(profit1);
                            double odds = Convert.ToDouble(OddsBox.Text);
                            double IznosOklade = Convert.ToDouble(LastBetBox.Text);

                            double profit2 = IznosOklade;
                            double profit3 = profit;
                            profit3 = Math.Round(profit3, 2);
                            string profit4 = profit3.ToString();
                            string query = @"UPDATE Bets SET profit = " + profit3.ToString().Replace(",", ".") + ", IznosOklade = " + IznosOklade.ToString().Replace(",", ".") +
                                ", odds = " + odds.ToString().Replace(",", ".") + ", status = 2, statusWL = 'PUSH', PlusMinus = 0 where IDKluba = '" + ID + "' and ID = '" + ID2 + "';";
                            SqlCommand cmd = new SqlCommand(query, connection);

                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                    saveRow = 0;
                    if (AllBetsDGV.Rows.Count > 0)
                        saveRow = AllBetsDGV.FirstDisplayedCell.RowIndex;

                    dateTimePicker1_ValueChanged(sender, e);
                }
                else
                {
                    try
                    {
                        // if (WINbutton1.BackColor == Color.DimGray)
                        if (status == "")
                        {
                            double profit = Convert.ToDouble(profit1);
                            double odds = Convert.ToDouble(OddsBox.Text);
                            double IznosOklade = Convert.ToDouble(LastBetBox.Text);

                            double profit2 = IznosOklade;
                            double profit3 = profit;
                            profit3 = Math.Round(profit3, 2);
                            string profit4 = profit3.ToString();
                            string query = @"UPDATE Bets SET profit = " + profit3.ToString().Replace(",", ".") + ", IznosOklade = " + IznosOklade.ToString().Replace(",", ".") +
                                ", odds = " + odds.ToString().Replace(",", ".") + ", status = 2, statusWL = 'PUSH', PlusMinus = 0 where IDKluba = '" + ID + "' and ID = '" + ID2 + "';";
                            SqlCommand cmd = new SqlCommand(query, connection);

                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                    listBox1_SelectedValueChanged(sender, e);
                }
            }
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // pictureBox1.Hide();

                AllBetsDGV.DataSource = null;

                connection.Open();
                Sum(sender, e);

                button9.Hide();

                value = (string)this.TeamslistBox1.SelectedItem;

                label33.Text = "";
                label8.Text = "";
                label17.Text = "";
                label18.Text = "";
                label19.Text = "";
                label20.Hide();
                label33.Text = "";

                TeamLogosPicBox.Image = null;

                if (NBAradioButton.Checked == true || MLBradioButton.Checked == true)
                {
                    if (NBAradioButton.Checked == true)
                    {
                        lastresult = "SELECT TOP 1 Team,Opp,Score,ATSr,OUr FROM AllNBAResults where team Like '" + value.Split(' ')[1] + "' or opp Like '" + value.Split(' ')[1] + "' order by ID desc";
                        _atssUr = "ATSr";
                        _scoreFinal = "Score";
                    }
                    else if (MLBradioButton.Checked == true)
                    {
                        lastresult = "SELECT TOP 1 Team,Opp,Final,SUr,OUr FROM AllMLBResults where team Like '" + value.Split(' ')[1] + "' or opp Like '" + value.Split(' ')[1] + "' order by ID desc";
                        _atssUr = "SUr";
                        _scoreFinal = "Final";
                    }

                    SqlCommand cmdlr = new SqlCommand(lastresult, connection);
                    SqlDataReader readlr = cmdlr.ExecuteReader();
                    while (readlr.Read())
                    {
                        strlrteam = (readlr["Team"].ToString());
                        strlropp = (readlr["Opp"].ToString());
                        strlastresult = (readlr["" + _scoreFinal + ""].ToString());
                        _strlratsr = (readlr["" + _atssUr + ""].ToString());
                        strlrour = (readlr["OUr"].ToString());
                    }
                    readlr.Close();

                    if (strlrteam != value.Split(' ')[1])
                    {
                        if (_strlratsr == "W")
                        {
                            _strlratsr = "L";
                        }
                        else if (_strlratsr == "L")
                        {
                            _strlratsr = "W";
                        }
                    }

                    label8.Text = "   " + strlrteam + "-" + strlropp + "\n" + strlastresult + " | ATS: " + _strlratsr + " | OU: " + strlrour;
                }

                checkBox1.Checked = false;
                winBtn.Enabled = true;
                failBtn.Enabled = true;
                pushBtn.Enabled = true;
                updatebtn.Enabled = true;
                check1 = 0;
                OfferBox.Text = "O/U";

                string query = "Select ID from Klubovi where ImeKluba = '" + value + "'";
                SqlCommand cmd = new SqlCommand(query, connection);
                SqlDataReader read2 = cmd.ExecuteReader();
                try
                {
                    while (read2.Read())
                    {
                        ID = (read2["ID"].ToString());
                    }
                    read2.Close();

                    // --------------------------------------------------------- //
                    string query23 = "Select COUNT(*) from Bets where IDKluba = " + ID + " and statusWL ='WIN'";
                    SqlCommand cmd23 = new SqlCommand(query23, connection);
                    SqlDataReader read23 = cmd23.ExecuteReader();

                    while (read23.Read())
                    {
                        winlable = (read23[0].ToString());
                    }
                    read23.Close();

                    string query24 = "Select COUNT(*) from Bets where IDKluba = " + ID + " and statusWL ='LOSS'";
                    SqlCommand cmd24 = new SqlCommand(query24, connection);
                    SqlDataReader read24 = cmd24.ExecuteReader();

                    while (read24.Read())
                    {
                        losslable = (read24[0].ToString());
                    }
                    read24.Close();

                    // ------------------------------------------------------------ //
                    if (losslable != "")
                    {
                        label9.Show();
                        label16.Show();
                    }

                    label17.Text = winlable;
                    label18.Text = losslable;
                    label17.ForeColor = Color.Green;
                    label18.ForeColor = Color.Black;

                    string query333 = "Select COUNT(1) from Bets where IDKluba = '" + ID + "';";
                    SqlCommand cmd333 = new SqlCommand(query333, connection);
                    SqlDataReader read333 = cmd333.ExecuteReader();

                    while (read333.Read())
                    {
                        IDKluba1 = read333[0].ToString();
                    }
                    read333.Close();

                    if (IDKluba1 == "0")
                    {
                        string query111 = @"INSERT INTO Bets (IDKluba, profit, iznosoklade,status, odds, date, IDVrijednost,PlusMinus,StatusWL)
                                                      VALUES ( " + ID + ",'0.00','0.0','1','1.000', '-', 1, 0, 'START');";
                        SqlCommand cmd111 = new SqlCommand(query111, connection);

                        cmd111.ExecuteNonQuery();
                    }

                    teamLogos.LoadTeamsLogos(TeamLogosPicBox, value);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                // POPUNJAVANJE TEXTBOXA //

                SqlCommand command = new SqlCommand("select TOP 1 IznosOklade,Profit,Odds,Status,IDVrijednost from Bets where IDKluba = '" + ID + "' and Status IS NULL order by ID", connection);
                //comboBox4.Clear();
                ProfitBox.Clear();
                LastBetBox.Clear();
                SqlDataReader read = command.ExecuteReader();
                try
                {
                    while (read.Read())
                    {
                        LastBetBox.Text = (read["IznosOklade"].ToString());
                        tb3 = (read["Profit"].ToString());
                        OddsBox.Text = (read["Odds"].ToString());
                        status = (read["Status"].ToString());
                        vrijednost = (read["IDVrijednost"].ToString());
                    }
                    read.Close();

                    ProfitBox.Text = String.Format("${0:n}", tb3);

                    if (LastBetBox.Text == "")
                    {
                        SqlCommand command2 = new SqlCommand("select TOP 1 IznosOklade,Profit,Odds,Status,IDVrijednost from Bets where IDKluba = '" + ID + "' order by ID desc", connection);
                        //comboBox4.Clear();
                        ProfitBox.Clear();
                        LastBetBox.Clear();
                        SqlDataReader read27 = command2.ExecuteReader();

                        while (read27.Read())
                        {
                            LastBetBox.Text = (read27["IznosOklade"].ToString());
                            tb3 = (read27["Profit"].ToString());
                            OddsBox.Text = (read27["Odds"].ToString());
                            status = (read27["Status"].ToString());
                            vrijednost = (read27["IDVrijednost"].ToString());
                        }
                        read27.Close();
                        ProfitBox.Text = String.Format("${0:n}", tb3);
                    }

                    // POPUNJAVANJE DATAGRIDVIEWA//

                    _sql3 = "select ID,IDKluba, Date, IznosOklade, Odds, Game, Offer, StatusWL, Profit, Site,Total,IsOver,IsUnder,HendiWin as 'Home ATS',HendiLoss as 'Away ATS',ParkFactor as 'Park Factor' from Bets where IDKluba = '" + ID + "' order by ID desc";

                    _adapter = new SqlDataAdapter(_sql3, connection);
                    _ds.Reset();
                    _adapter.Fill(_ds);
                    AllBetsDGV.DataSource = _ds.Tables[0];

                    System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                    boldStyle.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);


                    AllBetsDGV.Columns["ID"].DefaultCellStyle = boldStyle;
                    AllBetsDGV.Columns[3].DefaultCellStyle = boldStyle;
                    AllBetsDGV.Columns[5].DefaultCellStyle = boldStyle;
                    AllBetsDGV.Columns["ID"].Width = 33;
                    AllBetsDGV.Columns["Date"].Width = 80;
                    AllBetsDGV.Columns.Remove("IDKluba");
                    AllBetsDGV.CurrentCell.Selected = false;
                    //AllBetsDGV.Columns[1].HeaderText = "Date";
                    AllBetsDGV.Columns["IznosOklade"].HeaderText = "Bet Amount";
                    AllBetsDGV.Columns["Offer"].HeaderText = "Bet Value";
                    AllBetsDGV.Columns["Game"].Width = 110;
                    AllBetsDGV.Columns["Offer"].Width = 70;
                    AllBetsDGV.Columns["StatusWL"].HeaderText = "Bet Status";
                    AllBetsDGV.Columns["Profit"].HeaderText = "Profit / Loss";
                    AllBetsDGV.Columns["Profit"].Width = 72;
                    AllBetsDGV.Columns["IsOver"].HeaderText = "Is Over";
                    AllBetsDGV.Columns["IsUnder"].HeaderText = "Is Under";
                    AllBetsDGV.Columns[9].DefaultCellStyle = boldStyle;
                    AllBetsDGV.Columns[12].DefaultCellStyle = boldStyle;
                    AllBetsDGV.Columns[13].DefaultCellStyle = boldStyle;

                    SqlCommand commandv = new SqlCommand("select Value from Vrijednosti where NO = '" + vrijednost + "'", connection);
                    SqlDataReader readv = commandv.ExecuteReader();
                    while (readv.Read())
                    {
                        vrijednost2 = (readv["Value"].ToString());
                    }
                    readv.Close();
                    CalcNextBetTB.Text = vrijednost;

                    //Profit color
                    var prft = Convert.ToDouble(tb3);
                    ProfitBox.ForeColor = prft > 0 ? Color.LawnGreen : Color.Crimson;

                    /// COLOR BUTTONS DEPENDING OD BET STATUS //

                    if (status == "")
                    {
                        //  updatebtn.Enabled = false;
                    }

                    switch (status)
                    {
                        case "0":
                            {
                                int x;
                                Int32.TryParse(vrijednost, out x);
                                x2 = x + 1;
                                winBtn.Enabled = false;
                                failBtn.Enabled = false;
                                pushBtn.Enabled = false;
                                SqlCommand commandv2 = new SqlCommand("select Value from Vrijednosti where NO = " + x2 + "", connection);
                                SqlDataReader readv2 = commandv2.ExecuteReader();
                                while (readv2.Read())
                                {
                                    vrijednost3 = (readv2["Value"].ToString());
                                }
                                readv2.Close();

                                NextBetBox.Text = vrijednost3;
                            }
                            break;
                        case "1":
                            {
                                int x;
                                Int32.TryParse(vrijednost, out x);
                                x2 = x - 1;
                                if (x2 < 1)
                                {
                                    x2 = 1;
                                }

                                winBtn.Enabled = false;
                                failBtn.Enabled = false;
                                pushBtn.Enabled = false;
                                SqlCommand commandv2 = new SqlCommand("select Value from Vrijednosti where NO = " + x2 + "", connection);
                                SqlDataReader readv2 = commandv2.ExecuteReader();
                                while (readv2.Read())
                                {
                                    vrijednost3 = (readv2["Value"].ToString());
                                }
                                readv2.Close();

                                NextBetBox.Text = vrijednost3.ToString();
                            }
                            break;
                        case "2":
                            winBtn.Enabled = false;
                            failBtn.Enabled = false;
                            pushBtn.Enabled = false;
                            NextBetBox.Text = LastBetBox.Text;
                            break;
                    }
                    CalcNextBetTB.Text = NextBetBox.Text.Length > 1 ? NextBetBox.Text : "0";
                    textBox8.Text = "0";
                    textBox7.Text = "0";
                    tot = Convert.ToDouble(CalcNextBetTB.Text);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                string queryy = "Select TOP 1 ID from Bets where IDKluba = '" + ID + "' and Status IS NULL order by ID";
                SqlCommand cmdd = new SqlCommand(queryy, connection);
                SqlDataReader read22 = cmdd.ExecuteReader();

                while (read22.Read())
                {
                    ID2 = (read22["ID"].ToString());
                }
                read22.Close();

                string queryy4 = "Select SUM(PlusMinus) from Bets where IDKluba = '" + ID + "'";
                SqlCommand cmdd4 = new SqlCommand(queryy4, connection);
                SqlDataReader read224 = cmdd4.ExecuteReader();
                while (read224.Read())
                {
                    profit1 = (read224[0].ToString());
                }
                read224.Close();

                Colorrows(sender, e);

                _gameslist.Clear();

                _gameslist.Add(" ");

                if(matchupsAndScoresDGV.Columns.Count > 1) { 
                 for (int i = 0; i <= matchupsAndScoresDGV.RowCount - 1; i++)
                 {
                    string team = matchupsAndScoresDGV.Rows[i].Cells[0].Value.ToString();
                    string team2 = matchupsAndScoresDGV.Rows[i].Cells[1].Value.ToString();

                    _gameslist.Add("" + team + " @ " + team2 + "");
                    _gameslist.Add("" + team2 + " vs. " + team + "");

                    string queryy33 = "Select ImeKluba from Klubovi where ShortForCovers = '" + team + "' and League = '" + _leaguestring + "'";
                    SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                    SqlDataReader readd33 = cmdd33.ExecuteReader();
                    while (readd33.Read())
                    {
                        team = (readd33["ImeKluba"].ToString());
                    }
                    readd33.Close();

                    string queryy34 = "Select ImeKluba from Klubovi where ShortForCovers = '" + team2 + "' and League = '" + _leaguestring + "'";
                    SqlCommand cmdd34 = new SqlCommand(queryy34, connection);
                    SqlDataReader readd34 = cmdd34.ExecuteReader();
                    while (readd34.Read())
                    {
                        team2 = (readd34["ImeKluba"].ToString());
                    }
                    readd34.Close();

                    switch (team)
                    {
                        case "Philadelphia 76ers":
                            {
                                team = "Phi Seventysixers";
                                break;
                            }
                        case "Los Angeles Clippers":
                            {
                                team = "LA Clippers";
                                break;
                            }
                        case "San Antonio Spurs":
                            {
                                team = "SanAntonio Spurs";
                                break;
                            }
                        case "Golden State Warriors":
                            {
                                team = "GS Warriors";
                                break;
                            }
                        case "New Orleans Pelicans":
                            {
                                team = "NewOrleans Pelicans";
                                break;
                            }
                        case "Portland Trail Blazers":
                            {
                                team = "Por Trailblazers";
                                break;
                            }
                    }

                    if (team.Contains(value.Split(' ')[1]) || (team2.Contains(value.Split(' ')[1])))
                    {
                        if (matchupsAndScoresDGV.Rows[i].Cells["Total"].Value.ToString() != "-" && matchupsAndScoresDGV.Rows[i].Cells["Total"].Value.ToString() != "Off" && matchupsAndScoresDGV.Rows[i].Cells["Total"].Value.ToString() != "")
                        {
                            double total = Convert.ToDouble(matchupsAndScoresDGV.Rows[i].Cells["Total"].Value.ToString().Replace('.', ','));
                            // total = total / 10;
                            double total2 = total - 0.5;
                            double total3 = total - 1;
                            double total4 = total + 0.5;
                            double total5 = total + 1;

                            if (NBAradioButton.Checked == true)
                            {
                                _handicapDoubles[0] = Convert.ToDouble(matchupsAndScoresDGV.Rows[i].Cells["Line"].Value.ToString().Replace('.', ','));
                                _handicapDoubles[1] = _handicapDoubles[0] - 0.5;
                                _handicapDoubles[2] = _handicapDoubles[0] - 1;
                                _handicapDoubles[3] = _handicapDoubles[0] + 0.5;
                                _handicapDoubles[4] = _handicapDoubles[0] + 1;
                                if (_handicapDoubles[0] > 0)
                                {
                                    _handicapStrings[0] = "-" + _handicapDoubles[0];
                                }
                                else
                                {
                                    _handicapStrings[0] = _handicapDoubles[0].ToString().Replace('-', ' ');
                                }
                                if (_handicapDoubles[1] > 0)
                                {
                                    _handicapStrings[1] = "-" + _handicapDoubles[1];
                                }
                                else
                                {
                                    _handicapStrings[1] = _handicapDoubles[1].ToString().Replace('-', ' ');
                                }
                                if (_handicapDoubles[2] > 0)
                                {
                                    _handicapStrings[2] = "-" + _handicapDoubles[2];
                                }
                                else
                                {
                                    _handicapStrings[2] = _handicapDoubles[2].ToString().Replace('-', ' ');
                                }
                                if (_handicapDoubles[3] > 0)
                                {
                                    _handicapStrings[3] = "-" + _handicapDoubles[3];
                                }
                                else
                                {
                                    _handicapStrings[3] = _handicapDoubles[3].ToString().Replace('-', ' ');
                                }
                                if (_handicapDoubles[4] > 0)
                                {
                                    _handicapStrings[4] = "-" + _handicapDoubles[4];
                                }
                                else
                                {
                                    _handicapStrings[4] = _handicapDoubles[4].ToString().Replace('-', ' ');
                                }
                            }

                            if (MLBradioButton.Checked == true)
                            {
                                string[] ponuda = { "O/U", "ML","U"+total3.ToString().Replace('.',',')+"","U"+total2.ToString().Replace('.',',')+"","U"+total.ToString().Replace('.',',')+
                                                 "","U"+total4.ToString().Replace('.',',')+"","U"+total5.ToString().Replace('.',',')+"","O"+total3.ToString().Replace('.',',')+
                                                 "","O"+total2.ToString().Replace('.',',')+"","O"+total.ToString().Replace('.',',')+"","O"+total4.ToString().Replace('.',',')+
                                                 "","O"+total5.ToString().Replace('.',',')+"","-1","+1","-1,5","+1,5"};

                                OfferBox.DataSource = ponuda;
                            }
                            else if (NBAradioButton.Checked == true)
                            {
                                string[] ponuda = { "", "U"+total3.ToString().Replace('.',',')+"","U"+total2.ToString().Replace('.',',')+"","U"+total.ToString().Replace('.',',')+
                                                 "","U"+total4.ToString().Replace('.',',')+"","U"+total5.ToString().Replace('.',',')+"","O"+total3.ToString().Replace('.',',')+
                                                 "","O"+total2.ToString().Replace('.',',')+"","O"+total.ToString().Replace('.',',')+"","O"+total4.ToString().Replace('.',',')+
                                                 "","O"+total5.ToString().Replace('.',',')+"", ""+_handicapDoubles[2].ToString().Replace('.',',')+"",""+_handicapDoubles[1].ToString().Replace('.',',')+
                                                 "",""+_handicapDoubles[0].ToString().Replace('.',',')+
                                                 "",""+_handicapDoubles[3].ToString().Replace('.',',')+"",""+_handicapDoubles[4].ToString().Replace('.',',')+"",""+_handicapStrings[2].ToString().Replace('.',',')+
                                                 "",""+_handicapStrings[1].ToString().Replace('.',',')+"",""+_handicapStrings[0].ToString().Replace('.',',')+"",""+_handicapStrings[3].ToString().Replace('.',',')+
                                                 "",""+_handicapStrings[4].ToString().Replace('.',',')+"" };
                                OfferBox.DataSource = ponuda;
                            }
                        }
                    }
                 }
               }

                GameBox.DataSource = _gameslist;
            }
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                // UNDO LAST //
                Sum(sender, e);

                string zadnjiProfit = AllBetsDGV.Rows[1].Cells["Profit"].Value.ToString();

                string queryy = "Select TOP 1 ID from Bets where IDKluba = '" + ID + "' and Status IN (0,1,2) order by ID desc";
                SqlCommand cmdd = new SqlCommand(queryy, connection);
                SqlDataReader read22 = cmdd.ExecuteReader();
                try
                {
                    while (read22.Read())
                    {
                        ID22 = (read22["ID"].ToString());
                    }
                    read22.Close();

                    string str1 = "UPDATE Bets SET Status = NULL, StatusWL = NULL, Profit = " + zadnjiProfit.ToString().Replace(",", ".") + ",PlusMinus = '0' where ID=" + ID22 + "";
                    SqlCommand cmd = new SqlCommand(str1, connection);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                listBox1_SelectedValueChanged(sender, e);
            }
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                _sql3 = "select ID, IDKluba, Date, IznosOklade, Odds, Game, Offer, StatusWL, Profit from Bets where IDKluba = '" + ID + "' order by ID desc";

                _adapter = new SqlDataAdapter(_sql3, connection);

                SqlCommandBuilder cmd224 = new SqlCommandBuilder(_adapter);

                _adapter.Update(_ds);

                MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                BetsTracker_Load(sender, e);
            }
        }

        private void additionaldata()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string additionalData = @"INSERT INTO Bets (Total,Handicap,IsOver,IsUnder,HendiWin,HendiLoss,ParkFactor)
              (SELECT TOP 1
             [MLBBETS].[Total]
            ,[MLBBETS].[Handicap]
            ,[MLBBETS].[IsOver]
            ,[MLBBETS].[IsUnder]
            ,[MLBBETS].[HomeHendi]
            ,[MLBBETS].[AwayHendi]
            ,[MLBBETS].[ParkFactor]
        FROM [MLBBETS],Bets where MLBBETS.Datum='" + timenoww + "' and [MLBBETS].IDHKluba = " + ID7 + " or MLBBETS.Datum='" + timenoww + "' and [MLBBETS].IDAKluba = " + ID7 + ") where Bets.Datum = '" + timenoww + "'";
                SqlCommand cmdAD = new SqlCommand(additionalData, connection);

                cmdAD.ExecuteNonQuery();
            }
        }

        private void updatebtn_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                using (OkladesEntities context = new OkladesEntities())
                {

                }
                // UPDATE NEW //
                connection.Open();
                Sum(sender, e);

                timenoww = DateTime.Now.ToString("dd.MM.yyyy.");
                string timenow = DateTime.Now.ToShortTimeString();
                string timenow2 = Regex.Replace(timenow, ":", "").Trim();
                int timenow3 = Convert.ToInt16(timenow2);

                if (timenow3 < 300)
                {
                    timenoww = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy.");
                }

                if (OfferBox.Text == "")
                {
                    MessageBox.Show(@"Put in an offer!");
                }
                else
                {
                    if (checkBox1.Checked == false)
                    {
                        string queryyy = "Select NO from Vrijednosti where Value = '" + NextBetBox.Text + "'";

                        try
                        {
                            if (status != null)
                            {
                                SqlCommand cmddd = new SqlCommand(queryyy, connection);
                                SqlDataReader read222 = cmddd.ExecuteReader();
                                while (read222.Read())
                                {
                                    vrj = (read222["NO"].ToString());
                                }
                                read222.Close();

                                if (vrj == null)
                                {
                                    vrj = "1";
                                }
                                    ID7 = Convert.ToInt32(ID);
                                    profit7 = Convert.ToDouble(tb3);
                                    IO7 = Convert.ToDouble(NextBetBox.Text);
                                    odds7 = Convert.ToDouble(OddsBox.Text);
                                    profit7 = Math.Round(profit7, 2);
                                    IO7 = Math.Round(IO7, 1);
                                    odds7 = Math.Round(odds7, 3);

                                    _homeaway1 = null;
                                    _homeaway2 = null;

                                    string ishome = "Select ID from MLBBETS where IDHKluba = " + ID7 + " and MLBBETS.Datum='" + timenoww + "'";
                                    string isaway = "Select ID from MLBBETS where IDAKluba = " + ID7 + " and MLBBETS.Datum='" + timenoww + "'";

                                    SqlCommand cmdhome = new SqlCommand(ishome, connection);
                                    SqlDataReader readhome = cmdhome.ExecuteReader();
                                    while (readhome.Read())
                                    {
                                        _homeaway1 = (readhome["ID"].ToString());
                                    }
                                    readhome.Close();

                                    SqlCommand cmdaway = new SqlCommand(isaway, connection);
                                    SqlDataReader readaway = cmdaway.ExecuteReader();
                                    while (readaway.Read())
                                    {
                                        _homeaway2 = (readaway["ID"].ToString());
                                    }
                                    readaway.Close();

                                    if (_homeaway1 != null)
                                    {
                                        _homeaway = "Home";
                                    }
                                    else if (_homeaway2 != null)
                                    {
                                        _homeaway = "Away";
                                    }

                                    if (MLBradioButton.Checked == true)
                                    {
                                        string query99 = @"INSERT INTO Bets (Total,Handicap,IsOver,IsUnder,HendiWin,HendiLoss,ParkFactor,IDKluba, profit, iznosoklade, odds, date, IDVrijednost, Offer, Sport, Site, Game)
                                        SELECT TOP 1 [MLBBETS].[Total]
                                    ,[MLBBETS].[Handicap]
                                    ,[MLBBETS].[IsOver]
                                    ,[MLBBETS].[IsUnder]
                                    ,[MLBBETS].[HomeHendi]
                                    ,[MLBBETS].[AwayHendi]
                                    ,[MLBBETS].[ParkFactor]," + ID7 + "," + profit7.ToString().Replace(",", ".") + "," + IO7.ToString().Replace(",", ".") + ", " + odds7.ToString().Replace(",", ".") +
                                   ", '" + timenoww + "', '" + vrj + "','" + OfferBox.Text + "', '" + _leaguestring + "', '" + _homeaway + "','" + GameBox.Text + "' FROM [MLBBETS],Bets where MLBBETS.Datum='" + timenoww +
                                   "' and [MLBBETS].IDHKluba = " + ID7 + " or MLBBETS.Datum='" + timenoww + "' and [MLBBETS].IDAKluba = " + ID7 + "";

                                        SqlCommand cmd99 = new SqlCommand(query99, connection);

                                        cmd99.ExecuteNonQuery();
                                    }
                                    else if (NBAradioButton.Checked == true || NHLradioButton.Checked == true || BestBetsRB.Checked == true || CasinoRB.Checked == true)
                                    {
                                        string query99 = @"INSERT INTO Bets (IDKluba, profit, iznosoklade, odds, date, IDVrijednost, Offer, Game)
                                       values(" + ID7 + "," + profit7.ToString().Replace(",", ".") + "," + IO7.ToString().Replace(",", ".") + ", " + odds7.ToString().Replace(",", ".") +
                                      ", '" + timenoww + "', '" + vrj + "','" + OfferBox.Text + "','" + GameBox.Text + "')";

                                        SqlCommand cmd99 = new SqlCommand(query99, connection);

                                        cmd99.ExecuteNonQuery();
                                    }
                                     else
                                    {
                                    return;
                                    }
                            }
                        }
                        catch
                        {
                            MessageBox.Show(@"Unjeli ste neispravan unos! Unesite ispravan broj!");
                        }
                    } 
                }
                System.Windows.Forms.Clipboard.SetText(NextBetBox.Text.Replace(",", "."));
                OddsBox.Text = "";
                listBox1_SelectedValueChanged(sender, e);
            }
        }

        private void Colorrows(object sender, EventArgs e)
        {
            // COLOR ROWS
            try
            {
                foreach (DataGridViewRow row in AllBetsDGV.Rows)
                {
                    switch (row.Cells["StatusWL"].Value.ToString())
                    {
                        case "WIN":
                            {
                                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                                boldStyle.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
                                row.Cells["StatusWL"].Style = boldStyle;
                                //   row.Cells["StatusWL"].Style.BackColor = Color.Green;
                                row.Cells["StatusWL"].Style.ForeColor = Color.Green;

                                //then change row color to green
                            }
                            break;
                        case "LOSS":
                            {
                                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                                boldStyle.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
                                row.Cells["StatusWL"].Style = boldStyle;
                                row.Cells["StatusWL"].Style.ForeColor = Color.Crimson;
                            }
                            break;
                        case "PUSH":
                            {
                                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                                boldStyle.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
                                row.Cells["StatusWL"].Style = boldStyle;
                                row.Cells["StatusWL"].Style.ForeColor = Color.Orange;
                            }
                            break;
                        case "TRANSFER":
                            {
                                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                                boldStyle.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
                                row.Cells["StatusWL"].Style = boldStyle;
                                row.Cells["StatusWL"].Style.ForeColor = Color.DarkSlateBlue;
                                //  row.Cells["StatusWL"].Style.ForeColor = Color.AntiqueWhite;
                            }
                            break;
                    }

                    string rv = row.Cells["Profit"].Value.ToString();
                    double valuee = Convert.ToDouble(rv);
                    if (valuee > 0) //if check ==0
                    {
                        System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                        boldStyle.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
                        row.Cells["Profit"].Style = boldStyle;
                        row.Cells["Profit"].Style.ForeColor = Color.Green;
                        //    row.Cells["Profit"].Style.ForeColor = Color.AntiqueWhite;
                        //then change row color to green
                    }
                    else
                    {
                        System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                        boldStyle.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
                        row.Cells["Profit"].Style = boldStyle;
                        row.Cells["Profit"].Style.ForeColor = Color.Crimson;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void winbtn_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // WIN BUTTON //
                connection.Open();
                Sum(sender, e);
                //
                    try
                    {
                        if (status == "")
                        {
                                    string queryyy = "Select NO from Vrijednosti where Value = '" + LastBetBox.Text + "'";
                                    SqlCommand cmddd = new SqlCommand(queryyy, connection);
                                    SqlDataReader read222 = cmddd.ExecuteReader();
                                    while (read222.Read())
                                    {
                                        vrj = (read222["NO"].ToString());
                                    }
                                    read222.Close();

                                    if (vrj == null)
                                    {
                                        vrj = "1";
                                      }

                                    string sqlProfitCalc = "SELECT SUM(PlusMinus) FROM Bets where IDKluba = "+ ID +"";
                                        SqlCommand sqlcomProfitCalc = new SqlCommand(sqlProfitCalc, connection);
                                        SqlDataReader readerProfitCalc = sqlcomProfitCalc.ExecuteReader();
                                        while (readerProfitCalc.Read())
                                        {
                                                profit = Convert.ToDouble(readerProfitCalc[0]);
                                        }
                                         readerProfitCalc.Close();

                                        // profit = Convert.ToDouble(tb3);
                                        odds = Convert.ToDouble(OddsBox.Text);
                                        IznosOklade = Convert.ToDouble(LastBetBox.Text);

                                        profit2 = IznosOklade * (odds - 1);
                                        profit3 = profit + profit2;
                                   
                                        profit3 = Math.Round(profit3, 2);
                                        profit4 = profit3.ToString();
                                        query = @"UPDATE Bets SET profit = " + profit3.ToString().Replace(",", ".") + ", IznosOklade = " +
                            IznosOklade.ToString().Replace(",", ".") + ", odds = " + odds.ToString().Replace(",", ".") +
                            ", IDVrijednost = '1', status = 1, statusWL = 'WIN', PlusMinus = " + profit2.ToString().Replace(",", ".") +
                            " where IDKluba = '" + ID + "' and ID = '" + ID2 + "';";

                                        SqlCommand cmd = new SqlCommand(query, connection);

                                        cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    listBox1_SelectedValueChanged(sender, e);
            }
        }

        private void WINbutton1_Click(object sender, EventArgs e)
        {
        }

        private void UPDATEbutton3_Click(object sender, EventArgs e)
        {
        }

        private void failBtn_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                // FAIL BUTTON//
                Sum(sender, e);
                    try
                    {
                        if (status == "")
                        {
                            profit = Convert.ToDouble(profit1);
                            odds = Convert.ToDouble(OddsBox.Text);
                            IznosOklade = Convert.ToDouble(LastBetBox.Text);

                            profit2 = IznosOklade;
                            profit3 = profit - profit2;
                            double pr = 0 - IznosOklade;
                            profit3 = Math.Round(profit3, 2);
                            profit4 = profit3.ToString();
                            query = @"UPDATE Bets SET profit = " + profit3.ToString().Replace(",", ".") + ", IznosOklade = " +
                                IznosOklade.ToString().Replace(",", ".") + ", odds = " + odds.ToString().Replace(",", ".") +
                                ", status = 0, statusWL = 'LOSS', PlusMinus = " +
                                pr.ToString().Replace(",", ".") + " where IDKluba = '" + ID + "' and ID = '" + ID2 + "';";
                            SqlCommand cmd = new SqlCommand(query, connection);

                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    listBox1_SelectedValueChanged(sender, e);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void button4_Click(object sender, EventArgs e)
        {
        }

        private void NextBetBox_SelectedValueChanged(object sender, EventArgs e)
        {
            CalcNextBetTB.Text = NextBetBox.Text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
        }

        private void button5_Click(object sender, EventArgs e)
        {
        }

        private void button7_Click(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                suma = 0;
                _sql4 = "select t2.ID,t1.ImeKluba,t2.Game, t2.Offer, t2.IznosOklade, t2.Odds, t2.StatusWL, t2.PlusMinus,t2.Profit from Klubovi as t1,Bets as t2 where t2.Date = '" + dateTimePicker1.Text + "' and t1.ID=t2.IDKluba and t1.League != 'Casino' order by t2.IdKluba";
                //pictureBox1.Image = Oklade.Properties.Resources.MLB3;
                try
                {
                    dailybetstotal = 0;
                    label8.Text = "";

                    _adapter = new SqlDataAdapter(_sql4, connection);
                    _ds.Reset();
                    _adapter.Fill(_ds);
                    AllBetsDGV.DataSource = _ds.Tables[0];

                    TeamLogosPicBox.Image = null;

                    System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                    boldStyle.Font = new System.Drawing.Font("Arial Narrow", 13F, System.Drawing.FontStyle.Bold);

                    System.Windows.Forms.DataGridViewCellStyle regStyle = new System.Windows.Forms.DataGridViewCellStyle();
                    regStyle.Font = new System.Drawing.Font("Arial Narrow", 11.5F);

                    AllBetsDGV.Columns[0].Width = 45;
                    AllBetsDGV.Columns[1].DefaultCellStyle = boldStyle;
                    AllBetsDGV.Columns[2].DefaultCellStyle = regStyle;
                    AllBetsDGV.Columns[3].DefaultCellStyle = regStyle;
                    AllBetsDGV.Columns[6].DefaultCellStyle = boldStyle;
                    AllBetsDGV.Columns[7].DefaultCellStyle = boldStyle;
                    AllBetsDGV.Columns[8].DefaultCellStyle = boldStyle;
                    AllBetsDGV.Columns[1].Width = 145;
                    AllBetsDGV.Columns[2].Width = 105;
                    AllBetsDGV.Columns[3].Width = 105;
                    AllBetsDGV.Columns[4].Width = 80;
                    AllBetsDGV.Columns[5].Width = 80;
                    AllBetsDGV.Columns[6].Width = 80;
                    AllBetsDGV.Columns[7].Width = 80;
                    AllBetsDGV.Columns[1].HeaderText = "Team Name";
                    AllBetsDGV.Columns[3].HeaderText = "Bet Value";
                    AllBetsDGV.Columns[4].HeaderText = "Bet Amount";
                    AllBetsDGV.Columns[5].HeaderText = "Odds";
                    AllBetsDGV.Columns[6].HeaderText = "Bet Status";
                    AllBetsDGV.Columns[7].HeaderText = "Bet +/-";
                    AllBetsDGV.Columns[8].HeaderText = "Total +/-";

                    string query23 = "Select COUNT(*) from Bets where Date = '" + dateTimePicker1.Text + "' and statusWL ='WIN' and IDKluba NOT IN (114,115)";
                    SqlCommand cmd23 = new SqlCommand(query23, connection);
                    SqlDataReader read23 = cmd23.ExecuteReader();

                    while (read23.Read())
                    {
                        winlable = (read23[0].ToString());
                    }
                    read23.Close();

                    string query24 = "Select COUNT(*) from Bets where Date = '" + dateTimePicker1.Text + "' and statusWL ='LOSS' and IDKluba NOT IN (114,115)";
                    SqlCommand cmd24 = new SqlCommand(query24, connection);
                    SqlDataReader read24 = cmd24.ExecuteReader();

                    while (read24.Read())
                    {
                        losslable = (read24[0].ToString());
                    }
                    read24.Close();

                    // ------------------------------------------------------------ //
                    if (losslable != "")
                    {
                        label9.Show();
                        label16.Show();
                    }
                    foreach (DataGridViewRow row in AllBetsDGV.Rows)
                    {
                        if (row.Cells[7].Value.ToString() != "")
                        {
                            double vrjcelije = 0;
                            vrjcelije = Convert.ToDouble(row.Cells[7].Value);
                            suma = suma + vrjcelije;
                        }

                        dailybetstotal = dailybetstotal + Convert.ToDouble(row.Cells[4].Value);
                    }
                    dailybetstotal = Math.Round(dailybetstotal, 2);
                    suma = Math.Round(suma, 2);

                    label33.Text = dailybetstotal.ToString();

                    label17.Text = winlable;
                    label18.Text = losslable;
                    label17.ForeColor = Color.Green;
                    label18.ForeColor = Color.Black;
                    label20.Show();
                    label19.Text = suma.ToString();
                    label19.ForeColor = suma > 0 ? Color.Green : Color.Black;
                    if (suma != 0)
                    {
                        AllBetsDGV.CurrentCell.Selected = false;
                    }

                    DateTime dt = this.dateTimePicker1.Value.Date;

                    string day = dt.Day.ToString();
                    string month = dt.Month.ToString();
                    day = day.Count() == 1 ? "0" : "";
                    month = month.Count() == 1 ? "0" : "";

                    dailystatscheck = null;
                    string querydate = "Select Wins from DailyStats where Date = '" + dt.Year + "" + month + "" + dt.Month + "" + day + "" + dt.Day + "'";
                    SqlCommand cmd333 = new SqlCommand(querydate, connection);
                    SqlDataReader read333 = cmd333.ExecuteReader();

                    while (read333.Read())
                    {
                        dailystatscheck = read333["Wins"].ToString();
                    }
                    read333.Close();

                    if (dailystatscheck == null && winlable != "0" && losslable != "0")
                    {
                        string query1112 = @"INSERT INTO DailyStats (Date,Wins,Losses,Invested,Profit)
                                             VALUES ('" + dt.Year + "" + month + "" + dt.Month + "" + day + "" + dt.Day + "'," + winlable + "," + losslable + "," + dailybetstotal.ToString().Replace(",", ".") + "," + suma.ToString().Replace(",", ".") + ");";
                        SqlCommand cmd1112 = new SqlCommand(query1112, connection);

                        cmd1112.ExecuteNonQuery();
                    }
                    else if(dailystatscheck != null && winlable != "0" && losslable != "0")
                    {
                        string queryupdate = @"UPDATE [dbo].[DailyStats]
        SET [Wins] = " + winlable + ",[Losses] = " + losslable + " ,[Invested] = " + dailybetstotal.ToString().Replace(",", ".") + ",[Profit] = " + suma.ToString().Replace(",", ".") + " WHERE Date = '" + dt.Year + "" + month + "" + dt.Month + "" + day + "" + dt.Day + "'";
                        SqlCommand cmdupdate = new SqlCommand(queryupdate, connection);

                        cmdupdate.ExecuteNonQuery();
                    }
                    else if(dailystatscheck != null && winlable == "0" && losslable == "0")
                    {
                        string queryRemove = @"DELETE FROM [dbo].[DailyStats] WHERE Date = '" + dt.Year + "" + month + "" + dt.Month + "" + day + "" + dt.Day + "'";
                        SqlCommand cmdRemove = new SqlCommand(queryRemove, connection);
                        cmdRemove.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                Colorrows(sender, e);

                if (saveRow != 0 && saveRow < AllBetsDGV.Rows.Count)
                    AllBetsDGV.FirstDisplayedScrollingRowIndex = saveRow;
            }
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            textBox7.Text = comboBox2.Text;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            connectionString = ConfigurationManager.ConnectionStrings["MyDatabase"].ToString();
            BetsTracker_Load(sender, e);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            connectionString = ConfigurationManager.ConnectionStrings["MyDatabase2"].ToString();
            BetsTracker_Load(sender, e);
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, KeyEventArgs e)
        {
        }

        private void button8_Click(object sender, EventArgs e)
        {
        }

        private void label4_Click(object sender, EventArgs e)
        {
        }

        private void label5_Click(object sender, EventArgs e)
        {
        }

        private void label7_Click(object sender, EventArgs e)
        {
        }

        private void WINbutton1_MouseMove(object sender, MouseEventArgs e)
        {
        }

        private void button2_MouseHover(object sender, EventArgs e)
        {
        }

        private void button4_MouseHover(object sender, EventArgs e)
        {
        }

        private void button3_MouseHover(object sender, EventArgs e)
        {
        }

        private void button7_MouseHover(object sender, EventArgs e)
        {
        }

        private void radioButton1_MouseHover(object sender, EventArgs e)
        {
            radioButton1.Cursor = Cursors.Hand;
        }

        private void radioButton2_MouseHover(object sender, EventArgs e)
        {
            radioButton2.Cursor = Cursors.Hand;
        }

        private void button8_MouseHover(object sender, EventArgs e)
        {
        }

        private void button5_MouseHover(object sender, EventArgs e)
        {
        }

        private void button6_MouseHover(object sender, EventArgs e)
        {
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
        }

        private void listBox1_MouseHover(object sender, EventArgs e)
        {
            TeamslistBox1.Cursor = Cursors.Hand;
        }

        public void exitBtn_Click(object sender, EventArgs e)
        {
            //EXIT BUTTON//
            DialogResult d = MessageBox.Show(@"Are you sure you want to exit, did you check if all bets are entered?", "Exit Program", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            try
            {
                switch (d)
                {
                    case DialogResult.Yes:
                        System.Windows.Forms.Application.Exit();
                        break;
                    case DialogResult.No:
                        return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            // BETS CALC//

            switch (checkBox2.Checked)
            {
                case false:
                    am1 = Convert.ToDouble(textBox8.Text);
                    am2 = Convert.ToDouble(textBox7.Text);
                    tot = Convert.ToDouble(CalcNextBetTB.Text);
                    double odd1 = Convert.ToDouble(textBox10.Text);
                    double odd2 = Convert.ToDouble(textBox11.Text);
                    double am3 = am1 * (odd1 - 1);
                    double am4 = am2 * (odd2 - 1);
                    avg = (am3 + am4) / tot;
                    avg = Math.Round(avg, 3);
                    textBox9.Text = avg.ToString();
                    break;
                case true:
                    am1 = Convert.ToDouble(textBox8.Text);
                    am2 = Convert.ToDouble(textBox7.Text);
                    tot = Convert.ToDouble(CalcNextBetTB.Text);
                    avg = tot * am2;
                    avg = Math.Round(avg, 1);
                    textBox9.Text = avg.ToString();
                    break;
            }
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // DELETE LAST //
                connection.Open();
                Sum(sender, e);
                DialogResult d = MessageBox.Show("Are you sure you want to delete last record?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                try
                {
                    switch (d)
                    {
                        case DialogResult.Yes:
                            string queryy = "Select TOP 1 ID from Bets where IDKluba = '" + ID + "' order by ID desc";
                            SqlCommand cmdd = new SqlCommand(queryy, connection);
                            SqlDataReader read22 = cmdd.ExecuteReader();

                            while (read22.Read())
                            {
                                ID2 = (read22["ID"].ToString());
                            }
                            read22.Close();

                            string str1 = "DELETE from Bets where ID=" + ID2 + "";
                            SqlCommand cmd = new SqlCommand(str1, connection);
                            cmd.ExecuteNonQuery();
                            listBox1_SelectedValueChanged(sender, e);
                            break;
                        case DialogResult.No:
                            return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void exitBtn_MouseHover(object sender, EventArgs e)
        {
            //   exitBtn.Cursor = Cursors.Hand;
            //  exitBtn.Image = Oklade.Properties.Resources.exit2;
            exitBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            // DeleteBtn.Cursor = Cursors.Hand;
            //  DeleteBtn.Image = Oklade.Properties.Resources.Delete2;
            DeleteBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            //pictureBox6.Cursor = Cursors.Hand;
            // pictureBox6.Image = Oklade.Properties.Resources.Undo2;
            UndoBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void SaveBtn_MouseHover(object sender, EventArgs e)
        {
            //SaveBtn.Cursor = Cursors.Hand;
            // SaveBtn.Image = Oklade.Properties.Resources.floppysave2;
            SaveBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            pictureBox8.Cursor = Cursors.Hand;
            pictureBox8.Image = Oklade.Properties.Resources.calculator5;
        }

        private void pictureBox9_MouseHover(object sender, EventArgs e)
        {
            //updatebtn.Cursor = Cursors.Hand;
            // updatebtn.Image = Oklade.Properties.Resources.marketing_automation_icon2;
            updatebtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox10_MouseHover(object sender, EventArgs e)
        {
            //pushBtn.Cursor = Cursors.Hand;
            // pushBtn.Image = Oklade.Properties.Resources.push2;
            pushBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox11_MouseHover(object sender, EventArgs e)
        {
            //winBtn.Cursor = Cursors.Hand;
            //  winBtn.Image = Oklade.Properties.Resources.successo2;
            winBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox12_MouseHover(object sender, EventArgs e)
        {
            // failBtn.Cursor = Cursors.Hand;
            // failBtn.Image = Oklade.Properties.Resources.error2;
            failBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox11_MouseLeave(object sender, EventArgs e)
        {
            //winBtn.Image = Oklade.Properties.Resources.successo1;
            winBtn.BorderStyle = BorderStyle.None;
        }

        private void exitBtn_MouseLeave(object sender, EventArgs e)
        {
            // exitBtn.Image = Oklade.Properties.Resources.exit;
            exitBtn.BorderStyle = BorderStyle.None;
        }

        private void failBtn_MouseLeave(object sender, EventArgs e)
        {
            //failBtn.Image = Oklade.Properties.Resources.error1;
            failBtn.BorderStyle = BorderStyle.None;
        }

        private void updatebtn_MouseLeave(object sender, EventArgs e)
        {
            // updatebtn.Image = Oklade.Properties.Resources.marketing_automation_icon1;
            updatebtn.BorderStyle = BorderStyle.None;
        }

        private void SaveBtn_MouseLeave(object sender, EventArgs e)
        {
            // SaveBtn.Image = Oklade.Properties.Resources.floppysave1;
            SaveBtn.BorderStyle = BorderStyle.None;
        }

        private void pictureBox6_MouseLeave(object sender, EventArgs e)
        {
            //  pictureBox6.Image = Oklade.Properties.Resources.undo1;
            UndoBtn.BorderStyle = BorderStyle.None;
        }

        private void pictureBox5_MouseLeave(object sender, EventArgs e)
        {
            // DeleteBtn.Image = Oklade.Properties.Resources.Delete;
            DeleteBtn.BorderStyle = BorderStyle.None;
        }

        private void Excel()
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            app.Visible = false;

            try
            {
                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets["Sheet1"];
                worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.ActiveSheet;
                worksheet.Name = "Bets";
                for (i = 1; i < AllBetsDGV.Columns.Count + 1; i++)
                {
                    worksheet.Cells[1, i] = AllBetsDGV.Columns[i - 1].HeaderText;
                }
                for (i = 0; i < AllBetsDGV.Rows.Count; i++)
                {
                    for (int j = 0; j < AllBetsDGV.Columns.Count; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = AllBetsDGV.Rows[i].Cells[j].Value.ToString();
                    }
                }

                string fileName = String.Empty;
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                saveFileDialog1.Filter = "Excel files |*.xls|All files (*.*)|*.*";
                saveFileDialog1.FileName = "" + value + ".xlsx";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    fileName = saveFileDialog1.FileName;
                    workbook.SaveAs(fileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                }
                else
                    return;
            }
            catch (System.Exception ex)
            {
            }
            finally
            {
                app.Quit();
                workbook = null;
                app = null;
            }
        }

        private void excelBtn_MouseClick(object sender, MouseEventArgs e)
        {
            Excel();
        }

        private void excelBtn_MouseHover(object sender, EventArgs e)
        {
            // excelBtn.Cursor = Cursors.Hand;
            excelBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void excelBtn_MouseLeave(object sender, EventArgs e)
        {
            excelBtn.BorderStyle = BorderStyle.None;
        }

        private void pictureBox10_MouseLeave(object sender, EventArgs e)
        {
            //pushBtn.Image = Oklade.Properties.Resources.push;
            pushBtn.BorderStyle = BorderStyle.None;
        }

        private void pictureBox8_MouseLeave(object sender, EventArgs e)
        {
            pictureBox8.Image = Oklade.Properties.Resources.calculator4;
        }

        private void dbtoolBtn_MouseClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Users\Ivica\Desktop\Backup Tool\Spica DB Tools.exe");
        }

        private void dbtoolBtn_MouseHover_1(object sender, EventArgs e)
        {
        }

        private void dbtoolBtn_MouseLeave(object sender, EventArgs e)
        {
        }

        private void pictureBox9_MouseHover_1(object sender, EventArgs e)
        {
            //     pictureBox9.Image = Oklade.Properties.Resources.audit2;
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                for (k = 1; k <= 14; k++)
                {
                    string query23 = "Select TOP 1 Status from Bets where IDKluba = '" + k + "' order by ID desc";
                    using (SqlCommand cmd23 = new SqlCommand(query23, connection))
                    {
                        using (SqlDataReader read23 = cmd23.ExecuteReader())
                        {
                            while (read23.Read())
                            {
                                strr[k] = (read23["Status"].ToString());
                            }
                        }
                    }

                    if (strr[k] != "")
                    {
                        check1 = 1;
                    }
                }
                if (check1 == 1)
                {
                    MessageBox.Show(@"You are not done! Check if you forgot some teams!", "Important Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (check1 == 0)
                {
                    MessageBox.Show(@"You are done for today! G O O D  L U C K !!");
                }
            }
        }

        private void pictureBox9_MouseLeave(object sender, EventArgs e)
        {
            //   pictureBox9.Image = Oklade.Properties.Resources.audit;
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            Active(sender, e);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Updateactive(sender, e);
        }

        private void pictureBox10_MouseHover_1(object sender, EventArgs e)
        {
            // PickTeamsBtn.Image = Oklade.Properties.Resources.Edit2;
            // PickTeamsBtn.Cursor = Cursors.Hand;
            PickTeamsBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox10_MouseLeave_1(object sender, EventArgs e)
        {
            //  PickTeamsBtn.Image = Oklade.Properties.Resources.Edit1;
            PickTeamsBtn.BorderStyle = BorderStyle.None;
        }

        private void button10_Click(object sender, EventArgs e)
        {
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            // AnalyzerBtn.Cursor = Cursors.Hand;
            //  AnalyzerBtn.Image = Oklade.Properties.Resources.analyze2;
            AnalyzerBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            //AnalyzerBtn.Image = Oklade.Properties.Resources.analyze1;
            AnalyzerBtn.BorderStyle = BorderStyle.None;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Analyzer f2 = new Analyzer();
            f2.Show();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (AllBetsDGV.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                if (AllBetsDGV.Rows[e.RowIndex].Cells[0].Selected)
                {
                    ID2 = AllBetsDGV.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                }
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // pictureBox1.Hide();

                connection.Open();
                Sum(sender, e);

                button9.Hide();
                // dataGridView2.Hide();

                value = (string)this.TeamslistBox1.SelectedItem;

                checkBox1.Checked = false;
                winBtn.Enabled = true;
                failBtn.Enabled = true;
                pushBtn.Enabled = true;
                updatebtn.Enabled = true;
                check1 = 0;

                //String query = "Select ID from Klubovi where ImeKluba = '" + value + "'";
                //SqlCommand cmd = new SqlCommand(query, connection);
                //SqlDataReader read2 = cmd.ExecuteReader();
                try
                {
                    //    while (read2.Read())
                    //    {
                    //        ID = (read2["ID"].ToString());
                    //    }
                    //    read2.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                // POPUNJAVANJE TEXTBOXA //

                //  SqlCommand command = new SqlCommand("select IznosOklade,Profit,Odds,Status,IDVrijednost from Bets where IDKluba = '" + ID + "' and ID = '" + ID2 + "'", connection);
                SqlCommand command = new SqlCommand("select IznosOklade,Profit,Odds,Status,IDVrijednost,IDKluba from Bets where ID = '" + ID2 + "'", connection);
                //  comboBox4.Clear();
                ProfitBox.Clear();
                LastBetBox.Clear();
                SqlDataReader read = command.ExecuteReader();
                try
                {
                    while (read.Read())
                    {
                        LastBetBox.Text = (read["IznosOklade"].ToString());
                        tb3 = (read["Profit"].ToString());
                        OddsBox.Text = (read["Odds"].ToString());
                        status = (read["Status"].ToString());
                        vrijednost = (read["IDVrijednost"].ToString());
                        ID = (read["IDKluba"].ToString());
                    }
                    read.Close();

                    string query333 = "Select COUNT(1) from Bets where IDKluba = '" + ID + "';";
                    SqlCommand cmd333 = new SqlCommand(query333, connection);
                    SqlDataReader read333 = cmd333.ExecuteReader();

                    while (read333.Read())
                    {
                        IDKluba1 = read333[0].ToString();
                    }
                    read333.Close();

                    teamLogos.LoadTeamsLogos(TeamLogosPicBox, value);

                    ProfitBox.Text = String.Format("${0:n}", tb3);

                    SqlCommand commandv = new SqlCommand("select Value from Vrijednosti where NO = '" + vrijednost + "'", connection);
                    SqlDataReader readv = commandv.ExecuteReader();
                    while (readv.Read())
                    {
                        vrijednost2 = (readv["Value"].ToString());
                    }
                    readv.Close();
                    CalcNextBetTB.Text = vrijednost;
                    // LOGO //
                    SqlCommand commandv7 = new SqlCommand("select League from Klubovi where ID = '" + ID + "'", connection);
                    SqlDataReader readv7 = commandv7.ExecuteReader();
                    while (readv7.Read())
                    {
                        logo = (readv7["League"].ToString());
                    }
                    readv7.Close();
                    switch (logo)
                    {
                        case "NBA":
                            leagueLogoPb.Show();
                            leagueLogoPb.Image = Oklade.Properties.Resources.nba_logo_png;
                            break;
                        case "MLB":
                            leagueLogoPb.Show();
                            leagueLogoPb.Image = Oklade.Properties.Resources.mlb2;
                            break;
                        case "NHL":
                            leagueLogoPb.Show();
                            leagueLogoPb.Image = Oklade.Properties.Resources.NHLLogo;
                            break;
                        default:
                            leagueLogoPb.Hide();
                            break;
                    }

                    //Profit color
                    double prft;
                    prft = Convert.ToDouble(tb3);
                    ProfitBox.ForeColor = prft > 0 ? Color.Green : Color.Crimson;

                    /// COLOR BUTTONS DEPENDING OD BET STATUS //

                    if (status == "")
                    {
                        //  updatebtn.Enabled = false;
                    }

                    switch (status)
                    {
                        case "0":
                            {
                                int x;
                                Int32.TryParse(vrijednost, out x);
                                x2 = x + 1;
                                winBtn.Enabled = false;
                                failBtn.Enabled = false;
                                pushBtn.Enabled = false;
                                SqlCommand commandv2 = new SqlCommand("select Value from Vrijednosti where NO = " + x2 + "", connection);
                                SqlDataReader readv2 = commandv2.ExecuteReader();
                                while (readv2.Read())
                                {
                                    vrijednost3 = (readv2["Value"].ToString());
                                }
                                readv2.Close();

                                NextBetBox.Text = vrijednost3;
                            }
                            break;
                        case "1":
                            {
                                int x;
                                Int32.TryParse(vrijednost, out x);
                                x2 = x - 1;
                                if (NBAradioButton.Checked == true && x2 < 1)
                                {
                                    x2 = 1;
                                }
                                else if (CasinoRB.Checked == true && x2 < 44)
                                {
                                    x2 = 44;
                                }

                                winBtn.Enabled = false;
                                failBtn.Enabled = false;
                                pushBtn.Enabled = false;
                                SqlCommand commandv2 = new SqlCommand("select Value from Vrijednosti where NO = " + x2 + "", connection);
                                SqlDataReader readv2 = commandv2.ExecuteReader();
                                while (readv2.Read())
                                {
                                    vrijednost3 = (readv2["Value"].ToString());
                                }
                                readv2.Close();

                                NextBetBox.Text = vrijednost3.ToString();
                            }
                            break;
                        case "2":
                            winBtn.Enabled = false;
                            failBtn.Enabled = false;
                            pushBtn.Enabled = false;
                            NextBetBox.Text = LastBetBox.Text;
                            break;
                    }
                    CalcNextBetTB.Text = NextBetBox.Text.Length > 1 ? NextBetBox.Text : "0";
                    textBox8.Text = "0";
                    textBox7.Text = "0";
                    tot = Convert.ToDouble(CalcNextBetTB.Text);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                string queryy4 = "Select SUM(PlusMinus) from Bets where IDKluba = '" + ID + "'";
                SqlCommand cmdd4 = new SqlCommand(queryy4, connection);
                SqlDataReader read224 = cmdd4.ExecuteReader();
                while (read224.Read())
                {
                    profit1 = (read224[0].ToString());
                }
                read224.Close();

                Colorrows(sender, e);
            }
        }

        private void radioButton3_CheckedChanged_1(object sender, EventArgs e)
        {
            connectionString = ConfigurationManager.ConnectionStrings["MyDatabase3"].ToString();
            BetsTracker_Load(sender, e);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Linkovi();
        }

        private void MLBRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            BetsTracker_Load(sender, e);
            Scores(_date);
        }

        private void NBAradioButton_CheckedChanged(object sender, EventArgs e)
        {
            BetsTracker_Load(sender, e);
            Scores(_date);
        }

        private void NHLradioButton_CheckedChanged(object sender, EventArgs e)
        {
            BetsTracker_Load(sender, e);
            Scores(_date);
        }

        private void comboBox3_SelectedValueChanged(object sender, EventArgs e)
        {
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            ResultUpdater frm3 = new ResultUpdater();
            frm3.Show();
        }

        private void pictureBox11_MouseHover_1(object sender, EventArgs e)
        {
            //   ResultsUpdBtn.Cursor = Cursors.Hand;
            // ResultsUpdBtn.Image = Oklade.Properties.Resources.scoreboard2;
            ResultsUpdBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox11_MouseLeave_1(object sender, EventArgs e)
        {
            // ResultsUpdBtn.Image = Oklade.Properties.Resources.scoreboard1;
            ResultsUpdBtn.BorderStyle = BorderStyle.None;
        }

        private void textBox5_MouseClick(object sender, MouseEventArgs e)
        {
            CalcNextBetTB.Text = "";
        }

        private void textBox8_MouseClick(object sender, MouseEventArgs e)
        {
            textBox8.Text = "";
        }

        private void textBox7_MouseClick(object sender, MouseEventArgs e)
        {
            textBox7.Text = "";
        }

        private void pictureBox13_MouseHover(object sender, EventArgs e)
        {
            //pictureBox13.Cursor = Cursors.Hand;
            //pictureBox13.Image = Oklade.Properties.Resources.ControlPanel1;
            BetValuesBtn.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox13_MouseLeave(object sender, EventArgs e)
        {
            // pictureBox13.Image = Oklade.Properties.Resources.ControlPanel;
            BetValuesBtn.BorderStyle = BorderStyle.None;
        }

        private void pictureBox13_MouseClick(object sender, MouseEventArgs e)
        {
            string SqlVrijednosti = "select NO,Value,Sport from Vrijednosti";

            _connection.Open();

            SqlCommand cmd223 = new SqlCommand(SqlVrijednosti, _connection);
            _adapter2 = new SqlDataAdapter(cmd223);
            _ds2.Reset();
            _adapter2.Fill(_ds2);
            AllBetsDGV.DataSource = _ds2.Tables[0];

            _connection.Close();
        }

        private void pictureBox13_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && (ModifierKeys & Keys.Control) == Keys.Control)
            {
                _connection.Open();

                _sql3 = "select NO,Value,Sport from Vrijednosti";

                _adapter2 = new SqlDataAdapter(_sql3, _connection);

                SqlCommandBuilder cmd224 = new SqlCommandBuilder(_adapter2);

                _adapter2.Update(_ds2);

                MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                BetsTracker_Load(sender, e);

                _connection.Close();
            }
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            if (label21.Visible != true)
            {
                label21.Show();
                label22.Show();
                label23.Show();
                label24.Show();
                label25.Show();
                label26.Show();
                label27.Show();
                label28.Show();
                label29.Show();
                label30.Show();
                label31.Show();
                label32.Show();
            }
            else
            {
                label21.Hide();
                label22.Hide();
                label23.Hide();
                label24.Hide();
                label25.Hide();
                label26.Hide();
                label27.Hide();
                label28.Hide();
                label29.Hide();
                label30.Hide();
                label31.Hide();
                label32.Hide();
            }
        }

        private void pictureBox12_MouseHover_1(object sender, EventArgs e)
        {
            //pictureBox12.Cursor = Cursors.Hand;
            // pictureBox12.Image = Oklade.Properties.Resources.bar_graph2;
            pictureBox12.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox12_MouseLeave(object sender, EventArgs e)
        {
            // pictureBox12.Image = Oklade.Properties.Resources.bar_graph;
            pictureBox12.BorderStyle = BorderStyle.None;
        }

        private void comboBox4_SelectedValueChanged(object sender, EventArgs e)
        {
        }

        private void OfferBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                OfferBox.SelectionLength = 0;
                updatebtn_Click(sender, e);
                //enter key is down
            }
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            string datum2 = dateTimePicker1.Value.Date.AddDays(-1).ToShortDateString();
            dateTimePicker1.Text = datum2;
        }

        private void pictureBox15_Click(object sender, EventArgs e)
        {
            string datum2 = dateTimePicker1.Value.Date.AddDays(+1).ToShortDateString();
            dateTimePicker1.Text = datum2;
        }

        private void pictureBox14_MouseHover(object sender, EventArgs e)
        {
            previousDayGames.Cursor = Cursors.Hand;
            previousDayGames.Image = Oklade.Properties.Resources.back_arrow2;
        }

        private void pictureBox14_MouseLeave(object sender, EventArgs e)
        {
            previousDayGames.Image = Oklade.Properties.Resources.back_arrow1;
        }

        private void pictureBox15_MouseHover(object sender, EventArgs e)
        {
            nextDayGames.Cursor = Cursors.Hand;
            nextDayGames.Image = Oklade.Properties.Resources.arrow_right2;
        }

        private void pictureBox15_MouseLeave(object sender, EventArgs e)
        {
            nextDayGames.Image = Oklade.Properties.Resources.arrow_right1;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string dailystats = @"SELECT
                                   [Date]
                                  ,[Wins]
                                  ,[Losses]
                                  ,[Invested]
                                  ,[Profit]
                                  FROM [DailyStats] order by Date desc";

                SqlCommand cmd223 = new SqlCommand(dailystats, connection);
                _adapter2 = new SqlDataAdapter(cmd223);
                _ds2.Reset();
                _adapter2.Fill(_ds2);
                AllBetsDGV.DataSource = _ds2.Tables[0];

                System.Windows.Forms.DataGridViewCellStyle boldStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
                boldStyle2.Font = new System.Drawing.Font("ArialNarrow", 14F, System.Drawing.FontStyle.Regular);
                AllBetsDGV.Columns["Wins"].DefaultCellStyle = boldStyle2;
                AllBetsDGV.Columns["Losses"].DefaultCellStyle = boldStyle2;
                AllBetsDGV.Columns["Profit"].DefaultCellStyle = boldStyle2;
                AllBetsDGV.Columns["Invested"].DefaultCellStyle = boldStyle2;

                foreach (DataGridViewRow row in AllBetsDGV.Rows)
                {
                    row.Cells["Wins"].Style.ForeColor = Color.Green;
                    row.Cells["Losses"].Style.ForeColor = Color.Red;

                    if (Convert.ToDouble(row.Cells["Profit"].Value) > 0)
                    {
                        row.Cells["Profit"].Style.ForeColor = Color.Green;
                        row.Cells["Wins"].Style.ForeColor = Color.Green;
                        row.Cells["Losses"].Style.ForeColor = Color.Green;
                    }
                    else
                    {
                        row.Cells["Profit"].Style.ForeColor = Color.Red;
                        row.Cells["Wins"].Style.ForeColor = Color.Red;
                        row.Cells["Losses"].Style.ForeColor = Color.Red;
                    }
                }
                AllBetsDGV.Rows[0].Cells[0].Selected = false;
            }
        }

        public void Scores(string date)
        {
            try
            {
                string withDate = "?selectedDate=" + date + "";
                if (date == "")
                {
                    withDate = "";
                }
                if (SharedMethods.CheckForInternetConnection())
                {
                    DateTime dt = new DateTime();
                    dt = DateTime.Now;
                    if (NBAradioButton.Checked)
                    {
                        string url = @"http://www.covers.com/Sports/NBA/Matchups" + withDate + "";
                        _nbamlbScores.LoadHTML(url);
                    }
                    else if (BestBetsRB.Checked)
                    {
                        if (dt.Month < 6 || dt.Month > 9)
                        {
                            string url = @"http://www.covers.com/Sports/NBA/Matchups" + withDate + "";
                            _nbamlbScores.LoadHTML(url);
                        }
                        else if (dt.Month > 5 && dt.Month < 10)
                        {
                            string url = @"http://www.covers.com/Sports/MLB/Matchups" + withDate + "";
                            _nbamlbScores.LoadHTML(url);
                        }
                    }
                    else if (MLBradioButton.Checked)
                    {
                        string url = @"http://www.covers.com/Sports/MLB/Matchups" + withDate + "";
                        _nbamlbScores.LoadHTML(url);
                    }

                    _nbamlbScores.LiveScores(matchupsAndScoresDGV, NBAradioButton, MLBradioButton, BestBetsRB);

                }
            }
            catch (Exception ex)
            {
                matchupsAndScoresDGV.ColumnCount = 1;
                matchupsAndScoresDGV.RowCount = 1;
                matchupsAndScoresDGV.Columns[0].Name = "covers.com Info";
                matchupsAndScoresDGV.Rows[0].Cells[0].Value = ex.ToString();
            }
        }

        private void NextBetBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                OddsBox.Focus();
                NextBetBox.SelectionLength = 0;//enter key is down
            }
        }

        private void OddsBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                GameBox.Focus();
                OddsBox.SelectionLength = 0;//enter key is down
            }
        }

        private void GameBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                OfferBox.Focus();
                OddsBox.SelectionLength = 0;//enter key is down
            }
        }

        private void BestBetsRB_CheckedChanged(object sender, EventArgs e)
        {
            BetsTracker_Load(sender, e);
            Scores(_date);
        }

        private void CasinoRB_CheckedChanged(object sender, EventArgs e)
        {
            BetsTracker_Load(sender, e);
        }

        private void NextBetBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
          (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void OddsBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
          (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void isDefault_CheckedChanged(object sender, EventArgs e)
        {
            SqlQuery = @"UPDATE BetsGroups set isDefault = 'false'";
            _bta.ExecuteSqlUpdate(connectionString, SqlQuery);

            if (NBAradioButton.Checked == true)
                ChangeisDefault = "NBA";
            else if (MLBradioButton.Checked == true)
                ChangeisDefault = "MLB";
            else if (NHLradioButton.Checked == true)
                ChangeisDefault = "NHL";
            else if (BestBetsRB.Checked == true)
                ChangeisDefault = "BestBets";

            SqlQuery = @"UPDATE BetsGroups set isDefault = 'true' where BetsGroup = '" + ChangeisDefault + "'";
            _bta.ExecuteSqlUpdate(connectionString, SqlQuery);
        }

        public string SqlQuery { get; set; }

        public string ChangeisDefault { get; set; }

        private void matchupsAndScoresDGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Box / Matchup"].Selected)
            {
                int clicked = e.RowIndex;
                if (matchupsAndScoresDGV.Rows[clicked].Cells["Box / Matchup"].Value.ToString() == "Box Score")
                {
                    System.Diagnostics.Process.Start(_nbamlbScores._coversBoxScoreLinkList[clicked]);
                }
                else if (matchupsAndScoresDGV.Rows[clicked].Cells["Box / Matchup"].Value.ToString() == "Matchup")
                {
                    System.Diagnostics.Process.Start(_nbamlbScores._coversMatchupsLinkList[clicked]);
                }
            }
        }


        private void matchupsAndScoresDGV_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            var dataGridView = (sender as DataGridView);
            //Check the condition as per the requirement casting the cell value to the appropriate type
            var colIndex = 0;
            if (NBAradioButton.Checked)
            {
                colIndex = 7;
            }
            else if (MLBradioButton.Checked)
            {
                colIndex = 9;
            }
            dataGridView.Cursor = e.ColumnIndex == colIndex ? Cursors.Hand : Cursors.Arrow;
        }

        private void RefreshScores_Tick(object sender, EventArgs e)
        {
            if(matchupsAndScoresDGV.Columns.Count > 1) { 
            if (matchupsAndScoresDGV.Rows[0].Cells["Game Status"].Value.ToString() != "Final" && matchupsAndScoresDGV.Rows[0].Cells["Away Score"].Value.ToString() != "-")
                Scores(_date);
            }
        }

        private void previousDayResults_Click(object sender, EventArgs e)
        {
            DateTime dt2 = resultsDates.Value.Date.AddDays(-1);
            resultsDates.Text = dt2.ToString("yyyy-MM-dd");
        }

        private void nextDayResults_Click(object sender, EventArgs e)
        {
            DateTime dt2 = resultsDates.Value.Date.AddDays(+1);
            resultsDates.Text = dt2.ToString("yyyy-MM-dd");
        }

        private void previousDayResults_MouseHover(object sender, EventArgs e)
        {
            previousDayResults.Cursor = Cursors.Hand;
            previousDayResults.Image = Oklade.Properties.Resources.back_arrow2;
        }

        private void previousDayResults_MouseLeave(object sender, EventArgs e)
        {
            previousDayResults.Image = Oklade.Properties.Resources.back_arrow1;
        }

        private void nextDayResults_MouseHover(object sender, EventArgs e)
        {
            nextDayResults.Cursor = Cursors.Hand;
            nextDayResults.Image = Oklade.Properties.Resources.arrow_right2;
        }

        private void nextDayResults_MouseLeave(object sender, EventArgs e)
        {
            nextDayResults.Image = Oklade.Properties.Resources.arrow_right1;
        }

        private void resultsDates_ValueChanged(object sender, EventArgs e)
        {
            Scores(resultsDates.Text);
        }
    }
}
﻿using System.Threading;
using System.Windows.Forms;

namespace Oklade
{
    public partial class SplashFormOpenProgram : Form
    {
        public SplashFormOpenProgram()
        {
            InitializeComponent();
        }
        private delegate void CloseDelegate();

        //The type of form to be displayed as the splash screen.
        private static SplashFormOpenProgram _splashFormOpen;

        public static void ShowSplashScreenOpen()
        {
            // Make sure it is only launched once.

            if (_splashFormOpen != null)
                return;
            var thread = new Thread(new ThreadStart(SplashFormOpenProgram.ShowForm))
            {
                IsBackground = true
            };
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
           // Thread.Sleep(2000);
        }

        private static void ShowForm()
        {
            _splashFormOpen = new SplashFormOpenProgram();
            Application.Run(_splashFormOpen);
        }

        public static void CloseForm()
        {
            _splashFormOpen.Invoke(new CloseDelegate(SplashFormOpenProgram.CloseFormInternal));
        }

        private static void CloseFormInternal()
        {
            _splashFormOpen.Close();
            _splashFormOpen = null;
        }
    }
}

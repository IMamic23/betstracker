﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Oklade
{
    public partial class ResultUpdater : Form
    {
        string NBAMLBResultsVar;
        string NBAMLBBetsVar;
        string AllNBAMLBResultsVar;
        string LeagueNBAMLB;
        int SeasonNBAMLB;
        int addIDs;
        int Upd1;
        int Upd2;
        int Upd3;
        int Upd4;
        int Upd5;
        int Upd6;
        int Upd7;
        int Upd8;
        int Upd9;
        int o;
        int p;
        int l;
        string Sql3TeamGames;
        int ManualChange = 0;
        string SeasonYear;
        string ResultsLastDatequery;
        string ResultsLastDate;
        string sqllastID;
        string sqlinsert;
        string Sql1AllResults;
        String connectionString;
        SqlConnection connection;
        DataSet ds;
        SqlDataAdapter adapter;
        SqlDataAdapter adapter9;
        DataGridView dgv;
        String tislike;
        String tislike2;
        public int index = 0;
        String[] str;
        String s = null;
        ProgressDialog frm4 = new ProgressDialog();
        public string value;
        string Sql2;
        string[] lines;
        string season1;
        string season2;
        int unders;
        int overs;
        int suw;
        int sul;
        int atsw;
        int atsl;
        string Datum2;
        String str1;
        String datum1;
        String Datum;
        string value2;
        string teamname;
        double avg3;
        double avg4;
        double avg3avg4;
        string provjeradatumaday;
        string provjeradatumamonth;
        string provjeradatumayear;
        BindingSource bindingSource1 = new BindingSource();
        int lastid;

        public ResultUpdater()
        {
            SplashFormOpenProgram.ShowSplashScreenOpen();
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["MyDatabase"].ToString();
            connection = new SqlConnection(connectionString);

            this.TopMost = false;

            CultureInfo culture = new CultureInfo(ConfigurationManager.AppSettings["DefaultCulture"]);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            var lbl = new Label { Parent = this, Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft };
            this.Menu = new MainMenu();
            this.Menu.MenuItems.Add("Load", (s, e) => LoadData(str => lbl.Text = str));

            DateTime dtCheck = DateTime.Today;
            if (dtCheck.Month < 5 || dtCheck.Month > 10)
            {
                radioBtnNBA.Checked = true;

            }
            else
            {
                radioBtnMLB.Checked = true;
            }

            SplashFormOpenProgram.CloseForm();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            updateScoresPB.Hide();
            checkBox1.Checked = false;
            checkBox2.Checked = true;

            connection.Open();
            index = 0;

            DateTime dtCheck = DateTime.Today;

            DateTime dtCheckStart = new DateTime(2015, 6, 1);
            DateTime dtCheckEnd = new DateTime(2015, 10, 1);


            if (radioBtnNBA.Checked == true)
            {
                AllNBAMLBResultsVar = "AllNBAResults";
                NBAMLBBetsVar = "NBABets";
                NBAMLBResultsVar = "NBAResults";
                LeagueNBAMLB = "NBA";

                if (dtCheck.Month < 10)
                {
                    SeasonNBAMLB = dtCheck.Year - 1;
                }
                else
                {
                    SeasonNBAMLB = dtCheck.Year;
                }

                DateTime dtPlayoffsStart = new DateTime(dtCheck.Year, 4, 12);
                DateTime dtPlayoffsEnd = new DateTime(dtCheck.Year, 6, 12);


                if (dtCheck.Date >= dtPlayoffsStart && dtCheck.Date < dtPlayoffsEnd)
                {
                    pictureBox4.Image = Oklade.Properties.Resources.NBAPlayoffs;
                }

            }
            else if (radioBtnMLB.Checked == true)
            {
                AllNBAMLBResultsVar = "AllMLBResults";
                NBAMLBBetsVar = "MLBBets";
                NBAMLBResultsVar = "MLBResults";
                LeagueNBAMLB = "MLB";
                SeasonNBAMLB = dtCheck.Year;
            }
            //if(ManualChange == 0)
            //{
            //    if (dtCheck.Month >= dtCheckStart.Month && dtCheck.Month < dtCheckEnd.Month)
            //    {
            //      radioBtnMLB.Checked = true;
            //    }
            //    else
            //    {
            //      radioBtnNBA.Checked = true;
            //    }
            //}
            /////////////////////////////// RESIZE FOR SMALLER RESOLUTION ////////////////////////

            int horizsize = Screen.PrimaryScreen.WorkingArea.Width;
            if (horizsize < this.Width)
            {
                this.Location = new System.Drawing.Point(0, 0);
                this.Size = Screen.PrimaryScreen.WorkingArea.Size;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width;
            }

            ////////////////////////////////////////////////////////////////////////
            if (radioBtnNBA.Checked == true)
            {
                Sql1AllResults = "select * from AllNBAResults";

                ResultsLastDatequery = @"select top 1 Month,Dayofmonth,Year from AllNBAResults where Month != 'NULL' order by ID desc";
                SeasonYear = "Year";
            }
            else
            {
                Sql1AllResults = @"SELECT [ID],[Month],[Dayofmonth],[Season],[Link],[Day],[Site],[Team],[StarterHFirstName],[StarterHLastName],[StarterHdash],[StarterHHand],
                  [Opp],[StarterAFirstName],[StarterALastName],[StarterADash],[StarterAHand],[Final],[SUm],[SUr],[OUmLine],OUr,[Hits],[Errors],[BL],[Line],[Total],
                  [Innings],[Extra],[Hscore],[Ascore],[IsOkay],[HIsLike],[AIsLike] FROM [dbo].[AllMLBResults]";

                SeasonYear = "Season";
                ResultsLastDatequery = @"select top 1 Month,Dayofmonth,Season from AllMLBResults where Month != 'NULL' order by ID desc";
            }

            SqlCommand cmdd2lastdate = new SqlCommand(ResultsLastDatequery, connection);
            SqlDataReader readd2lastdate = cmdd2lastdate.ExecuteReader();
            while (readd2lastdate.Read())
            {
                ResultsLastDate = (readd2lastdate["Month"].ToString());
                ResultsLastDate = ResultsLastDate + "," + (readd2lastdate["Dayofmonth"].ToString());
                ResultsLastDate = ResultsLastDate + (readd2lastdate["" + SeasonYear + ""].ToString());
            }
            readd2lastdate.Close();

            DateTime dtLastRes = Convert.ToDateTime(ResultsLastDate).AddDays(+2);

            dateTimePicker2.Text = dtLastRes.ToShortDateString();

            dataGridView1.DataSource = null;

            ds = new DataSet();
            adapter = new SqlDataAdapter(Sql1AllResults, connection);
            ds.Reset();
            adapter.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            dataGridView1.Columns.Remove("ID");

            label7.Text = "";
            label8.Text = "";
            label9.Text = "";

            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1.Rows[i].Cells[6].Value.ToString() != "")
                {
                    index++;
                }
            }

            dataGridView1.CurrentCell = dataGridView1.Rows[index].Cells[0];
            dataGridView1.Rows[index].Selected = true;
            if (radioBtnNBA.Checked == true)
            {
                Sql2 = "select ImeKluba from Klubovi where League = 'NBA' order by ImeKluba";
            }
            else
            {
                Sql2 = "select ImeKluba from Klubovi where League = 'MLB' order by ImeKluba";
            }
            comboBox1.Items.Clear();
            DataSet myDataSet2 = new DataSet();
            adapter9 = new SqlDataAdapter(Sql2, connection);
            adapter9.Fill(myDataSet2, "ImeKluba");
            System.Data.DataTable myDataTable = myDataSet2.Tables[0];
            DataRow tempRow = null;

            foreach (DataRow tempRow_Variable in myDataTable.Rows)
            {
                tempRow = tempRow_Variable;
                comboBox1.Items.Add((tempRow["ImeKluba"]));
            }

            connection.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            index = 0;
            label7.Text = "";
            label8.Text = "";
            label9.Text = "";
            connection.Open();
            String Sql8 = "select ID, Month, DayofMonth, Year, Season, Team, Opp, Score, Hscore, Ascore, ot from AllNBAResults where ot = '1'  or ot = '2' or ot = '3' or ot = '4'";
            ds = new DataSet();
            adapter = new SqlDataAdapter(Sql8, connection);
            ds.Reset();
            adapter.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            dataGridView1.Columns.Remove("ID");

            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1.Rows[i].Cells[6].Value.ToString() != "")
                {
                    index++;
                }
            }

            dataGridView1.CurrentCell = dataGridView1.Rows[index].Cells[0];
            dataGridView1.Rows[index].Selected = true;

            connection.Close();
        }

        private void PasteClipboard()
        {
            try
            {
                l = 0;
                frm4.Show();
                frm4.Refresh();


                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "Blue Jays", "BlueJays");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "White Sox", "WhiteSox");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "Red Sox", "RedSox");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "De La Rosa", "DeLaRosa");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "De La Cruz", "DeLaCruz");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "Wei Yin", "Wei-Yin");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "Chi Chi", "Chi-Chi");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "Hyun Jin", "Hyun-Jin");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "Nicholas Tropeano", "Nick Tropeano");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "Jose De Leon", "Jose DeLeon");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "Lance McCullers Jr", "Lance McCullers");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "Chase De Jong", "Chase DeJong");
                richTextBox1.Text = Regex.Replace(richTextBox1.Text, "Austin Bibens Dirkx", "Austin Bibens");

                lines = richTextBox1.Text.Split('\n');

                updateScoresPB.Show();
                updateScoresPB.Value = 0;
                updateScoresPB.Maximum = lines.Length - 1;
                updateScoresPB.Step = 1;

                int iFail = 0, iRow = dataGridView1.CurrentCell.RowIndex;  //+1
                int iCol = dataGridView1.CurrentCell.ColumnIndex;
                DataGridViewCell oCell;
                for (int j = 1; j < lines.Length; j++)
                {
                    updateScoresPB.PerformStep();

                    string line = lines[j];
                    if (iRow < dataGridView1.RowCount && line.Length > 0)
                    {
                        dataGridView1.Refresh();

                        string[] sCells = line.Split(' ');
                        for (int i = 0; i < sCells.GetLength(0) - 1; ++i)
                        {
                            l++;
                            if (iCol + i < this.dataGridView1.ColumnCount)
                            {

                                oCell = dataGridView1[iCol + i, iRow];
                                if (!oCell.ReadOnly)
                                {
                                    if (oCell.Value.ToString() != sCells[i])
                                    {
                                        //oCell.Value = Convert.ChangeType(sCells[i],
                                        //                      oCell.ValueType);
                                        oCell.Value = sCells[i].ToString().Replace(".", ",");
                                        oCell.Style.BackColor = Color.Green;

                                    }
                                    else
                                        iFail++;
                                    //only traps a fail if the data has changed
                                    //and you are pasting into a read only cell
                                }
                            }
                            else
                            { break; }
                        }
                        iRow++;
                    }
                    else
                    { break; }
                    if (iFail > 1000)
                        MessageBox.Show(string.Format("{0} updates failed due" +
                                        " to read only column setting", iFail));
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("The data you pasted is in the wrong format for the cell");
                return;
            }
            frm4.Hide();
            updateScoresPB.Hide();

            l = l / dataGridView1.ColumnCount + 5;

            dataGridView1.Rows[index].Selected = false;

            //  dataGridView1.Rows[index].Cells[0].Selected = true;
            dataGridView1.CurrentCell = dataGridView1.Rows[index + l].Cells[0];
            Thread.Sleep(200);

            dataGridView1.CurrentCell = dataGridView1.Rows[index - 1].Cells[0];
            dataGridView1.Rows[index - 1].Selected = true;
            //connection.Open();
            //SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapter);
            //adapter.Update(ds);
            //connection.Close();
            //  button4_Click(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            connection.Open();

            frm4.Show();
            frm4.Refresh();

            updateScoresPB.Show();
            updateScoresPB.Value = 0;
            updateScoresPB.Maximum = lines.Length - 1;
            updateScoresPB.Step = 1;

            if (radioBtnNBA.Checked == true)
            {
                //isokay
                Upd1 = 24;

                Upd2 = 9;
                ///Hres
                Upd3 = 22;
                ///Ares
                Upd4 = 23;
                //result
                Upd5 = 9;
                //tislike1
                Upd6 = 25;
                //tislike2
                Upd7 = 26;
                //Hteam
                Upd8 = 6;
                //ATeam
                Upd9 = 7;
            }
            else
            {
                //isokay
                Upd1 = 30;

                Upd2 = 16;
                ///Hres
                Upd3 = 28;
                ///Ares
                Upd4 = 29;
                //result
                Upd5 = 16;
                //tislike1
                Upd6 = 31;
                //tislike2
                Upd7 = 32;
                //Hteam
                Upd8 = 6;
                //ATeam
                Upd9 = 11;
            }


            for (int i = index; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1.Rows[i].Cells[Upd1].Value.ToString() == "")
                {
                    if (dataGridView1.Rows[i].Cells[Upd5].Value.ToString() != "")
                    {
                        if (dataGridView1.Rows[i].Cells[16].Value.ToString() != "")
                        {
                            String str2 = dataGridView1.Rows[i].Cells[Upd5].Value.ToString();
                            String str3 = str2.Split('-')[0];
                            String str4 = str2.Split('-')[1];
                            dataGridView1.Rows[i].Cells[Upd3].Value = str3;
                            dataGridView1.Rows[i].Cells[Upd4].Value = str4;
                            dataGridView1.Rows[i].Cells[Upd1].Value = "1";

                            updateScoresPB.PerformStep();

                            if (dataGridView1.Rows[i].Cells[Upd6].Value.ToString() == "")
                            {
                                String t1 = dataGridView1.Rows[i].Cells[Upd8].Value.ToString();
                                String t2 = dataGridView1.Rows[i].Cells[Upd9].Value.ToString();

                                String queryy2 = "Select TeamIsLike from Klubovi where KratkoIme = '" + t1 + "'";
                                SqlCommand cmdd2 = new SqlCommand(queryy2, connection);
                                SqlDataReader readd2 = cmdd2.ExecuteReader();
                                while (readd2.Read())
                                {
                                    tislike = (readd2["TeamIsLike"].ToString());
                                }
                                readd2.Close();
                                String queryy3 = "Select TeamIsLike from Klubovi where KratkoIme = '" + t2 + "'";
                                SqlCommand cmdd3 = new SqlCommand(queryy3, connection);
                                SqlDataReader readd3 = cmdd3.ExecuteReader();
                                while (readd3.Read())
                                {
                                    tislike2 = (readd3["TeamIsLike"].ToString());
                                }
                                readd3.Close();
                                dataGridView1.Rows[i].Cells[Upd6].Value = tislike;
                                dataGridView1.Rows[i].Cells[Upd7].Value = tislike2;

                            }
                        }
                    }
                }


            }

            int provjera = 0;

            for (int i = index; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1.Rows[i].Cells[Upd1].Value.ToString() == "")
                {
                    if (dataGridView1.Rows[i].Cells[Upd5].Value.ToString() != "")
                    {
                        if (dataGridView1.Rows[i].Cells[16].Value.ToString() == "")
                        {
                            provjera++;
                        }
                    }
                }
            }

            for (int i = 0; i < index; i++)
            {
                if (dataGridView1.Rows[i].Cells[Upd1].Value.ToString() == "")
                {
                    if (dataGridView1.Rows[i].Cells[12].Value.ToString() == "")
                    {
                        dataGridView1.Rows.Remove(dataGridView1.Rows[i]);
                    }
                }
            }



            if (provjera == 0)
            {

                // Thread.Sleep(200);

                //SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapter);
                //adapter.Update(ds);
                //MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                button5_Click(sender, e);

                frm4.Hide();
                updateScoresPB.Hide();
            }
            else
            {
                MessageBox.Show("Podaci nisu potpuni, probajte kasnije", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                frm4.Hide();
                updateScoresPB.Hide();
            }

            connection.Close();


            Form3_Load(sender, e);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (connection != null && connection.State == ConnectionState.Closed)
            {
                connection.Open();
                //String Sql1 = "select * from AllNBAResults";
                //adapter = new SqlDataAdapter(Sql1, connection);
                SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapter);
                adapter.Update(ds);
                MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1.Refresh();
                connection.Close();
            }
            else
            {
                SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapter);
                adapter.Update(ds);
                MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1.Refresh();
            }
        }

        void LoadData(Action<string> callback)
        {
            connection.Open();
            String queryy667 = "SELECT TOP 1 [Month],[Dayofmonth],[Year] FROM AllNBAResults WHERE Dayofmonth != '' ORDER BY id DESC";

            SqlCommand cmdd667 = new SqlCommand(queryy667, connection);
            SqlDataReader readd667 = cmdd667.ExecuteReader();

            while (readd667.Read())
            {
                provjeradatumamonth = (readd667["Month"].ToString());
                provjeradatumaday = (readd667["Dayofmonth"].ToString());
                provjeradatumayear = (readd667["Year"].ToString());
            }
            readd667.Close();

            string provjeradatumaall = provjeradatumaday + provjeradatumamonth + "," + provjeradatumayear + ".";
            provjeradatumaall = Regex.Replace(provjeradatumaall, ",", ".");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Apr", "tra");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Feb", "vlj");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Jan", "sij");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Mar", "ožu");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "May", "svi");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "June", "lip");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "July", "srp");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Aug", "kol");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Sept", "ruj");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Oct", "lis");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Nov", "stu");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Dec", "pro");

            DateTime dt2 = DateTime.Today.AddDays(-1);
            string datum2 = dt2.ToString("dd.MMM.yyyy.");

            var lbl = new Label { Parent = this, Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft };

            //  if (datum2 != provjeradatumaall)
            //{
            if (radioBtnNBA.Checked == true)
            {
                sqllastID = "Select TOP 1 ID from AllNbaResults order by ID desc";
            }
            else
            {
                sqllastID = "Select TOP 1 ID from AllMlbResults order by ID desc";
            }
            SqlCommand cmdd2 = new SqlCommand(sqllastID, connection);
            SqlDataReader readd2 = cmdd2.ExecuteReader();
            while (readd2.Read())
            {
                lastid = (Convert.ToInt32(readd2["ID"]));
            }
            readd2.Close();

            lastid = lastid + 1;


            if (radioBtnNBA.Checked == true)
            {
                addIDs = 5;
            }
            else
            {
                addIDs = 14;
            }


            for (int i = lastid; i < lastid + addIDs; i++)
            {
                if (radioBtnNBA.Checked == true)
                {
                    sqlinsert = @"INSERT INTO [AllNBAResults]([ID]) VALUES (" + i + ")";
                }
                else
                {
                    sqlinsert = @"INSERT INTO [AllMLBResults]([ID]) VALUES (" + i + ")";
                }
                SqlCommand cmdinsertID = new SqlCommand(sqlinsert, connection);

                cmdinsertID.ExecuteNonQuery();
            }


            Datum2 = dateTimePicker2.Value.Date.AddDays(-1).ToString("yyyyMMdd");

            if (Datum2 == "")
            {
                DateTime dt = DateTime.Today.AddDays(-1);
                datum1 = dt.ToString("yyyyMMdd");
                if (radioBtnNBA.Checked == true)
                {
                    str1 = "http://sportsdatabase.com/nba/query?output=default&sdql=season+%3D+2015+and+H+and+date%3D" + datum1 + "&submit=++S+D+Q+L+%21++";
                }
                else
                {
                    str1 = "http://sportsdatabase.com/mlb/query?output=default&sdql=season+%3D+" + dt.Year + "+and+H+and+date%3D" + datum1 + "&submit=++S+D+Q+L+%21++";
                }
            }
            else
            {
                if (radioBtnNBA.Checked == true)
                {
                    str1 = "http://sportsdatabase.com/nba/query?output=default&sdql=season+%3D+2016+and+H+and+date%3D" + Datum2 + "&submit=++S+D+Q+L+%21++";
                }
                else
                {
                    str1 = "http://sportsdatabase.com/mlb/query?output=default&sdql=season+%3D+" + dt2.Year + "+and+H+and+date%3D" + Datum2 + "&submit=++S+D+Q+L+%21++";
                }
            }

            // String str1 = "http://sportsdatabase.com/nba/query?output=default&sdql=season+%3D+2015+and+H+and+date%3D20151119&submit=++S+D+Q+L+%21++";

            var url = new Uri(str1);
            var wb = new WebBrowser();
            wb.ScriptErrorsSuppressed = true;
            wb.DocumentCompleted += (s, e) =>
            {
                if (wb.ReadyState == WebBrowserReadyState.Complete && url == e.Url)
                {
                    var el = wb.Document.GetElementById("DT_Table");
                    if (el != null)
                        s = el.InnerText;
                    richTextBox1.Text = el.InnerText;
                    str = el.InnerText.Split(' ');
                    // PasteClipboard(sender, e);
                    PasteClipboard();
                }
            };
            wb.Navigate(url);
            //  }
            connection.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //  LoadData();
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {

            value = (string)this.comboBox1.SelectedItem;
            value2 = value;
            value = value.Split(' ')[1];

            teamslogos();

            if (checkBox1.Checked == true & checkBox2.Checked == false)
            {
                season1 = "2015";
                season2 = "";
            }
            if (checkBox1.Checked == true & checkBox2.Checked == true)
            {
                season1 = "2015";
                season2 = "2016";
            }
            if (checkBox1.Checked == false & checkBox2.Checked == true)
            {
                season2 = "2016";
                season1 = "";
            }
            if (radioBtnNBA.Checked == true)
            {
                Sql3TeamGames = "select Month,DayofMonth,Year, Team, Opp, Hscore, Ascore, Line, Total, SUr, ATSr, OUr, ot from AllNBAResults where Team = '" + value + "' and season = '" + season1 +
      "' or Team = '" + value + "' and season = '" + season2 + "' or Opp = '" + value + "' and season = '" + season1 + "' or Opp = '" + value + "' and season = '" + season2 + "' order by ID desc";
            }
            else if (radioBtnMLB.Checked == true)
            {
                Sql3TeamGames = "select Month,DayofMonth,Season, Team,StarterHLastName,StarterALastName, Opp, Hscore, Ascore, Line, Total, SUr, Hits, OUr from AllMLBResults where Team = '" + value
                    + "' and season = '" + SeasonNBAMLB + "' or Team = '" + value + "' and season = '" + SeasonNBAMLB + "' or Opp = '" + value + "' and season = '" + SeasonNBAMLB + "' or Opp = '" + value + "' and season = '" + SeasonNBAMLB + "' order by ID desc";
            }
            ds = new DataSet();
            adapter = new SqlDataAdapter(Sql3TeamGames, connection);
            ds.Reset();
            adapter.Fill(ds);
            dataGridView2.DataSource = ds.Tables[0];
            dataGridView2.Refresh();
            tabControl1.SelectedIndex = 1;
            dataGridView2.Columns[0].Width = 35;
            dataGridView2.Columns[1].Width = 35;
            dataGridView2.Columns[2].Width = 35;
            dataGridView2.Columns[3].Width = 150;
            dataGridView2.Columns[4].Width = 150;
            if (radioBtnMLB.Checked == true)
            {
                dataGridView2.Columns[5].Width = 150;
                dataGridView2.Columns[6].Width = 150;
            }
            // dataGridView2.CurrentCell.Selected = false;
            this.ActiveControl = label1;
            colorresults(sender, e);
            connection.Open();
            if (radioBtnNBA.Checked == true)
            {
                string sql15 = @"UPDATE NBATeamsStatistics set SUTotWin = '" + suw + "',SUTotLoss = '" + sul + "',ATSTotWin = '" + atsw + "', ATSTotLoss = '" + atsl + "', OTotal = '" + overs + "', UTotal = '" + unders + "' where TeamName = '" + value + "'";
                SqlCommand cmd = new SqlCommand(sql15, connection);

                cmd.ExecuteNonQuery();
            }
            colorrows2(sender, e);
            if (radioBtnNBA.Checked == true)
            {
                ///////////////////////////////// TEAMS PPG /////////////////////////////////////////

                SqlCommand comm = new SqlCommand("SELECT HScore FROM " + AllNBAMLBResultsVar + " Where Team = '" + value + "' and season = '" + SeasonNBAMLB + "';", connection);

                SqlDataReader reader = comm.ExecuteReader();
                List<double> str = new List<double>();
                int i = 0;
                while (reader.Read())
                {
                    str.Add(Convert.ToDouble(reader.GetValue(0)));
                }
                reader.Close();

                SqlCommand comm2 = new SqlCommand("SELECT AScore FROM " + AllNBAMLBResultsVar + " Where opp = '" + value + "' and season = '" + SeasonNBAMLB + "';", connection);
                SqlDataReader reader2 = comm2.ExecuteReader();
                while (reader2.Read())
                {
                    str.Add(Convert.ToDouble(reader2.GetValue(0)));
                }
                reader2.Close();

                avg3 = str.Average();
                avg3 = Math.Round(avg3, 2);

                /////////////////////////////////////////////// OPPONENT //////////////////////////////

                SqlCommand comm3 = new SqlCommand("SELECT AScore FROM " + AllNBAMLBResultsVar + " Where Team = '" + value + "' and season = '" + SeasonNBAMLB + "';", connection);

                SqlDataReader reader3 = comm3.ExecuteReader();
                List<double> str2 = new List<double>();
                while (reader3.Read())
                {
                    str2.Add(Convert.ToDouble(reader3.GetValue(0)));
                }
                reader3.Close();

                SqlCommand comm4 = new SqlCommand("SELECT HScore FROM " + AllNBAMLBResultsVar + " Where opp = '" + value + "' and season = '" + SeasonNBAMLB + "';", connection);
                SqlDataReader reader4 = comm4.ExecuteReader();
                while (reader4.Read())
                {
                    str2.Add(Convert.ToDouble(reader4.GetValue(0)));
                }
                reader4.Close();

                avg4 = str2.Average();
                avg4 = Math.Round(avg4, 2);

                avg3avg4 = avg3 + avg4;
                if (radioBtnNBA.Checked == true)
                {
                    string sql157 = @"UPDATE NBATeamsPPG set PPGTotal = " + avg3.ToString().Replace(',', '.') + ", OppPPGTotal =" + avg4.ToString().Replace(',', '.') +
                        ", PPGTotalTotal = " + avg3avg4.ToString().Replace(',', '.') + "  where TeamName = '" + value + "'";
                    SqlCommand cmd157 = new SqlCommand(sql157, connection);

                    cmd157.ExecuteNonQuery();
                }
                ///////////////////////////////// TEAMS PPG /////////////////////////////////////////
            }
            connection.Close();
        }

        public void button7_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                value = (string)this.comboBox1.SelectedItem;
                value = value.Split(' ')[1];
            }



            if (checkBox1.Checked == true & checkBox2.Checked == false)
            {
                season1 = "2014";
                season2 = "";
            }
            if (checkBox1.Checked == true & checkBox2.Checked == true)
            {
                season1 = "2014";
                season2 = "2015";
            }
            if (checkBox1.Checked == false & checkBox2.Checked == true)
            {
                season2 = "2015";
                season1 = "";
            }

            String Sql3 = "select Month,DayofMonth,Year, Team, Opp, Hscore, Ascore, Line, Total, SUr, ATSr, OUr, ot from AllNBAResults where Team = '" + value + "' and season = '" + season1 +
  "' or Team = '" + value + "' and season = '" + season2 + "' or Opp = '" + value + "' and season = '" + season1 + "' or Opp = '" + value + "' and season = '" + season2 + "' order by ID desc";
            ds = new DataSet();
            adapter = new SqlDataAdapter(Sql3, connection);
            ds.Reset();
            adapter.Fill(ds);
            dataGridView2.DataSource = ds.Tables[0];
            dataGridView2.Columns[0].Width = 35;
            dataGridView2.Columns[1].Width = 35;
            dataGridView2.Columns[2].Width = 35;
            dataGridView2.Columns[3].Width = 150;
            dataGridView2.Columns[4].Width = 150;
            dataGridView2.CurrentCell.Selected = false;

            colorresults(sender, e);
            connection.Open();
            if (radioBtnNBA.Checked == true)
            {
                string sql15 = @"UPDATE NBATeamsStatistics set SUTotWin = '" + suw + "',SUTotLoss = '" + sul + "',ATSTotWin = '" + atsw + "', ATSTotLoss = '" + atsl + "', OTotal = '" + overs + "', UTotal = '" + unders + "' where TeamName = '" + value + "'";
                SqlCommand cmd = new SqlCommand(sql15, connection);

                cmd.ExecuteNonQuery();
            }
            ///////////////////////////////// TEAMS PPG /////////////////////////////////////////

            SqlCommand comm = new SqlCommand("SELECT HScore FROM " + AllNBAMLBResultsVar + " Where Team = '" + value + "' and season = '" + SeasonNBAMLB + "';", connection);

            SqlDataReader reader = comm.ExecuteReader();
            List<double> str = new List<double>();
            int i = 0;
            while (reader.Read())
            {
                str.Add(Convert.ToDouble(reader.GetValue(0)));
            }
            reader.Close();

            SqlCommand comm2 = new SqlCommand("SELECT AScore FROM " + AllNBAMLBResultsVar + " Where opp = '" + value + "' and season = '" + SeasonNBAMLB + "';", connection);
            SqlDataReader reader2 = comm2.ExecuteReader();
            while (reader2.Read())
            {
                str.Add(Convert.ToDouble(reader2.GetValue(0)));
            }
            reader2.Close();

            avg3 = str.Average();
            avg3 = Math.Round(avg3, 2);

            /////////////////////////////////////////////// OPPONENT //////////////////////////////

            SqlCommand comm3 = new SqlCommand("SELECT AScore FROM " + AllNBAMLBResultsVar + " Where Team = '" + value + "' and season = '" + SeasonNBAMLB + "';", connection);

            SqlDataReader reader3 = comm3.ExecuteReader();
            List<double> str2 = new List<double>();
            while (reader3.Read())
            {
                str.Add(Convert.ToDouble(reader3.GetValue(0)));
            }
            reader3.Close();

            SqlCommand comm4 = new SqlCommand("SELECT HScore FROM " + AllNBAMLBResultsVar + " Where opp = '" + value + "' and season = '" + SeasonNBAMLB + "';", connection);
            SqlDataReader reader4 = comm4.ExecuteReader();
            while (reader4.Read())
            {
                str2.Add(Convert.ToDouble(reader4.GetValue(0)));
            }
            reader4.Close();

            avg4 = str2.Average();
            avg4 = Math.Round(avg4, 2);

            avg3avg4 = avg3 + avg4;
            if (radioBtnNBA.Checked == true)
            {
                string sql157 = @"UPDATE NBATeamsPPG set PPGTotal = " + avg3.ToString().Replace(',', '.') + ", OppPPGTotal =" + avg4.ToString().Replace(',', '.') +
                    ", PPGTotalTotal = " + avg3avg4.ToString().Replace(',', '.') + "  where TeamName = '" + value + "'";
                SqlCommand cmd157 = new SqlCommand(sql157, connection);

                cmd157.ExecuteNonQuery();
            }
            ///////////////////////////////// TEAMS PPG /////////////////////////////////////////


            colorrows2(sender, e);
            connection.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                value = (string)this.comboBox1.SelectedItem;
                value = value.Split(' ')[1];
            }


            if (checkBox1.Checked == true & checkBox2.Checked == false)
            {
                season1 = "2015";
                season2 = "";
            }
            if (checkBox1.Checked == true & checkBox2.Checked == true)
            {
                season1 = "2015";
                season2 = "2016";
            }
            if (checkBox1.Checked == false & checkBox2.Checked == true)
            {
                season2 = "2016";
                season1 = "";
            }

            String Sql3 = "select  Month,DayofMonth,Year,Team, Opp, Hscore, Ascore, Line, Total, SUr, ATSr, OUr, ot from " + AllNBAMLBResultsVar + " where Team = '" + value + "' and season = '" +
                season1 + "' or Team = '" + value + "' and season = '" + season2 + "' order by ID desc ";
            ds = new DataSet();
            adapter = new SqlDataAdapter(Sql3, connection);
            ds.Reset();
            adapter.Fill(ds);
            dataGridView2.DataSource = ds.Tables[0];
            dataGridView2.Columns[0].Width = 35;
            dataGridView2.Columns[1].Width = 35;
            dataGridView2.Columns[2].Width = 35;
            dataGridView2.Columns[3].Width = 150;
            dataGridView2.Columns[4].Width = 150;
            dataGridView2.CurrentCell.Selected = false;

            colorresults(sender, e);
            connection.Open();
            if (radioBtnNBA.Checked == true)
            {
                string sql15 = @"UPDATE NBATeamsStatistics set SUHWin = '" + suw + "',SUHLoss='" + sul + "', ATSHWin = '" + atsw + "', ATSHLoss = '" + atsl + "', OHome = '" + overs + "', UHome = '" + unders + "' where TeamName = '" + value + "'";
                SqlCommand cmd = new SqlCommand(sql15, connection);

                cmd.ExecuteNonQuery();
            }
            ///////////////////////////////// TEAMS PPG /////////////////////////////////////////

            String queryy66 = "SELECT AVG(HScore) FROM " + AllNBAMLBResultsVar + " Where Team = '" + value + "' and season = '" + SeasonNBAMLB + "'";

            SqlCommand cmdd66 = new SqlCommand(queryy66, connection);
            SqlDataReader readd66 = cmdd66.ExecuteReader();

            while (readd66.Read())
            {
                avg3 = (Convert.ToDouble(readd66[0]));
            }
            readd66.Close();
            avg3 = Math.Round(avg3, 2);


            String queryy667 = "SELECT AVG(AScore) FROM " + AllNBAMLBResultsVar + " Where Team = '" + value + "' and season = '" + SeasonNBAMLB + "'";

            SqlCommand cmdd667 = new SqlCommand(queryy667, connection);
            SqlDataReader readd667 = cmdd667.ExecuteReader();

            while (readd667.Read())
            {
                avg4 = (Convert.ToDouble(readd667[0]));
            }
            readd667.Close();
            avg4 = Math.Round(avg4, 2);

            avg3avg4 = avg3 + avg4;
            if (radioBtnNBA.Checked == true)
            {
                string sql157 = @"UPDATE NBATeamsPPG set PPGHome = " + avg3.ToString().Replace(',', '.') + ", OppPPGHome =" + avg4.ToString().Replace(',', '.') +
                    ", PPGHomeTotal = " + avg3avg4.ToString().Replace(',', '.') + " where TeamName = '" + value + "'";
                SqlCommand cmd157 = new SqlCommand(sql157, connection);

                cmd157.ExecuteNonQuery();
            }
            ///////////////////////////////// TEAMS PPG /////////////////////////////////////////

            colorrows2(sender, e);
            connection.Close();

        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                value = (string)this.comboBox1.SelectedItem;
                value = value.Split(' ')[1];
            }


            if (checkBox1.Checked == true & checkBox2.Checked == false)
            {
                season1 = "2015";
                season2 = "";
            }
            if (checkBox1.Checked == true & checkBox2.Checked == true)
            {
                season1 = "2015";
                season2 = "2016";
            }
            if (checkBox1.Checked == false & checkBox2.Checked == true)
            {
                season2 = "2016";
                season1 = "";
            }

            String Sql3 = "select  Month,DayofMonth,Year,Team, Opp, Hscore, Ascore, Line, Total, SUr, ATSr, OUr, ot from " + AllNBAMLBResultsVar + " where Opp = '" + value +
                "' and season = '" + season1 + "' or Opp = '" + value + "' and season = '" + season2 + "' order by ID desc";
            ds = new DataSet();
            adapter = new SqlDataAdapter(Sql3, connection);
            ds.Reset();
            adapter.Fill(ds);
            dataGridView2.DataSource = ds.Tables[0];
            dataGridView2.Columns[0].Width = 38;
            dataGridView2.Columns[1].Width = 38;
            dataGridView2.Columns[2].Width = 38;
            dataGridView2.Columns[3].Width = 150;
            dataGridView2.Columns[4].Width = 150;
            dataGridView2.CurrentCell.Selected = false;

            colorresults(sender, e);
            connection.Open();
            if (radioBtnNBA.Checked == true)
            {
                string sql15 = @"UPDATE NBATeamsStatistics set SUAWin = '" + suw + "',SUALoss='" + sul + "', ATSAWin = '" + atsw + "', ATSALoss = '" + atsl + "', OAway = '" + overs + "', UAway = '" + unders + "' where TeamName = '" + value + "'";
                SqlCommand cmd = new SqlCommand(sql15, connection);

                cmd.ExecuteNonQuery();
            }
            ///////////////////////////////// TEAMS PPG /////////////////////////////////////////

            String queryy66 = "SELECT AVG(AScore) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + value + "' and season = '" + SeasonNBAMLB + "'";

            SqlCommand cmdd66 = new SqlCommand(queryy66, connection);
            SqlDataReader readd66 = cmdd66.ExecuteReader();

            while (readd66.Read())
            {
                avg3 = (Convert.ToDouble(readd66[0]));
            }
            readd66.Close();
            avg3 = Math.Round(avg3, 2);


            String queryy667 = "SELECT AVG(HScore) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + value + "' and season = '" + SeasonNBAMLB + "'";

            SqlCommand cmdd667 = new SqlCommand(queryy667, connection);
            SqlDataReader readd667 = cmdd667.ExecuteReader();

            while (readd667.Read())
            {
                avg4 = (Convert.ToDouble(readd667[0]));
            }
            readd667.Close();
            avg4 = Math.Round(avg4, 2);

            avg3avg4 = avg3 + avg4;
            if (radioBtnNBA.Checked == true)
            {
                string sql157 = @"UPDATE NBATeamsPPG set PPGAway = " + avg3.ToString().Replace(',', '.') + ", OppPPGAway =" + avg4.ToString().Replace(',', '.') +
                    ", PPGAwayTotal = " + avg3avg4.ToString().Replace(',', '.') + " where TeamName = '" + value + "'";
                SqlCommand cmd157 = new SqlCommand(sql157, connection);

                cmd157.ExecuteNonQuery();
            }
            ///////////////////////////////// TEAMS PPG /////////////////////////////////////////

            colorrows2(sender, e);
            connection.Close();
        }

        private void colorresults(object sender, EventArgs e)
        {
            suw = 0;
            sul = 0;
            atsl = 0;
            atsw = 0;
            overs = 0;
            unders = 0;

            try
            {
                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                boldStyle.Font = new System.Drawing.Font("Ariel", 9.75F, System.Drawing.FontStyle.Bold);
                dataGridView2.Columns[3].DefaultCellStyle = boldStyle;
                dataGridView2.Columns[4].DefaultCellStyle = boldStyle;
                dataGridView2.Columns[9].DefaultCellStyle = boldStyle;
                dataGridView2.Columns[10].DefaultCellStyle = boldStyle;
                dataGridView2.Columns[11].DefaultCellStyle = boldStyle;

                double line1;

                foreach (DataGridViewRow row in dataGridView2.Rows)
                {
                    if (row.Cells[11].Value.ToString() == "U")
                    {
                        row.Cells[11].Style.ForeColor = Color.ForestGreen;
                    }
                    else if (row.Cells[11].Value.ToString() == "O")
                    {
                        row.Cells[11].Style.ForeColor = Color.DarkRed;
                    }
                    //////////////////////////////////////////////////////////////////////////////////////////////

                    if (value == row.Cells[3].Value.ToString())
                    {
                        row.Cells[3].Style.ForeColor = Color.ForestGreen;
                        if (row.Cells[9].Value.ToString() == "L")
                        {
                            row.Cells[9].Style.ForeColor = Color.DarkRed;
                        }
                        if (row.Cells[9].Value.ToString() == "W")
                        {
                            row.Cells[9].Style.ForeColor = Color.ForestGreen;
                        }
                        if (row.Cells[10].Value.ToString() == "L")
                        {
                            row.Cells[10].Style.ForeColor = Color.DarkRed;
                        }
                        if (row.Cells[10].Value.ToString() == "W")
                        {
                            row.Cells[10].Style.ForeColor = Color.ForestGreen;
                        }
                    }
                    if (value == row.Cells[4].Value.ToString())
                    {
                        row.Cells[4].Style.ForeColor = Color.ForestGreen;
                        if (row.Cells[9].Value.ToString() == "L")
                        {
                            row.Cells[9].Value = "W";
                            row.Cells[9].Style.ForeColor = Color.ForestGreen;
                        }
                        else if (row.Cells[9].Value.ToString() == "W")
                        {
                            row.Cells[9].Value = "L";
                            row.Cells[9].Style.ForeColor = Color.DarkRed;
                        }
                        if (row.Cells[10].Value.ToString() == "L")
                        {
                            row.Cells[10].Value = "W";
                            row.Cells[10].Style.ForeColor = Color.ForestGreen;
                        }
                        else if (row.Cells[10].Value.ToString() == "W")
                        {
                            row.Cells[10].Value = "L";
                            row.Cells[10].Style.ForeColor = Color.DarkRed;
                        }
                        line1 = Convert.ToDouble(row.Cells[7].Value.ToString().Replace(".", ","));
                        line1 = line1 * (-1);
                        row.Cells[7].Value = line1.ToString();
                    }

                    if (row.Cells["SUr"].Value.ToString() == "W")
                    {
                        suw++;
                    }
                    else if (row.Cells["SUr"].Value.ToString() == "L")
                    {
                        sul++;
                    }
                    if (row.Cells["OUr"].Value.ToString() == "O")
                    {
                        overs++;
                    }
                    else if (row.Cells["OUr"].Value.ToString() == "U")
                    {
                        unders++;
                    }
                    if (row.Cells["ATSr"].Value.ToString() == "W")
                    {
                        atsw++;
                    }
                    else if (row.Cells["ATSr"].Value.ToString() == "L")
                    {
                        atsl++;
                    }
                    label7.Text = suw.ToString() + " - " + sul.ToString();
                    label8.Text = atsw.ToString() + " - " + atsl.ToString();
                    label9.Text = overs.ToString() + " - " + unders.ToString();

                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.ToString());
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                Datum = dateTimePicker1.Text;
                Datum = dateTimePicker1.Value.Date.ToString("MMM,dd,yyyy");
                String month = Datum.Split(',')[0];
                if (month == "stu")
                {
                    month = "Nov";
                }
                else if (month == "pro")
                {
                    month = "Dec";
                }
                else if (month == "sij")
                {
                    month = "Jan";
                }
                else if (month == "vlj")
                {
                    month = "Feb";
                }
                else if (month == "ožu")
                {
                    month = "Mar";
                }
                else if (month == "tra")
                {
                    month = "Apr";
                }



                String dan = Datum.Split(',')[1] + ",";
                String godina = Datum.Split(',')[2];
                // godina = "2014";
                String Sql3 = "select  Month,DayofMonth,Year,Team, Opp, Hscore, Ascore, Line, Total, SUr, ATSr, OUr from AllNBAResults where Month = '" + month +
                    "' and Dayofmonth = '" + dan + "' and Year = '" + godina + "'";
                ds = new DataSet();
                adapter = new SqlDataAdapter(Sql3, connection);
                ds.Reset();
                adapter.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
                dataGridView1.Columns[0].Width = 38;
                dataGridView1.Columns[1].Width = 38;
                dataGridView1.Columns[2].Width = 38;
                dataGridView1.Columns[3].Width = 150;
                dataGridView1.Columns[4].Width = 150;
                // dataGridView1.CurrentCell.Selected = false;

                colorresults(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button6_Click_1(object sender, EventArgs e)
        {

            dataGridView1.DataSource = null;
            Form3_Load(sender, e);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            connection.Open();
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1.Rows[i].Cells[5].Value.ToString() == "2015")
                {
                    String t1 = dataGridView1.Rows[i].Cells[6].Value.ToString();
                    String t2 = dataGridView1.Rows[i].Cells[7].Value.ToString();

                    String queryy2 = "Select TeamIsLike from Klubovi where KratkoIme = '" + t1 + "'";
                    SqlCommand cmdd2 = new SqlCommand(queryy2, connection);
                    SqlDataReader readd2 = cmdd2.ExecuteReader();
                    while (readd2.Read())
                    {
                        tislike = (readd2["TeamIsLike"].ToString());
                    }
                    readd2.Close();
                    String queryy3 = "Select TeamIsLike from Klubovi where KratkoIme = '" + t2 + "'";
                    SqlCommand cmdd3 = new SqlCommand(queryy3, connection);
                    SqlDataReader readd3 = cmdd3.ExecuteReader();
                    while (readd3.Read())
                    {
                        tislike2 = (readd3["TeamIsLike"].ToString());
                    }
                    readd3.Close();
                    dataGridView1.Rows[i].Cells[25].Value = tislike;
                    dataGridView1.Rows[i].Cells[26].Value = tislike2;
                }
            }
            SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapter);
            adapter.Update(ds);
            MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            connection.Close();
        }
        private void teamslogos()
        {
            if (value2 == "Atlanta Hawks")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Atlanta_Hawks_Logo1;
            }
            else if (value2 == "Boston Celtics")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Boston_Celtics_Logo;
            }
            else if (value2 == "Brooklyn Nets")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Brooklyn_Nets_logo;
            }
            else if (value2 == "Charlotte Hornets")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Charlotte_Hornets_new_logo1;
            }
            else if (value2 == "Chicago Bulls")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.chicago_bulls_logo1;
            }
            else if (value2 == "Cleveland Cavaliers")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Cleveland;
            }
            else if (value2 == "Dallas Mavericks")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.dallas_mavericks_logo1;
            }
            else if (value2 == "Denver Nuggets")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Denver_Nuggets_logo;
            }
            else if (value2 == "Detroit Pistons")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Detroit_Pistons_logo;
            }
            else if (value2 == "GS Warriors")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.GoldenStateWarriors;
            }
            else if (value2 == "Houston Rockets")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Houston_Rockets;
            }
            else if (value2 == "Indiana Pacers")
            {
                pictureBox1.Show();
                // pictureBox1.Image = Oklade.Properties.Resources.Indiana_Pacers_Logo;
            }
            else if (value2 == "LA Clippers")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Los_Angeles_Clippers_Log1o;
            }
            else if (value2 == "LA Lakers")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Los_Angeles_Lakers_logo;
            }
            else if (value2 == "Memphis Grizzlies")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Grizzlies_Logo1;
            }
            else if (value2 == "Miami Heat")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Miami_Heat_Logo;
            }
            else if (value2 == "Milwaukee Bucks")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Milwaukee_Bucks_logo;
            }
            else if (value2 == "Min Timberwolves")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Minnesota_Timberwolves_logo;
            }
            else if (value2 == "NewOrleans Pelicans")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.New_Orleans_Pelicans;
            }
            else if (value2 == "NewYork Knicks")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.new_york_knicks_logo;
            }
            else if (value2 == "OKC Thunder")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.OKC;
            }
            else if (value2 == "Orlando Magic")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Orlando_magic_logo;
            }
            else if (value2 == "Phi Seventysixers")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.phi_logo;
            }
            else if (value2 == "Phoenix Suns")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Phoenix_Suns;
            }
            else if (value2 == "Por Trailblazers")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.portland_trail_blazers_logo2;
            }
            else if (value2 == "Sacramento Kings")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Sacramento_Kings_Logo;
            }
            else if (value2 == "SanAntonio Spurs")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.San_Antonio_Spurs_logo;
            }
            else if (value2 == "Toronto Raptors")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.Toronto_Raptors_logo1;
            }
            else if (value2 == "Utah Jazz")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.utahjazz;
            }
            else if (value2 == "Washington Wizards")
            {
                pictureBox1.Show();
                pictureBox1.Image = Oklade.Properties.Resources.wiz_should_have01;
            }
            else
            {
                pictureBox1.Hide();
            }
        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex > 0)
            {
                comboBox1.SelectedIndex = comboBox1.SelectedIndex - 1;
                this.ActiveControl = label1;
            }

        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 29)
            {
                comboBox1.SelectedIndex = comboBox1.SelectedIndex + 1;
                this.ActiveControl = label1;
            }
        }

        private void pictureBox16_MouseHover(object sender, EventArgs e)
        {
            pictureBox16.Cursor = Cursors.Hand;
            pictureBox16.Image = Oklade.Properties.Resources.back_arrow2;
        }

        private void pictureBox16_MouseLeave(object sender, EventArgs e)
        {
            pictureBox16.Image = Oklade.Properties.Resources.back_arrow1;
        }

        private void pictureBox17_MouseHover(object sender, EventArgs e)
        {
            pictureBox17.Cursor = Cursors.Hand;
            pictureBox17.Image = Oklade.Properties.Resources.arrow_right2;
        }

        private void pictureBox17_MouseLeave(object sender, EventArgs e)
        {
            pictureBox17.Image = Oklade.Properties.Resources.arrow_right1;
        }

        private void button11_Click(object sender, EventArgs e)
        {

            pictureBox1.Hide();
            comboBox1.Text = "";

            connection.Open();

            string sql16 = @"SELECT
      [TeamNameF]
      ,[TeamName]
      ,[SUHWin]
      ,[SUHLoss]
      ,[SUAWin]
      ,[SUALoss]
      ,[SUTotWin]
      ,[SUTotLoss]
      ,[ATSHWin]
      ,[ATSHLoss]
      ,[ATSAWin]
      ,[ATSALoss]
      ,[ATSTotWin]
      ,[ATSTotLoss]
      ,[OHome]
      ,[UHome]
      ,[OAway]
      ,[UAway]
      ,[OTotal]
      ,[UTotal]
       FROM [NBATeamsStatistics]";
            ds = new DataSet();
            adapter = new SqlDataAdapter(sql16, connection);
            ds.Reset();
            adapter.Fill(ds);
            dataGridView4.DataSource = ds.Tables[0];

            System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
            boldStyle.Font = new System.Drawing.Font("Ariel", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridView4.Columns[0].DefaultCellStyle = boldStyle;
            dataGridView4.Columns[1].DefaultCellStyle = boldStyle;
            //dataGridView4.Columns[2].DefaultCellStyle = boldStyle;
            //dataGridView4.Columns[4].DefaultCellStyle = boldStyle;
            //dataGridView4.Columns[6].DefaultCellStyle = boldStyle;
            //dataGridView4.Columns[8].DefaultCellStyle = boldStyle;
            dataGridView4.Columns[0].Width = 110;
            dataGridView4.Columns[1].Width = 110;
            dataGridView4.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
            dataGridView4.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
            dataGridView4.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[6].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[7].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[8].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[9].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[10].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[11].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[12].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[13].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[14].DefaultCellStyle.BackColor = Color.WhiteSmoke;
            dataGridView4.Columns[15].DefaultCellStyle.BackColor = Color.WhiteSmoke;
            dataGridView4.Columns[16].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[17].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[18].DefaultCellStyle.BackColor = Color.WhiteSmoke;
            dataGridView4.Columns[19].DefaultCellStyle.BackColor = Color.WhiteSmoke;
            dataGridView4.Columns[0].HeaderText = "Team";
            dataGridView4.Columns[1].HeaderText = "Name";
            dataGridView4.Columns[2].HeaderText = "SUH Win";
            dataGridView4.Columns[3].HeaderText = "SUH Loss";
            dataGridView4.Columns[4].HeaderText = "SUA Win";
            dataGridView4.Columns[5].HeaderText = "SUA Loss";
            dataGridView4.Columns[6].HeaderText = "SUTot Win";
            dataGridView4.Columns[7].HeaderText = "SUTot Loss";
            dataGridView4.Columns[8].HeaderText = "ATSH Win";
            dataGridView4.Columns[9].HeaderText = "ATSH Loss";
            dataGridView4.Columns[10].HeaderText = "ATSA Win";
            dataGridView4.Columns[11].HeaderText = "ATSA Loss";
            dataGridView4.Columns[12].HeaderText = "ATSTot Win";
            dataGridView4.Columns[13].HeaderText = "ATSTot Loss";

            connection.Close();

            dataGridView4.CurrentCell.Selected = false;
            this.ActiveControl = label1;
            label7.Text = "";
            label8.Text = "";
            label9.Text = "";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 30; i++)
            {
                pictureBox17_Click(sender, e);
                button8_Click(sender, e);
                button9_Click(sender, e);

            }


            button11_Click(sender, e);
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                connection.Open();
                if (dataGridView1.Rows[e.RowIndex].Cells[1].Selected)
                {
                    string team = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    string team2 = @"SELECT ImeKluba from Klubovi where KratkoIme = '" + team + "'";

                    SqlCommand cmdd = new SqlCommand(team2, connection);
                    SqlDataReader readd = cmdd.ExecuteReader();

                    while (readd.Read())
                    {
                        teamname = (readd["ImeKluba"].ToString());
                    }
                    readd.Close();

                    comboBox1.Text = teamname;

                }
            }
        }
        private void colorrows2(object sender, EventArgs e)
        {
            if (radioBtnNBA.Checked == true)
            {
                dataGridView2.Columns[0].HeaderText = "M";
                dataGridView2.Columns[1].HeaderText = "D";
                dataGridView2.Columns[2].HeaderText = "Y";
                dataGridView2.Columns[2].Width = 50;
                dataGridView2.Columns[12].Width = 30;
                dataGridView2.Columns[3].HeaderText = "Home Team";
                dataGridView2.Columns[4].HeaderText = "Away Team";
                dataGridView2.Columns[5].HeaderText = "Home Points";
                dataGridView2.Columns[6].HeaderText = "Away Points";
                dataGridView2.Columns[12].HeaderText = "OT";
                dataGridView2.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView2.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView2.Columns[2].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView2.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[4].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[6].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[7].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[8].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[9].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[10].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[11].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[12].DefaultCellStyle.BackColor = Color.WhiteSmoke;
            }
            if (radioBtnMLB.Checked == true)
            {
                dataGridView2.Columns[0].HeaderText = "M";
                dataGridView2.Columns[1].HeaderText = "D";
                dataGridView2.Columns[2].HeaderText = "Y";
                dataGridView2.Columns[2].Width = 50;
                dataGridView2.Columns[12].Width = 30;
                dataGridView2.Columns[3].HeaderText = "Home Team";
                dataGridView2.Columns[4].HeaderText = "Home Starter";
                dataGridView2.Columns[5].HeaderText = "Away Starter";
                dataGridView2.Columns[6].HeaderText = "Away Team";
                dataGridView2.Columns[5].HeaderText = "Home Points";
                dataGridView2.Columns[6].HeaderText = "Away Points";
                dataGridView2.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView2.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView2.Columns[2].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView2.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[4].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[6].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[7].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[8].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[9].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[10].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[11].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[12].DefaultCellStyle.BackColor = Color.WhiteSmoke;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            string Datum2 = dateTimePicker1.Value.Date.AddDays(-1).ToShortDateString();
            dateTimePicker1.Text = Datum2;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            string Datum2 = dateTimePicker1.Value.Date.AddDays(+1).ToShortDateString();
            dateTimePicker1.Text = Datum2;
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            pictureBox2.Cursor = Cursors.Hand;
            pictureBox2.Image = Oklade.Properties.Resources.back_arrow2;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            pictureBox2.Image = Oklade.Properties.Resources.back_arrow1;
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            pictureBox3.Cursor = Cursors.Hand;
            pictureBox3.Image = Oklade.Properties.Resources.arrow_right2;
        }

        private void pictureBox3_MouseLeave(object sender, EventArgs e)
        {
            pictureBox3.Image = Oklade.Properties.Resources.arrow_right1;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            comboBox1.Text = "";

            connection.Open();

            dataGridView3.DataSource = null;


            string sql16 = @"SELECT
       [Conference]
      ,[Division]
      ,[TeamNameF]
      ,[TeamName]
      ,[SUTotWin]
      ,[SUTotLoss]
      ,[ATSTotWin]
      ,[ATSTotLoss]
      ,[OTotal]
      ,[UTotal]
       FROM [NBATeamsStatistics] order by Conference, Division, SUTotWin desc";
            ds = new DataSet();
            adapter = new SqlDataAdapter(sql16, connection);
            ds.Reset();
            adapter.Fill(ds);
            dataGridView3.DataSource = ds.Tables[0];

            System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
            boldStyle.Font = new System.Drawing.Font("Ariel", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridView3.Columns[0].DefaultCellStyle = boldStyle;
            dataGridView3.Columns[1].DefaultCellStyle = boldStyle;

            dataGridView3.Columns[0].Width = 150;
            dataGridView3.Columns[1].Width = 110;
            dataGridView3.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
            dataGridView3.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
            dataGridView3.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView3.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView3.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView3.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView3.Columns[6].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView3.Columns[7].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView3.Columns[8].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView3.Columns[9].DefaultCellStyle.BackColor = Color.LightGray;

            dataGridView3.Columns[2].HeaderText = "Team";
            dataGridView3.Columns[3].HeaderText = "Name";
            dataGridView3.Columns[4].HeaderText = "SU Win";
            dataGridView3.Columns[5].HeaderText = "SU Loss";
            dataGridView3.Columns[6].HeaderText = "ATS Win";
            dataGridView3.Columns[7].HeaderText = "ATS Loss";

            for (int i = 1; i < 30; i++)
            {

                if (i < 5)
                {
                    dataGridView3.Rows[i].Cells[1].Value = " ";
                }
                //if (i <= 5)
                //{
                //    dataGridView3.Rows[0].Cells[1].Style.ForeColor = Color.Blue;
                //    dataGridView3.Rows[i].Cells[1].Style.ForeColor = Color.Blue;
                //}

                if (i > 5 & i < 10)
                {
                    dataGridView3.Rows[i].Cells[1].Value = " ";
                }

                if (i > 10 & i < 15)
                {
                    dataGridView3.Rows[i].Cells[1].Value = " ";
                }
                if (i > 15 & i < 20)
                {
                    dataGridView3.Rows[i].Cells[1].Value = " ";
                }
                if (i > 20 & i < 25)
                {
                    dataGridView3.Rows[i].Cells[1].Value = " ";
                }
                if (i > 25 & i < 30)
                {
                    dataGridView3.Rows[i].Cells[1].Value = " ";
                }

                if (i < 15)
                {

                    dataGridView3.Rows[i].Cells[0].Value = " ";
                }
                if (i > 15)
                {
                    dataGridView3.Rows[i].Cells[0].Value = " ";
                }
            }


            connection.Close();

            dataGridView3.CurrentCell.Selected = false;
            this.ActiveControl = label1;
            label7.Text = "";
            label8.Text = "";
            label9.Text = "";
        }

        private void button14_Click(object sender, EventArgs e)
        {
        }

        private void button15_Click(object sender, EventArgs e)
        {
            connection.Open();

            dataGridView4.DataSource = null;

            string sqlppgstats = @"SELECT
                                   [TeamNameF]
                                  ,[TeamName]
                                  ,[PPGHome]
                                  ,[OppPPGHome]
                                  ,[PPGHomeTotal]
                                  ,[PPGAway]
                                  ,[OppPPGAway]
                                  ,[PPGAwayTotal]
                                  ,[PPGTotal]
                                  ,[OppPPGTotal]
                                  ,[PPGTotalTotal]
                                 FROM [NBATeamsPPG]";

            ds = new DataSet();
            adapter = new SqlDataAdapter(sqlppgstats, connection);
            ds.Reset();
            adapter.Fill(ds);
            dataGridView4.DataSource = ds.Tables[0];

            System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
            boldStyle.Font = new System.Drawing.Font("Ariel", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridView4.Columns[0].DefaultCellStyle = boldStyle;
            dataGridView4.Columns[1].DefaultCellStyle = boldStyle;

            dataGridView4.Columns[0].Width = 110;
            dataGridView4.Columns[1].Width = 110;
            dataGridView4.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
            dataGridView4.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
            dataGridView4.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[4].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[6].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[7].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView4.Columns[8].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[9].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView4.Columns[10].DefaultCellStyle.BackColor = Color.Silver;


            dataGridView4.Columns[0].HeaderText = "Team";
            dataGridView4.Columns[1].HeaderText = "Name";
            dataGridView4.Columns[2].HeaderText = "PPG Home";
            dataGridView4.Columns[3].HeaderText = "Opp PPG";
            dataGridView4.Columns[4].HeaderText = "Home PPG Total";
            dataGridView4.Columns[5].HeaderText = "PPG Away";
            dataGridView4.Columns[6].HeaderText = "Opp PPG";
            dataGridView4.Columns[7].HeaderText = "Away PPG Total";
            dataGridView4.Columns[8].HeaderText = "PPG Total";
            dataGridView4.Columns[9].HeaderText = "Opp PPG";
            dataGridView4.Columns[10].HeaderText = "PPG Total";


            connection.Close();

            dataGridView4.CurrentCell.Selected = false;
            this.ActiveControl = label1;
            label7.Text = "";
            label8.Text = "";
            label9.Text = "";
        }

        private void button16_Click(object sender, EventArgs e)
        {
            connection.Open();
            String queryy667 = "SELECT TOP 1 [Month],[Dayofmonth],[Year] FROM AllNBAResults WHERE Dayofmonth != '' ORDER BY id DESC";

            SqlCommand cmdd667 = new SqlCommand(queryy667, connection);
            SqlDataReader readd667 = cmdd667.ExecuteReader();

            while (readd667.Read())
            {
                provjeradatumamonth = (readd667["Month"].ToString());
                provjeradatumaday = (readd667["Dayofmonth"].ToString());
                provjeradatumayear = (readd667["Year"].ToString());
            }
            readd667.Close();

            string provjeradatumaall = provjeradatumaday + provjeradatumamonth + "," + provjeradatumayear + ".";
            provjeradatumaall = Regex.Replace(provjeradatumaall, ",", ".");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Apr", "tra");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Feb", "vlj");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Jan", "sij");
            provjeradatumaall = Regex.Replace(provjeradatumaall, "Mar", "ožu");

            DateTime dt2 = DateTime.Today.AddDays(-2);
            string datum2 = dt2.ToString("dd.MMM.yyyy.");

            var lbl = new Label { Parent = this, Dock = DockStyle.Fill, TextAlign = ContentAlignment.MiddleLeft };

            if (datum2 == provjeradatumaall)
            {

                Datum2 = dateTimePicker2.Value.Date.AddDays(-1).ToString("yyyyMMdd");

                if (Datum2 == "")
                {
                    DateTime dt = DateTime.Today.AddDays(-1);
                    datum1 = dt.ToString("yyyyMMdd");
                    str1 = "http://sportsdatabase.com/nba/query?output=default&sdql=season+%3D+2015+and+H+and+date%3D" + datum1 + "&submit=++S+D+Q+L+%21++";
                }
                else
                {
                    str1 = "http://sportsdatabase.com/nba/query?output=default&sdql=season+%3D+2015+and+H+and+date%3D" + Datum2 + "&submit=++S+D+Q+L+%21++";
                }

                // String str1 = "http://sportsdatabase.com/nba/query?output=default&sdql=season+%3D+2015+and+H+and+date%3D20151119&submit=++S+D+Q+L+%21++";
                var url = new Uri(str1);
                var wb = new WebBrowser();
                wb.ScriptErrorsSuppressed = true;
                wb.DocumentCompleted += (s, t) =>
                {
                    if (wb.ReadyState == WebBrowserReadyState.Complete && url == t.Url)
                    {
                        var el = wb.Document.GetElementById("DT_Table");
                        if (el != null)
                            s = el.InnerText;
                        richTextBox1.Text = el.InnerText;
                        str = el.InnerText.Split(' ');
                        //   PasteClipboard(sender, e);
                    }
                };
                wb.Navigate(url);
            }
            connection.Close();

        }

        private void button17_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            comboBox1.Text = "";

            connection.Open();

            dataGridView3.DataSource = null;


            string sql16 = @"SELECT
       [Conference]
      ,[Division]
      ,[TeamNameF]
      ,[TeamName]
      ,[SUTotWin]
      ,[SUTotLoss]
      ,[ATSTotWin]
      ,[ATSTotLoss]
      ,[OTotal]
      ,[UTotal]
       FROM [NBATeamsStatistics] order by Conference,SUTotWin desc";
            ds = new DataSet();
            adapter = new SqlDataAdapter(sql16, connection);
            ds.Reset();
            adapter.Fill(ds);
            dataGridView3.DataSource = ds.Tables[0];

            System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
            boldStyle.Font = new System.Drawing.Font("Ariel", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridView3.Columns[0].DefaultCellStyle = boldStyle;
            dataGridView3.Columns[1].DefaultCellStyle = boldStyle;

            dataGridView3.Columns[0].Width = 150;
            dataGridView3.Columns[1].Width = 110;
            dataGridView3.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
            dataGridView3.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
            dataGridView3.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView3.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView3.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView3.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView3.Columns[6].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView3.Columns[7].DefaultCellStyle.BackColor = Color.Silver;
            dataGridView3.Columns[8].DefaultCellStyle.BackColor = Color.LightGray;
            dataGridView3.Columns[9].DefaultCellStyle.BackColor = Color.LightGray;

            dataGridView3.Columns[2].HeaderText = "Team";
            dataGridView3.Columns[3].HeaderText = "Name";
            dataGridView3.Columns[4].HeaderText = "SU Win";
            dataGridView3.Columns[5].HeaderText = "SU Loss";
            dataGridView3.Columns[6].HeaderText = "ATS Win";
            dataGridView3.Columns[7].HeaderText = "ATS Loss";

            //for (int i = 1; i < 30; i++)
            //{

            //    if (i < 5)
            //    {
            //        dataGridView3.Rows[i].Cells[1].Value = " ";
            //    }
            //    //if (i <= 5)
            //    //{
            //    //    dataGridView3.Rows[0].Cells[1].Style.ForeColor = Color.Blue;
            //    //    dataGridView3.Rows[i].Cells[1].Style.ForeColor = Color.Blue;
            //    //}

            //    if (i > 5 & i < 10)
            //    {
            //        dataGridView3.Rows[i].Cells[1].Value = " ";
            //    }

            //    if (i > 10 & i < 15)
            //    {
            //        dataGridView3.Rows[i].Cells[1].Value = " ";
            //    }
            //    if (i > 15 & i < 20)
            //    {
            //        dataGridView3.Rows[i].Cells[1].Value = " ";
            //    }
            //    if (i > 20 & i < 25)
            //    {
            //        dataGridView3.Rows[i].Cells[1].Value = " ";
            //    }
            //    if (i > 25 & i < 30)
            //    {
            //        dataGridView3.Rows[i].Cells[1].Value = " ";
            //    }

            //    if (i < 15)
            //    {

            //        dataGridView3.Rows[i].Cells[0].Value = " ";
            //    }
            //    if (i > 15)
            //    {
            //        dataGridView3.Rows[i].Cells[0].Value = " ";
            //    }
            //}


            connection.Close();

            dataGridView3.CurrentCell.Selected = false;
            this.ActiveControl = label1;
            label7.Text = "";
            label8.Text = "";
            label9.Text = "";
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 3)
            {
                button11_Click(sender, e);
            }
            if (tabControl1.SelectedIndex == 2)
            {
                button13_Click(sender, e);
            }
        }

        private void radioBtnNBA_CheckedChanged(object sender, EventArgs e)
        {
            //  ManualChange = 1;
            Form3_Load(sender, e);
        }

        private void radioBtnMLB_CheckedChanged(object sender, EventArgs e)
        {
            //   ManualChange = 1;
            Form3_Load(sender, e);
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            string Datum2 = dateTimePicker2.Value.Date.AddDays(-1).ToShortDateString();
            dateTimePicker2.Text = Datum2;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            string Datum2 = dateTimePicker2.Value.Date.AddDays(+1).ToShortDateString();
            dateTimePicker2.Text = Datum2;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}

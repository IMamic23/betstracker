﻿using System.Windows.Forms;
using System.Drawing;
using Oklade.DALEntityFramework;
using System;
using System.Threading.Tasks;
using System.Threading;

namespace Oklade.DataGridViews
{

    class DgvActions
    {
        public void Dgv1Actions(DataGridView dgv1, Klubovi homeTeam, Klubovi awayTeam,
            RadioButton radioBtnNba, RadioButton radioBtnMlb, ComboBox awayStarterCb, ComboBox homeStarterCb)
        {
            dgv1.Columns.Remove("IDOklade");
           // dgv1.Columns.Remove("ID");
            dgv1.Columns[@"ImeHKluba"].Width = 130;
            dgv1.Columns[@"ImeHKluba"].DefaultCellStyle.BackColor = Color.Silver;
            dgv1.Columns[@"ImeHKluba"].HeaderText = "Home Team";
            dgv1.Columns[@"ImeAKluba"].Width = 130;
            dgv1.Columns[@"ImeAKluba"].DefaultCellStyle.BackColor = Color.Silver;
            dgv1.Columns[@"ImeAKluba"].HeaderText = "Away Team";
            dgv1.Columns["HResult"].Width = 65;
            dgv1.Columns["HResult"].DefaultCellStyle.BackColor = Color.LightGray;
            dgv1.Columns["HResult"].HeaderText = "Home Points";
            dgv1.Columns["AResult"].Width = 65;
            dgv1.Columns["AResult"].DefaultCellStyle.BackColor = Color.LightGray;
            dgv1.Columns["AResult"].HeaderText = "Away Points";
            dgv1.Columns[@"TotalResult"].DefaultCellStyle.BackColor = Color.Gainsboro;
            dgv1.Columns[@"TotalResult"].HeaderText = "Tot";
            dgv1.Columns[@"HandicapResult"].DefaultCellStyle.BackColor = Color.Gainsboro;
            dgv1.Columns[@"HandicapResult"].HeaderText = "Hand";

            if (radioBtnMlb.Checked == true)
            {
                dgv1.Columns[@"HomeStarter"].Width = 120;
                dgv1.Columns[@"HomeStarter"].HeaderText = "Home Starter";
                dgv1.Columns[@"HomeStarter"].DefaultCellStyle.BackColor = Color.Silver;
                dgv1.Columns[@"HomeStarterHand"].Width = 30;
                dgv1.Columns[@"HomeStarterHand"].HeaderText = "HH";
                dgv1.Columns[@"HomeStarterHand"].DefaultCellStyle.BackColor = Color.Silver;
                dgv1.Columns[@"AwayStarter"].Width = 120;
                dgv1.Columns[@"AwayStarter"].HeaderText = "Away Starter";
                dgv1.Columns[@"AwayStarter"].DefaultCellStyle.BackColor = Color.Silver;
                dgv1.Columns[@"AwayStarterHand"].Width = 30;
                dgv1.Columns[@"AwayStarterHand"].HeaderText = "AH";
                dgv1.Columns[@"AwayStarterHand"].DefaultCellStyle.BackColor = Color.Silver;
                dgv1.Columns[@"ParkFactor"].Width = 50;
                dgv1.Columns[@"ParkFactor"].HeaderText = "PF";
            }

            dgv1.Rows[0].Cells[0].Selected = false;
            dgv1.CurrentCell.Selected = false;

            DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
            boldStyle.Font = new Font("Ariel Narrow", 11.25F, FontStyle.Bold);
            boldStyle.ForeColor = Color.Navy;

            DataGridViewCellStyle boldStyle2 = new DataGridViewCellStyle();
            boldStyle2.Font = new Font("Ariel Narrow", 10.25F, FontStyle.Bold);
            boldStyle2.ForeColor = Color.Indigo;

            DataGridViewCellStyle boldStyle3 = new DataGridViewCellStyle();
            boldStyle3.Font = new Font("Ariel Narrow", 11.25F, FontStyle.Bold);
            boldStyle3.ForeColor = Color.MidnightBlue;

            for (int i = 0; i < dgv1.Rows.Count - 1; i++)
            //Parallel.For(0, dgv1.Rows.Count - 1, i =>
            {
                dgv1.InvokeIfRequired(d =>
                    {
                        dgv1.Rows[i].Cells["HResult"].Style = boldStyle3;
                        dgv1.Rows[i].Cells["AResult"].Style = boldStyle3;

                        if (dgv1.Rows[i].Cells["ImeHKluba"].Value.ToString() == homeTeam.KratkoIme)
                        {
                            dgv1.Rows[i].Cells["ImeHKluba"].Style = boldStyle;
                        }
                        if (dgv1.Rows[i].Cells["ImeAKluba"].Value.ToString() == awayTeam.KratkoIme)
                        {
                            dgv1.Rows[i].Cells["ImeAKluba"].Style = boldStyle;
                        }
                        if (radioBtnMlb.Checked == true)
                        {
                            awayStarterCb.InvokeIfRequired(a =>
                                {
                                    if (dgv1.Rows[i].Cells["AwayStarter"].Value.ToString() == awayStarterCb.Text.Split(' ')[1])
                                    {
                                        dgv1.Rows[i].Cells["AwayStarter"].Style = boldStyle2;
                                    }
                                });
                            homeStarterCb.InvokeIfRequired(h =>
                                {
                                    if (dgv1.Rows[i].Cells["HomeStarter"].Value.ToString() == homeStarterCb.Text.Split(' ')[1])
                                    {
                                        dgv1.Rows[i].Cells["HomeStarter"].Style = boldStyle2;
                                    }
                                });
                        }
                    });
            }//);
        }

        public void ColorDgv1RowsAndCalculate(DataGridView dgv1, TextBox totalBox, TextBox handicapBox,
                                             out int isOver, out int isUnder, out int hAway, out int hHome, out double hAvg, out double aAvg)
        {
            // COLOR ROWS
            float totall = 0;
            float hendii = 0;
            isUnder = 0;
            isOver = 0;
            hHome = 0;
            hAway = 0;
            hAvg = 0;
            aAvg = 0;

            try
            {

                foreach (DataGridViewRow row in dgv1.Rows)
                {
                    if (row.Cells["TotalResult"].Value.ToString() == DBNull.Value.ToString())
                    {
                        dgv1.Rows.Remove(row);
                    }
                    else
                    {
                        var cellv = Convert.ToDouble(row.Cells["TotalResult"].Value);

                        if (totalBox.Text != "")
                        {
                            totall = float.Parse(totalBox.Text);
                        }

                        var cellv2 = Convert.ToDouble(row.Cells["HandicapResult"].Value);
                        if (handicapBox.Text != "")
                        {
                            hendii = float.Parse(handicapBox.Text);
                        }

                        hAvg = hAvg + Convert.ToDouble(row.Cells["HResult"].Value);
                        aAvg = aAvg + Convert.ToDouble(row.Cells["AResult"].Value);

                        if (cellv < totall) //if check ==0
                        {
                            isUnder++;
                            DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
                            boldStyle.Font = new Font("Ariel Narrow", 8.25F, FontStyle.Bold);
                            row.Cells["TotalResult"].Style = boldStyle;
                            row.Cells["TotalResult"].Style.ForeColor = Color.Green;
                        }
                        else if (cellv > totall) //if check ==0
                        {
                            isOver++;
                            DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
                            boldStyle.Font = new Font("Ariel Narrow", 8.25F, FontStyle.Bold);
                            row.Cells["TotalResult"].Style = boldStyle;
                            row.Cells["TotalResult"].Style.ForeColor = Color.Red;
                        }

                        else if (cellv.Equals(totall)) //if check ==0
                        {

                            DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
                            boldStyle.Font = new Font("Ariel Narrow", 8.25F, FontStyle.Bold);
                            row.Cells["TotalResult"].Style = boldStyle;
                            row.Cells["TotalResult"].Style.ForeColor = Color.Orange;
                        }

                        if (cellv2 < hendii) //if check ==0
                        {
                            hHome++;
                            DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
                            boldStyle.Font = new Font("Ariel Narrow", 8.25F, FontStyle.Bold);
                            row.Cells["HandicapResult"].Style = boldStyle;
                            row.Cells["HandicapResult"].Style.ForeColor = Color.Green;
                        }
                        else if (cellv2 > hendii) //if check ==0
                        {
                            hAway++;
                            DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
                            boldStyle.Font = new Font("Ariel Narrow", 8.25F, FontStyle.Bold);
                            row.Cells["HandicapResult"].Style = boldStyle;
                            row.Cells["HandicapResult"].Style.ForeColor = Color.Red;
                        }
                        else if (cellv2.Equals(hendii)) //if check ==0
                        {
                            DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
                            boldStyle.Font = new Font("Ariel Narrow", 8.25F, FontStyle.Bold);
                            row.Cells["HandicapResult"].Style = boldStyle;
                            row.Cells["HandicapResult"].Style.ForeColor = Color.Orange;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.ToString());
            }
        }

        public void FormatLast50Games(DataGridView last50Games, Klubovi teamKlubovi, RadioButton radioBtnNba)
        {
            DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
            boldStyle.Font = new Font("Arial Narrow", 11.75F, FontStyle.Bold);
            boldStyle.ForeColor = Color.Navy;
            DataGridViewCellStyle boldStyle2 = new DataGridViewCellStyle();
            boldStyle2.Font = new Font("Arial Narrow", 10.75F, FontStyle.Bold);

            last50Games.InvokeIfRequired(l =>
            {
                last50Games.Columns["Dayofmonth"].DefaultCellStyle.BackColor = Color.LightGray;
                last50Games.Columns["Dayofmonth"].HeaderText = "Day";
                last50Games.Columns["Dayofmonth"].Width = 35;
                last50Games.Columns["Month"].DefaultCellStyle.BackColor = Color.LightGray;
                last50Games.Columns["Month"].HeaderText = "Month";
                last50Games.Columns["Month"].Width = 40;
                last50Games.Columns["Team"].DefaultCellStyle.BackColor = Color.LightGray;
                last50Games.Columns["Team"].Width = 107;
                last50Games.Columns["Team"].HeaderText = "Home Team";
                last50Games.Columns["Opp"].DefaultCellStyle.BackColor = Color.LightGray;
                last50Games.Columns["Opp"].Width = 107;
                last50Games.Columns["Opp"].HeaderText = "Away Team";
                var column = last50Games.Columns["StarterHLastName"];
                if (column != null)
                    column.DefaultCellStyle.BackColor = Color.LightGray;
                var o = last50Games.Columns["StarterALastName"];
                if (o != null)
                    o.DefaultCellStyle.BackColor = Color.LightGray;
                last50Games.Columns["Hscore"].DefaultCellStyle.BackColor = Color.Silver;
                last50Games.Columns["Hscore"].DefaultCellStyle = boldStyle2;
                last50Games.Columns["Hscore"].HeaderText = "Home Points";
                last50Games.Columns["Ascore"].DefaultCellStyle.BackColor = Color.Silver;
                last50Games.Columns["Ascore"].HeaderText = "Away Points";
                last50Games.Columns["Ascore"].DefaultCellStyle = boldStyle2;
                last50Games.Columns["Line"].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                last50Games.Columns["Total"].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                last50Games.Columns["SUr"].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                last50Games.Columns["SUr"].DefaultCellStyle = boldStyle2;
                last50Games.Columns["OUr"].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                last50Games.Columns["OUr"].DefaultCellStyle = boldStyle2;
                var dataGridViewColumn1 = last50Games.Columns["ATSr"];
                if (dataGridViewColumn1 != null) dataGridViewColumn1.DefaultCellStyle = boldStyle2;
                var viewColumn = last50Games.Columns["Hits"];
                if (viewColumn != null)
                    viewColumn.DefaultCellStyle.BackColor = Color.WhiteSmoke;

                var dataGridViewColumn = last50Games.Columns["StarterHLastName"];
                if (dataGridViewColumn != null)
                    dataGridViewColumn.HeaderText = "HS";
                var gridViewColumn = last50Games.Columns["StarterALastName"];
                if (gridViewColumn != null)
                    gridViewColumn.HeaderText = "AS";

                last50Games.CurrentCell.Selected = false;

                //for (int i = 0; i < last50Games.Rows.Count; i++)
                Parallel.For(0, last50Games.Rows.Count, i =>
                 {

                     if (last50Games.Rows[i].Cells["OUr"].Value.ToString() == "U")
                     {
                         last50Games.Rows[i].Cells["OUr"].Style.ForeColor = Color.ForestGreen;
                     }
                     else if (last50Games.Rows[i].Cells["OUr"].Value.ToString() == "O")
                     {
                         last50Games.Rows[i].Cells["OUr"].Style.ForeColor = Color.DarkRed;
                     }

                     ////////////////////////////////////////////////////////////////////////////////////////////

                     if (teamKlubovi.KratkoIme == last50Games.Rows[i].Cells["Team"].Value.ToString())
                     {
                         last50Games.Rows[i].Cells["Team"].Style = boldStyle;
                         if (last50Games.Rows[i].Cells["SUr"].Value.ToString().Contains("L"))
                         {
                             last50Games.Rows[i].Cells["SUr"].Style.ForeColor = Color.DarkRed;
                         }
                         else if (last50Games.Rows[i].Cells["SUr"].Value.ToString().Contains("W"))
                         {
                             last50Games.Rows[i].Cells["SUr"].Style.ForeColor = Color.ForestGreen;
                         }
                         if (radioBtnNba.Checked)
                         {
                             if (last50Games.Rows[i].Cells["ATSr"].Value.ToString() == "L")
                             {
                                 last50Games.Rows[i].Cells["ATSr"].Style.ForeColor = Color.DarkRed;
                             }
                             else if (last50Games.Rows[i].Cells["ATSr"].Value.ToString() == "W")
                             {
                                 last50Games.Rows[i].Cells["ATSr"].Style.ForeColor = Color.ForestGreen;
                             }
                         }
                     }
                     if (teamKlubovi.KratkoIme == last50Games.Rows[i].Cells["Opp"].Value.ToString())
                     {
                         last50Games.Rows[i].Cells["Opp"].Style = boldStyle;
                         if (last50Games.Rows[i].Cells["SUr"].Value.ToString() == "L")
                         {
                             last50Games.Rows[i].Cells["SUr"].Style.ForeColor = Color.DarkRed;
                         }
                         else if (last50Games.Rows[i].Cells["SUr"].Value.ToString() == "W")
                         {
                             last50Games.Rows[i].Cells["SUr"].Style.ForeColor = Color.ForestGreen;
                         }
                         if (radioBtnNba.Checked)
                         {
                             if (last50Games.Rows[i].Cells["ATSr"].Value.ToString() == "L")
                             {
                                 last50Games.Rows[i].Cells["ATSr"].Style.ForeColor = Color.DarkRed;
                             }
                             else if (last50Games.Rows[i].Cells["ATSr"].Value.ToString() == "W")
                             {
                                 last50Games.Rows[i].Cells["ATSr"].Style.ForeColor = Color.ForestGreen;
                             }
                         }
                         if (last50Games.Rows[i].Cells["Line"].Value != null)
                         {
                             var line1 = Convert.ToDouble(last50Games.Rows[i].Cells["Line"].Value.ToString().Replace(".", ","));
                             line1 = line1 * (-1);
                             last50Games.Rows[i].Cells["Line"].Value = line1.ToString();
                         }
                     }
                 });

            });
        }

        public void FormatLast50StarterGames(DataGridView starterLast50Games, string starterLastName)
        {
            DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
            boldStyle.Font = new Font("Arial Narrow", 10.75F, FontStyle.Bold);
            DataGridViewCellStyle boldStyle2 = new DataGridViewCellStyle();
            boldStyle2.Font = new Font("Arial Narrow", 11.75F, FontStyle.Bold);
            boldStyle2.ForeColor = Color.Navy;

            starterLast50Games.InvokeIfRequired(s =>
            {
                starterLast50Games.Rows[0].Cells[0].Selected = false;
                starterLast50Games.Columns["Dayofmonth"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["Dayofmonth"].HeaderText = "Day";
                starterLast50Games.Columns["Dayofmonth"].Width = 35;
                starterLast50Games.Columns["Month"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["Month"].HeaderText = "Month";
                starterLast50Games.Columns["Month"].Width = 40;
                starterLast50Games.Columns["Team"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["Opp"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["Team"].HeaderText = "Home Team";
                starterLast50Games.Columns["Opp"].HeaderText = "Away Team";
                starterLast50Games.Columns["Team"].Width = 90;
                starterLast50Games.Columns["Opp"].Width = 90;
                starterLast50Games.Columns["StarterHLastName"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["StarterHLastName"].Width = 125;
                starterLast50Games.Columns["StarterHLastName"].HeaderText = "Home Starter";
                starterLast50Games.Columns["StarterALastName"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["StarterALastName"].Width = 125;
                starterLast50Games.Columns["StarterALastName"].HeaderText = "Away Starter";
                starterLast50Games.Columns["Line"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["Total"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["Hscore"].DefaultCellStyle.BackColor = Color.Silver;
                starterLast50Games.Columns["Hscore"].HeaderText = "Home Points";
                starterLast50Games.Columns["Hscore"].DefaultCellStyle = boldStyle;
                starterLast50Games.Columns["Ascore"].DefaultCellStyle.BackColor = Color.Silver;
                starterLast50Games.Columns["Ascore"].HeaderText = "Away Points";
                starterLast50Games.Columns["Ascore"].DefaultCellStyle = boldStyle;
                starterLast50Games.Columns["SUr"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["OUr"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["SUr"].DefaultCellStyle = boldStyle;
                starterLast50Games.Columns["OUr"].DefaultCellStyle = boldStyle;
                starterLast50Games.Columns["Hits"].DefaultCellStyle.BackColor = Color.LightGray;
                starterLast50Games.Columns["Hits"].DefaultCellStyle = boldStyle;


                //for (int i = 0; i < starterLast50Games.Rows.Count - 1; i++)
                Parallel.For(0, starterLast50Games.Rows.Count - 1, i =>
                {
                    if (starterLast50Games.Rows[i].Cells["OUr"].Value.ToString() == "U")
                    {
                        starterLast50Games.Rows[i].Cells["OUr"].Style.ForeColor = Color.ForestGreen;
                    }
                    else if (starterLast50Games.Rows[i].Cells["OUr"].Value.ToString() == "O")
                    {
                        starterLast50Games.Rows[i].Cells["OUr"].Style.ForeColor = Color.DarkRed;
                    }

                    ////////////////////////////////////////////////////////////////////////////////////////////

                    if (starterLastName == starterLast50Games.Rows[i].Cells["StarterHLastName"].Value.ToString())
                    {
                        starterLast50Games.Rows[i].Cells["StarterHLastName"].Style = boldStyle2;
                        if (starterLast50Games.Rows[i].Cells["SUr"].Value.ToString() == "L")
                        {
                            starterLast50Games.Rows[i].Cells["SUr"].Style.ForeColor = Color.DarkRed;
                        }
                        if (starterLast50Games.Rows[i].Cells["SUr"].Value.ToString() == "W")
                        {
                            starterLast50Games.Rows[i].Cells["SUr"].Style.ForeColor = Color.ForestGreen;
                        }
                    }
                    if (starterLastName == starterLast50Games.Rows[i].Cells["StarterALastName"].Value.ToString())
                    {
                        starterLast50Games.Rows[i].Cells["StarterALastName"].Style = boldStyle2;
                        if (starterLast50Games.Rows[i].Cells["SUr"].Value.ToString() == "L")
                        {
                            starterLast50Games.Rows[i].Cells["SUr"].Style.ForeColor = Color.DarkRed;
                        }
                        else if (starterLast50Games.Rows[i].Cells["SUr"].Value.ToString() == "W")
                        {
                            starterLast50Games.Rows[i].Cells["SUr"].Style.ForeColor = Color.ForestGreen;
                        }
                        if (starterLast50Games.Rows[i].Cells["Line"].Value != null)
                        {
                            var line1 = Convert.ToDouble(starterLast50Games.Rows[i].Cells["Line"].Value.ToString().Replace(".", ","));
                            line1 = line1 * (-1);
                            starterLast50Games.Rows[i].Cells["Line"].Value = line1.ToString();
                        }
                    }
                });
            });
        }
    }
}

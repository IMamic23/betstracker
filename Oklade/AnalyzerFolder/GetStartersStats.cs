﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using System.Net;
using System.Runtime.InteropServices;
using Oklade.DALEntityFramework;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Threading;

namespace Oklade.AnalyzerFolder
{
    class GetStartersStats
    {
        private HtmlAgilityPack.HtmlDocument _doc;
        private string _url;
        List<string> _list = new List<string>();

        public void UpdateStartersStats(Label progressLabel, System.Windows.Forms.ProgressBar progressBar)
        {
            using (var bw = new BackgroundWorker())
            {
                // this allows our worker to report progress during work
                bw.WorkerReportsProgress = true;

                // what to do in the background thread
                bw.DoWork += new DoWorkEventHandler(
                delegate(object o, DoWorkEventArgs args)
                {
                    var b = o as BackgroundWorker;

                    using (OkladesEntities context = new OkladesEntities())
                    {
                        progressLabel.InvokeIfRequired(p =>
                            {
                                progressLabel.Text = "Starting update...please wait...";
                            });
                        var starters = context.MLBStartersStatsNEWs.Where(s => s.ID > 0);
                        context.MLBStartersStatsNEWs.RemoveRange(starters);
                        context.SaveChanges();

                    }
                    // AWAY PAGE1//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/34/sort/gamesStarted";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Away");
                    b.ReportProgress(8);
                    //AWAY PAGE2//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/34/sort/gamesStarted/count/41/qualified/false";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Away");
                    b.ReportProgress(16);
                    //AWAY PAGE3//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/34/sort/gamesStarted/count/81/qualified/false";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Away");
                    b.ReportProgress(24);
                    //AWAY PAGE 4//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/34/sort/gamesStarted/count/121/qualified/false";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Away");
                    b.ReportProgress(32);
                    //AWAY PAGE 5//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/34/sort/gamesStarted/count/161/qualified/false";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Away");
                    b.ReportProgress(40);
                    //AWAY PAGE 6//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/34/sort/gamesStarted/count/201/qualified/false";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Away");
                    b.ReportProgress(48);
                    //HOME PAGE 1//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/33/sort/gamesStarted";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Home");
                    b.ReportProgress(56);
                    //HOME PAGE 2//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/33/sort/gamesStarted/count/41/qualified/false";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Home");
                    b.ReportProgress(64);
                    //HOME PAGE 3//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/33/sort/gamesStarted/count/81/qualified/false";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Home");
                    b.ReportProgress(74);
                    //HOME PAGE 4//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/33/sort/gamesStarted/count/121/qualified/false";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Home");
                    b.ReportProgress(82);
                    //HOME PAGE 5//
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/33/sort/gamesStarted/count/161/qualified/false";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Home");
                    b.ReportProgress(90);
                    //HOME PAGE 6 //
                    _url = @"http://espn.go.com/mlb/stats/pitching/_/split/33/sort/gamesStarted/count/201/qualified/false";
                    _doc = null;
                    LoadHtml(_url);
                    SaveStartersStatsToDb("Home");
                });

                // what to do when progress changed (update the progress bar for example)
                bw.ProgressChanged += new ProgressChangedEventHandler(
                delegate(object o, ProgressChangedEventArgs args)
                {
                    progressLabel.Text = string.Format("{0}% Completed", args.ProgressPercentage);
                    progressBar.PerformStep();
                });

                // what to do when worker completes its task (notify the user)
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
                delegate(object o, RunWorkerCompletedEventArgs args)
                {
                    progressLabel.Text = "Finished!";
                    progressBar.Value = 0;
                });

                bw.RunWorkerAsync();
            }
        }

        public void LoadHtml(string url)
        {
            using (var webClient = new WebClient())
            {
                try
                {
                    Thread.Sleep(2000);
                    var html = webClient.DownloadString(url);
                    _doc = new HtmlAgilityPack.HtmlDocument();
                    _doc.LoadHtml(html);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public void SaveStartersStatsToDb(string homeAway)
        {
            var queryStartersStats = from table in _doc.DocumentNode.SelectNodes("//table").Cast<HtmlNode>()
                                     from row in table.SelectNodes("tr[contains(@class, 'player')]").Cast<HtmlNode>()
                                     from cell in row.SelectNodes("th|td").Cast<HtmlNode>()
                                     select new
                                     {
                                         Table = table.Id,
                                         CellText = cell.InnerText.Trim()
                                     };
            int i = 0;
            _list.Clear();

            using (OkladesEntities context = new OkladesEntities())
            {
                foreach (var cell in queryStartersStats.Where(n => n.CellText != "Sortable Pitching"))
                {
                    _list.Add(cell.CellText);
                    i++;
                    if (i == 18)
                    {
                        i = 0;
                        context.MLBStartersStatsNEWs.Add(new MLBStartersStatsNEW
                        {
                            PlayerName = _list[1],
                            Team = _list[2],
                            GP = Convert.ToInt32(_list[3]),
                            GS = Convert.ToInt32(_list[4]),
                            IP = Convert.ToDecimal(_list[5].Replace(".", ",")) / Convert.ToDecimal(_list[3]),
                            H = Convert.ToDecimal(_list[6]) / Convert.ToDecimal(_list[3]),
                            R = Convert.ToDecimal(_list[7]) / Convert.ToDecimal(_list[3]),
                            ER = Convert.ToDecimal(_list[8]) / Convert.ToDecimal(_list[3]),
                            BB = Convert.ToDecimal(_list[9]) / Convert.ToDecimal(_list[3]),
                            SO = Convert.ToDecimal(_list[10]) / Convert.ToDecimal(_list[3]),
                            W = Convert.ToInt32(_list[11]),
                            L = Convert.ToInt32(_list[12]),
                            WHIP = Convert.ToDecimal(_list[16].Replace(".", ",")),
                            ERA = Convert.ToDecimal(_list[17].Replace(".", ",")),
                            Venue = homeAway
                        });
                        _list.Clear();
                        context.SaveChanges();
                    }
                };
            }
        }
    }
    public class StartersStats
    {
        public string PlayerName { get; set; }
        public string Team { get; set; }
        public int GP { get; set; }
        public int GS { get; set; }
        public decimal IP { get; set; }
        public decimal H { get; set; }
        public decimal R { get; set; }
        public decimal ER { get; set; }
        public decimal BB { get; set; }
        public decimal SO { get; set; }
        public int W { get; set; }
        public int L { get; set; }
        public decimal Whip { get; set; }
        public decimal Era { get; set; }
    }
}

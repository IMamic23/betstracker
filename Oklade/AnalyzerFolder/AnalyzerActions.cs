﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using Oklade.DALEntityFramework;
using System.Data.SqlClient;

namespace Oklade.AnalyzerFolder
{
    class AnalyzerActions
    {
        public int AwayStarterGames;
        public int HomeStarterGames;
        private OkladesEntities _context = new OkladesEntities();
        List<string> validOpps = null;

        public Klubovi FillTeams(string varTeam)
        {
            Klubovi team = (Klubovi)null;

            team = _context.Klubovis.FirstOrDefault(t => t.ImeKluba == varTeam);

            return team;
        }

        public List<NBAResult> FillResultsNba(int idOklade, RadioButton mlbRadioButton,
                                              RadioButton nbaRadioButton)
        {
            List<NBAResult> results = null;
            results = _context.NBAResults.Where(r => r.IDOklade == idOklade)
                                            .Select(r => r)
                                            .OrderBy(r => r.ID)
                                            .ToList();
            return results;
        }

        public List<MLBResult> FillResultsMlb(int idOklade, RadioButton mlbRadioButton,
                                               RadioButton nbaRadioButton)
        {
            List<MLBResult> results = null;
            results = _context.MLBResults.Where(r => r.IDOklade == idOklade)
                                            .Select(r => r)
                                            .OrderBy(r => r.ID)
                                            .ToList();

            return results;
        }

        public object FillResults(int idOklade, RadioButton mlbRadioButton, RadioButton nbaRadioButton)
        {
            object results = null;
            if (mlbRadioButton.Checked == true)
            {
                results = _context.MLBResults.Where(r => r.IDOklade == idOklade)
                                            .Distinct()
                                            .Select(r => r)
                                            .ToList();
            }
            else if (nbaRadioButton.Checked == true)
            {
                results = _context.NBAResults.Where(r => r.IDOklade == idOklade)
                                            .Distinct()
                                            .Select(r => r)
                                            .ToList();
            }
            return results;
        }

        public void InsertNbaResultsAway(string teamOuh, int season1, int season2, int?[] tilList, Klubovi awayTeam, Klubovi homeTeam,
                                         int lastXGames, int ido, CheckBox ouCheckbox, CheckBox paceCheckbox, CheckBox rankingsCb)
        {
            validOpps = null;
            if (ouCheckbox.Checked)
            {
                validOpps = _context.Klubovis.Where(t => t.TeamOUH == teamOuh && t.League == "NBA")
                                             .Select(t => t.KratkoIme).ToList();
            }
            else if (paceCheckbox.Checked)
            {
                validOpps = null;
                decimal paceHome = Convert.ToDecimal(homeTeam.PaceHome);
                var calcMinus = 2.2m;
                var calcPlus = 2.2m;
                if(paceHome >= 99.3m)
                {
                    calcMinus = 3m;
                }
                else if(paceHome <= 91m)
                {
                    calcPlus = 3m;
                }
                validOpps = _context.Klubovis.Where(t => t.League == "NBA" && t.PaceHome >= (paceHome - calcMinus) && t.PaceHome <= (paceHome + calcPlus))
                                             .Select(t => t.KratkoIme).ToList();
            }
            else
            {
                validOpps = _context.Klubovis.Where(t => t.League == "NBA")
                                             .Select(t => t.KratkoIme).ToList();
            }
            if (!rankingsCb.Checked)
            {
                tilList = null;
                tilList = new int?[] { 0, 1, 2, 3, 4, 5, 6 };
            }
            List<AllNBAResult> allNbaResult = _context.AllNBAResults.Where(a => a.Opp == awayTeam.KratkoIme &&
                                                                         a.Team != homeTeam.KratkoIme &&
                                                                         a.Season == season1 &&
                                                                         tilList.Contains(a.HIsLike) &&
                                                                         validOpps.Contains(a.Team) ||
                                                                         a.Opp == awayTeam.KratkoIme &&
                                                                         a.Team != homeTeam.KratkoIme &&
                                                                         a.Season == season2 &&
                                                                         tilList.Contains(a.HIsLike) &&
                                                                         validOpps.Contains(a.Team))
                                                                         .OrderByDescending(o => o.ID)
                                                                         .Take(lastXGames)
                                                                         .ToList();

            InsertUpdateNba(allNbaResult, ido);
        }

        public void InsertNbaResultsHome(string teamOua, int season1, int season2, int?[] tilList, Klubovi homeTeam, Klubovi awayTeam,
                                         int lastXGames, int ido, CheckBox ouCheckbox, CheckBox paceCheckbox, CheckBox rankingsCb)
        {
            validOpps = null;
            if(ouCheckbox.Checked)
            {
              validOpps = _context.Klubovis.Where(t => t.TeamOUA == teamOua && t.League == "NBA")
                                           .Select(t => t.KratkoIme).ToList();
            }
            else if (paceCheckbox.Checked)
            {
                validOpps = null;
                decimal paceAway = Convert.ToDecimal(awayTeam.PaceAway);
                var calcMinus = 2.2m;
                var calcPlus = 2.2m;
                if (paceAway >= 99.3m)
                {
                    calcMinus = 3m;
                }
                else if (paceAway <= 91m)
                {
                    calcPlus = 3m;
                }
                validOpps = _context.Klubovis.Where(t => t.League == "NBA" && t.PaceAway >= (paceAway - calcMinus) && t.PaceHome <= (paceAway + calcPlus))
                                             .Select(t => t.KratkoIme).ToList();
            }
            else
            {
              validOpps = _context.Klubovis.Where(t => t.League == "NBA")
                                           .Select(t => t.KratkoIme).ToList();
            }
            if (!rankingsCb.Checked)
            {
                tilList = null;
                tilList = new int?[] { 0, 1, 2, 3, 4, 5, 6 };
            }
              List<AllNBAResult> allNbaResult = _context.AllNBAResults.Where(a => a.Team == homeTeam.KratkoIme &&
                                                                         a.Season == season1 &&
                                                                         tilList.Contains(a.AIsLike) &&
                                                                         validOpps.Contains(a.Opp) ||
                                                                         a.Team == homeTeam.KratkoIme &&
                                                                         a.Season == season2 &&
                                                                         tilList.Contains(a.AIsLike) &&
                                                                         validOpps.Contains(a.Opp))
                                                                         .OrderByDescending(o => o.ID)
                                                                         .Take(lastXGames)
                                                                         .ToList();

            InsertUpdateNba(allNbaResult, ido);
        }
        public void InsertUpdateNba(List<AllNBAResult> allNbaResult, int ido)
        {
            foreach (var allResults in allNbaResult)
            {
                var homePace = _context.Klubovis.Where(t => t.KratkoIme == allResults.Team && t.League == "NBA")
                                                .Select(p => p.PaceHome)
                                                .First();
                var awayPace = _context.Klubovis.Where(t => t.KratkoIme == allResults.Opp && t.League == "NBA")
                                                .Select(p => p.PaceAway)
                                                .First();

                NBAResult nbaResult = new NBAResult
                {
                    Season = allResults.Season,
                    Line = allResults.Line,
                    Total = allResults.Total,
                    ImeHKluba = allResults.Team,
                    ImeAKluba = allResults.Opp,
                    HResult = allResults.Hscore,
                    AResult = allResults.Ascore,
                    TotalResult = allResults.Hscore + allResults.Ascore,
                    HandicapResult = allResults.Ascore - allResults.Hscore,
                    IDOklade = ido,
                    HomePace = homePace,
                    AwayPace = awayPace
                };
                _context.NBAResults.Add(nbaResult);
                _context.SaveChanges();
            }
        }

        public void InsertMlbResultsHome(string teamOua, int season1, int season2, CheckBox useFullNameCB, 
                                         int lastXGames, string homeStarterFirstName, string homeStarterLastName, int ido)
        {
            List<string> validOpps = _context.Klubovis.Where(t => t.League == "MLB")
                                                                .Select(t => t.KratkoIme).ToList();
            List<AllMLBResult> allMlbResult = null;
            if (!useFullNameCB.Checked)
            {
                allMlbResult = _context.AllMLBResults.Where(a => a.Season == season1 &&
                                                                         validOpps.Contains(a.Team) &&
                                                                         a.StarterHFirstName.Substring(0, 1).Contains(homeStarterFirstName.Substring(0, 1)) &&
                                                                         a.StarterHLastName.Contains(homeStarterLastName) &&
                                                                         a.StarterHFirstName.Contains(homeStarterFirstName.Substring(homeStarterFirstName.Length - 1, homeStarterFirstName.Length)) ||
                                                                         a.Season == season2 &&
                                                                         validOpps.Contains(a.Team) &&
                                                                         a.StarterHFirstName.Substring(0, 1).Contains(homeStarterFirstName.Substring(0, 1)) &&
                                                                         a.StarterHLastName.Contains(homeStarterLastName) &&
                                                                         a.StarterHFirstName.Contains(homeStarterFirstName.Substring(homeStarterFirstName.Length - 1, homeStarterFirstName.Length)))
                                                                         .OrderByDescending(o => o.ID)
                                                                         .Take(lastXGames)
                                                                         .Select(r => r)
                                                                         .ToList();
            }
            else
            {
                allMlbResult = _context.AllMLBResults.Where(a => a.Season == season1 &&
                                                                        validOpps.Contains(a.Team) &&
                                                                        a.StarterHFirstName.Contains(homeStarterFirstName) &&
                                                                        a.StarterHLastName.Contains(homeStarterLastName) ||
                                                                        a.Season == season2 &&
                                                                        validOpps.Contains(a.Team) &&
                                                                        a.StarterHFirstName.Contains(homeStarterFirstName) &&
                                                                        a.StarterHLastName.Contains(homeStarterLastName))
                                                                        .OrderByDescending(o => o.ID)
                                                                        .Take(lastXGames)
                                                                        .Select(r => r)
                                                                        .ToList();
            }
            HomeStarterGames = allMlbResult.Count;
            InsertUpdateMlb(allMlbResult, ido);
        }
        public void InsertMlbResultsAway(string teamOuh, int season1, int season2, CheckBox useFullNameCB, 
                                         int lastXGames, string awayStarterFirstName, string awayStarterLastName, string homeStarterLastName, int ido)
        {
            List<string> validOpps = _context.Klubovis.Where(t => t.League == "MLB")
                                                                .Select(t => t.KratkoIme).ToList();
            List<AllMLBResult> allMlbResult = null;
            if (!useFullNameCB.Checked)
            {
                allMlbResult = _context.AllMLBResults.Where(a => a.Season == season1 &&
                                                                             validOpps.Contains(a.Team) &&
                                                                             a.StarterAFirstName.Substring(0, 1).Contains(awayStarterFirstName.Substring(0, 1)) &&
                                                                             a.StarterALastName.Contains(awayStarterLastName) &&
                                                                             a.StarterAFirstName.Contains(awayStarterFirstName.Substring(awayStarterFirstName.Length - 1, awayStarterFirstName.Length)) &&
                                                                             a.StarterHLastName != homeStarterLastName||
                                                                             a.Season == season2 &&
                                                                             validOpps.Contains(a.Team) &&
                                                                             a.StarterAFirstName.Substring(0, 1).Contains(awayStarterFirstName.Substring(0, 1)) &&
                                                                             a.StarterALastName.Contains(awayStarterLastName) &&
                                                                             a.StarterAFirstName.Contains(awayStarterFirstName.Substring(awayStarterFirstName.Length - 1, awayStarterFirstName.Length)) &&
                                                                             a.StarterHLastName != homeStarterLastName)
                                                                             .OrderByDescending(o => o.ID)
                                                                             .Take(lastXGames)
                                                                             .Select(r => r)
                                                                             .ToList();
            }
            else
            {
                allMlbResult = _context.AllMLBResults.Where(a => a.Season == season1 &&
                                                                             validOpps.Contains(a.Team) &&
                                                                             a.StarterAFirstName.Contains(awayStarterFirstName) &&
                                                                             a.StarterALastName.Contains(awayStarterLastName) &&
                                                                             a.StarterHLastName != homeStarterLastName ||
                                                                             a.Season == season2 &&
                                                                             validOpps.Contains(a.Team) &&
                                                                             a.StarterAFirstName.Contains(awayStarterFirstName) &&
                                                                             a.StarterALastName.Contains(awayStarterLastName) &&
                                                                             a.StarterHLastName != homeStarterLastName)
                                                                             .OrderByDescending(o => o.ID)
                                                                             .Take(lastXGames)
                                                                             .Select(r => r)
                                                                             .ToList();
            }
            AwayStarterGames = allMlbResult.Count;
            InsertUpdateMlb(allMlbResult, ido);
        }

        public void InsertMlbExtraGames(Klubovi homeMlbteam, Klubovi awayMlbTeam, string teamOu, int season1, int season2,
                                         int addgames, int ido, string teamStarterHand, string starterLastName, string oppStarterLastName, string homeAway)
        {
            List<string> validOpps;
            List<AllMLBResult> allMlbResult = new List<AllMLBResult>();
            if (homeAway.Equals("Away"))
            {
                validOpps = _context.Klubovis.Where(t => t.League == "MLB" &&
                                                    t.KratkoIme != homeMlbteam.KratkoIme)
                                                    .Select(t => t.KratkoIme)
                                                    .ToList();

                allMlbResult = _context.AllMLBResults.Where(a => a.Season == season1 && 
                                                            a.Opp == awayMlbTeam.KratkoIme &&
                                                            a.StarterALastName != starterLastName &&
                                                            a.StarterHLastName != oppStarterLastName &&
                                                            validOpps.Contains(a.Team) &&
                                                            a.StarterHHand == teamStarterHand ||
                                                            a.Season == season2 &&
                                                            a.Opp == awayMlbTeam.KratkoIme &&
                                                            a.StarterALastName != starterLastName &&
                                                            a.StarterHLastName != oppStarterLastName &&
                                                            validOpps.Contains(a.Team) &&
                                                            a.StarterHHand == teamStarterHand)
                                                            .OrderByDescending(o => o.ID)
                                                            .Take(addgames)
                                                            .Select(r => r)
                                                            .ToList();
            }
            else if (homeAway.Equals("Home"))
            {
                validOpps = _context.Klubovis.Where(t => t.League == "MLB")
                                                    .Select(t => t.KratkoIme)
                                                    .ToList();

                allMlbResult = _context.AllMLBResults.Where(a => a.Season == season1 &&
                                                            a.Team == homeMlbteam.KratkoIme &&
                                                            a.StarterHLastName != starterLastName &&
                                                            a.StarterALastName != oppStarterLastName &&
                                                            validOpps.Contains(a.Team) &&
                                                            a.StarterAHand == teamStarterHand ||
                                                            a.Season == season2 &&
                                                            a.Team == homeMlbteam.KratkoIme &&
                                                            a.StarterHLastName != starterLastName &&
                                                            a.StarterALastName != oppStarterLastName &&
                                                            validOpps.Contains(a.Team) &&
                                                            a.StarterAHand == teamStarterHand)
                                                            .OrderByDescending(o => o.ID)
                                                            .Take(addgames)
                                                            .Select(r => r)
                                                            .ToList();
            }
            InsertUpdateMlb(allMlbResult, ido);
        }
        /// <summary>
        /// InsertUpdateMlb
        /// </summary>
        /// <param name="allMlbResult"></param>
        /// <param name="ido"></param>
        public void InsertUpdateMlb(List<AllMLBResult> allMlbResult, int ido)
        {
            foreach (var allResults in allMlbResult)
            {
                var parkFactor = _context.Klubovis.Where(t => t.KratkoIme == allResults.Team &&
                                                              t.League == "MLB")
                                                  .Select(p => p.ParkFactor)
                                                  .First();

                MLBResult mlbResult = new MLBResult()
                {
                    ImeHKluba = allResults.Team,
                    HomeStarter = allResults.StarterHLastName,
                    HomeStarterHand = allResults.StarterHHand,
                    ImeAKluba = allResults.Opp,
                    AwayStarter = allResults.StarterALastName,
                    AwayStarterHand = allResults.StarterAHand,
                    HResult = allResults.Hscore,
                    AResult = allResults.Ascore,
                    TotalResult = allResults.Hscore + allResults.Ascore,
                    HandicapResult = allResults.Ascore - allResults.Hscore,
                    IDOklade = ido,
                    ParkFactor = parkFactor,
                    Line = Convert.ToInt32(allResults.Line),
                    Total = Convert.ToDecimal(allResults.Total),
                };
                _context.MLBResults.Add(mlbResult);
                _context.SaveChanges();
            }
        }
        /// <summary>
        /// InsertNbaBets
        /// </summary>
        /// <param name="idh"></param>
        /// <param name="ida"></param>
        /// <param name="ido"></param>
        /// <param name="homeTeamValue"></param>
        /// <param name="awayTeamValue"></param>
        /// <param name="totalBox"></param>
        /// <param name="handicapBox"></param>
        /// <param name="isOver"></param>
        /// <param name="isUnder"></param>
        /// <param name="hHome"></param>
        /// <param name="hAway"></param>
        /// <param name="timenoww"></param>
        public void InsertNbaBets(int idh, int ida, int ido, string homeTeamValue, string awayTeamValue, TextBox totalBox, TextBox handicapBox,
                                   int isOver, int isUnder, int hHome, int hAway, string timenoww)
        {
            decimal total = Convert.ToDecimal(totalBox.Text.ToString());
            decimal line = Convert.ToDecimal(handicapBox.Text.ToString());

            NBABET nbaBet = new NBABET
            {
                IDHKluba = idh,
                IDAKluba = ida,
                IDOklade = ido,
                ImeHKluba = homeTeamValue,
                ImeAKluba = awayTeamValue,
                Total = total,
                Handicap = line,
                IsOver = isOver,
                IsUnder = isUnder,
                HomeHendi = hHome,
                AwayHendi = hAway,
                Datum = timenoww,
                ToBetOn = 0,
                BetProcessed = 0,
                Edited = 0
            };
            _context.NBABETS.Add(nbaBet);
            _context.SaveChanges();
        }

        /// <summary>
        /// InsertMlbBets
        /// </summary>
        /// <param name="idh"></param>
        /// <param name="homeStarter"></param>
        /// <param name="ida"></param>
        /// <param name="awayStarter"></param>
        /// <param name="homeTeamValue"></param>
        /// <param name="awayTeamValue"></param>
        /// <param name="totalBox"></param>
        /// <param name="handicapBox"></param>
        /// <param name="isOver"></param>
        /// <param name="isUnder"></param>
        /// <param name="hHome"></param>
        /// <param name="hAway"></param>
        /// <param name="timenoww"></param>
        /// <param name="parkFactor"></param>
        public void InsertMlbBets(int idh, string homeStarter, int ida, string awayStarter, int ido, string homeTeamValue, string awayTeamValue, TextBox totalBox, TextBox handicapBox,
                                   int isOver, int isUnder, int hHome, int hAway, string timenoww, decimal? parkFactor)
        {
            decimal total = Convert.ToDecimal(totalBox.Text.ToString());
            decimal line = Convert.ToDecimal(handicapBox.Text.ToString());

            MLBBET mlbBet = new MLBBET
            {
                IDHKluba = idh,
                HomeStarter = homeStarter,
                IDAKluba = ida,
                AwayStarter = awayStarter,
                IDOklade = ido,
                ImeHKluba = homeTeamValue,
                ImeAKluba = awayTeamValue,
                Total = total,
                Handicap = line,
                IsOver = isOver,
                IsUnder = isUnder,
                HomeHendi = hHome,
                AwayHendi = hAway,
                Datum = timenoww,
                ParkFactor = parkFactor,
                ToBetOn = 0,
                BetProcessed = 0,
                Edited = 0
            };

            _context.MLBBETS.Add(mlbBet);
            _context.SaveChanges();
        }

        public static T GetDataFromSQL<T>(string sqlQuery, SqlConnection connection, bool mathRound = false, int mathRoundNo = 0)
        {
            object result = null;
            var sqlCommand = new SqlCommand(sqlQuery, connection);
            using (var sqlReader = sqlCommand.ExecuteReader()) { 
                while (sqlReader.Read())
                {
                    result = sqlReader[0];
                }
            }
            if (mathRound)
            {
                result = Math.Round(Convert.ToDouble(result), mathRoundNo);
                return (T) Convert.ChangeType(result, typeof(T));
            }
            else
            {
                return (T)Convert.ChangeType(result, typeof(T));
            }
        }
    }
}

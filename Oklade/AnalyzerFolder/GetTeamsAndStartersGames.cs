﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Oklade.DALEntityFramework;
using System.Threading.Tasks;

namespace Oklade.AnalyzerFolder
{
    class GetTeamsAndStartersGames
    {
        private List<TeamLast50Mlb> _teamLast50Mlb = null;
        private List<TeamLast50Nba> _teamLast50Nba = null;
        private List<StarterLast50Mlb> _starterLast50 = null;
        public void GetGames(RadioButton radioBtnNba, RadioButton radioBtnMlb, Klubovi team, int seasonNbaMlb, ComboBox awayStarterCb, DataGridView teamL50Dgv)
        {
            using (OkladesEntities context = new OkladesEntities())
            {
                if (radioBtnNba.Checked)
                {
                    _teamLast50Nba = context.AllNBAResults.Where(t => t.Team == team.KratkoIme ||
                                                                      t.Opp == team.KratkoIme)
                                                                    .OrderByDescending(t => t.ID)
                                                                    .Select(t => new TeamLast50Nba
                                                                    {
                                                                        Dayofmonth = t.Dayofmonth,
                                                                        Month = t.Month,
                                                                        Team = t.Team,
                                                                        Opp = t.Opp,
                                                                        Hscore = t.Hscore,
                                                                        Ascore = t.Ascore,
                                                                        Line = t.Line,
                                                                        Total = t.Total,
                                                                        SUr = t.SUr,
                                                                        ATSr = t.ATSr,
                                                                        OUr = t.OUr
                                                                    })
                                                                    .Take(50)
                                                                    .ToList();
                    // foreach (var aTL10 in _teamLast50Nba)
                    Parallel.ForEach(_teamLast50Nba, aTL10 =>
                    {
                        if (aTL10.Opp == team.KratkoIme)
                        {
                            if (aTL10.SUr == "W")
                            {
                                aTL10.SUr = "L";
                            }
                            else if (aTL10.SUr == "L")
                            {
                                aTL10.SUr = "W";
                            }

                        }
                        if (aTL10.Opp == team.KratkoIme)
                        {
                            if (aTL10.ATSr == "W")
                            {
                                aTL10.ATSr = "L";
                            }
                            else if (aTL10.ATSr == "L")
                            {
                                aTL10.ATSr = "W";
                            }

                        }
                    });
                    teamL50Dgv.InvokeIfRequired(t =>
                   {
                       teamL50Dgv.DataSource = _teamLast50Nba;
                   });
                }
                else if (radioBtnMlb.Checked)
                {
                    awayStarterCb.InvokeIfRequired(aS =>
                    {
                        awayStarterCb.Enabled = true;
                    });


                    _teamLast50Mlb = context.AllMLBResults.Where(t => t.Team == team.KratkoIme ||
                                                                      t.Opp == team.KratkoIme)
                                                                    .OrderByDescending(t => t.ID)
                                                                    .Select(t => new TeamLast50Mlb
                                                                    {
                                                                        Dayofmonth = t.Dayofmonth,
                                                                        Month = t.Month,
                                                                        Team = t.Team,
                                                                        Opp = t.Opp,
                                                                        Hscore = t.Hscore,
                                                                        Ascore = t.Ascore,
                                                                        Line = t.Line,
                                                                        Total = t.Total,
                                                                        SUr = t.SUr,
                                                                        Hits = t.Hits,
                                                                        OUr = t.OUr,
                                                                        StarterHLastName = t.StarterHLastName,
                                                                        StarterALastName = t.StarterALastName
                                                                    })
                                                                    .Take(50)
                                                                    .ToList();


                    //  foreach (var aTL50 in _teamLast50Mlb)
                    Parallel.ForEach(_teamLast50Mlb, aTL50 =>
                      {
                          if (aTL50.Opp == team.KratkoIme)
                          {
                              if (aTL50.SUr == "W")
                              {
                                  aTL50.SUr = "L";
                              }
                              else if (aTL50.SUr == "L")
                              {
                                  aTL50.SUr = "W";
                              }
                          }
                      });
                    teamL50Dgv.InvokeIfRequired(t =>
                     {
                         teamL50Dgv.DataSource = _teamLast50Mlb;
                     });
                }
            }
        }

        public void GetStartersGames(DataGridView starterL50Dgv, string starterFirstName, string starterLastName)
        {
            using (OkladesEntities context = new OkladesEntities())
            {
                _starterLast50 = context.AllMLBResults.Where(t => t.StarterHFirstName.Substring(0, 1) == starterFirstName.Substring(0, 1) &&
                                                                    t.StarterHFirstName.Contains(starterFirstName.Substring(starterFirstName.Length - 1, starterFirstName.Length)) &&
                                                                     t.StarterHLastName == starterLastName ||
                                                                     t.StarterAFirstName.Substring(0, 1) == starterFirstName.Substring(0, 1) &&
                                                                     t.StarterAFirstName.Contains(starterFirstName.Substring(starterFirstName.Length - 1, starterFirstName.Length)) &&
                                                                     t.StarterALastName == starterLastName)
                                                                   .OrderByDescending(t => t.ID)
                                                                   .Select(t => new StarterLast50Mlb
                                                                   {
                                                                       Dayofmonth = t.Dayofmonth,
                                                                       Month = t.Month,
                                                                       Team = t.Team,
                                                                       StarterHLastName = t.StarterHLastName,
                                                                       Opp = t.Opp,
                                                                       StarterALastName = t.StarterALastName,
                                                                       Hscore = t.Hscore,
                                                                       Ascore = t.Ascore,
                                                                       Line = t.Line,
                                                                       Total = t.Total,
                                                                       SUr = t.SUr,
                                                                       Hits = t.Hits,
                                                                       OUr = t.OUr
                                                                   })
                                                                   .Take(50)
                                                                   .ToList();
                // foreach (var sTL50 in _starterLast50)
                Parallel.ForEach(_starterLast50, sTL50 =>
                   {
                       if (sTL50.StarterALastName == starterLastName)
                       {
                           if (sTL50.SUr == "W")
                           {
                               sTL50.SUr = "L";
                           }
                           else if (sTL50.SUr == "L")
                           {
                               sTL50.SUr = "W";
                           }
                       }
                   });
                starterL50Dgv.InvokeIfRequired(c =>
                    {
                        starterL50Dgv.DataSource = _starterLast50;
                        if(starterL50Dgv.DataSource != null)
                        starterL50Dgv.Rows[0].Cells[0].Selected = false;
                    });
            }
        }

    }
    public class TeamLast50Nba
    {
        public string Dayofmonth { get; set; }
        public string Month { get; set; }
        public string Team { get; set; }
        public string Opp { get; set; }
        public int? Hscore { get; set; }
        public int? Ascore { get; set; }
        public decimal? Line { get; set; }
        public decimal? Total { get; set; }
        public string SUr { get; set; }
        public string ATSr { get; set; }
        public string OUr { get; set; }
    }
    public class TeamLast50Mlb
    {
        public string Dayofmonth { get; set; }
        public string Month { get; set; }
        public string Team { get; set; }
        public string StarterHLastName { get; set; }
        public string Opp { get; set; }
        public string StarterALastName { get; set; }
        public int? Hscore { get; set; }
        public int? Ascore { get; set; }
        public string Line { get; set; }
        public string Total { get; set; }
        public string SUr { get; set; }
        public string OUr { get; set; }
        public string Hits { get; set; }
    }

    public class StarterLast50Mlb
    {
        public string Dayofmonth { get; set; }
        public string Month { get; set; }
        public string Team { get; set; }
        public string StarterHLastName { get; set; }
        public string Opp { get; set; }
        public string StarterALastName { get; set; }
        public int? Hscore { get; set; }
        public int? Ascore { get; set; }
        public string Line { get; set; }
        public string Total { get; set; }
        public string SUr { get; set; }
        public string OUr { get; set; }
        public string Hits { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oklade.Models
{
    public class TeamStats
    {
        public double AvgPoints { get; set; }
        public double AvgPointsVsHand { get; set; }
        public string Overs { get; set; }
        public string Unders { get; set; }
        public string OversVsHand { get; set; }
        public string UndersVsHand { get; set; }
        public string ATSLoss { get; set; }
        public string ATSWin { get; set; }
        public int SULoss { get; set; }
        public int SUWin { get; set; }
        public int SULossVsHand { get; set; }
        public int SUWinVsHand { get; set; }
        public double AvgPointsOpp { get; set; }
        public double AvgPointsVsHandOpp { get; set; }
    }
}

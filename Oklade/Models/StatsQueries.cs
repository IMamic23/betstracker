﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oklade.Models
{
    class StatsQueries
    {
        public string AvgPointsQuery { get; set; }
        public string AvgPointsVsHandQuery { get; set; }
        public string OversQuery { get; set; }
        public string UndersQuery { get; set; }
        public string OversVsHandQuery { get; set; }
        public string UndersVsHandQuery { get; set; }
        public string ATSLossQuery { get; set; }
        public string ATSWinQuery { get; set; }
        public string SULossQuery { get; set; }
        public string SUWinQuery { get; set; }
        public string SULossVsHandQuery { get; set; }
        public string SUWinVsHandQuery { get; set; }
        public string AvgPointsOppQuery { get; set; }
        public string AvgPointsVsHandOppQuery { get; set; }
    }
}

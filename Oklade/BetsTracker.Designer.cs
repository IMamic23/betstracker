﻿namespace Oklade
{
    partial class BetsTracker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BetsTracker));
            this.TeamslistBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LastBetBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ProfitBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AllBetsDGV = new System.Windows.Forms.DataGridView();
            this.NextBetBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.CalcNextBetTB = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.DeleteBtn = new System.Windows.Forms.PictureBox();
            this.TeamLogosPicBox = new System.Windows.Forms.PictureBox();
            this.winBtn = new System.Windows.Forms.PictureBox();
            this.exitBtn = new System.Windows.Forms.PictureBox();
            this.failBtn = new System.Windows.Forms.PictureBox();
            this.updatebtn = new System.Windows.Forms.PictureBox();
            this.UndoBtn = new System.Windows.Forms.PictureBox();
            this.SaveBtn = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.excelBtn = new System.Windows.Forms.PictureBox();
            this.PickTeamsBtn = new System.Windows.Forms.PictureBox();
            this.button9 = new System.Windows.Forms.Button();
            this.leagueLogoPb = new System.Windows.Forms.PictureBox();
            this.AnalyzerBtn = new System.Windows.Forms.PictureBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.versionLabel = new System.Windows.Forms.Label();
            this.MLBradioButton = new System.Windows.Forms.RadioButton();
            this.NBAradioButton = new System.Windows.Forms.RadioButton();
            this.NHLradioButton = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CasinoRB = new System.Windows.Forms.RadioButton();
            this.BestBetsRB = new System.Windows.Forms.RadioButton();
            this.OfferBox = new System.Windows.Forms.ComboBox();
            this.ResultsUpdBtn = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pushBtn = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.BetValuesBtn = new System.Windows.Forms.PictureBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.OddsBox = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.previousDayGames = new System.Windows.Forms.PictureBox();
            this.nextDayGames = new System.Windows.Forms.PictureBox();
            this.nextDayResults = new System.Windows.Forms.PictureBox();
            this.previousDayResults = new System.Windows.Forms.PictureBox();
            this.matchupsAndScoresDGV = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.GameBox = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.isDefault = new System.Windows.Forms.CheckBox();
            this.RefreshScores = new System.Windows.Forms.Timer(this.components);
            this.resultsDates = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.AllBetsDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeleteBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamLogosPicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.winBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatebtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UndoBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaveBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PickTeamsBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leagueLogoPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnalyzerBtn)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResultsUpdBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pushBtn)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BetValuesBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.previousDayGames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextDayGames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextDayResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.previousDayResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchupsAndScoresDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // TeamslistBox1
            // 
            this.TeamslistBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TeamslistBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.TeamslistBox1.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeamslistBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.TeamslistBox1.FormattingEnabled = true;
            this.TeamslistBox1.ItemHeight = 19;
            this.TeamslistBox1.Location = new System.Drawing.Point(85, 195);
            this.TeamslistBox1.Name = "TeamslistBox1";
            this.TeamslistBox1.Size = new System.Drawing.Size(182, 289);
            this.TeamslistBox1.TabIndex = 0;
            this.TeamslistBox1.SelectedValueChanged += new System.EventHandler(this.listBox1_SelectedValueChanged);
            this.TeamslistBox1.MouseHover += new System.EventHandler(this.listBox1_MouseHover);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 195);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pick a team:";
            // 
            // LastBetBox
            // 
            this.LastBetBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LastBetBox.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastBetBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LastBetBox.Location = new System.Drawing.Point(273, 202);
            this.LastBetBox.Name = "LastBetBox";
            this.LastBetBox.ReadOnly = true;
            this.LastBetBox.Size = new System.Drawing.Size(125, 26);
            this.LastBetBox.TabIndex = 1;
            this.LastBetBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(280, 179);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Current/Last bet";
            // 
            // ProfitBox
            // 
            this.ProfitBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ProfitBox.Cursor = System.Windows.Forms.Cursors.No;
            this.ProfitBox.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProfitBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ProfitBox.Location = new System.Drawing.Point(603, 603);
            this.ProfitBox.Name = "ProfitBox";
            this.ProfitBox.ReadOnly = true;
            this.ProfitBox.Size = new System.Drawing.Size(66, 26);
            this.ProfitBox.TabIndex = 4;
            this.ProfitBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ProfitBox.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(303, 225);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Next bet";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(613, 580);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Profit";
            this.label4.Visible = false;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(309, 271);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "Odds";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // AllBetsDGV
            // 
            this.AllBetsDGV.AllowUserToAddRows = false;
            this.AllBetsDGV.AllowUserToDeleteRows = false;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.AllBetsDGV.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle31;
            this.AllBetsDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.AllBetsDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.AllBetsDGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AllBetsDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.AllBetsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Arial Narrow", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.AllBetsDGV.DefaultCellStyle = dataGridViewCellStyle33;
            this.AllBetsDGV.EnableHeadersVisualStyles = false;
            this.AllBetsDGV.Location = new System.Drawing.Point(473, 197);
            this.AllBetsDGV.Name = "AllBetsDGV";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AllBetsDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.AllBetsDGV.RowHeadersVisible = false;
            this.AllBetsDGV.RowHeadersWidth = 5;
            this.AllBetsDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.AllBetsDGV.RowsDefaultCellStyle = dataGridViewCellStyle35;
            this.AllBetsDGV.Size = new System.Drawing.Size(953, 379);
            this.AllBetsDGV.TabIndex = 15;
            this.AllBetsDGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.AllBetsDGV.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentDoubleClick);
            this.AllBetsDGV.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // NextBetBox
            // 
            this.NextBetBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.NextBetBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.NextBetBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.NextBetBox.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NextBetBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.NextBetBox.FormattingEnabled = true;
            this.NextBetBox.Location = new System.Drawing.Point(271, 246);
            this.NextBetBox.Name = "NextBetBox";
            this.NextBetBox.Size = new System.Drawing.Size(128, 27);
            this.NextBetBox.TabIndex = 2;
            this.NextBetBox.SelectedValueChanged += new System.EventHandler(this.NextBetBox_SelectedValueChanged);
            this.NextBetBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NextBetBox_KeyDown);
            this.NextBetBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NextBetBox_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(2, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 12);
            this.label10.TabIndex = 26;
            this.label10.Text = "Total Bet";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(2, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 12);
            this.label11.TabIndex = 27;
            this.label11.Text = "Value 1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(2, 82);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 12);
            this.label12.TabIndex = 28;
            this.label12.Text = "Value 2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(-1, 126);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 17);
            this.label13.TabIndex = 29;
            this.label13.Text = "Result";
            // 
            // CalcNextBetTB
            // 
            this.CalcNextBetTB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CalcNextBetTB.Font = new System.Drawing.Font("Rockwell", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalcNextBetTB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.CalcNextBetTB.Location = new System.Drawing.Point(4, 25);
            this.CalcNextBetTB.Name = "CalcNextBetTB";
            this.CalcNextBetTB.Size = new System.Drawing.Size(40, 25);
            this.CalcNextBetTB.TabIndex = 30;
            this.CalcNextBetTB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox5_MouseClick);
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBox7.Font = new System.Drawing.Font("Rockwell", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.textBox7.Location = new System.Drawing.Point(4, 96);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(40, 25);
            this.textBox7.TabIndex = 31;
            this.textBox7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox7_MouseClick);
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBox8.Font = new System.Drawing.Font("Rockwell", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.textBox8.Location = new System.Drawing.Point(4, 60);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(40, 25);
            this.textBox8.TabIndex = 31;
            this.textBox8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox8_MouseClick);
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBox9.Font = new System.Drawing.Font("Rockwell", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.textBox9.Location = new System.Drawing.Point(48, 122);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(47, 25);
            this.textBox9.TabIndex = 32;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarMonthBackground = System.Drawing.Color.LemonChiffon;
            this.dateTimePicker1.Checked = false;
            this.dateTimePicker1.CustomFormat = "DD.MM.YY.";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(472, 174);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(101, 20);
            this.dateTimePicker1.TabIndex = 34;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(368, 229);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 35;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // comboBox2
            // 
            this.comboBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboBox2.Font = new System.Drawing.Font("Rockwell", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "2",
            "1,63",
            "1,5",
            "1,44",
            "1,42",
            "1,40",
            "1,37",
            "1,34",
            "1,30",
            "1,27",
            "1,24",
            "1,22",
            "1,19",
            "1,17",
            "1,15",
            "1,15",
            "1,14",
            "1,14"});
            this.comboBox2.Location = new System.Drawing.Point(90, 96);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(35, 25);
            this.comboBox2.TabIndex = 36;
            this.comboBox2.SelectedValueChanged += new System.EventHandler(this.comboBox2_SelectedValueChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(69, 0);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 38;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.BackColor = System.Drawing.Color.Transparent;
            this.radioButton1.ForeColor = System.Drawing.Color.Black;
            this.radioButton1.Location = new System.Drawing.Point(3, 12);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(60, 17);
            this.radioButton1.TabIndex = 39;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Pinn5D";
            this.radioButton1.UseVisualStyleBackColor = false;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            this.radioButton1.MouseHover += new System.EventHandler(this.radioButton1_MouseHover);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.BackColor = System.Drawing.Color.Transparent;
            this.radioButton2.ForeColor = System.Drawing.Color.Black;
            this.radioButton2.Location = new System.Drawing.Point(3, 30);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(59, 17);
            this.radioButton2.TabIndex = 40;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Bet365";
            this.radioButton2.UseVisualStyleBackColor = false;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            this.radioButton2.MouseHover += new System.EventHandler(this.radioButton2_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = global::Oklade.Properties.Resources.profit_icon;
            this.pictureBox4.Location = new System.Drawing.Point(85, 490);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(55, 55);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 48;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.BackColor = System.Drawing.Color.Transparent;
            this.DeleteBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DeleteBtn.Image = global::Oklade.Properties.Resources.Trash_empty;
            this.DeleteBtn.Location = new System.Drawing.Point(1302, 582);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(52, 52);
            this.DeleteBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.DeleteBtn.TabIndex = 49;
            this.DeleteBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.DeleteBtn, "Delete last bet");
            this.DeleteBtn.Click += new System.EventHandler(this.pictureBox5_Click);
            this.DeleteBtn.MouseLeave += new System.EventHandler(this.pictureBox5_MouseLeave);
            this.DeleteBtn.MouseHover += new System.EventHandler(this.pictureBox5_MouseHover);
            // 
            // TeamLogosPicBox
            // 
            this.TeamLogosPicBox.BackColor = System.Drawing.Color.Transparent;
            this.TeamLogosPicBox.Location = new System.Drawing.Point(435, 35);
            this.TeamLogosPicBox.Name = "TeamLogosPicBox";
            this.TeamLogosPicBox.Size = new System.Drawing.Size(196, 134);
            this.TeamLogosPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.TeamLogosPicBox.TabIndex = 42;
            this.TeamLogosPicBox.TabStop = false;
            // 
            // winBtn
            // 
            this.winBtn.BackColor = System.Drawing.Color.Transparent;
            this.winBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.winBtn.Image = global::Oklade.Properties.Resources.Actions_dialog_ok_apply_icon;
            this.winBtn.Location = new System.Drawing.Point(407, 255);
            this.winBtn.Name = "winBtn";
            this.winBtn.Size = new System.Drawing.Size(60, 60);
            this.winBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.winBtn.TabIndex = 55;
            this.winBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.winBtn, "Grade Bet \'WIN\'");
            this.winBtn.Click += new System.EventHandler(this.winbtn_Click);
            this.winBtn.MouseLeave += new System.EventHandler(this.pictureBox11_MouseLeave);
            this.winBtn.MouseHover += new System.EventHandler(this.pictureBox11_MouseHover);
            // 
            // exitBtn
            // 
            this.exitBtn.BackColor = System.Drawing.Color.Transparent;
            this.exitBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBtn.Image = global::Oklade.Properties.Resources._1436446147_exit;
            this.exitBtn.Location = new System.Drawing.Point(1366, 584);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(56, 56);
            this.exitBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitBtn.TabIndex = 47;
            this.exitBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.exitBtn, "Exit");
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            this.exitBtn.MouseLeave += new System.EventHandler(this.exitBtn_MouseLeave);
            this.exitBtn.MouseHover += new System.EventHandler(this.exitBtn_MouseHover);
            // 
            // failBtn
            // 
            this.failBtn.BackColor = System.Drawing.Color.Transparent;
            this.failBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.failBtn.Image = global::Oklade.Properties.Resources.Actions_edit_delete_icon;
            this.failBtn.Location = new System.Drawing.Point(407, 314);
            this.failBtn.Name = "failBtn";
            this.failBtn.Size = new System.Drawing.Size(60, 60);
            this.failBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.failBtn.TabIndex = 56;
            this.failBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.failBtn, "Grade Bet \'LOSS\'");
            this.failBtn.Click += new System.EventHandler(this.failBtn_Click);
            this.failBtn.MouseLeave += new System.EventHandler(this.failBtn_MouseLeave);
            this.failBtn.MouseHover += new System.EventHandler(this.pictureBox12_MouseHover);
            // 
            // updatebtn
            // 
            this.updatebtn.BackColor = System.Drawing.Color.Transparent;
            this.updatebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updatebtn.Image = global::Oklade.Properties.Resources.worm_gear_icon;
            this.updatebtn.Location = new System.Drawing.Point(406, 193);
            this.updatebtn.Name = "updatebtn";
            this.updatebtn.Size = new System.Drawing.Size(62, 62);
            this.updatebtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.updatebtn.TabIndex = 53;
            this.updatebtn.TabStop = false;
            this.toolTip1.SetToolTip(this.updatebtn, "Process new bet");
            this.updatebtn.Click += new System.EventHandler(this.updatebtn_Click);
            this.updatebtn.MouseLeave += new System.EventHandler(this.updatebtn_MouseLeave);
            this.updatebtn.MouseHover += new System.EventHandler(this.pictureBox9_MouseHover);
            // 
            // UndoBtn
            // 
            this.UndoBtn.BackColor = System.Drawing.Color.Transparent;
            this.UndoBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UndoBtn.Image = global::Oklade.Properties.Resources.Actions_blue_arrow_undo_icon;
            this.UndoBtn.Location = new System.Drawing.Point(909, 582);
            this.UndoBtn.Name = "UndoBtn";
            this.UndoBtn.Size = new System.Drawing.Size(52, 52);
            this.UndoBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.UndoBtn.TabIndex = 50;
            this.UndoBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.UndoBtn, "Undo last bet status");
            this.UndoBtn.Click += new System.EventHandler(this.pictureBox6_Click);
            this.UndoBtn.MouseLeave += new System.EventHandler(this.pictureBox6_MouseLeave);
            this.UndoBtn.MouseHover += new System.EventHandler(this.pictureBox6_MouseHover);
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.Transparent;
            this.SaveBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SaveBtn.Image = global::Oklade.Properties.Resources.Actions_document_save_icon;
            this.SaveBtn.Location = new System.Drawing.Point(849, 582);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(52, 52);
            this.SaveBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SaveBtn.TabIndex = 51;
            this.SaveBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.SaveBtn, "Save changes");
            this.SaveBtn.Click += new System.EventHandler(this.pictureBox7_Click);
            this.SaveBtn.MouseLeave += new System.EventHandler(this.SaveBtn_MouseLeave);
            this.SaveBtn.MouseHover += new System.EventHandler(this.SaveBtn_MouseHover);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox8.Image = global::Oklade.Properties.Resources.calculator4;
            this.pictureBox8.Location = new System.Drawing.Point(132, 95);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(56, 56);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox8.TabIndex = 52;
            this.pictureBox8.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox8, "Calculate");
            this.pictureBox8.Click += new System.EventHandler(this.pictureBox8_Click);
            this.pictureBox8.MouseLeave += new System.EventHandler(this.pictureBox8_MouseLeave);
            this.pictureBox8.MouseHover += new System.EventHandler(this.pictureBox8_MouseHover);
            // 
            // excelBtn
            // 
            this.excelBtn.BackColor = System.Drawing.Color.Transparent;
            this.excelBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.excelBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.excelBtn.Image = global::Oklade.Properties.Resources.excel_xls_icon;
            this.excelBtn.Location = new System.Drawing.Point(1238, 582);
            this.excelBtn.Name = "excelBtn";
            this.excelBtn.Size = new System.Drawing.Size(60, 60);
            this.excelBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.excelBtn.TabIndex = 57;
            this.excelBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.excelBtn, "Export to Excel");
            this.excelBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.excelBtn_MouseClick);
            this.excelBtn.MouseLeave += new System.EventHandler(this.excelBtn_MouseLeave);
            this.excelBtn.MouseHover += new System.EventHandler(this.excelBtn_MouseHover);
            // 
            // PickTeamsBtn
            // 
            this.PickTeamsBtn.BackColor = System.Drawing.Color.Transparent;
            this.PickTeamsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PickTeamsBtn.Image = global::Oklade.Properties.Resources.document_checkbox_icon;
            this.PickTeamsBtn.Location = new System.Drawing.Point(787, 581);
            this.PickTeamsBtn.Name = "PickTeamsBtn";
            this.PickTeamsBtn.Size = new System.Drawing.Size(52, 52);
            this.PickTeamsBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PickTeamsBtn.TabIndex = 60;
            this.PickTeamsBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.PickTeamsBtn, "Edit Teams");
            this.PickTeamsBtn.Click += new System.EventHandler(this.pictureBox10_Click);
            this.PickTeamsBtn.MouseLeave += new System.EventHandler(this.pictureBox10_MouseLeave_1);
            this.PickTeamsBtn.MouseHover += new System.EventHandler(this.pictureBox10_MouseHover_1);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(796, 633);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(40, 23);
            this.button9.TabIndex = 61;
            this.button9.Text = "Save";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // leagueLogoPb
            // 
            this.leagueLogoPb.BackColor = System.Drawing.Color.Transparent;
            this.leagueLogoPb.Image = global::Oklade.Properties.Resources.nba11;
            this.leagueLogoPb.Location = new System.Drawing.Point(142, 9);
            this.leagueLogoPb.Name = "leagueLogoPb";
            this.leagueLogoPb.Size = new System.Drawing.Size(260, 151);
            this.leagueLogoPb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.leagueLogoPb.TabIndex = 44;
            this.leagueLogoPb.TabStop = false;
            // 
            // AnalyzerBtn
            // 
            this.AnalyzerBtn.BackColor = System.Drawing.Color.Transparent;
            this.AnalyzerBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.AnalyzerBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AnalyzerBtn.Image = global::Oklade.Properties.Resources.chart_search_icon;
            this.AnalyzerBtn.Location = new System.Drawing.Point(472, 583);
            this.AnalyzerBtn.Name = "AnalyzerBtn";
            this.AnalyzerBtn.Size = new System.Drawing.Size(62, 62);
            this.AnalyzerBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AnalyzerBtn.TabIndex = 63;
            this.AnalyzerBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.AnalyzerBtn, "Analyzer");
            this.AnalyzerBtn.Click += new System.EventHandler(this.pictureBox2_Click);
            this.AnalyzerBtn.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            this.AnalyzerBtn.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBox10.Font = new System.Drawing.Font("Rockwell", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.textBox10.Location = new System.Drawing.Point(47, 60);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(40, 25);
            this.textBox10.TabIndex = 64;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBox11.Font = new System.Drawing.Font("Rockwell", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.textBox11.Location = new System.Drawing.Point(47, 96);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(40, 25);
            this.textBox11.TabIndex = 65;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(309, 366);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 20);
            this.label14.TabIndex = 66;
            this.label14.Text = "Offer";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.BackColor = System.Drawing.Color.Transparent;
            this.radioButton3.ForeColor = System.Drawing.Color.Black;
            this.radioButton3.Location = new System.Drawing.Point(3, 47);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(42, 17);
            this.radioButton3.TabIndex = 68;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Hrv";
            this.radioButton3.UseVisualStyleBackColor = false;
            this.radioButton3.Visible = false;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged_1);
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.BackColor = System.Drawing.Color.Transparent;
            this.versionLabel.Font = new System.Drawing.Font("Arial Narrow", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.versionLabel.ForeColor = System.Drawing.Color.Black;
            this.versionLabel.Location = new System.Drawing.Point(8, 628);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(0, 13);
            this.versionLabel.TabIndex = 69;
            // 
            // MLBradioButton
            // 
            this.MLBradioButton.AutoSize = true;
            this.MLBradioButton.BackColor = System.Drawing.Color.Transparent;
            this.MLBradioButton.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MLBradioButton.ForeColor = System.Drawing.Color.Black;
            this.MLBradioButton.Location = new System.Drawing.Point(3, 3);
            this.MLBradioButton.Name = "MLBradioButton";
            this.MLBradioButton.Size = new System.Drawing.Size(55, 24);
            this.MLBradioButton.TabIndex = 70;
            this.MLBradioButton.TabStop = true;
            this.MLBradioButton.Text = "MLB";
            this.MLBradioButton.UseVisualStyleBackColor = false;
            this.MLBradioButton.CheckedChanged += new System.EventHandler(this.MLBRadioButton_CheckedChanged);
            // 
            // NBAradioButton
            // 
            this.NBAradioButton.AutoSize = true;
            this.NBAradioButton.BackColor = System.Drawing.Color.Transparent;
            this.NBAradioButton.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NBAradioButton.ForeColor = System.Drawing.Color.Black;
            this.NBAradioButton.Location = new System.Drawing.Point(3, 26);
            this.NBAradioButton.Name = "NBAradioButton";
            this.NBAradioButton.Size = new System.Drawing.Size(54, 24);
            this.NBAradioButton.TabIndex = 71;
            this.NBAradioButton.TabStop = true;
            this.NBAradioButton.Text = "NBA";
            this.NBAradioButton.UseVisualStyleBackColor = false;
            this.NBAradioButton.CheckedChanged += new System.EventHandler(this.NBAradioButton_CheckedChanged);
            // 
            // NHLradioButton
            // 
            this.NHLradioButton.AutoSize = true;
            this.NHLradioButton.BackColor = System.Drawing.Color.Transparent;
            this.NHLradioButton.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NHLradioButton.ForeColor = System.Drawing.Color.Black;
            this.NHLradioButton.Location = new System.Drawing.Point(3, 50);
            this.NHLradioButton.Name = "NHLradioButton";
            this.NHLradioButton.Size = new System.Drawing.Size(53, 24);
            this.NHLradioButton.TabIndex = 72;
            this.NHLradioButton.TabStop = true;
            this.NHLradioButton.Text = "NHL";
            this.NHLradioButton.UseVisualStyleBackColor = false;
            this.NHLradioButton.CheckedChanged += new System.EventHandler(this.NHLradioButton_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.radioButton2);
            this.panel1.Controls.Add(this.radioButton3);
            this.panel1.Location = new System.Drawing.Point(16, 416);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(70, 67);
            this.panel1.TabIndex = 75;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.CasinoRB);
            this.panel2.Controls.Add(this.BestBetsRB);
            this.panel2.Controls.Add(this.MLBradioButton);
            this.panel2.Controls.Add(this.NBAradioButton);
            this.panel2.Controls.Add(this.NHLradioButton);
            this.panel2.Location = new System.Drawing.Point(1, 232);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(85, 133);
            this.panel2.TabIndex = 76;
            // 
            // CasinoRB
            // 
            this.CasinoRB.AutoSize = true;
            this.CasinoRB.BackColor = System.Drawing.Color.Transparent;
            this.CasinoRB.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CasinoRB.ForeColor = System.Drawing.Color.Black;
            this.CasinoRB.Location = new System.Drawing.Point(3, 98);
            this.CasinoRB.Name = "CasinoRB";
            this.CasinoRB.Size = new System.Drawing.Size(70, 24);
            this.CasinoRB.TabIndex = 74;
            this.CasinoRB.TabStop = true;
            this.CasinoRB.Text = "Casino";
            this.CasinoRB.UseVisualStyleBackColor = false;
            this.CasinoRB.CheckedChanged += new System.EventHandler(this.CasinoRB_CheckedChanged);
            // 
            // BestBetsRB
            // 
            this.BestBetsRB.AutoSize = true;
            this.BestBetsRB.BackColor = System.Drawing.Color.Transparent;
            this.BestBetsRB.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BestBetsRB.ForeColor = System.Drawing.Color.Black;
            this.BestBetsRB.Location = new System.Drawing.Point(3, 73);
            this.BestBetsRB.Name = "BestBetsRB";
            this.BestBetsRB.Size = new System.Drawing.Size(81, 24);
            this.BestBetsRB.TabIndex = 73;
            this.BestBetsRB.TabStop = true;
            this.BestBetsRB.Text = "BestBets";
            this.BestBetsRB.UseVisualStyleBackColor = false;
            this.BestBetsRB.CheckedChanged += new System.EventHandler(this.BestBetsRB_CheckedChanged);
            // 
            // OfferBox
            // 
            this.OfferBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.OfferBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.OfferBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.OfferBox.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OfferBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.OfferBox.FormattingEnabled = true;
            this.OfferBox.Location = new System.Drawing.Point(271, 386);
            this.OfferBox.Name = "OfferBox";
            this.OfferBox.Size = new System.Drawing.Size(128, 27);
            this.OfferBox.TabIndex = 5;
            this.OfferBox.SelectedValueChanged += new System.EventHandler(this.comboBox3_SelectedValueChanged);
            this.OfferBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OfferBox_KeyDown);
            // 
            // ResultsUpdBtn
            // 
            this.ResultsUpdBtn.BackColor = System.Drawing.Color.Transparent;
            this.ResultsUpdBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ResultsUpdBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ResultsUpdBtn.Image = global::Oklade.Properties.Resources.scoreboard_icon;
            this.ResultsUpdBtn.Location = new System.Drawing.Point(535, 583);
            this.ResultsUpdBtn.Name = "ResultsUpdBtn";
            this.ResultsUpdBtn.Size = new System.Drawing.Size(63, 63);
            this.ResultsUpdBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ResultsUpdBtn.TabIndex = 78;
            this.ResultsUpdBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.ResultsUpdBtn, "Results viewer");
            this.ResultsUpdBtn.Click += new System.EventHandler(this.pictureBox11_Click);
            this.ResultsUpdBtn.MouseLeave += new System.EventHandler(this.pictureBox11_MouseLeave_1);
            this.ResultsUpdBtn.MouseHover += new System.EventHandler(this.pictureBox11_MouseHover_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Cambria", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(144, 518);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 22);
            this.label7.TabIndex = 79;
            // 
            // pushBtn
            // 
            this.pushBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pushBtn.BackColor = System.Drawing.Color.Transparent;
            this.pushBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pushBtn.Image = global::Oklade.Properties.Resources._1436445520_agt_reload;
            this.pushBtn.Location = new System.Drawing.Point(412, 380);
            this.pushBtn.Name = "pushBtn";
            this.pushBtn.Size = new System.Drawing.Size(52, 52);
            this.pushBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pushBtn.TabIndex = 54;
            this.pushBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.pushBtn, "Grade Bet \'PUSH\'");
            this.pushBtn.Click += new System.EventHandler(this.pushBtn_Click);
            this.pushBtn.MouseLeave += new System.EventHandler(this.pictureBox10_MouseLeave);
            this.pushBtn.MouseHover += new System.EventHandler(this.pictureBox10_MouseHover);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.comboBox2);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBox10);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.textBox9);
            this.groupBox1.Controls.Add(this.textBox11);
            this.groupBox1.Controls.Add(this.CalcNextBetTB);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.pictureBox8);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(271, 438);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(197, 150);
            this.groupBox1.TabIndex = 81;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Calcuator";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.Location = new System.Drawing.Point(151, 51);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(26, 15);
            this.label20.TabIndex = 71;
            this.label20.Text = "+/-";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.Location = new System.Drawing.Point(145, 69);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(25, 14);
            this.label19.TabIndex = 70;
            this.label19.Text = "l19";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.Location = new System.Drawing.Point(168, 34);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(27, 15);
            this.label18.TabIndex = 69;
            this.label18.Text = "l18";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.Location = new System.Drawing.Point(168, 12);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 15);
            this.label17.TabIndex = 68;
            this.label17.Text = "l17";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(142, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 15);
            this.label16.TabIndex = 67;
            this.label16.Text = "L";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(142, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 15);
            this.label9.TabIndex = 66;
            this.label9.Text = "W";
            // 
            // BetValuesBtn
            // 
            this.BetValuesBtn.BackColor = System.Drawing.Color.Transparent;
            this.BetValuesBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BetValuesBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BetValuesBtn.Image = global::Oklade.Properties.Resources.Categories_preferences_system_icon;
            this.BetValuesBtn.Location = new System.Drawing.Point(725, 582);
            this.BetValuesBtn.Name = "BetValuesBtn";
            this.BetValuesBtn.Size = new System.Drawing.Size(52, 52);
            this.BetValuesBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BetValuesBtn.TabIndex = 83;
            this.BetValuesBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.BetValuesBtn, "Edit bet amounts");
            this.BetValuesBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox13_MouseClick);
            this.BetValuesBtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox13_MouseDown);
            this.BetValuesBtn.MouseLeave += new System.EventHandler(this.pictureBox13_MouseLeave);
            this.BetValuesBtn.MouseHover += new System.EventHandler(this.pictureBox13_MouseHover);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(22, 14);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 12);
            this.label21.TabIndex = 84;
            this.label21.Text = "Over Wins:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(11, 27);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(77, 12);
            this.label22.TabIndex = 85;
            this.label22.Text = "Over Losses:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(4, 62);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(84, 12);
            this.label23.TabIndex = 87;
            this.label23.Text = "Under Losses:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(15, 49);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(71, 12);
            this.label24.TabIndex = 86;
            this.label24.Text = "Under Wins:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(4, 96);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(82, 12);
            this.label25.TabIndex = 89;
            this.label25.Text = "Hendi Losses:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(15, 83);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(69, 12);
            this.label26.TabIndex = 88;
            this.label26.Text = "Hendi Wins:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(88, 11);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(28, 15);
            this.label27.TabIndex = 90;
            this.label27.Text = "L27";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(88, 25);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(28, 15);
            this.label28.TabIndex = 91;
            this.label28.Text = "L28";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(88, 61);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(28, 15);
            this.label29.TabIndex = 93;
            this.label29.Text = "L29";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(88, 47);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(28, 15);
            this.label30.TabIndex = 92;
            this.label30.Text = "L30";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(88, 94);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(28, 15);
            this.label31.TabIndex = 95;
            this.label31.Text = "L31";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(88, 80);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(28, 15);
            this.label32.TabIndex = 94;
            this.label32.Text = "L32";
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox12.Image = global::Oklade.Properties.Resources.Actions_office_chart_pie_icon;
            this.pictureBox12.Location = new System.Drawing.Point(85, 136);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(55, 55);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 96;
            this.pictureBox12.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox12, "Statistics");
            this.pictureBox12.Click += new System.EventHandler(this.pictureBox12_Click);
            this.pictureBox12.MouseLeave += new System.EventHandler(this.pictureBox12_MouseLeave);
            this.pictureBox12.MouseHover += new System.EventHandler(this.pictureBox12_MouseHover_1);
            // 
            // OddsBox
            // 
            this.OddsBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.OddsBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.OddsBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.OddsBox.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OddsBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.OddsBox.FormattingEnabled = true;
            this.OddsBox.Location = new System.Drawing.Point(271, 289);
            this.OddsBox.Name = "OddsBox";
            this.OddsBox.Size = new System.Drawing.Size(127, 27);
            this.OddsBox.TabIndex = 3;
            this.OddsBox.SelectedValueChanged += new System.EventHandler(this.comboBox4_SelectedValueChanged);
            this.OddsBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OddsBox_KeyDown);
            this.OddsBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OddsBox_KeyPress);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 1600;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            this.toolTip1.ShowAlways = true;
            // 
            // previousDayGames
            // 
            this.previousDayGames.BackColor = System.Drawing.Color.Transparent;
            this.previousDayGames.Image = global::Oklade.Properties.Resources.back_arrow1;
            this.previousDayGames.Location = new System.Drawing.Point(575, 169);
            this.previousDayGames.Name = "previousDayGames";
            this.previousDayGames.Size = new System.Drawing.Size(29, 28);
            this.previousDayGames.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.previousDayGames.TabIndex = 105;
            this.previousDayGames.TabStop = false;
            this.toolTip1.SetToolTip(this.previousDayGames, "Previous day");
            this.previousDayGames.Click += new System.EventHandler(this.pictureBox14_Click);
            this.previousDayGames.MouseLeave += new System.EventHandler(this.pictureBox14_MouseLeave);
            this.previousDayGames.MouseHover += new System.EventHandler(this.pictureBox14_MouseHover);
            // 
            // nextDayGames
            // 
            this.nextDayGames.BackColor = System.Drawing.Color.Transparent;
            this.nextDayGames.Image = global::Oklade.Properties.Resources.arrow_right1;
            this.nextDayGames.Location = new System.Drawing.Point(605, 169);
            this.nextDayGames.Name = "nextDayGames";
            this.nextDayGames.Size = new System.Drawing.Size(29, 28);
            this.nextDayGames.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.nextDayGames.TabIndex = 104;
            this.nextDayGames.TabStop = false;
            this.toolTip1.SetToolTip(this.nextDayGames, "Next day");
            this.nextDayGames.Click += new System.EventHandler(this.pictureBox15_Click);
            this.nextDayGames.MouseLeave += new System.EventHandler(this.pictureBox15_MouseLeave);
            this.nextDayGames.MouseHover += new System.EventHandler(this.pictureBox15_MouseHover);
            // 
            // nextDayResults
            // 
            this.nextDayResults.BackColor = System.Drawing.Color.Transparent;
            this.nextDayResults.Image = global::Oklade.Properties.Resources.arrow_right1;
            this.nextDayResults.Location = new System.Drawing.Point(644, 5);
            this.nextDayResults.Name = "nextDayResults";
            this.nextDayResults.Size = new System.Drawing.Size(29, 28);
            this.nextDayResults.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.nextDayResults.TabIndex = 118;
            this.nextDayResults.TabStop = false;
            this.toolTip1.SetToolTip(this.nextDayResults, "Next day");
            this.nextDayResults.Click += new System.EventHandler(this.nextDayResults_Click);
            this.nextDayResults.MouseLeave += new System.EventHandler(this.nextDayResults_MouseLeave);
            this.nextDayResults.MouseHover += new System.EventHandler(this.nextDayResults_MouseHover);
            // 
            // previousDayResults
            // 
            this.previousDayResults.BackColor = System.Drawing.Color.Transparent;
            this.previousDayResults.Image = global::Oklade.Properties.Resources.back_arrow1;
            this.previousDayResults.Location = new System.Drawing.Point(612, 5);
            this.previousDayResults.Name = "previousDayResults";
            this.previousDayResults.Size = new System.Drawing.Size(29, 28);
            this.previousDayResults.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.previousDayResults.TabIndex = 119;
            this.previousDayResults.TabStop = false;
            this.toolTip1.SetToolTip(this.previousDayResults, "Previous day");
            this.previousDayResults.Click += new System.EventHandler(this.previousDayResults_Click);
            this.previousDayResults.MouseLeave += new System.EventHandler(this.previousDayResults_MouseLeave);
            this.previousDayResults.MouseHover += new System.EventHandler(this.previousDayResults_MouseHover);
            // 
            // matchupsAndScoresDGV
            // 
            this.matchupsAndScoresDGV.AllowUserToAddRows = false;
            this.matchupsAndScoresDGV.AllowUserToDeleteRows = false;
            this.matchupsAndScoresDGV.AllowUserToResizeColumns = false;
            this.matchupsAndScoresDGV.AllowUserToResizeRows = false;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.matchupsAndScoresDGV.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle36;
            this.matchupsAndScoresDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.matchupsAndScoresDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.matchupsAndScoresDGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.matchupsAndScoresDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this.matchupsAndScoresDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.matchupsAndScoresDGV.DefaultCellStyle = dataGridViewCellStyle38;
            this.matchupsAndScoresDGV.EnableHeadersVisualStyles = false;
            this.matchupsAndScoresDGV.Location = new System.Drawing.Point(676, 8);
            this.matchupsAndScoresDGV.Name = "matchupsAndScoresDGV";
            this.matchupsAndScoresDGV.ReadOnly = true;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.matchupsAndScoresDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle39;
            this.matchupsAndScoresDGV.RowHeadersVisible = false;
            this.matchupsAndScoresDGV.RowHeadersWidth = 5;
            this.matchupsAndScoresDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.matchupsAndScoresDGV.RowsDefaultCellStyle = dataGridViewCellStyle40;
            this.matchupsAndScoresDGV.Size = new System.Drawing.Size(750, 183);
            this.matchupsAndScoresDGV.TabIndex = 106;
            this.matchupsAndScoresDGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.matchupsAndScoresDGV_CellContentClick);
            this.matchupsAndScoresDGV.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.matchupsAndScoresDGV_CellMouseEnter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(967, 582);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 20);
            this.label6.TabIndex = 107;
            this.label6.Text = "Last Result:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Black;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(967, 604);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 24);
            this.label8.TabIndex = 108;
            this.label8.Text = "L8";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label33.Location = new System.Drawing.Point(676, 606);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(25, 14);
            this.label33.TabIndex = 72;
            this.label33.Text = "l33";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label34.Location = new System.Drawing.Point(672, 583);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(38, 14);
            this.label34.TabIndex = 109;
            this.label34.Text = "Total:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.ImageIndex = 4;
            this.button1.ImageList = this.imageList1;
            this.button1.Location = new System.Drawing.Point(1162, 582);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 54);
            this.button1.TabIndex = 110;
            this.button1.Text = "Daily Stats";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "spur-gear-icon.png");
            this.imageList1.Images.SetKeyName(1, "Categories-applications-system-icon.png");
            this.imageList1.Images.SetKeyName(2, "Actions-system-run-icon.png");
            this.imageList1.Images.SetKeyName(3, "Actions-office-chart-line-stacked-icon.png");
            this.imageList1.Images.SetKeyName(4, "Statistics-icon.png");
            // 
            // GameBox
            // 
            this.GameBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.GameBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.GameBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.GameBox.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GameBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.GameBox.FormattingEnabled = true;
            this.GameBox.Location = new System.Drawing.Point(271, 338);
            this.GameBox.Name = "GameBox";
            this.GameBox.Size = new System.Drawing.Size(128, 27);
            this.GameBox.TabIndex = 111;
            this.GameBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GameBox_KeyDown);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(309, 319);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(44, 20);
            this.label35.TabIndex = 112;
            this.label35.Text = "Game";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(266, 9);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(49, 20);
            this.label36.TabIndex = 113;
            this.label36.Text = "BANK:";
            this.label36.Visible = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(316, 12);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 15);
            this.label37.TabIndex = 114;
            this.label37.Text = "L37";
            this.label37.Visible = false;
            // 
            // isDefault
            // 
            this.isDefault.AutoSize = true;
            this.isDefault.BackColor = System.Drawing.Color.Transparent;
            this.isDefault.Location = new System.Drawing.Point(4, 367);
            this.isDefault.Name = "isDefault";
            this.isDefault.Size = new System.Drawing.Size(67, 17);
            this.isDefault.TabIndex = 115;
            this.isDefault.Text = "isDefault";
            this.isDefault.UseVisualStyleBackColor = false;
            this.isDefault.CheckedChanged += new System.EventHandler(this.isDefault_CheckedChanged);
            // 
            // RefreshScores
            // 
            this.RefreshScores.Enabled = true;
            this.RefreshScores.Interval = 60000;
            this.RefreshScores.Tick += new System.EventHandler(this.RefreshScores_Tick);
            // 
            // resultsDates
            // 
            this.resultsDates.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.resultsDates.Location = new System.Drawing.Point(509, 9);
            this.resultsDates.Name = "resultsDates";
            this.resultsDates.Size = new System.Drawing.Size(99, 20);
            this.resultsDates.TabIndex = 120;
            this.resultsDates.ValueChanged += new System.EventHandler(this.resultsDates_ValueChanged);
            // 
            // BetsTracker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1438, 657);
            this.Controls.Add(this.resultsDates);
            this.Controls.Add(this.previousDayResults);
            this.Controls.Add(this.nextDayResults);
            this.Controls.Add(this.isDefault);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.GameBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.previousDayGames);
            this.Controls.Add(this.nextDayGames);
            this.Controls.Add(this.OddsBox);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.pushBtn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.OfferBox);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.AllBetsDGV);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.NextBetBox);
            this.Controls.Add(this.ProfitBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LastBetBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TeamslistBox1);
            this.Controls.Add(this.TeamLogosPicBox);
            this.Controls.Add(this.winBtn);
            this.Controls.Add(this.updatebtn);
            this.Controls.Add(this.excelBtn);
            this.Controls.Add(this.AnalyzerBtn);
            this.Controls.Add(this.ResultsUpdBtn);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.DeleteBtn);
            this.Controls.Add(this.UndoBtn);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.PickTeamsBtn);
            this.Controls.Add(this.failBtn);
            this.Controls.Add(this.versionLabel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.leagueLogoPb);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.BetValuesBtn);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.matchupsAndScoresDGV);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label35);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BetsTracker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bets Tracker";
            this.Load += new System.EventHandler(this.BetsTracker_Load);
            ((System.ComponentModel.ISupportInitialize)(this.AllBetsDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeleteBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamLogosPicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.winBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updatebtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UndoBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SaveBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.excelBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PickTeamsBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leagueLogoPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AnalyzerBtn)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResultsUpdBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pushBtn)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BetValuesBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.previousDayGames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextDayGames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextDayResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.previousDayResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchupsAndScoresDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ListBox TeamslistBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LastBetBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ProfitBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView AllBetsDGV;
        private System.Windows.Forms.ComboBox NextBetBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox CalcNextBetTB;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.PictureBox TeamLogosPicBox;
        private System.Windows.Forms.PictureBox exitBtn;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox DeleteBtn;
        private System.Windows.Forms.PictureBox UndoBtn;
        private System.Windows.Forms.PictureBox SaveBtn;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox updatebtn;
        private System.Windows.Forms.PictureBox winBtn;
        private System.Windows.Forms.PictureBox failBtn;
        private System.Windows.Forms.PictureBox excelBtn;
        private System.Windows.Forms.PictureBox PickTeamsBtn;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.PictureBox leagueLogoPb;
        private System.Windows.Forms.PictureBox AnalyzerBtn;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.RadioButton MLBradioButton;
        private System.Windows.Forms.RadioButton NBAradioButton;
        private System.Windows.Forms.RadioButton NHLradioButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox OfferBox;
        private System.Windows.Forms.PictureBox ResultsUpdBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pushBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox BetValuesBtn;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.ComboBox OddsBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox previousDayGames;
        private System.Windows.Forms.PictureBox nextDayGames;
        private System.Windows.Forms.DataGridView matchupsAndScoresDGV;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ComboBox GameBox;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.RadioButton CasinoRB;
        private System.Windows.Forms.RadioButton BestBetsRB;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox isDefault;
        private System.Windows.Forms.Timer RefreshScores;
        private System.Windows.Forms.PictureBox nextDayResults;
        private System.Windows.Forms.PictureBox previousDayResults;
        private System.Windows.Forms.DateTimePicker resultsDates;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Oklade.BetsTrackerFolder
{
    public class BetsTrackerActions
    {

        public void SetDefaultGroup(string isDefaultGroup, RadioButton nbaButton, RadioButton nhlButton, RadioButton mlbRbButton, RadioButton bestbetsButton, CheckBox isDefault)
        {
            if (isDefaultGroup == null) return;
            switch (isDefaultGroup)
            {
                case "MLB":
                    isDefault.Checked = true;
                    mlbRbButton.Checked = true;
                    break;
                case "NBA":
                    isDefault.Checked = true;
                    nbaButton.Checked = true;
                    break;
                case "NHL":
                    isDefault.Checked = true;
                    nhlButton.Checked = true;
                    break;
                case "BestBets":
                    isDefault.Checked = true;
                    bestbetsButton.Checked = true;
                    break;
            }
        }

        public void CheckDefaultChecked(string isDefaultGroup, RadioButton nbaButton, RadioButton nhlButton, RadioButton mlbButton, RadioButton bestbetsButton, CheckBox isDefault)
        {
            if (isDefaultGroup == null) return;
            if (mlbButton.Checked.Equals(true) && isDefaultGroup.Equals("MLB")) {
                isDefault.Checked = true;
            }
            else if (nbaButton.Checked.Equals(true) && isDefaultGroup.Equals("NBA")) {
                isDefault.Checked = true;
            }
            else if (bestbetsButton.Checked.Equals(true) && isDefaultGroup.Equals("BestBets")) {
                isDefault.Checked = true;
            }
            else if (nhlButton.Checked.Equals(true) && isDefaultGroup.Equals("NHL")) {
                isDefault.Checked = true;
            }
        }

        public void ExecuteSqlUpdate(string connectionString, string sqlQuery)
        {
            using (SqlConnection connection = new SqlConnection(connectionString)) {
                if (connection.State.Equals(ConnectionState.Closed)) {
                    connection.Open();
                }
                using (SqlCommand cmd = new SqlCommand(sqlQuery, connection)) {
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}

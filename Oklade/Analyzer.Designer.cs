﻿namespace Oklade
{
    partial class Analyzer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Analyzer));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle109 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle110 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle111 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle112 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle113 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle114 = new System.Windows.Forms.DataGridViewCellStyle();
            this.IDoklBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TotalBox = new System.Windows.Forms.TextBox();
            this.HandicapBox = new System.Windows.Forms.TextBox();
            this.DGVAnalysisResults = new System.Windows.Forms.DataGridView();
            this.hteamradio = new System.Windows.Forms.RadioButton();
            this.ateamradio = new System.Windows.Forms.RadioButton();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.getGamesBtn = new System.Windows.Forms.Button();
            this.teamBox = new System.Windows.Forms.TextBox();
            this.homecheckBox1 = new System.Windows.Forms.CheckBox();
            this.awaycheckBox2 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lineBox1 = new System.Windows.Forms.TextBox();
            this.lineBox2 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.getBtn = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.GetTeam = new System.Windows.Forms.Button();
            this.AwayTeamCB = new System.Windows.Forms.ComboBox();
            this.HomeTeamCB = new System.Windows.Forms.ComboBox();
            this.awayTeamLogoPB = new System.Windows.Forms.PictureBox();
            this.homeTeamLogoPB = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.logoPB = new System.Windows.Forms.PictureBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.labelAwayAvg = new System.Windows.Forms.Label();
            this.labelHomeAvg = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.labelTotalAvg = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.lastSeasonCB = new System.Windows.Forms.CheckBox();
            this.thisSeasonCB = new System.Windows.Forms.CheckBox();
            this.label41 = new System.Windows.Forms.Label();
            this.PickGamesBtn = new System.Windows.Forms.Button();
            this.AutoProcessBtn = new System.Windows.Forms.PictureBox();
            this.results2 = new System.Windows.Forms.PictureBox();
            this.results1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DelRowBtn = new System.Windows.Forms.Button();
            this.editedCB = new System.Windows.Forms.CheckBox();
            this.BetProcessedCB = new System.Windows.Forms.CheckBox();
            this.ToBetOnCB = new System.Windows.Forms.CheckBox();
            this.HandicapMinusButton = new System.Windows.Forms.Button();
            this.TotalMinusButton = new System.Windows.Forms.Button();
            this.HandicapPlusButton = new System.Windows.Forms.Button();
            this.TotalPlusButton = new System.Windows.Forms.Button();
            this.SaveandRecalcbtn = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.AwayTeamTabControl = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.ATeamL50DGV = new System.Windows.Forms.DataGridView();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.AStarterL50DGV = new System.Windows.Forms.DataGridView();
            this.label42 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.LoadGameBtn = new System.Windows.Forms.Button();
            this.NewGameBtn = new System.Windows.Forms.Button();
            this.HStarterHandLabel = new System.Windows.Forms.Label();
            this.AStarterHandLabel = new System.Windows.Forms.Label();
            this.homeStarterCB = new System.Windows.Forms.ComboBox();
            this.awayStarterCB = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bestBetBtn = new System.Windows.Forms.RadioButton();
            this.radioBtnNBA = new System.Windows.Forms.RadioButton();
            this.radioBtnMLB = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TeamsStatsDGV = new System.Windows.Forms.DataGridView();
            this.tabControlInjuries = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.updatePFBtn = new System.Windows.Forms.Button();
            this.updateStartersPB = new System.Windows.Forms.ProgressBar();
            this.progressLabel = new System.Windows.Forms.Label();
            this.updateStartersBtn = new System.Windows.Forms.Button();
            this.secondBtoBbtn = new System.Windows.Forms.Button();
            this.firstBtoBbtn = new System.Windows.Forms.Button();
            this.vsNODIVBTN = new System.Windows.Forms.Button();
            this.AfterWBTN = new System.Windows.Forms.Button();
            this.AfterLBTN = new System.Windows.Forms.Button();
            this.TrendsLabel = new System.Windows.Forms.Label();
            this.AvsDivBTN = new System.Windows.Forms.Button();
            this.StartersStatsDGV = new System.Windows.Forms.DataGridView();
            this.ViewGamesHomeBtn = new System.Windows.Forms.Button();
            this.ViewGamesAwayBtn = new System.Windows.Forms.Button();
            this.L47 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.UpdMLBStrBtn = new System.Windows.Forms.Button();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.teamsDetailedStatsDGV = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.SQDLTB = new System.Windows.Forms.TextBox();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.useFullNameCB = new System.Windows.Forms.CheckBox();
            this.lastXGamesSetBtn = new System.Windows.Forms.Button();
            this.LastXgamesCB = new System.Windows.Forms.ComboBox();
            this.label49 = new System.Windows.Forms.Label();
            this.paceCB = new System.Windows.Forms.CheckBox();
            this.ouCheckbox = new System.Windows.Forms.CheckBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.button4 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.matchupsAndScoresDGV = new System.Windows.Forms.DataGridView();
            this.rankingsCB = new System.Windows.Forms.CheckBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.homeNonDivGamesBtn = new System.Windows.Forms.Button();
            this.awayNonDivGamesBtn = new System.Windows.Forms.Button();
            this.vsnoDIVDGV = new System.Windows.Forms.DataGridView();
            this.homeDivGamesbtn = new System.Windows.Forms.Button();
            this.awayDivGamesBtn = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.vsDIVDGV = new System.Windows.Forms.DataGridView();
            this.label43 = new System.Windows.Forms.Label();
            this.finishandCalcPB = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.previousGamePB = new System.Windows.Forms.PictureBox();
            this.nextGamePB = new System.Windows.Forms.PictureBox();
            this.updateResultsPB = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.prevPB = new System.Windows.Forms.PictureBox();
            this.nextPB = new System.Windows.Forms.PictureBox();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.linkLabel7 = new System.Windows.Forms.LinkLabel();
            this.linkLabel6 = new System.Windows.Forms.LinkLabel();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.linkLabel14 = new System.Windows.Forms.LinkLabel();
            this.linkLabel13 = new System.Windows.Forms.LinkLabel();
            this.linkLabel12 = new System.Windows.Forms.LinkLabel();
            this.linkLabel11 = new System.Windows.Forms.LinkLabel();
            this.linkLabel10 = new System.Windows.Forms.LinkLabel();
            this.linkLabel9 = new System.Windows.Forms.LinkLabel();
            this.linkLabel8 = new System.Windows.Forms.LinkLabel();
            this.HomeTeamTabControl = new System.Windows.Forms.TabControl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.HTeamL50DGV = new System.Windows.Forms.DataGridView();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.HStarterL50DGV = new System.Windows.Forms.DataGridView();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lastXgamesAvgCB = new System.Windows.Forms.CheckBox();
            this.lastXCB = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.DGVAnalysisResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.awayTeamLogoPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homeTeamLogoPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutoProcessBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.results2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.results1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.AwayTeamTabControl.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ATeamL50DGV)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AStarterL50DGV)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeamsStatsDGV)).BeginInit();
            this.tabControlInjuries.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartersStatsDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamsDetailedStatsDGV)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchupsAndScoresDGV)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsnoDIVDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsDIVDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finishandCalcPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.previousGamePB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextGamePB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateResultsPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prevPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.HomeTeamTabControl.SuspendLayout();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HTeamL50DGV)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HStarterL50DGV)).BeginInit();
            this.SuspendLayout();
            // 
            // IDoklBox
            // 
            this.IDoklBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDoklBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.IDoklBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IDoklBox.Location = new System.Drawing.Point(178, 37);
            this.IDoklBox.Name = "IDoklBox";
            this.IDoklBox.Size = new System.Drawing.Size(57, 22);
            this.IDoklBox.TabIndex = 2;
            this.IDoklBox.TextChanged += new System.EventHandler(this.IDoklBox_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(117, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "ID Oklade";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(75, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 19);
            this.label2.TabIndex = 6;
            this.label2.Text = "Home Team";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(80, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 19);
            this.label3.TabIndex = 7;
            this.label3.Text = "Away Team";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(121, 247);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 19);
            this.label4.TabIndex = 8;
            this.label4.Text = "Total";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(86, 273);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 19);
            this.label5.TabIndex = 9;
            this.label5.Text = "Handicap";
            // 
            // TotalBox
            // 
            this.TotalBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TotalBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TotalBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.TotalBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.TotalBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TotalBox.Location = new System.Drawing.Point(190, 246);
            this.TotalBox.Name = "TotalBox";
            this.TotalBox.Size = new System.Drawing.Size(57, 22);
            this.TotalBox.TabIndex = 4;
            this.TotalBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TotalBox_MouseClick);
            this.TotalBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TotalBox_KeyDown);
            this.TotalBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TotalBox_KeyPress);
            this.TotalBox.Validating += new System.ComponentModel.CancelEventHandler(this.TotalBox_Validating);
            // 
            // HandicapBox
            // 
            this.HandicapBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.HandicapBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.HandicapBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.HandicapBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.HandicapBox.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.HandicapBox.Location = new System.Drawing.Point(190, 272);
            this.HandicapBox.Name = "HandicapBox";
            this.HandicapBox.Size = new System.Drawing.Size(57, 22);
            this.HandicapBox.TabIndex = 5;
            this.HandicapBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.HandicapBox_MouseClick);
            this.HandicapBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HandicapBox_KeyDown);
            this.HandicapBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HandicapBox_KeyPress);
            this.HandicapBox.Validating += new System.ComponentModel.CancelEventHandler(this.HandicapBox_Validating);
            // 
            // DGVAnalysisResults
            // 
            this.DGVAnalysisResults.AllowUserToDeleteRows = false;
            this.DGVAnalysisResults.AllowUserToResizeColumns = false;
            this.DGVAnalysisResults.AllowUserToResizeRows = false;
            this.DGVAnalysisResults.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DGVAnalysisResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVAnalysisResults.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DGVAnalysisResults.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.DGVAnalysisResults.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle77.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle77.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle77.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle77.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle77.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle77.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVAnalysisResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle77;
            this.DGVAnalysisResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVAnalysisResults.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle78.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle78.Font = new System.Drawing.Font("Arial Narrow", 11.75F);
            dataGridViewCellStyle78.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle78.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle78.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle78.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVAnalysisResults.DefaultCellStyle = dataGridViewCellStyle78;
            this.DGVAnalysisResults.EnableHeadersVisualStyles = false;
            this.DGVAnalysisResults.Location = new System.Drawing.Point(11, 402);
            this.DGVAnalysisResults.Name = "DGVAnalysisResults";
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle79.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(75)))), ((int)(((byte)(65)))));
            dataGridViewCellStyle79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle79.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle79.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle79.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle79.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVAnalysisResults.RowHeadersDefaultCellStyle = dataGridViewCellStyle79;
            this.DGVAnalysisResults.RowHeadersVisible = false;
            this.DGVAnalysisResults.RowHeadersWidth = 5;
            this.DGVAnalysisResults.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DGVAnalysisResults.Size = new System.Drawing.Size(911, 385);
            this.DGVAnalysisResults.TabIndex = 12;
            this.DGVAnalysisResults.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentDoubleClick);
            // 
            // hteamradio
            // 
            this.hteamradio.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.hteamradio.AutoSize = true;
            this.hteamradio.BackColor = System.Drawing.Color.Transparent;
            this.hteamradio.Location = new System.Drawing.Point(413, 182);
            this.hteamradio.Name = "hteamradio";
            this.hteamradio.Size = new System.Drawing.Size(14, 13);
            this.hteamradio.TabIndex = 18;
            this.hteamradio.UseVisualStyleBackColor = false;
            // 
            // ateamradio
            // 
            this.ateamradio.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ateamradio.AutoSize = true;
            this.ateamradio.BackColor = System.Drawing.Color.Transparent;
            this.ateamradio.Location = new System.Drawing.Point(414, 117);
            this.ateamradio.Name = "ateamradio";
            this.ateamradio.Size = new System.Drawing.Size(14, 13);
            this.ateamradio.TabIndex = 19;
            this.ateamradio.UseVisualStyleBackColor = false;
            this.ateamradio.CheckedChanged += new System.EventHandler(this.ateamradio_CheckedChanged);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle80.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle80.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle80.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle80.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle80.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle80.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle80;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle81.BackColor = System.Drawing.Color.LemonChiffon;
            dataGridViewCellStyle81.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle81.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle81.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle81.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle81.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle81;
            this.dataGridView2.EnableHeadersVisualStyles = false;
            this.dataGridView2.Location = new System.Drawing.Point(11, 303);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowHeadersWidth = 5;
            this.dataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView2.Size = new System.Drawing.Size(911, 45);
            this.dataGridView2.TabIndex = 20;
            // 
            // getGamesBtn
            // 
            this.getGamesBtn.Enabled = false;
            this.getGamesBtn.Location = new System.Drawing.Point(555, 8);
            this.getGamesBtn.Name = "getGamesBtn";
            this.getGamesBtn.Size = new System.Drawing.Size(78, 40);
            this.getGamesBtn.TabIndex = 21;
            this.getGamesBtn.Text = "Get Games";
            this.getGamesBtn.UseVisualStyleBackColor = true;
            this.getGamesBtn.Click += new System.EventHandler(this.getGamesBtn_Click);
            // 
            // teamBox
            // 
            this.teamBox.BackColor = System.Drawing.Color.WhiteSmoke;
            this.teamBox.Location = new System.Drawing.Point(67, 11);
            this.teamBox.Name = "teamBox";
            this.teamBox.Size = new System.Drawing.Size(87, 21);
            this.teamBox.TabIndex = 22;
            // 
            // homecheckBox1
            // 
            this.homecheckBox1.AutoSize = true;
            this.homecheckBox1.BackColor = System.Drawing.Color.Transparent;
            this.homecheckBox1.Checked = true;
            this.homecheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.homecheckBox1.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.homecheckBox1.ForeColor = System.Drawing.Color.Black;
            this.homecheckBox1.Location = new System.Drawing.Point(67, 31);
            this.homecheckBox1.Name = "homecheckBox1";
            this.homecheckBox1.Size = new System.Drawing.Size(59, 19);
            this.homecheckBox1.TabIndex = 23;
            this.homecheckBox1.Text = "Home";
            this.homecheckBox1.UseVisualStyleBackColor = false;
            // 
            // awaycheckBox2
            // 
            this.awaycheckBox2.AutoSize = true;
            this.awaycheckBox2.BackColor = System.Drawing.Color.Transparent;
            this.awaycheckBox2.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.awaycheckBox2.ForeColor = System.Drawing.Color.Black;
            this.awaycheckBox2.Location = new System.Drawing.Point(67, 50);
            this.awaycheckBox2.Name = "awaycheckBox2";
            this.awaycheckBox2.Size = new System.Drawing.Size(57, 19);
            this.awaycheckBox2.TabIndex = 24;
            this.awaycheckBox2.Text = "Away";
            this.awaycheckBox2.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(22, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 19);
            this.label8.TabIndex = 25;
            this.label8.Text = "Team";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(161, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 15);
            this.label9.TabIndex = 26;
            this.label9.Text = "Line <=";
            // 
            // lineBox1
            // 
            this.lineBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lineBox1.Location = new System.Drawing.Point(210, 11);
            this.lineBox1.Name = "lineBox1";
            this.lineBox1.Size = new System.Drawing.Size(72, 21);
            this.lineBox1.TabIndex = 27;
            // 
            // lineBox2
            // 
            this.lineBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lineBox2.Location = new System.Drawing.Point(210, 37);
            this.lineBox2.Name = "lineBox2";
            this.lineBox2.Size = new System.Drawing.Size(72, 21);
            this.lineBox2.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(161, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 15);
            this.label10.TabIndex = 29;
            this.label10.Text = "Line >=";
            // 
            // getBtn
            // 
            this.getBtn.Location = new System.Drawing.Point(485, 9);
            this.getBtn.Name = "getBtn";
            this.getBtn.Size = new System.Drawing.Size(70, 30);
            this.getBtn.TabIndex = 34;
            this.getBtn.Text = "get";
            this.getBtn.UseVisualStyleBackColor = true;
            this.getBtn.Click += new System.EventHandler(this.button3_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dateTimePicker1.CalendarMonthBackground = System.Drawing.Color.LemonChiffon;
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(14, 374);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(94, 20);
            this.dateTimePicker1.TabIndex = 36;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            this.dateTimePicker1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dateTimePicker1_MouseDown);
            // 
            // GetTeam
            // 
            this.GetTeam.Location = new System.Drawing.Point(398, 9);
            this.GetTeam.Name = "GetTeam";
            this.GetTeam.Size = new System.Drawing.Size(70, 30);
            this.GetTeam.TabIndex = 38;
            this.GetTeam.Text = "GetTeam";
            this.GetTeam.UseVisualStyleBackColor = true;
            this.GetTeam.Click += new System.EventHandler(this.GetTeam_Click);
            // 
            // AwayTeamCB
            // 
            this.AwayTeamCB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.AwayTeamCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.AwayTeamCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AwayTeamCB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.AwayTeamCB.Font = new System.Drawing.Font("Rockwell", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AwayTeamCB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.AwayTeamCB.FormattingEnabled = true;
            this.AwayTeamCB.Location = new System.Drawing.Point(174, 111);
            this.AwayTeamCB.Name = "AwayTeamCB";
            this.AwayTeamCB.Size = new System.Drawing.Size(234, 29);
            this.AwayTeamCB.TabIndex = 0;
            this.AwayTeamCB.Tag = "AwayTeam";
            this.toolTip1.SetToolTip(this.AwayTeamCB, "Select Away Team");
            this.AwayTeamCB.SelectedIndexChanged += new System.EventHandler(this.AwayTeamCB_SelectedIndexChanged);
            this.AwayTeamCB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox1_KeyDown);
            this.AwayTeamCB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // HomeTeamCB
            // 
            this.HomeTeamCB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.HomeTeamCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.HomeTeamCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.HomeTeamCB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.HomeTeamCB.Font = new System.Drawing.Font("Rockwell", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HomeTeamCB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.HomeTeamCB.FormattingEnabled = true;
            this.HomeTeamCB.Location = new System.Drawing.Point(174, 175);
            this.HomeTeamCB.Name = "HomeTeamCB";
            this.HomeTeamCB.Size = new System.Drawing.Size(233, 29);
            this.HomeTeamCB.TabIndex = 2;
            this.HomeTeamCB.Tag = "HomeTeam";
            this.toolTip1.SetToolTip(this.HomeTeamCB, "Select Home Team");
            this.HomeTeamCB.SelectedValueChanged += new System.EventHandler(this.HomeTeamCB_SelectedValueChanged);
            this.HomeTeamCB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox2_KeyDown);
            this.HomeTeamCB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox2_MouseClick);
            // 
            // awayTeamLogoPB
            // 
            this.awayTeamLogoPB.BackColor = System.Drawing.Color.Transparent;
            this.awayTeamLogoPB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.awayTeamLogoPB.Location = new System.Drawing.Point(1, 167);
            this.awayTeamLogoPB.Name = "awayTeamLogoPB";
            this.awayTeamLogoPB.Size = new System.Drawing.Size(138, 112);
            this.awayTeamLogoPB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.awayTeamLogoPB.TabIndex = 48;
            this.awayTeamLogoPB.TabStop = false;
            this.awayTeamLogoPB.Visible = false;
            this.awayTeamLogoPB.Click += new System.EventHandler(this.pictureBox2_Click);
            this.awayTeamLogoPB.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            this.awayTeamLogoPB.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // homeTeamLogoPB
            // 
            this.homeTeamLogoPB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.homeTeamLogoPB.BackColor = System.Drawing.Color.Transparent;
            this.homeTeamLogoPB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.homeTeamLogoPB.Location = new System.Drawing.Point(355, 167);
            this.homeTeamLogoPB.Name = "homeTeamLogoPB";
            this.homeTeamLogoPB.Size = new System.Drawing.Size(138, 112);
            this.homeTeamLogoPB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.homeTeamLogoPB.TabIndex = 49;
            this.homeTeamLogoPB.TabStop = false;
            this.homeTeamLogoPB.Visible = false;
            this.homeTeamLogoPB.Click += new System.EventHandler(this.pictureBox3_Click);
            this.homeTeamLogoPB.MouseLeave += new System.EventHandler(this.pictureBox3_MouseLeave);
            this.homeTeamLogoPB.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(223, 164);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 20);
            this.label14.TabIndex = 56;
            this.label14.Text = "PPG";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(219, 187);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 20);
            this.label15.TabIndex = 57;
            this.label15.Text = "OPPG";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(223, 210);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 20);
            this.label16.TabIndex = 58;
            this.label16.Text = "ATS";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(226, 235);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 20);
            this.label17.TabIndex = 59;
            this.label17.Text = "OU";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.ForeColor = System.Drawing.Color.Navy;
            this.label19.Location = new System.Drawing.Point(167, 186);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 20);
            this.label19.TabIndex = 61;
            this.label19.Text = "l19";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label21.ForeColor = System.Drawing.Color.Navy;
            this.label21.Location = new System.Drawing.Point(272, 186);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(27, 20);
            this.label21.TabIndex = 63;
            this.label21.Text = "l21";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label22.ForeColor = System.Drawing.Color.Navy;
            this.label22.Location = new System.Drawing.Point(164, 209);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 20);
            this.label22.TabIndex = 64;
            this.label22.Text = "l22";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label23.ForeColor = System.Drawing.Color.Navy;
            this.label23.Location = new System.Drawing.Point(164, 234);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(27, 20);
            this.label23.TabIndex = 65;
            this.label23.Text = "l23";
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label24.ForeColor = System.Drawing.Color.Navy;
            this.label24.Location = new System.Drawing.Point(267, 209);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(27, 20);
            this.label24.TabIndex = 66;
            this.label24.Text = "l24";
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label25.ForeColor = System.Drawing.Color.Navy;
            this.label25.Location = new System.Drawing.Point(267, 234);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(27, 20);
            this.label25.TabIndex = 67;
            this.label25.Text = "l25";
            // 
            // logoPB
            // 
            this.logoPB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.logoPB.BackColor = System.Drawing.Color.Transparent;
            this.logoPB.Location = new System.Drawing.Point(77, 3);
            this.logoPB.Name = "logoPB";
            this.logoPB.Size = new System.Drawing.Size(321, 151);
            this.logoPB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logoPB.TabIndex = 68;
            this.logoPB.TabStop = false;
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(304, 368);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(107, 19);
            this.label26.TabIndex = 71;
            this.label26.Text = "Prediction %";
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label27.ForeColor = System.Drawing.Color.Navy;
            this.label27.Location = new System.Drawing.Point(472, 358);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(27, 20);
            this.label27.TabIndex = 72;
            this.label27.Text = "l26";
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label28.ForeColor = System.Drawing.Color.Navy;
            this.label28.Location = new System.Drawing.Point(472, 377);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(27, 20);
            this.label28.TabIndex = 73;
            this.label28.Text = "l27";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(418, 359);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(42, 19);
            this.label29.TabIndex = 74;
            this.label29.Text = "Over";
            // 
            // label30
            // 
            this.label30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(418, 378);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(52, 19);
            this.label30.TabIndex = 75;
            this.label30.Text = "Under";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(592, 359);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(87, 19);
            this.label31.TabIndex = 76;
            this.label31.Text = "Away Team";
            // 
            // label32
            // 
            this.label32.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(592, 378);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(91, 19);
            this.label32.TabIndex = 77;
            this.label32.Text = "Home Team";
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label33.ForeColor = System.Drawing.Color.Navy;
            this.label33.Location = new System.Drawing.Point(542, 358);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(27, 20);
            this.label33.TabIndex = 78;
            this.label33.Text = "l28";
            // 
            // label34
            // 
            this.label34.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label34.ForeColor = System.Drawing.Color.Navy;
            this.label34.Location = new System.Drawing.Point(542, 377);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(27, 20);
            this.label34.TabIndex = 79;
            this.label34.Text = "l29";
            // 
            // labelAwayAvg
            // 
            this.labelAwayAvg.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelAwayAvg.AutoSize = true;
            this.labelAwayAvg.BackColor = System.Drawing.Color.Transparent;
            this.labelAwayAvg.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAwayAvg.ForeColor = System.Drawing.Color.Navy;
            this.labelAwayAvg.Location = new System.Drawing.Point(686, 358);
            this.labelAwayAvg.Name = "labelAwayAvg";
            this.labelAwayAvg.Size = new System.Drawing.Size(27, 20);
            this.labelAwayAvg.TabIndex = 80;
            this.labelAwayAvg.Text = "l35";
            // 
            // labelHomeAvg
            // 
            this.labelHomeAvg.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelHomeAvg.AutoSize = true;
            this.labelHomeAvg.BackColor = System.Drawing.Color.Transparent;
            this.labelHomeAvg.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelHomeAvg.ForeColor = System.Drawing.Color.Navy;
            this.labelHomeAvg.Location = new System.Drawing.Point(686, 377);
            this.labelHomeAvg.Name = "labelHomeAvg";
            this.labelHomeAvg.Size = new System.Drawing.Size(27, 20);
            this.labelHomeAvg.TabIndex = 81;
            this.labelHomeAvg.Text = "l36";
            // 
            // label37
            // 
            this.label37.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(735, 363);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(35, 15);
            this.label37.TabIndex = 82;
            this.label37.Text = "Total";
            // 
            // labelTotalAvg
            // 
            this.labelTotalAvg.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelTotalAvg.AutoSize = true;
            this.labelTotalAvg.BackColor = System.Drawing.Color.Transparent;
            this.labelTotalAvg.Font = new System.Drawing.Font("Arial Narrow", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTotalAvg.ForeColor = System.Drawing.Color.Navy;
            this.labelTotalAvg.Location = new System.Drawing.Point(771, 358);
            this.labelTotalAvg.Name = "labelTotalAvg";
            this.labelTotalAvg.Size = new System.Drawing.Size(27, 20);
            this.labelTotalAvg.TabIndex = 83;
            this.labelTotalAvg.Text = "l38";
            // 
            // label39
            // 
            this.label39.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(523, 361);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(10, 13);
            this.label39.TabIndex = 84;
            this.label39.Text = "|";
            // 
            // label40
            // 
            this.label40.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(523, 383);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(10, 13);
            this.label40.TabIndex = 85;
            this.label40.Text = "|";
            // 
            // lastSeasonCB
            // 
            this.lastSeasonCB.AutoSize = true;
            this.lastSeasonCB.BackColor = System.Drawing.Color.Transparent;
            this.lastSeasonCB.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lastSeasonCB.ForeColor = System.Drawing.Color.Black;
            this.lastSeasonCB.Location = new System.Drawing.Point(163, 2);
            this.lastSeasonCB.Name = "lastSeasonCB";
            this.lastSeasonCB.Size = new System.Drawing.Size(77, 16);
            this.lastSeasonCB.TabIndex = 86;
            this.lastSeasonCB.Text = "2014\\2015";
            this.lastSeasonCB.UseVisualStyleBackColor = false;
            // 
            // thisSeasonCB
            // 
            this.thisSeasonCB.AutoSize = true;
            this.thisSeasonCB.BackColor = System.Drawing.Color.Transparent;
            this.thisSeasonCB.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.thisSeasonCB.ForeColor = System.Drawing.Color.Black;
            this.thisSeasonCB.Location = new System.Drawing.Point(163, 20);
            this.thisSeasonCB.Name = "thisSeasonCB";
            this.thisSeasonCB.Size = new System.Drawing.Size(77, 16);
            this.thisSeasonCB.TabIndex = 87;
            this.thisSeasonCB.Text = "2015\\2016";
            this.thisSeasonCB.UseVisualStyleBackColor = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(76, 4);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(84, 15);
            this.label41.TabIndex = 88;
            this.label41.Text = "Use Seasons:";
            // 
            // PickGamesBtn
            // 
            this.PickGamesBtn.Enabled = false;
            this.PickGamesBtn.Location = new System.Drawing.Point(634, 8);
            this.PickGamesBtn.Name = "PickGamesBtn";
            this.PickGamesBtn.Size = new System.Drawing.Size(78, 40);
            this.PickGamesBtn.TabIndex = 91;
            this.PickGamesBtn.Text = "Pick Games";
            this.PickGamesBtn.UseVisualStyleBackColor = true;
            this.PickGamesBtn.Click += new System.EventHandler(this.button18_Click);
            // 
            // AutoProcessBtn
            // 
            this.AutoProcessBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.AutoProcessBtn.Image = ((System.Drawing.Image)(resources.GetObject("AutoProcessBtn.Image")));
            this.AutoProcessBtn.Location = new System.Drawing.Point(271, 240);
            this.AutoProcessBtn.Name = "AutoProcessBtn";
            this.AutoProcessBtn.Size = new System.Drawing.Size(69, 59);
            this.AutoProcessBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.AutoProcessBtn.TabIndex = 99;
            this.AutoProcessBtn.TabStop = false;
            this.toolTip1.SetToolTip(this.AutoProcessBtn, "Automatic calculation");
            this.AutoProcessBtn.Click += new System.EventHandler(this.AutoProcess_Click_1);
            this.AutoProcessBtn.MouseLeave += new System.EventHandler(this.autoProcess_MouseLeave_1);
            this.AutoProcessBtn.MouseHover += new System.EventHandler(this.autoProcess_MouseHover_1);
            // 
            // results2
            // 
            this.results2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.results2.Image = ((System.Drawing.Image)(resources.GetObject("results2.Image")));
            this.results2.Location = new System.Drawing.Point(432, 169);
            this.results2.Name = "results2";
            this.results2.Size = new System.Drawing.Size(48, 41);
            this.results2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.results2.TabIndex = 98;
            this.results2.TabStop = false;
            this.toolTip1.SetToolTip(this.results2, "Home team results");
            this.results2.Click += new System.EventHandler(this.results2_Click);
            this.results2.MouseLeave += new System.EventHandler(this.results2_MouseLeave);
            this.results2.MouseHover += new System.EventHandler(this.results2_MouseHover);
            // 
            // results1
            // 
            this.results1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.results1.Image = ((System.Drawing.Image)(resources.GetObject("results1.Image")));
            this.results1.Location = new System.Drawing.Point(433, 103);
            this.results1.Name = "results1";
            this.results1.Size = new System.Drawing.Size(48, 41);
            this.results1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.results1.TabIndex = 97;
            this.results1.TabStop = false;
            this.toolTip1.SetToolTip(this.results1, "Away team results");
            this.results1.Click += new System.EventHandler(this.results1_Click);
            this.results1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.results1_MouseClick);
            this.results1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.results1_MouseDoubleClick);
            this.results1.MouseLeave += new System.EventHandler(this.results1_MouseLeave);
            this.results1.MouseHover += new System.EventHandler(this.results1_MouseHover);
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.DelRowBtn);
            this.groupBox2.Controls.Add(this.editedCB);
            this.groupBox2.Controls.Add(this.BetProcessedCB);
            this.groupBox2.Controls.Add(this.ToBetOnCB);
            this.groupBox2.Controls.Add(this.HandicapMinusButton);
            this.groupBox2.Controls.Add(this.TotalMinusButton);
            this.groupBox2.Controls.Add(this.HandicapPlusButton);
            this.groupBox2.Controls.Add(this.TotalPlusButton);
            this.groupBox2.Controls.Add(this.SaveandRecalcbtn);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.AwayTeamTabControl);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this.btnClose);
            this.groupBox2.Controls.Add(this.btnExit);
            this.groupBox2.Controls.Add(this.LoadGameBtn);
            this.groupBox2.Controls.Add(this.NewGameBtn);
            this.groupBox2.Controls.Add(this.HStarterHandLabel);
            this.groupBox2.Controls.Add(this.AStarterHandLabel);
            this.groupBox2.Controls.Add(this.homeStarterCB);
            this.groupBox2.Controls.Add(this.awayStarterCB);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.tabControlInjuries);
            this.groupBox2.Controls.Add(this.label43);
            this.groupBox2.Controls.Add(this.finishandCalcPB);
            this.groupBox2.Controls.Add(this.pictureBox13);
            this.groupBox2.Controls.Add(this.pictureBox12);
            this.groupBox2.Controls.Add(this.pictureBox11);
            this.groupBox2.Controls.Add(this.pictureBox9);
            this.groupBox2.Controls.Add(this.previousGamePB);
            this.groupBox2.Controls.Add(this.nextGamePB);
            this.groupBox2.Controls.Add(this.updateResultsPB);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.AutoProcessBtn);
            this.groupBox2.Controls.Add(this.results2);
            this.groupBox2.Controls.Add(this.results1);
            this.groupBox2.Controls.Add(this.dateTimePicker1);
            this.groupBox2.Controls.Add(this.DGVAnalysisResults);
            this.groupBox2.Controls.Add(this.dataGridView2);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.labelTotalAvg);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.IDoklBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label37);
            this.groupBox2.Controls.Add(this.labelHomeAvg);
            this.groupBox2.Controls.Add(this.TotalBox);
            this.groupBox2.Controls.Add(this.labelAwayAvg);
            this.groupBox2.Controls.Add(this.HandicapBox);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.hteamradio);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.ateamradio);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.HomeTeamCB);
            this.groupBox2.Controls.Add(this.AwayTeamCB);
            this.groupBox2.Controls.Add(this.prevPB);
            this.groupBox2.Controls.Add(this.nextPB);
            this.groupBox2.Controls.Add(this.dataGridView5);
            this.groupBox2.Controls.Add(this.linkLabel1);
            this.groupBox2.Controls.Add(this.linkLabel2);
            this.groupBox2.Controls.Add(this.linkLabel3);
            this.groupBox2.Controls.Add(this.linkLabel4);
            this.groupBox2.Controls.Add(this.linkLabel7);
            this.groupBox2.Controls.Add(this.linkLabel6);
            this.groupBox2.Controls.Add(this.linkLabel5);
            this.groupBox2.Controls.Add(this.linkLabel14);
            this.groupBox2.Controls.Add(this.linkLabel13);
            this.groupBox2.Controls.Add(this.linkLabel12);
            this.groupBox2.Controls.Add(this.linkLabel11);
            this.groupBox2.Controls.Add(this.linkLabel10);
            this.groupBox2.Controls.Add(this.linkLabel9);
            this.groupBox2.Controls.Add(this.linkLabel8);
            this.groupBox2.Controls.Add(this.HomeTeamTabControl);
            this.groupBox2.Location = new System.Drawing.Point(12, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1876, 995);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // DelRowBtn
            // 
            this.DelRowBtn.Location = new System.Drawing.Point(481, 267);
            this.DelRowBtn.Name = "DelRowBtn";
            this.DelRowBtn.Size = new System.Drawing.Size(75, 34);
            this.DelRowBtn.TabIndex = 158;
            this.DelRowBtn.Text = "Delete row - Recalculate";
            this.DelRowBtn.UseVisualStyleBackColor = true;
            this.DelRowBtn.Click += new System.EventHandler(this.DelRowBtn_Click);
            // 
            // editedCB
            // 
            this.editedCB.AutoSize = true;
            this.editedCB.BackColor = System.Drawing.Color.Transparent;
            this.editedCB.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.editedCB.ForeColor = System.Drawing.Color.Black;
            this.editedCB.Location = new System.Drawing.Point(14, 353);
            this.editedCB.Name = "editedCB";
            this.editedCB.Size = new System.Drawing.Size(61, 19);
            this.editedCB.TabIndex = 157;
            this.editedCB.Text = "Edited";
            this.editedCB.UseVisualStyleBackColor = false;
            this.editedCB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.editedCB_MouseClick);
            // 
            // BetProcessedCB
            // 
            this.BetProcessedCB.AutoSize = true;
            this.BetProcessedCB.BackColor = System.Drawing.Color.Transparent;
            this.BetProcessedCB.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BetProcessedCB.ForeColor = System.Drawing.Color.Black;
            this.BetProcessedCB.Location = new System.Drawing.Point(182, 378);
            this.BetProcessedCB.Name = "BetProcessedCB";
            this.BetProcessedCB.Size = new System.Drawing.Size(103, 19);
            this.BetProcessedCB.TabIndex = 156;
            this.BetProcessedCB.Text = "Bet Processed";
            this.BetProcessedCB.UseVisualStyleBackColor = false;
            this.BetProcessedCB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.BetProcessedCB_MouseClick);
            // 
            // ToBetOnCB
            // 
            this.ToBetOnCB.AutoSize = true;
            this.ToBetOnCB.BackColor = System.Drawing.Color.Transparent;
            this.ToBetOnCB.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ToBetOnCB.ForeColor = System.Drawing.Color.Black;
            this.ToBetOnCB.Location = new System.Drawing.Point(182, 354);
            this.ToBetOnCB.Name = "ToBetOnCB";
            this.ToBetOnCB.Size = new System.Drawing.Size(80, 19);
            this.ToBetOnCB.TabIndex = 155;
            this.ToBetOnCB.Text = "To Bet On";
            this.ToBetOnCB.UseVisualStyleBackColor = false;
            this.ToBetOnCB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ToBetOnCB_MouseClick);
            // 
            // HandicapMinusButton
            // 
            this.HandicapMinusButton.Location = new System.Drawing.Point(168, 272);
            this.HandicapMinusButton.Name = "HandicapMinusButton";
            this.HandicapMinusButton.Size = new System.Drawing.Size(22, 22);
            this.HandicapMinusButton.TabIndex = 155;
            this.HandicapMinusButton.Text = "-";
            this.HandicapMinusButton.UseVisualStyleBackColor = true;
            this.HandicapMinusButton.Click += new System.EventHandler(this.HandicapMinusButton_Click);
            // 
            // TotalMinusButton
            // 
            this.TotalMinusButton.Location = new System.Drawing.Point(168, 246);
            this.TotalMinusButton.Name = "TotalMinusButton";
            this.TotalMinusButton.Size = new System.Drawing.Size(22, 22);
            this.TotalMinusButton.TabIndex = 154;
            this.TotalMinusButton.Text = "-";
            this.TotalMinusButton.UseVisualStyleBackColor = true;
            this.TotalMinusButton.Click += new System.EventHandler(this.TotalMinusButton_Click);
            // 
            // HandicapPlusButton
            // 
            this.HandicapPlusButton.Location = new System.Drawing.Point(247, 272);
            this.HandicapPlusButton.Name = "HandicapPlusButton";
            this.HandicapPlusButton.Size = new System.Drawing.Size(22, 22);
            this.HandicapPlusButton.TabIndex = 153;
            this.HandicapPlusButton.Text = "+";
            this.HandicapPlusButton.UseVisualStyleBackColor = true;
            this.HandicapPlusButton.Click += new System.EventHandler(this.HandicapPlusButton_Click);
            // 
            // TotalPlusButton
            // 
            this.TotalPlusButton.Location = new System.Drawing.Point(247, 246);
            this.TotalPlusButton.Name = "TotalPlusButton";
            this.TotalPlusButton.Size = new System.Drawing.Size(22, 22);
            this.TotalPlusButton.TabIndex = 152;
            this.TotalPlusButton.Text = "+";
            this.TotalPlusButton.UseVisualStyleBackColor = true;
            this.TotalPlusButton.Click += new System.EventHandler(this.TotalPlusButton_Click);
            // 
            // SaveandRecalcbtn
            // 
            this.SaveandRecalcbtn.Location = new System.Drawing.Point(481, 220);
            this.SaveandRecalcbtn.Name = "SaveandRecalcbtn";
            this.SaveandRecalcbtn.Size = new System.Drawing.Size(75, 45);
            this.SaveandRecalcbtn.TabIndex = 151;
            this.SaveandRecalcbtn.Text = "Save and Recalculate";
            this.SaveandRecalcbtn.UseVisualStyleBackColor = true;
            this.SaveandRecalcbtn.Click += new System.EventHandler(this.SaveandRecalcbtn_Click);
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(1158, 329);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 20);
            this.label12.TabIndex = 88;
            // 
            // AwayTeamTabControl
            // 
            this.AwayTeamTabControl.Controls.Add(this.tabPage6);
            this.AwayTeamTabControl.Controls.Add(this.tabPage7);
            this.AwayTeamTabControl.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AwayTeamTabControl.Location = new System.Drawing.Point(941, 330);
            this.AwayTeamTabControl.Name = "AwayTeamTabControl";
            this.AwayTeamTabControl.SelectedIndex = 0;
            this.AwayTeamTabControl.Size = new System.Drawing.Size(929, 295);
            this.AwayTeamTabControl.TabIndex = 149;
            this.AwayTeamTabControl.SelectedIndexChanged += new System.EventHandler(this.AwayTeamTabControl_SelectedIndexChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.ATeamL50DGV);
            this.tabPage6.Location = new System.Drawing.Point(4, 25);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(921, 266);
            this.tabPage6.TabIndex = 0;
            this.tabPage6.Text = "Away Team L50";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // ATeamL50DGV
            // 
            this.ATeamL50DGV.AllowUserToDeleteRows = false;
            this.ATeamL50DGV.AllowUserToResizeColumns = false;
            this.ATeamL50DGV.AllowUserToResizeRows = false;
            this.ATeamL50DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ATeamL50DGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle90.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle90.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle90.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle90.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle90.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle90.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ATeamL50DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle90;
            this.ATeamL50DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle91.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle91.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle91.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle91.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle91.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle91.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ATeamL50DGV.DefaultCellStyle = dataGridViewCellStyle91;
            this.ATeamL50DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ATeamL50DGV.EnableHeadersVisualStyles = false;
            this.ATeamL50DGV.Location = new System.Drawing.Point(3, 3);
            this.ATeamL50DGV.Name = "ATeamL50DGV";
            this.ATeamL50DGV.RowHeadersVisible = false;
            this.ATeamL50DGV.RowHeadersWidth = 5;
            this.ATeamL50DGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ATeamL50DGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ATeamL50DGV.Size = new System.Drawing.Size(915, 260);
            this.ATeamL50DGV.TabIndex = 86;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.AStarterL50DGV);
            this.tabPage7.Location = new System.Drawing.Point(4, 25);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(921, 266);
            this.tabPage7.TabIndex = 1;
            this.tabPage7.Text = "Away Starter L10";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // AStarterL50DGV
            // 
            this.AStarterL50DGV.AllowUserToDeleteRows = false;
            this.AStarterL50DGV.AllowUserToResizeColumns = false;
            this.AStarterL50DGV.AllowUserToResizeRows = false;
            this.AStarterL50DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.AStarterL50DGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle92.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle92.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle92.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle92.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle92.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle92.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AStarterL50DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle92;
            this.AStarterL50DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle93.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle93.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle93.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle93.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle93.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle93.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.AStarterL50DGV.DefaultCellStyle = dataGridViewCellStyle93;
            this.AStarterL50DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AStarterL50DGV.EnableHeadersVisualStyles = false;
            this.AStarterL50DGV.Location = new System.Drawing.Point(3, 3);
            this.AStarterL50DGV.Name = "AStarterL50DGV";
            this.AStarterL50DGV.ReadOnly = true;
            this.AStarterL50DGV.RowHeadersVisible = false;
            this.AStarterL50DGV.RowHeadersWidth = 5;
            this.AStarterL50DGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.AStarterL50DGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.AStarterL50DGV.Size = new System.Drawing.Size(915, 260);
            this.AStarterL50DGV.TabIndex = 87;
            // 
            // label42
            // 
            this.label42.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label42.Location = new System.Drawing.Point(1168, 628);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(0, 20);
            this.label42.TabIndex = 89;
            // 
            // btnClose
            // 
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.ImageIndex = 3;
            this.btnClose.ImageList = this.imageList1;
            this.btnClose.Location = new System.Drawing.Point(1717, 921);
            this.btnClose.Margin = new System.Windows.Forms.Padding(1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(69, 74);
            this.btnClose.TabIndex = 148;
            this.btnClose.Text = "Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "next-icon.png");
            this.imageList1.Images.SetKeyName(1, "Places-mail-folder-outbox-icon.png");
            this.imageList1.Images.SetKeyName(2, "1436474333_db_update.png");
            this.imageList1.Images.SetKeyName(3, "Actions-edit-delete-icon.png");
            this.imageList1.Images.SetKeyName(4, "1436446147_exit.png");
            this.imageList1.Images.SetKeyName(5, "1436359092_130_ArrowRightDouble.png");
            // 
            // btnExit
            // 
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnExit.ImageIndex = 4;
            this.btnExit.ImageList = this.imageList1;
            this.btnExit.Location = new System.Drawing.Point(1790, 921);
            this.btnExit.Margin = new System.Windows.Forms.Padding(1);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(69, 74);
            this.btnExit.TabIndex = 147;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // LoadGameBtn
            // 
            this.LoadGameBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LoadGameBtn.FlatAppearance.BorderSize = 0;
            this.LoadGameBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.LoadGameBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.LoadGameBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadGameBtn.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LoadGameBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LoadGameBtn.ImageIndex = 1;
            this.LoadGameBtn.ImageList = this.imageList1;
            this.LoadGameBtn.Location = new System.Drawing.Point(272, 13);
            this.LoadGameBtn.Margin = new System.Windows.Forms.Padding(1);
            this.LoadGameBtn.Name = "LoadGameBtn";
            this.LoadGameBtn.Size = new System.Drawing.Size(69, 74);
            this.LoadGameBtn.TabIndex = 146;
            this.LoadGameBtn.Text = "Load Game";
            this.LoadGameBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.LoadGameBtn.UseVisualStyleBackColor = true;
            this.LoadGameBtn.Click += new System.EventHandler(this.LoadGameBtn_Click);
            // 
            // NewGameBtn
            // 
            this.NewGameBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.NewGameBtn.FlatAppearance.BorderSize = 0;
            this.NewGameBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.NewGameBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.NewGameBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NewGameBtn.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NewGameBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.NewGameBtn.ImageIndex = 5;
            this.NewGameBtn.ImageList = this.imageList1;
            this.NewGameBtn.Location = new System.Drawing.Point(357, 11);
            this.NewGameBtn.Margin = new System.Windows.Forms.Padding(1);
            this.NewGameBtn.Name = "NewGameBtn";
            this.NewGameBtn.Size = new System.Drawing.Size(64, 90);
            this.NewGameBtn.TabIndex = 145;
            this.NewGameBtn.Text = "Clear / Next Game";
            this.NewGameBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.NewGameBtn.UseVisualStyleBackColor = true;
            this.NewGameBtn.Click += new System.EventHandler(this.NewGameBtn_Click);
            // 
            // HStarterHandLabel
            // 
            this.HStarterHandLabel.AutoSize = true;
            this.HStarterHandLabel.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.HStarterHandLabel.Location = new System.Drawing.Point(363, 210);
            this.HStarterHandLabel.Name = "HStarterHandLabel";
            this.HStarterHandLabel.Size = new System.Drawing.Size(31, 20);
            this.HStarterHandLabel.TabIndex = 144;
            this.HStarterHandLabel.Text = "L44";
            // 
            // AStarterHandLabel
            // 
            this.AStarterHandLabel.AutoSize = true;
            this.AStarterHandLabel.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AStarterHandLabel.Location = new System.Drawing.Point(363, 147);
            this.AStarterHandLabel.Name = "AStarterHandLabel";
            this.AStarterHandLabel.Size = new System.Drawing.Size(31, 20);
            this.AStarterHandLabel.TabIndex = 143;
            this.AStarterHandLabel.Text = "L11";
            // 
            // homeStarterCB
            // 
            this.homeStarterCB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.homeStarterCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.homeStarterCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.homeStarterCB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.homeStarterCB.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.homeStarterCB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.homeStarterCB.FormattingEnabled = true;
            this.homeStarterCB.Location = new System.Drawing.Point(174, 207);
            this.homeStarterCB.Name = "homeStarterCB";
            this.homeStarterCB.Size = new System.Drawing.Size(186, 27);
            this.homeStarterCB.TabIndex = 3;
            this.homeStarterCB.TextChanged += new System.EventHandler(this.homeStarterCB_TextChanged);
            this.homeStarterCB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.homeStarterCB_KeyDown);
            // 
            // awayStarterCB
            // 
            this.awayStarterCB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.awayStarterCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.awayStarterCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.awayStarterCB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.awayStarterCB.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.awayStarterCB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.awayStarterCB.FormattingEnabled = true;
            this.awayStarterCB.Location = new System.Drawing.Point(174, 144);
            this.awayStarterCB.Name = "awayStarterCB";
            this.awayStarterCB.Size = new System.Drawing.Size(186, 27);
            this.awayStarterCB.TabIndex = 1;
            this.awayStarterCB.TextChanged += new System.EventHandler(this.awayStarterCB_TextChanged);
            this.awayStarterCB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.awayStarterCB_KeyDown);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(71, 211);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 17);
            this.label7.TabIndex = 140;
            this.label7.Text = "Home Starter";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(76, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 17);
            this.label6.TabIndex = 139;
            this.label6.Text = "Away Starter";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bestBetBtn);
            this.groupBox1.Controls.Add(this.radioBtnNBA);
            this.groupBox1.Controls.Add(this.radioBtnMLB);
            this.groupBox1.Location = new System.Drawing.Point(2, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(110, 100);
            this.groupBox1.TabIndex = 138;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pick NBA or MLB";
            // 
            // bestBetBtn
            // 
            this.bestBetBtn.AutoSize = true;
            this.bestBetBtn.Location = new System.Drawing.Point(9, 79);
            this.bestBetBtn.Name = "bestBetBtn";
            this.bestBetBtn.Size = new System.Drawing.Size(85, 17);
            this.bestBetBtn.TabIndex = 138;
            this.bestBetBtn.TabStop = true;
            this.bestBetBtn.Text = "radioButton1";
            this.bestBetBtn.UseVisualStyleBackColor = true;
            this.bestBetBtn.Visible = false;
            // 
            // radioBtnNBA
            // 
            this.radioBtnNBA.AutoSize = true;
            this.radioBtnNBA.Font = new System.Drawing.Font("Arial Narrow", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioBtnNBA.Location = new System.Drawing.Point(10, 25);
            this.radioBtnNBA.Name = "radioBtnNBA";
            this.radioBtnNBA.Size = new System.Drawing.Size(58, 26);
            this.radioBtnNBA.TabIndex = 136;
            this.radioBtnNBA.Text = "NBA";
            this.radioBtnNBA.UseVisualStyleBackColor = true;
            this.radioBtnNBA.CheckedChanged += new System.EventHandler(this.radioBtnNBA_CheckedChanged);
            // 
            // radioBtnMLB
            // 
            this.radioBtnMLB.AutoSize = true;
            this.radioBtnMLB.Font = new System.Drawing.Font("Arial Narrow", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.radioBtnMLB.Location = new System.Drawing.Point(10, 49);
            this.radioBtnMLB.Name = "radioBtnMLB";
            this.radioBtnMLB.Size = new System.Drawing.Size(60, 26);
            this.radioBtnMLB.TabIndex = 137;
            this.radioBtnMLB.Text = "MLB";
            this.radioBtnMLB.UseVisualStyleBackColor = true;
            this.radioBtnMLB.CheckedChanged += new System.EventHandler(this.radioBtnMLB_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.TeamsStatsDGV);
            this.panel1.Controls.Add(this.logoPB);
            this.panel1.Controls.Add(this.homeTeamLogoPB);
            this.panel1.Controls.Add(this.awayTeamLogoPB);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Location = new System.Drawing.Point(566, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(494, 288);
            this.panel1.TabIndex = 128;
            // 
            // TeamsStatsDGV
            // 
            this.TeamsStatsDGV.AllowUserToAddRows = false;
            this.TeamsStatsDGV.AllowUserToDeleteRows = false;
            this.TeamsStatsDGV.AllowUserToResizeColumns = false;
            this.TeamsStatsDGV.AllowUserToResizeRows = false;
            this.TeamsStatsDGV.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TeamsStatsDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TeamsStatsDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.TeamsStatsDGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle94.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle94.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle94.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle94.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle94.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle94.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TeamsStatsDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle94;
            this.TeamsStatsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TeamsStatsDGV.ColumnHeadersVisible = false;
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle95.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle95.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle95.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle95.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle95.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle95.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TeamsStatsDGV.DefaultCellStyle = dataGridViewCellStyle95;
            this.TeamsStatsDGV.EnableHeadersVisualStyles = false;
            this.TeamsStatsDGV.Location = new System.Drawing.Point(147, 169);
            this.TeamsStatsDGV.Name = "TeamsStatsDGV";
            this.TeamsStatsDGV.ReadOnly = true;
            this.TeamsStatsDGV.RowHeadersVisible = false;
            this.TeamsStatsDGV.RowHeadersWidth = 5;
            this.TeamsStatsDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.TeamsStatsDGV.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.TeamsStatsDGV.Size = new System.Drawing.Size(200, 107);
            this.TeamsStatsDGV.TabIndex = 145;
            // 
            // tabControlInjuries
            // 
            this.tabControlInjuries.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tabControlInjuries.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tabControlInjuries.Controls.Add(this.tabPage1);
            this.tabControlInjuries.Controls.Add(this.tabPage2);
            this.tabControlInjuries.Controls.Add(this.tabPage3);
            this.tabControlInjuries.Controls.Add(this.tabPage4);
            this.tabControlInjuries.Controls.Add(this.tabPage5);
            this.tabControlInjuries.Controls.Add(this.tabPage10);
            this.tabControlInjuries.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlInjuries.Location = new System.Drawing.Point(1065, 12);
            this.tabControlInjuries.Name = "tabControlInjuries";
            this.tabControlInjuries.SelectedIndex = 0;
            this.tabControlInjuries.Size = new System.Drawing.Size(790, 316);
            this.tabControlInjuries.TabIndex = 127;
            this.tabControlInjuries.SelectedIndexChanged += new System.EventHandler(this.tabControlInjuries_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gray;
            this.tabPage1.Controls.Add(this.lastXCB);
            this.tabPage1.Controls.Add(this.lastXgamesAvgCB);
            this.tabPage1.Controls.Add(this.updatePFBtn);
            this.tabPage1.Controls.Add(this.updateStartersPB);
            this.tabPage1.Controls.Add(this.progressLabel);
            this.tabPage1.Controls.Add(this.updateStartersBtn);
            this.tabPage1.Controls.Add(this.secondBtoBbtn);
            this.tabPage1.Controls.Add(this.firstBtoBbtn);
            this.tabPage1.Controls.Add(this.vsNODIVBTN);
            this.tabPage1.Controls.Add(this.AfterWBTN);
            this.tabPage1.Controls.Add(this.AfterLBTN);
            this.tabPage1.Controls.Add(this.TrendsLabel);
            this.tabPage1.Controls.Add(this.AvsDivBTN);
            this.tabPage1.Controls.Add(this.StartersStatsDGV);
            this.tabPage1.Controls.Add(this.ViewGamesHomeBtn);
            this.tabPage1.Controls.Add(this.ViewGamesAwayBtn);
            this.tabPage1.Controls.Add(this.L47);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.UpdMLBStrBtn);
            this.tabPage1.Controls.Add(this.teamsDetailedStatsDGV);
            this.tabPage1.Controls.Add(this.getBtn);
            this.tabPage1.Controls.Add(this.GetTeam);
            this.tabPage1.Controls.Add(this.getGamesBtn);
            this.tabPage1.Controls.Add(this.PickGamesBtn);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.awaycheckBox2);
            this.tabPage1.Controls.Add(this.homecheckBox1);
            this.tabPage1.Controls.Add(this.teamBox);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.lineBox2);
            this.tabPage1.Controls.Add(this.lineBox1);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(782, 289);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Get/PickGames";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // updatePFBtn
            // 
            this.updatePFBtn.Location = new System.Drawing.Point(3, 243);
            this.updatePFBtn.Name = "updatePFBtn";
            this.updatePFBtn.Size = new System.Drawing.Size(133, 23);
            this.updatePFBtn.TabIndex = 165;
            this.updatePFBtn.Text = "Update ParkFactors";
            this.updatePFBtn.UseVisualStyleBackColor = true;
            this.updatePFBtn.Click += new System.EventHandler(this.updatePFBtn_Click);
            // 
            // updateStartersPB
            // 
            this.updateStartersPB.Location = new System.Drawing.Point(443, 262);
            this.updateStartersPB.MarqueeAnimationSpeed = 500;
            this.updateStartersPB.Maximum = 11;
            this.updateStartersPB.Name = "updateStartersPB";
            this.updateStartersPB.Size = new System.Drawing.Size(160, 23);
            this.updateStartersPB.Step = 1;
            this.updateStartersPB.TabIndex = 164;
            // 
            // progressLabel
            // 
            this.progressLabel.AutoSize = true;
            this.progressLabel.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressLabel.Location = new System.Drawing.Point(625, 240);
            this.progressLabel.Name = "progressLabel";
            this.progressLabel.Size = new System.Drawing.Size(114, 20);
            this.progressLabel.TabIndex = 163;
            this.progressLabel.Text = "Click to update...";
            // 
            // updateStartersBtn
            // 
            this.updateStartersBtn.Location = new System.Drawing.Point(609, 262);
            this.updateStartersBtn.Name = "updateStartersBtn";
            this.updateStartersBtn.Size = new System.Drawing.Size(163, 23);
            this.updateStartersBtn.TabIndex = 162;
            this.updateStartersBtn.Text = "Update Starters Stats";
            this.updateStartersBtn.UseVisualStyleBackColor = true;
            this.updateStartersBtn.Click += new System.EventHandler(this.updateStartersBtn_Click);
            // 
            // secondBtoBbtn
            // 
            this.secondBtoBbtn.Location = new System.Drawing.Point(688, 216);
            this.secondBtoBbtn.Name = "secondBtoBbtn";
            this.secondBtoBbtn.Size = new System.Drawing.Size(85, 23);
            this.secondBtoBbtn.TabIndex = 161;
            this.secondBtoBbtn.Text = "2nd of B2B";
            this.secondBtoBbtn.UseVisualStyleBackColor = true;
            this.secondBtoBbtn.Click += new System.EventHandler(this.secondBtoBbtn_Click);
            // 
            // firstBtoBbtn
            // 
            this.firstBtoBbtn.Location = new System.Drawing.Point(609, 216);
            this.firstBtoBbtn.Name = "firstBtoBbtn";
            this.firstBtoBbtn.Size = new System.Drawing.Size(75, 23);
            this.firstBtoBbtn.TabIndex = 160;
            this.firstBtoBbtn.Text = "1st of B2B";
            this.firstBtoBbtn.UseVisualStyleBackColor = true;
            this.firstBtoBbtn.Click += new System.EventHandler(this.firstBtoBbtn_Click);
            // 
            // vsNODIVBTN
            // 
            this.vsNODIVBTN.Location = new System.Drawing.Point(688, 158);
            this.vsNODIVBTN.Name = "vsNODIVBTN";
            this.vsNODIVBTN.Size = new System.Drawing.Size(85, 23);
            this.vsNODIVBTN.TabIndex = 159;
            this.vsNODIVBTN.Text = "vs NODIV";
            this.vsNODIVBTN.UseVisualStyleBackColor = true;
            this.vsNODIVBTN.Click += new System.EventHandler(this.vsNODIVBTN_Click);
            // 
            // AfterWBTN
            // 
            this.AfterWBTN.Location = new System.Drawing.Point(609, 187);
            this.AfterWBTN.Name = "AfterWBTN";
            this.AfterWBTN.Size = new System.Drawing.Size(75, 23);
            this.AfterWBTN.TabIndex = 158;
            this.AfterWBTN.Text = "After W";
            this.AfterWBTN.UseVisualStyleBackColor = true;
            this.AfterWBTN.Click += new System.EventHandler(this.AfterWBTN_Click);
            // 
            // AfterLBTN
            // 
            this.AfterLBTN.Location = new System.Drawing.Point(688, 187);
            this.AfterLBTN.Name = "AfterLBTN";
            this.AfterLBTN.Size = new System.Drawing.Size(85, 23);
            this.AfterLBTN.TabIndex = 157;
            this.AfterLBTN.Text = "After L";
            this.AfterLBTN.UseVisualStyleBackColor = true;
            this.AfterLBTN.Click += new System.EventHandler(this.AafterLBTN_Click);
            // 
            // TrendsLabel
            // 
            this.TrendsLabel.AutoSize = true;
            this.TrendsLabel.Location = new System.Drawing.Point(606, 140);
            this.TrendsLabel.Name = "TrendsLabel";
            this.TrendsLabel.Size = new System.Drawing.Size(100, 14);
            this.TrendsLabel.TabIndex = 156;
            this.TrendsLabel.Text = "Trends query\'s:";
            // 
            // AvsDivBTN
            // 
            this.AvsDivBTN.Location = new System.Drawing.Point(609, 159);
            this.AvsDivBTN.Name = "AvsDivBTN";
            this.AvsDivBTN.Size = new System.Drawing.Size(75, 23);
            this.AvsDivBTN.TabIndex = 155;
            this.AvsDivBTN.Text = "vs DIV";
            this.AvsDivBTN.UseVisualStyleBackColor = true;
            this.AvsDivBTN.Click += new System.EventHandler(this.AvsDivBTN_Click);
            // 
            // StartersStatsDGV
            // 
            this.StartersStatsDGV.AllowUserToAddRows = false;
            this.StartersStatsDGV.AllowUserToDeleteRows = false;
            this.StartersStatsDGV.AllowUserToResizeColumns = false;
            this.StartersStatsDGV.AllowUserToResizeRows = false;
            this.StartersStatsDGV.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.StartersStatsDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.StartersStatsDGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle82.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle82.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle82.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle82.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle82.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle82.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.StartersStatsDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle82;
            this.StartersStatsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle83.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle83.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle83.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle83.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle83.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle83.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.StartersStatsDGV.DefaultCellStyle = dataGridViewCellStyle83;
            this.StartersStatsDGV.EnableHeadersVisualStyles = false;
            this.StartersStatsDGV.Location = new System.Drawing.Point(7, 67);
            this.StartersStatsDGV.Name = "StartersStatsDGV";
            this.StartersStatsDGV.ReadOnly = true;
            this.StartersStatsDGV.RowHeadersVisible = false;
            this.StartersStatsDGV.RowHeadersWidth = 5;
            this.StartersStatsDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.StartersStatsDGV.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.StartersStatsDGV.Size = new System.Drawing.Size(769, 66);
            this.StartersStatsDGV.TabIndex = 151;
            // 
            // ViewGamesHomeBtn
            // 
            this.ViewGamesHomeBtn.Location = new System.Drawing.Point(449, 168);
            this.ViewGamesHomeBtn.Name = "ViewGamesHomeBtn";
            this.ViewGamesHomeBtn.Size = new System.Drawing.Size(65, 41);
            this.ViewGamesHomeBtn.TabIndex = 154;
            this.ViewGamesHomeBtn.Text = "View Games";
            this.ViewGamesHomeBtn.UseVisualStyleBackColor = true;
            this.ViewGamesHomeBtn.Click += new System.EventHandler(this.ViewGamesHomeBtn_Click);
            // 
            // ViewGamesAwayBtn
            // 
            this.ViewGamesAwayBtn.Location = new System.Drawing.Point(69, 168);
            this.ViewGamesAwayBtn.Name = "ViewGamesAwayBtn";
            this.ViewGamesAwayBtn.Size = new System.Drawing.Size(65, 41);
            this.ViewGamesAwayBtn.TabIndex = 153;
            this.ViewGamesAwayBtn.Text = "View Games";
            this.ViewGamesAwayBtn.UseVisualStyleBackColor = true;
            this.ViewGamesAwayBtn.Click += new System.EventHandler(this.ViewGamesAwayBtn_Click);
            // 
            // L47
            // 
            this.L47.AutoSize = true;
            this.L47.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.L47.Location = new System.Drawing.Point(84, 268);
            this.L47.Name = "L47";
            this.L47.Size = new System.Drawing.Size(31, 20);
            this.L47.TabIndex = 152;
            this.L47.Text = "L47";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(3, 267);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(82, 20);
            this.label20.TabIndex = 151;
            this.label20.Text = "Park Factor:";
            // 
            // UpdMLBStrBtn
            // 
            this.UpdMLBStrBtn.AutoEllipsis = true;
            this.UpdMLBStrBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.UpdMLBStrBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UpdMLBStrBtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.UpdMLBStrBtn.FlatAppearance.BorderSize = 0;
            this.UpdMLBStrBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.UpdMLBStrBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.UpdMLBStrBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UpdMLBStrBtn.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdMLBStrBtn.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.UpdMLBStrBtn.ImageKey = "1436474333_db_update.png";
            this.UpdMLBStrBtn.ImageList = this.imageList2;
            this.UpdMLBStrBtn.Location = new System.Drawing.Point(537, 159);
            this.UpdMLBStrBtn.Name = "UpdMLBStrBtn";
            this.UpdMLBStrBtn.Size = new System.Drawing.Size(66, 83);
            this.UpdMLBStrBtn.TabIndex = 148;
            this.UpdMLBStrBtn.Text = "Update    Starters";
            this.UpdMLBStrBtn.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.UpdMLBStrBtn.UseVisualStyleBackColor = true;
            this.UpdMLBStrBtn.Click += new System.EventHandler(this.UpdMLBStrBtn_Click);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "1436474333_db_update.png");
            // 
            // teamsDetailedStatsDGV
            // 
            this.teamsDetailedStatsDGV.AllowUserToAddRows = false;
            this.teamsDetailedStatsDGV.AllowUserToDeleteRows = false;
            this.teamsDetailedStatsDGV.AllowUserToResizeColumns = false;
            this.teamsDetailedStatsDGV.AllowUserToResizeRows = false;
            this.teamsDetailedStatsDGV.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.teamsDetailedStatsDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.teamsDetailedStatsDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.teamsDetailedStatsDGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle84.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle84.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle84.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle84.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle84.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle84.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.teamsDetailedStatsDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle84;
            this.teamsDetailedStatsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.teamsDetailedStatsDGV.ColumnHeadersVisible = false;
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle85.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle85.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle85.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle85.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle85.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle85.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.teamsDetailedStatsDGV.DefaultCellStyle = dataGridViewCellStyle85;
            this.teamsDetailedStatsDGV.EnableHeadersVisualStyles = false;
            this.teamsDetailedStatsDGV.Location = new System.Drawing.Point(143, 139);
            this.teamsDetailedStatsDGV.Name = "teamsDetailedStatsDGV";
            this.teamsDetailedStatsDGV.ReadOnly = true;
            this.teamsDetailedStatsDGV.RowHeadersVisible = false;
            this.teamsDetailedStatsDGV.RowHeadersWidth = 5;
            this.teamsDetailedStatsDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.teamsDetailedStatsDGV.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.teamsDetailedStatsDGV.Size = new System.Drawing.Size(295, 136);
            this.teamsDetailedStatsDGV.TabIndex = 146;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.BackColor = System.Drawing.Color.Gray;
            this.tabPage2.Controls.Add(this.richTextBox3);
            this.tabPage2.Controls.Add(this.richTextBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(782, 289);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Injuries";
            // 
            // richTextBox3
            // 
            this.richTextBox3.BackColor = System.Drawing.Color.Silver;
            this.richTextBox3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.richTextBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.richTextBox3.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBox3.Location = new System.Drawing.Point(466, 3);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox3.Size = new System.Drawing.Size(313, 283);
            this.richTextBox3.TabIndex = 119;
            this.richTextBox3.Text = "";
            this.richTextBox3.VScroll += new System.EventHandler(this.richTextBox3_VScroll);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.Silver;
            this.richTextBox1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBox1.Location = new System.Drawing.Point(3, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(313, 283);
            this.richTextBox1.TabIndex = 115;
            this.richTextBox1.Text = "";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Gray;
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.SQDLTB);
            this.tabPage3.Controls.Add(this.dataGridView6);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(782, 289);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Query_Db";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(53, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 19);
            this.label18.TabIndex = 149;
            this.label18.Text = "Query:";
            // 
            // SQDLTB
            // 
            this.SQDLTB.Location = new System.Drawing.Point(85, 6);
            this.SQDLTB.Name = "SQDLTB";
            this.SQDLTB.Size = new System.Drawing.Size(691, 21);
            this.SQDLTB.TabIndex = 137;
            this.SQDLTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SQDLTB_KeyDown);
            // 
            // dataGridView6
            // 
            this.dataGridView6.AllowUserToDeleteRows = false;
            this.dataGridView6.AllowUserToResizeColumns = false;
            this.dataGridView6.AllowUserToResizeRows = false;
            this.dataGridView6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView6.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView6.BackgroundColor = System.Drawing.Color.Silver;
            this.dataGridView6.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle96.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle96.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle96.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle96.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle96.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle96.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView6.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle96;
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle97.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle97.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle97.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle97.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle97.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle97.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView6.DefaultCellStyle = dataGridViewCellStyle97;
            this.dataGridView6.EnableHeadersVisualStyles = false;
            this.dataGridView6.Location = new System.Drawing.Point(3, 32);
            this.dataGridView6.Name = "dataGridView6";
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle98.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(75)))), ((int)(((byte)(65)))));
            dataGridViewCellStyle98.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle98.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle98.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle98.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle98.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView6.RowHeadersDefaultCellStyle = dataGridViewCellStyle98;
            this.dataGridView6.RowHeadersVisible = false;
            this.dataGridView6.RowHeadersWidth = 5;
            this.dataGridView6.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView6.Size = new System.Drawing.Size(773, 254);
            this.dataGridView6.TabIndex = 136;
            this.dataGridView6.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView6_CellContentDoubleClick);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Gray;
            this.tabPage4.Controls.Add(this.useFullNameCB);
            this.tabPage4.Controls.Add(this.lastXGamesSetBtn);
            this.tabPage4.Controls.Add(this.LastXgamesCB);
            this.tabPage4.Controls.Add(this.label49);
            this.tabPage4.Controls.Add(this.paceCB);
            this.tabPage4.Controls.Add(this.ouCheckbox);
            this.tabPage4.Controls.Add(this.button6);
            this.tabPage4.Controls.Add(this.button5);
            this.tabPage4.Controls.Add(this.pictureBox1);
            this.tabPage4.Controls.Add(this.pictureBox18);
            this.tabPage4.Controls.Add(this.dateTimePicker2);
            this.tabPage4.Controls.Add(this.button4);
            this.tabPage4.Controls.Add(this.button1);
            this.tabPage4.Controls.Add(this.matchupsAndScoresDGV);
            this.tabPage4.Controls.Add(this.lastSeasonCB);
            this.tabPage4.Controls.Add(this.thisSeasonCB);
            this.tabPage4.Controls.Add(this.label41);
            this.tabPage4.Controls.Add(this.rankingsCB);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(782, 289);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Live Scores";
            // 
            // useFullNameCB
            // 
            this.useFullNameCB.AutoSize = true;
            this.useFullNameCB.BackColor = System.Drawing.Color.Transparent;
            this.useFullNameCB.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.useFullNameCB.ForeColor = System.Drawing.Color.Black;
            this.useFullNameCB.Location = new System.Drawing.Point(368, 26);
            this.useFullNameCB.Name = "useFullNameCB";
            this.useFullNameCB.Size = new System.Drawing.Size(89, 16);
            this.useFullNameCB.TabIndex = 160;
            this.useFullNameCB.Text = "Use Full Name";
            this.useFullNameCB.UseVisualStyleBackColor = false;
            // 
            // lastXGamesSetBtn
            // 
            this.lastXGamesSetBtn.Location = new System.Drawing.Point(534, 0);
            this.lastXGamesSetBtn.Name = "lastXGamesSetBtn";
            this.lastXGamesSetBtn.Size = new System.Drawing.Size(40, 23);
            this.lastXGamesSetBtn.TabIndex = 159;
            this.lastXGamesSetBtn.Text = "Set";
            this.lastXGamesSetBtn.UseVisualStyleBackColor = true;
            this.lastXGamesSetBtn.Click += new System.EventHandler(this.lastXGamesSetBtn_Click);
            // 
            // LastXgamesCB
            // 
            this.LastXgamesCB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LastXgamesCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.LastXgamesCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.LastXgamesCB.BackColor = System.Drawing.Color.White;
            this.LastXgamesCB.Font = new System.Drawing.Font("Rockwell", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastXgamesCB.ForeColor = System.Drawing.Color.Black;
            this.LastXgamesCB.FormattingEnabled = true;
            this.LastXgamesCB.Items.AddRange(new object[] {
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"});
            this.LastXgamesCB.Location = new System.Drawing.Point(479, 1);
            this.LastXgamesCB.Name = "LastXgamesCB";
            this.LastXgamesCB.Size = new System.Drawing.Size(51, 22);
            this.LastXgamesCB.TabIndex = 158;
            this.LastXgamesCB.Text = "8";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(396, 5);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(75, 14);
            this.label49.TabIndex = 158;
            this.label49.Text = "Select Last:";
            // 
            // paceCB
            // 
            this.paceCB.AutoSize = true;
            this.paceCB.BackColor = System.Drawing.Color.Transparent;
            this.paceCB.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.paceCB.ForeColor = System.Drawing.Color.Black;
            this.paceCB.Location = new System.Drawing.Point(250, 18);
            this.paceCB.Name = "paceCB";
            this.paceCB.Size = new System.Drawing.Size(66, 16);
            this.paceCB.TabIndex = 156;
            this.paceCB.Text = "Use Pace ";
            this.paceCB.UseVisualStyleBackColor = false;
            // 
            // ouCheckbox
            // 
            this.ouCheckbox.AutoSize = true;
            this.ouCheckbox.BackColor = System.Drawing.Color.Transparent;
            this.ouCheckbox.Checked = true;
            this.ouCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ouCheckbox.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ouCheckbox.ForeColor = System.Drawing.Color.Black;
            this.ouCheckbox.Location = new System.Drawing.Point(250, 2);
            this.ouCheckbox.Name = "ouCheckbox";
            this.ouCheckbox.Size = new System.Drawing.Size(107, 16);
            this.ouCheckbox.TabIndex = 155;
            this.ouCheckbox.Text = "Use OU for season";
            this.ouCheckbox.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(650, 5);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 140;
            this.button6.Text = "Insert";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(650, 26);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 139;
            this.button5.Text = "Update";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(578, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(29, 28);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 137;
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, "Previous day games");
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave_2);
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover_2);
            // 
            // pictureBox18
            // 
            this.pictureBox18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox18.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox18.Image")));
            this.pictureBox18.Location = new System.Drawing.Point(608, 20);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(29, 28);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox18.TabIndex = 136;
            this.pictureBox18.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox18, "Next day games");
            this.pictureBox18.Click += new System.EventHandler(this.pictureBox18_Click);
            this.pictureBox18.MouseLeave += new System.EventHandler(this.pictureBox18_MouseLeave);
            this.pictureBox18.MouseHover += new System.EventHandler(this.pictureBox18_MouseHover);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dateTimePicker2.CalendarMonthBackground = System.Drawing.Color.LemonChiffon;
            this.dateTimePicker2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.dateTimePicker2.CustomFormat = "";
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(479, 26);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(94, 21);
            this.dateTimePicker2.TabIndex = 136;
            this.dateTimePicker2.Value = new System.DateTime(2015, 5, 14, 0, 15, 28, 0);
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(0, 21);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 138;
            this.button4.Text = "Scores";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 137;
            this.button1.Text = "Yesterday";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // matchupsAndScoresDGV
            // 
            this.matchupsAndScoresDGV.AllowUserToAddRows = false;
            this.matchupsAndScoresDGV.AllowUserToDeleteRows = false;
            this.matchupsAndScoresDGV.AllowUserToResizeColumns = false;
            this.matchupsAndScoresDGV.AllowUserToResizeRows = false;
            this.matchupsAndScoresDGV.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.matchupsAndScoresDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.matchupsAndScoresDGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.matchupsAndScoresDGV.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle99.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle99.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle99.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle99.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle99.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle99.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.matchupsAndScoresDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle99;
            this.matchupsAndScoresDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.matchupsAndScoresDGV.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle100.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle100.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle100.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle100.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle100.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle100.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.matchupsAndScoresDGV.DefaultCellStyle = dataGridViewCellStyle100;
            this.matchupsAndScoresDGV.EnableHeadersVisualStyles = false;
            this.matchupsAndScoresDGV.Location = new System.Drawing.Point(6, 50);
            this.matchupsAndScoresDGV.Name = "matchupsAndScoresDGV";
            dataGridViewCellStyle101.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle101.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(75)))), ((int)(((byte)(65)))));
            dataGridViewCellStyle101.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle101.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle101.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle101.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle101.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.matchupsAndScoresDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle101;
            this.matchupsAndScoresDGV.RowHeadersVisible = false;
            this.matchupsAndScoresDGV.RowHeadersWidth = 5;
            this.matchupsAndScoresDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.matchupsAndScoresDGV.Size = new System.Drawing.Size(770, 238);
            this.matchupsAndScoresDGV.TabIndex = 136;
            this.matchupsAndScoresDGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.matchupsAndScoresDGV_CellClick);
            this.matchupsAndScoresDGV.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView8_CellMouseDoubleClick);
            this.matchupsAndScoresDGV.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.matchupsAndScoresDGV_CellMouseEnter);
            // 
            // rankingsCB
            // 
            this.rankingsCB.AutoSize = true;
            this.rankingsCB.BackColor = System.Drawing.Color.Transparent;
            this.rankingsCB.Checked = true;
            this.rankingsCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.rankingsCB.Font = new System.Drawing.Font("Cambria", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rankingsCB.ForeColor = System.Drawing.Color.Black;
            this.rankingsCB.Location = new System.Drawing.Point(250, 35);
            this.rankingsCB.Name = "rankingsCB";
            this.rankingsCB.Size = new System.Drawing.Size(86, 16);
            this.rankingsCB.TabIndex = 157;
            this.rankingsCB.Text = "Use Rankings";
            this.rankingsCB.UseVisualStyleBackColor = false;
            // 
            // tabPage5
            // 
            this.tabPage5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tabPage5.BackgroundImage")));
            this.tabPage5.Controls.Add(this.dataGridView7);
            this.tabPage5.Location = new System.Drawing.Point(4, 23);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(782, 289);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Standings";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dataGridView7
            // 
            this.dataGridView7.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView7.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView7.BackgroundColor = System.Drawing.Color.Silver;
            this.dataGridView7.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle102.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle102.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle102.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle102.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle102.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle102.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle102.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView7.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle102;
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle103.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle103.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle103.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle103.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle103.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle103.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView7.DefaultCellStyle = dataGridViewCellStyle103;
            this.dataGridView7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView7.EnableHeadersVisualStyles = false;
            this.dataGridView7.Location = new System.Drawing.Point(3, 3);
            this.dataGridView7.Name = "dataGridView7";
            dataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle104.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(75)))), ((int)(((byte)(65)))));
            dataGridViewCellStyle104.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle104.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle104.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle104.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle104.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView7.RowHeadersDefaultCellStyle = dataGridViewCellStyle104;
            this.dataGridView7.RowHeadersVisible = false;
            this.dataGridView7.RowHeadersWidth = 5;
            this.dataGridView7.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView7.Size = new System.Drawing.Size(776, 283);
            this.dataGridView7.TabIndex = 136;
            // 
            // tabPage10
            // 
            this.tabPage10.BackgroundImage = global::Oklade.Properties.Resources.LightGrey_linen;
            this.tabPage10.Controls.Add(this.homeNonDivGamesBtn);
            this.tabPage10.Controls.Add(this.awayNonDivGamesBtn);
            this.tabPage10.Controls.Add(this.vsnoDIVDGV);
            this.tabPage10.Controls.Add(this.homeDivGamesbtn);
            this.tabPage10.Controls.Add(this.awayDivGamesBtn);
            this.tabPage10.Controls.Add(this.label47);
            this.tabPage10.Controls.Add(this.label48);
            this.tabPage10.Controls.Add(this.vsDIVDGV);
            this.tabPage10.Location = new System.Drawing.Point(4, 23);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(782, 289);
            this.tabPage10.TabIndex = 5;
            this.tabPage10.Text = "vsDivision";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // homeNonDivGamesBtn
            // 
            this.homeNonDivGamesBtn.Location = new System.Drawing.Point(356, 170);
            this.homeNonDivGamesBtn.Name = "homeNonDivGamesBtn";
            this.homeNonDivGamesBtn.Size = new System.Drawing.Size(65, 41);
            this.homeNonDivGamesBtn.TabIndex = 164;
            this.homeNonDivGamesBtn.Text = "View Games";
            this.homeNonDivGamesBtn.UseVisualStyleBackColor = true;
            // 
            // awayNonDivGamesBtn
            // 
            this.awayNonDivGamesBtn.Location = new System.Drawing.Point(33, 172);
            this.awayNonDivGamesBtn.Name = "awayNonDivGamesBtn";
            this.awayNonDivGamesBtn.Size = new System.Drawing.Size(65, 41);
            this.awayNonDivGamesBtn.TabIndex = 163;
            this.awayNonDivGamesBtn.Text = "View Games";
            this.awayNonDivGamesBtn.UseVisualStyleBackColor = true;
            // 
            // vsnoDIVDGV
            // 
            this.vsnoDIVDGV.AllowUserToAddRows = false;
            this.vsnoDIVDGV.AllowUserToDeleteRows = false;
            this.vsnoDIVDGV.AllowUserToResizeColumns = false;
            this.vsnoDIVDGV.AllowUserToResizeRows = false;
            this.vsnoDIVDGV.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.vsnoDIVDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.vsnoDIVDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.vsnoDIVDGV.BackgroundColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle105.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle105.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle105.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle105.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle105.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle105.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle105.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.vsnoDIVDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle105;
            this.vsnoDIVDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vsnoDIVDGV.ColumnHeadersVisible = false;
            dataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle106.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle106.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle106.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle106.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle106.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle106.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.vsnoDIVDGV.DefaultCellStyle = dataGridViewCellStyle106;
            this.vsnoDIVDGV.EnableHeadersVisualStyles = false;
            this.vsnoDIVDGV.Location = new System.Drawing.Point(125, 170);
            this.vsnoDIVDGV.Name = "vsnoDIVDGV";
            this.vsnoDIVDGV.ReadOnly = true;
            this.vsnoDIVDGV.RowHeadersVisible = false;
            this.vsnoDIVDGV.RowHeadersWidth = 5;
            this.vsnoDIVDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsnoDIVDGV.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.vsnoDIVDGV.Size = new System.Drawing.Size(200, 109);
            this.vsnoDIVDGV.TabIndex = 160;
            // 
            // homeDivGamesbtn
            // 
            this.homeDivGamesbtn.Location = new System.Drawing.Point(356, 34);
            this.homeDivGamesbtn.Name = "homeDivGamesbtn";
            this.homeDivGamesbtn.Size = new System.Drawing.Size(65, 41);
            this.homeDivGamesbtn.TabIndex = 159;
            this.homeDivGamesbtn.Text = "View Games";
            this.homeDivGamesbtn.UseVisualStyleBackColor = true;
            // 
            // awayDivGamesBtn
            // 
            this.awayDivGamesBtn.Location = new System.Drawing.Point(33, 34);
            this.awayDivGamesBtn.Name = "awayDivGamesBtn";
            this.awayDivGamesBtn.Size = new System.Drawing.Size(65, 41);
            this.awayDivGamesBtn.TabIndex = 158;
            this.awayDivGamesBtn.Text = "View Games";
            this.awayDivGamesBtn.UseVisualStyleBackColor = true;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(217, 10);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(31, 20);
            this.label47.TabIndex = 157;
            this.label47.Text = "L47";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(86, 10);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(31, 20);
            this.label48.TabIndex = 156;
            this.label48.Text = "L48";
            // 
            // vsDIVDGV
            // 
            this.vsDIVDGV.AllowUserToAddRows = false;
            this.vsDIVDGV.AllowUserToDeleteRows = false;
            this.vsDIVDGV.AllowUserToResizeColumns = false;
            this.vsDIVDGV.AllowUserToResizeRows = false;
            this.vsDIVDGV.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.vsDIVDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.vsDIVDGV.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.vsDIVDGV.BackgroundColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle107.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle107.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle107.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle107.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle107.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle107.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle107.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.vsDIVDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle107;
            this.vsDIVDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vsDIVDGV.ColumnHeadersVisible = false;
            dataGridViewCellStyle108.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle108.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle108.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle108.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle108.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle108.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle108.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.vsDIVDGV.DefaultCellStyle = dataGridViewCellStyle108;
            this.vsDIVDGV.EnableHeadersVisualStyles = false;
            this.vsDIVDGV.Location = new System.Drawing.Point(125, 32);
            this.vsDIVDGV.Name = "vsDIVDGV";
            this.vsDIVDGV.ReadOnly = true;
            this.vsDIVDGV.RowHeadersVisible = false;
            this.vsDIVDGV.RowHeadersWidth = 5;
            this.vsDIVDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsDIVDGV.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.vsDIVDGV.Size = new System.Drawing.Size(200, 109);
            this.vsDIVDGV.TabIndex = 155;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label43.Location = new System.Drawing.Point(19, 790);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(0, 20);
            this.label43.TabIndex = 118;
            // 
            // finishandCalcPB
            // 
            this.finishandCalcPB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.finishandCalcPB.Image = ((System.Drawing.Image)(resources.GetObject("finishandCalcPB.Image")));
            this.finishandCalcPB.Location = new System.Drawing.Point(407, 243);
            this.finishandCalcPB.Name = "finishandCalcPB";
            this.finishandCalcPB.Size = new System.Drawing.Size(50, 50);
            this.finishandCalcPB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.finishandCalcPB.TabIndex = 110;
            this.finishandCalcPB.TabStop = false;
            this.toolTip1.SetToolTip(this.finishandCalcPB, "Finish&Calculate");
            this.finishandCalcPB.Click += new System.EventHandler(this.finishandCalcPB_Click);
            this.finishandCalcPB.MouseLeave += new System.EventHandler(this.pictureBox14_MouseLeave);
            this.finishandCalcPB.MouseHover += new System.EventHandler(this.pictureBox14_MouseHover);
            // 
            // pictureBox13
            // 
            this.pictureBox13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(1124, 920);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(59, 55);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox13.TabIndex = 109;
            this.pictureBox13.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox13, "Results viewer");
            this.pictureBox13.Click += new System.EventHandler(this.pictureBox13_Click);
            this.pictureBox13.MouseLeave += new System.EventHandler(this.pictureBox13_MouseLeave);
            this.pictureBox13.MouseHover += new System.EventHandler(this.pictureBox13_MouseHover);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(1011, 924);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(54, 50);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox12.TabIndex = 108;
            this.pictureBox12.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox12, "Save changes");
            this.pictureBox12.Click += new System.EventHandler(this.pictureBox12_Click);
            this.pictureBox12.MouseLeave += new System.EventHandler(this.pictureBox12_MouseLeave);
            this.pictureBox12.MouseHover += new System.EventHandler(this.pictureBox12_MouseHover);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(958, 924);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(54, 50);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox11.TabIndex = 107;
            this.pictureBox11.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox11, "Edit teams");
            this.pictureBox11.Click += new System.EventHandler(this.EditTeamsbtn_Click);
            this.pictureBox11.MouseLeave += new System.EventHandler(this.pictureBox11_MouseLeave);
            this.pictureBox11.MouseHover += new System.EventHandler(this.pictureBox11_MouseHover);
            // 
            // pictureBox9
            // 
            this.pictureBox9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(1064, 924);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(54, 50);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox9.TabIndex = 105;
            this.pictureBox9.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox9, "Delete selected analysis");
            this.pictureBox9.Click += new System.EventHandler(this.deleteBtn_Click);
            this.pictureBox9.MouseLeave += new System.EventHandler(this.pictureBox9_MouseLeave);
            this.pictureBox9.MouseHover += new System.EventHandler(this.pictureBox9_MouseHover);
            // 
            // previousGamePB
            // 
            this.previousGamePB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.previousGamePB.Image = ((System.Drawing.Image)(resources.GetObject("previousGamePB.Image")));
            this.previousGamePB.Location = new System.Drawing.Point(176, 67);
            this.previousGamePB.Name = "previousGamePB";
            this.previousGamePB.Size = new System.Drawing.Size(29, 28);
            this.previousGamePB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.previousGamePB.TabIndex = 103;
            this.previousGamePB.TabStop = false;
            this.toolTip1.SetToolTip(this.previousGamePB, "Previous game");
            this.previousGamePB.Click += new System.EventHandler(this.previousGamePB_Click);
            this.previousGamePB.MouseLeave += new System.EventHandler(this.pictureBox7_MouseLeave);
            this.previousGamePB.MouseHover += new System.EventHandler(this.pictureBox7_MouseHover);
            // 
            // nextGamePB
            // 
            this.nextGamePB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nextGamePB.Image = ((System.Drawing.Image)(resources.GetObject("nextGamePB.Image")));
            this.nextGamePB.Location = new System.Drawing.Point(209, 67);
            this.nextGamePB.Name = "nextGamePB";
            this.nextGamePB.Size = new System.Drawing.Size(29, 28);
            this.nextGamePB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.nextGamePB.TabIndex = 102;
            this.nextGamePB.TabStop = false;
            this.toolTip1.SetToolTip(this.nextGamePB, "Next game");
            this.nextGamePB.Click += new System.EventHandler(this.nextGamePB_Click);
            this.nextGamePB.MouseLeave += new System.EventHandler(this.pictureBox6_MouseLeave);
            this.nextGamePB.MouseHover += new System.EventHandler(this.pictureBox6_MouseHover);
            // 
            // updateResultsPB
            // 
            this.updateResultsPB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.updateResultsPB.Image = ((System.Drawing.Image)(resources.GetObject("updateResultsPB.Image")));
            this.updateResultsPB.Location = new System.Drawing.Point(343, 243);
            this.updateResultsPB.Name = "updateResultsPB";
            this.updateResultsPB.Size = new System.Drawing.Size(62, 49);
            this.updateResultsPB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.updateResultsPB.TabIndex = 101;
            this.updateResultsPB.TabStop = false;
            this.toolTip1.SetToolTip(this.updateResultsPB, "Update Total&Handicap");
            this.updateResultsPB.Click += new System.EventHandler(this.updateResultsPB_Click);
            this.updateResultsPB.MouseLeave += new System.EventHandler(this.updateResultsPB_MouseLeave);
            this.updateResultsPB.MouseHover += new System.EventHandler(this.updateResultsPB_MouseHover);
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1205, 928);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(0, 13);
            this.label13.TabIndex = 100;
            // 
            // prevPB
            // 
            this.prevPB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.prevPB.Image = ((System.Drawing.Image)(resources.GetObject("prevPB.Image")));
            this.prevPB.Location = new System.Drawing.Point(111, 368);
            this.prevPB.Name = "prevPB";
            this.prevPB.Size = new System.Drawing.Size(29, 28);
            this.prevPB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.prevPB.TabIndex = 113;
            this.prevPB.TabStop = false;
            this.toolTip1.SetToolTip(this.prevPB, "Previous day games");
            this.prevPB.Click += new System.EventHandler(this.pictureBox16_Click);
            this.prevPB.MouseLeave += new System.EventHandler(this.pictureBox16_MouseLeave);
            this.prevPB.MouseHover += new System.EventHandler(this.pictureBox16_MouseHover);
            // 
            // nextPB
            // 
            this.nextPB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.nextPB.Image = ((System.Drawing.Image)(resources.GetObject("nextPB.Image")));
            this.nextPB.Location = new System.Drawing.Point(141, 368);
            this.nextPB.Name = "nextPB";
            this.nextPB.Size = new System.Drawing.Size(29, 28);
            this.nextPB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.nextPB.TabIndex = 112;
            this.nextPB.TabStop = false;
            this.toolTip1.SetToolTip(this.nextPB, "Next day games");
            this.nextPB.Click += new System.EventHandler(this.pictureBox17_Click);
            this.nextPB.MouseLeave += new System.EventHandler(this.pictureBox17_MouseLeave);
            this.nextPB.MouseHover += new System.EventHandler(this.pictureBox17_MouseHover);
            // 
            // dataGridView5
            // 
            this.dataGridView5.AllowUserToDeleteRows = false;
            this.dataGridView5.AllowUserToResizeColumns = false;
            this.dataGridView5.AllowUserToResizeRows = false;
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView5.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle86;
            this.dataGridView5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dataGridView5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView5.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle87.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle87.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle87.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle87.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle87.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle87.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle87;
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle88.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle88.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle88.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle88.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle88.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle88.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView5.DefaultCellStyle = dataGridViewCellStyle88;
            this.dataGridView5.EnableHeadersVisualStyles = false;
            this.dataGridView5.Location = new System.Drawing.Point(11, 821);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.ReadOnly = true;
            dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle89.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle89.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle89.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle89.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle89.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView5.RowHeadersDefaultCellStyle = dataGridViewCellStyle89;
            this.dataGridView5.RowHeadersVisible = false;
            this.dataGridView5.RowHeadersWidth = 5;
            this.dataGridView5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView5.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView5.Size = new System.Drawing.Size(911, 150);
            this.dataGridView5.TabIndex = 117;
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(1320, 385);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(55, 13);
            this.linkLabel1.TabIndex = 120;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "linkLabel1";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(1374, 395);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(55, 13);
            this.linkLabel2.TabIndex = 121;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "linkLabel2";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel3
            // 
            this.linkLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Location = new System.Drawing.Point(1374, 390);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(55, 13);
            this.linkLabel3.TabIndex = 122;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "linkLabel3";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // linkLabel4
            // 
            this.linkLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.Location = new System.Drawing.Point(1341, 409);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(55, 13);
            this.linkLabel4.TabIndex = 123;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "linkLabel4";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // linkLabel7
            // 
            this.linkLabel7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel7.AutoSize = true;
            this.linkLabel7.Location = new System.Drawing.Point(1226, 437);
            this.linkLabel7.Name = "linkLabel7";
            this.linkLabel7.Size = new System.Drawing.Size(55, 13);
            this.linkLabel7.TabIndex = 126;
            this.linkLabel7.TabStop = true;
            this.linkLabel7.Text = "linkLabel7";
            this.linkLabel7.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel7_LinkClicked);
            // 
            // linkLabel6
            // 
            this.linkLabel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel6.AutoSize = true;
            this.linkLabel6.Location = new System.Drawing.Point(1207, 395);
            this.linkLabel6.Name = "linkLabel6";
            this.linkLabel6.Size = new System.Drawing.Size(55, 13);
            this.linkLabel6.TabIndex = 125;
            this.linkLabel6.TabStop = true;
            this.linkLabel6.Text = "linkLabel6";
            this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel6_LinkClicked);
            // 
            // linkLabel5
            // 
            this.linkLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.Location = new System.Drawing.Point(1208, 434);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(55, 13);
            this.linkLabel5.TabIndex = 124;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "linkLabel5";
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // linkLabel14
            // 
            this.linkLabel14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel14.AutoSize = true;
            this.linkLabel14.Location = new System.Drawing.Point(1279, 390);
            this.linkLabel14.Name = "linkLabel14";
            this.linkLabel14.Size = new System.Drawing.Size(61, 13);
            this.linkLabel14.TabIndex = 135;
            this.linkLabel14.TabStop = true;
            this.linkLabel14.Text = "linkLabel14";
            this.linkLabel14.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel14_LinkClicked);
            // 
            // linkLabel13
            // 
            this.linkLabel13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel13.AutoSize = true;
            this.linkLabel13.Location = new System.Drawing.Point(1193, 378);
            this.linkLabel13.Name = "linkLabel13";
            this.linkLabel13.Size = new System.Drawing.Size(61, 13);
            this.linkLabel13.TabIndex = 134;
            this.linkLabel13.TabStop = true;
            this.linkLabel13.Text = "linkLabel13";
            this.linkLabel13.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel13_LinkClicked);
            // 
            // linkLabel12
            // 
            this.linkLabel12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel12.AutoSize = true;
            this.linkLabel12.Location = new System.Drawing.Point(1223, 450);
            this.linkLabel12.Name = "linkLabel12";
            this.linkLabel12.Size = new System.Drawing.Size(61, 13);
            this.linkLabel12.TabIndex = 133;
            this.linkLabel12.TabStop = true;
            this.linkLabel12.Text = "linkLabel12";
            this.linkLabel12.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel12_LinkClicked);
            // 
            // linkLabel11
            // 
            this.linkLabel11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel11.AutoSize = true;
            this.linkLabel11.Location = new System.Drawing.Point(1341, 402);
            this.linkLabel11.Name = "linkLabel11";
            this.linkLabel11.Size = new System.Drawing.Size(61, 13);
            this.linkLabel11.TabIndex = 132;
            this.linkLabel11.TabStop = true;
            this.linkLabel11.Text = "linkLabel11";
            this.linkLabel11.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel11_LinkClicked);
            // 
            // linkLabel10
            // 
            this.linkLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel10.AutoSize = true;
            this.linkLabel10.Location = new System.Drawing.Point(1261, 385);
            this.linkLabel10.Name = "linkLabel10";
            this.linkLabel10.Size = new System.Drawing.Size(61, 13);
            this.linkLabel10.TabIndex = 131;
            this.linkLabel10.TabStop = true;
            this.linkLabel10.Text = "linkLabel10";
            this.linkLabel10.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel10_LinkClicked);
            // 
            // linkLabel9
            // 
            this.linkLabel9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel9.AutoSize = true;
            this.linkLabel9.Location = new System.Drawing.Point(1189, 404);
            this.linkLabel9.Name = "linkLabel9";
            this.linkLabel9.Size = new System.Drawing.Size(55, 13);
            this.linkLabel9.TabIndex = 130;
            this.linkLabel9.TabStop = true;
            this.linkLabel9.Text = "linkLabel9";
            this.linkLabel9.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel9_LinkClicked);
            // 
            // linkLabel8
            // 
            this.linkLabel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.linkLabel8.AutoSize = true;
            this.linkLabel8.Location = new System.Drawing.Point(1181, 358);
            this.linkLabel8.Name = "linkLabel8";
            this.linkLabel8.Size = new System.Drawing.Size(55, 13);
            this.linkLabel8.TabIndex = 129;
            this.linkLabel8.TabStop = true;
            this.linkLabel8.Text = "linkLabel8";
            this.linkLabel8.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
            // 
            // HomeTeamTabControl
            // 
            this.HomeTeamTabControl.Controls.Add(this.tabPage8);
            this.HomeTeamTabControl.Controls.Add(this.tabPage9);
            this.HomeTeamTabControl.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.HomeTeamTabControl.Location = new System.Drawing.Point(941, 625);
            this.HomeTeamTabControl.Name = "HomeTeamTabControl";
            this.HomeTeamTabControl.SelectedIndex = 0;
            this.HomeTeamTabControl.Size = new System.Drawing.Size(929, 295);
            this.HomeTeamTabControl.TabIndex = 150;
            this.HomeTeamTabControl.SelectedIndexChanged += new System.EventHandler(this.HomeTeamTabControl_SelectedIndexChanged_1);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.HTeamL50DGV);
            this.tabPage8.Location = new System.Drawing.Point(4, 25);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(921, 266);
            this.tabPage8.TabIndex = 0;
            this.tabPage8.Text = "Home Team L50";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // HTeamL50DGV
            // 
            this.HTeamL50DGV.AllowUserToDeleteRows = false;
            this.HTeamL50DGV.AllowUserToResizeColumns = false;
            this.HTeamL50DGV.AllowUserToResizeRows = false;
            dataGridViewCellStyle109.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.HTeamL50DGV.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle109;
            this.HTeamL50DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.HTeamL50DGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle110.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle110.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle110.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle110.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle110.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle110.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HTeamL50DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle110;
            this.HTeamL50DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle111.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle111.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle111.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle111.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle111.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle111.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.HTeamL50DGV.DefaultCellStyle = dataGridViewCellStyle111;
            this.HTeamL50DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HTeamL50DGV.EnableHeadersVisualStyles = false;
            this.HTeamL50DGV.Location = new System.Drawing.Point(3, 3);
            this.HTeamL50DGV.Name = "HTeamL50DGV";
            this.HTeamL50DGV.ReadOnly = true;
            dataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle112.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle112.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle112.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle112.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle112.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle112.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HTeamL50DGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle112;
            this.HTeamL50DGV.RowHeadersVisible = false;
            this.HTeamL50DGV.RowHeadersWidth = 5;
            this.HTeamL50DGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.HTeamL50DGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.HTeamL50DGV.Size = new System.Drawing.Size(915, 260);
            this.HTeamL50DGV.TabIndex = 87;
            this.HTeamL50DGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellContentClick);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.HStarterL50DGV);
            this.tabPage9.Location = new System.Drawing.Point(4, 25);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(921, 266);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Home Starter L10";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // HStarterL50DGV
            // 
            this.HStarterL50DGV.AllowUserToDeleteRows = false;
            this.HStarterL50DGV.AllowUserToResizeColumns = false;
            this.HStarterL50DGV.AllowUserToResizeRows = false;
            this.HStarterL50DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.HStarterL50DGV.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle113.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle113.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle113.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle113.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle113.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle113.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle113.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.HStarterL50DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle113;
            this.HStarterL50DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle114.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle114.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle114.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle114.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle114.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle114.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle114.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.HStarterL50DGV.DefaultCellStyle = dataGridViewCellStyle114;
            this.HStarterL50DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HStarterL50DGV.EnableHeadersVisualStyles = false;
            this.HStarterL50DGV.Location = new System.Drawing.Point(3, 3);
            this.HStarterL50DGV.Name = "HStarterL50DGV";
            this.HStarterL50DGV.ReadOnly = true;
            this.HStarterL50DGV.RowHeadersVisible = false;
            this.HStarterL50DGV.RowHeadersWidth = 5;
            this.HStarterL50DGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.HStarterL50DGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.HStarterL50DGV.Size = new System.Drawing.Size(915, 260);
            this.HStarterL50DGV.TabIndex = 88;
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 2000;
            this.toolTip1.AutoPopDelay = 2000;
            this.toolTip1.InitialDelay = 1700;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 400;
            this.toolTip1.ShowAlways = true;
            this.toolTip1.UseAnimation = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lastXgamesAvgCB
            // 
            this.lastXgamesAvgCB.AutoSize = true;
            this.lastXgamesAvgCB.Location = new System.Drawing.Point(7, 140);
            this.lastXgamesAvgCB.Name = "lastXgamesAvgCB";
            this.lastXgamesAvgCB.Size = new System.Drawing.Size(132, 18);
            this.lastXgamesAvgCB.TabIndex = 166;
            this.lastXgamesAvgCB.Text = "Use Last X Games";
            this.lastXgamesAvgCB.UseVisualStyleBackColor = true;
            this.lastXgamesAvgCB.CheckedChanged += new System.EventHandler(this.lastXgamesAvgCB_CheckedChanged);
            // 
            // lastXCB
            // 
            this.lastXCB.FormattingEnabled = true;
            this.lastXCB.Items.AddRange(new object[] {
            "3",
            "5",
            "10",
            "15",
            "20",
            "25",
            "30"});
            this.lastXCB.Location = new System.Drawing.Point(7, 162);
            this.lastXCB.Name = "lastXCB";
            this.lastXCB.Size = new System.Drawing.Size(42, 22);
            this.lastXCB.TabIndex = 167;
            this.lastXCB.Text = "5";
            this.lastXCB.SelectedValueChanged += new System.EventHandler(this.lastXCB_SelectedValueChanged);
            // 
            // Analyzer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Gray;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1900, 1003);
            this.Controls.Add(this.groupBox2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Analyzer";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "NBA & MLB ANALYZER";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form2_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Analyzer_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.DGVAnalysisResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.awayTeamLogoPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homeTeamLogoPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logoPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutoProcessBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.results2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.results1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.AwayTeamTabControl.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ATeamL50DGV)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AStarterL50DGV)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeamsStatsDGV)).EndInit();
            this.tabControlInjuries.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartersStatsDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamsDetailedStatsDGV)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchupsAndScoresDGV)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsnoDIVDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsDIVDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finishandCalcPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.previousGamePB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextGamePB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateResultsPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prevPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nextPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.HomeTeamTabControl.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HTeamL50DGV)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HStarterL50DGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
     
        private System.Windows.Forms.TextBox IDoklBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox TotalBox;
        public System.Windows.Forms.TextBox HandicapBox;
        private System.Windows.Forms.DataGridView DGVAnalysisResults;
        private System.Windows.Forms.RadioButton hteamradio;
        private System.Windows.Forms.RadioButton ateamradio;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button getGamesBtn;
        private System.Windows.Forms.TextBox teamBox;
        private System.Windows.Forms.CheckBox homecheckBox1;
        private System.Windows.Forms.CheckBox awaycheckBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox lineBox1;
        private System.Windows.Forms.TextBox lineBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button getBtn;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button GetTeam;
        private System.Windows.Forms.ComboBox AwayTeamCB;
        private System.Windows.Forms.ComboBox HomeTeamCB;
        private System.Windows.Forms.PictureBox awayTeamLogoPB;
        private System.Windows.Forms.PictureBox homeTeamLogoPB;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.PictureBox logoPB;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label labelAwayAvg;
        private System.Windows.Forms.Label labelHomeAvg;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label labelTotalAvg;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.CheckBox lastSeasonCB;
        private System.Windows.Forms.CheckBox thisSeasonCB;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button PickGamesBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView HTeamL50DGV;
        private System.Windows.Forms.DataGridView ATeamL50DGV;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox results1;
        private System.Windows.Forms.PictureBox results2;
        private System.Windows.Forms.PictureBox AutoProcessBtn;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox updateResultsPB;
        private System.Windows.Forms.PictureBox nextGamePB;
        private System.Windows.Forms.PictureBox previousGamePB;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox finishandCalcPB;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.PictureBox prevPB;
        private System.Windows.Forms.PictureBox nextPB;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.LinkLabel linkLabel7;
        private System.Windows.Forms.LinkLabel linkLabel6;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.TabControl tabControlInjuries;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.LinkLabel linkLabel14;
        private System.Windows.Forms.LinkLabel linkLabel13;
        private System.Windows.Forms.LinkLabel linkLabel12;
        private System.Windows.Forms.LinkLabel linkLabel11;
        private System.Windows.Forms.LinkLabel linkLabel10;
        private System.Windows.Forms.LinkLabel linkLabel9;
        private System.Windows.Forms.LinkLabel linkLabel8;
        private System.Windows.Forms.TabPage tabPage3;
        public System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.DataGridView matchupsAndScoresDGV;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button button5;
        public System.Windows.Forms.RadioButton radioBtnMLB;
        public System.Windows.Forms.RadioButton radioBtnNBA;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox homeStarterCB;
        private System.Windows.Forms.ComboBox awayStarterCB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label HStarterHandLabel;
        private System.Windows.Forms.Label AStarterHandLabel;
        private System.Windows.Forms.DataGridView TeamsStatsDGV;
        private System.Windows.Forms.DataGridView teamsDetailedStatsDGV;
        private System.Windows.Forms.Button UpdMLBStrBtn;
        private System.Windows.Forms.Button NewGameBtn;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button LoadGameBtn;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox SQDLTB;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabControl AwayTeamTabControl;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.DataGridView AStarterL50DGV;
        private System.Windows.Forms.TabControl HomeTeamTabControl;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView HStarterL50DGV;
        private System.Windows.Forms.Label L47;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button ViewGamesHomeBtn;
        private System.Windows.Forms.Button ViewGamesAwayBtn;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DataGridView StartersStatsDGV;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.Button homeNonDivGamesBtn;
        private System.Windows.Forms.Button awayNonDivGamesBtn;
        private System.Windows.Forms.DataGridView vsnoDIVDGV;
        private System.Windows.Forms.Button homeDivGamesbtn;
        private System.Windows.Forms.Button awayDivGamesBtn;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.DataGridView vsDIVDGV;
        private System.Windows.Forms.CheckBox ouCheckbox;
        private System.Windows.Forms.Button SaveandRecalcbtn;
        private System.Windows.Forms.Button HandicapMinusButton;
        private System.Windows.Forms.Button TotalMinusButton;
        private System.Windows.Forms.Button HandicapPlusButton;
        private System.Windows.Forms.Button TotalPlusButton;
        private System.Windows.Forms.CheckBox BetProcessedCB;
        private System.Windows.Forms.CheckBox ToBetOnCB;
        private System.Windows.Forms.CheckBox editedCB;
        private System.Windows.Forms.CheckBox paceCB;
        private System.Windows.Forms.CheckBox rankingsCB;
        private System.Windows.Forms.ComboBox LastXgamesCB;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button DelRowBtn;
        private System.Windows.Forms.Label TrendsLabel;
        private System.Windows.Forms.Button AvsDivBTN;
        private System.Windows.Forms.Button AfterLBTN;
        private System.Windows.Forms.Button vsNODIVBTN;
        private System.Windows.Forms.Button AfterWBTN;
        private System.Windows.Forms.Button secondBtoBbtn;
        private System.Windows.Forms.Button firstBtoBbtn;
        private System.Windows.Forms.Button lastXGamesSetBtn;
        private System.Windows.Forms.Button updateStartersBtn;
        private System.Windows.Forms.Label progressLabel;
        private System.Windows.Forms.ProgressBar updateStartersPB;
        private System.Windows.Forms.RadioButton bestBetBtn;
        private System.Windows.Forms.Button updatePFBtn;
        private System.Windows.Forms.CheckBox useFullNameCB;
        private System.Windows.Forms.CheckBox lastXgamesAvgCB;
        private System.Windows.Forms.ComboBox lastXCB;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Oklade.SharedMethodsCollection
{
    class TeamLogos
    {
        public void LoadTeamsLogos(PictureBox pictureBox, string teamValue)
        {
            if (pictureBox != null)
            {
                pictureBox.InvokeIfRequired(pb =>
                {
                    switch (teamValue)
                    {
                        case "Atlanta Hawks":

                            pictureBox.Show();
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/f/fe/Atlanta_Hawks_2015_Logo.svg/739px-Atlanta_Hawks_2015_Logo.svg.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Boston Celtics":

                            pictureBox.Show();
                            //  pictureBox.Image = Oklade.Properties.Resources.Boston_Celtics_Logo;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/8/8f/Boston_Celtics.svg/250px-Boston_Celtics.svg.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Brooklyn Nets":

                            pictureBox.Show();
                            // pictureBox.Image = Oklade.Properties.Resources.Brooklyn_Nets_logo;
                            pictureBox.ImageLocation = "http://cdn.bleacherreport.net/images/team_logos/164x164/brooklyn_nets.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Charlotte Hornets":

                            pictureBox.Show();
                            //   pictureBox.Image = Oklade.Properties.Resources.Charlotte_Hornets_new_logo1;
                            pictureBox.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/cha.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Chicago Bulls":

                            pictureBox.Show();
                            //  pictureBox.Image = Oklade.Properties.Resources.chicago_bulls_logo1;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/6/67/Chicago_Bulls_logo.svg/475px-Chicago_Bulls_logo.svg.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Cleveland Cavaliers":

                            pictureBox.Show();
                            pictureBox.ImageLocation = "http://www.printyourbrackets.com/nba-logos/cleveland-cavaliers-logo.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Dallas Mavericks":

                            pictureBox.Show();
                            //  pictureBox.Image = Oklade.Properties.Resources.dallas_mavericks_logo1;
                            pictureBox.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/dal.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Denver Nuggets":

                            pictureBox.Show();
                            //     pictureBox.Image = Oklade.Properties.Resources.Denver_Nuggets_logo;
                            pictureBox.ImageLocation = "http://content.sportslogos.net/logos/6/229/full/xeti0fjbyzmcffue57vz5o1gl.gif";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Detroit Pistons":

                            pictureBox.Show();
                            //   pictureBox.Image = Oklade.Properties.Resources.Detroit_Pistons_logo;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/1/1e/Detroit_Pistons_logo.svg/1229px-Detroit_Pistons_logo.svg.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "GS Warriors":

                            pictureBox.Show();
                            pictureBox.ImageLocation = "http://img1.findthebest.com/sites/default/files/5881/media/images/_4173363.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Houston Rockets":

                            pictureBox.Show();
                            //  pictureBox.Image = Oklade.Properties.Resources.Houston_Rockets;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/2/28/Houston_Rockets.svg/1280px-Houston_Rockets.svg.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Indiana Pacers":

                            pictureBox.Show();
                            // pictureBox.Image = Oklade.Properties.Resources.Indiana_Pacers_Logo;
                            pictureBox.ImageLocation = "http://content.sportslogos.net/logos/6/224/full/3083.gif";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "LA Clippers":

                            pictureBox.Show();
                            pictureBox.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/media/2.0/teamsites/clippers/clippers-primary-150617-v2.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "LA Lakers":

                            pictureBox.Show();
                            // pictureBox.Image = Oklade.Properties.Resources.Los_Angeles_Lakers_logo;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/LosAngeles_Lakers_logo.svg/360px-LosAngeles_Lakers_logo.svg.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Memphis Grizzlies":

                            pictureBox.Show();
                            //    pictureBox.Image = Oklade.Properties.Resources.Grizzlies_Logo1;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/it/6/6f/Memphis_Grizzlies_logo.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Miami Heat":

                            pictureBox.Show();
                            //  pictureBox.Image = Oklade.Properties.Resources.Miami_Heat_Logo;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/f/fb/Miami_Heat_logo.svg/741px-Miami_Heat_logo.svg.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Milwaukee Bucks":

                            pictureBox.Show();
                            pictureBox.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/mil.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Min Timberwolves":

                            pictureBox.Show();
                            //   pictureBox.Image = Oklade.Properties.Resources.Minnesota_Timberwolves_logo;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/hr/5/57/Minnesota_Timberwolves.gif";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "NewOrleans Pelicans":

                            pictureBox.Show();
                            // pictureBox.Image = Oklade.Properties.Resources.New_Orleans_Pelicans;
                            pictureBox.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/nop.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "NewYork Knicks":

                            pictureBox.Show();
                            // pictureBox.Image = Oklade.Properties.Resources.new_york_knicks_logo;
                            pictureBox.ImageLocation = "http://vignette1.wikia.nocookie.net/logopedia/images/8/80/NewYorkKnicks.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "OKC Thunder":

                            pictureBox.Show();
                            // pictureBox.Image = Oklade.Properties.Resources.OKC;
                            pictureBox.ImageLocation = "http://a3.espncdn.com/combiner/i?img=%2Fi%2Fteamlogos%2Fnba%2F500%2Fokc.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Orlando Magic":

                            pictureBox.Show();
                            //  pictureBox.Image = Oklade.Properties.Resources.Orlando_magic_logo;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/8/85/Orlando_magic_logo.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Phi Seventysixers":

                            pictureBox.Show();
                            //   pictureBox.Image = Oklade.Properties.Resources.phi_logo;
                            pictureBox.ImageLocation = "https://s-media-cache-ak0.pinimg.com/originals/36/8a/b9/368ab94dbe2732e963c9b94456e6b7c5.jpg";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Phoenix Suns":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Phoenix_Suns;
                            break;

                        case "Por Trailblazers":

                            pictureBox.Show();
                            // pictureBox.Image = Oklade.Properties.Resources.portland_trail_blazers_logo2;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/7/74/Portland_Trail_Blazers.svg/840px-Portland_Trail_Blazers.svg.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Sacramento Kings":

                            pictureBox.Show();
                            //   pictureBox.Image = Oklade.Properties.Resources.Sacramento_Kings_Logo;
                            pictureBox.ImageLocation = "https://upload.wikimedia.org/wikipedia/hr/a/a8/Sacramento_Kings.gif";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "SanAntonio Spurs":

                            pictureBox.Show();
                            //  pictureBox.Image = Oklade.Properties.Resources.San_Antonio_Spurs_logo;
                            pictureBox.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/sas.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Toronto Raptors":

                            pictureBox.Show();
                            pictureBox.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/tor.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Utah Jazz":

                            pictureBox.Show();
                            //   pictureBox.Image = Oklade.Properties.Resources.utahjazz;
                            pictureBox.ImageLocation = "http://img.zanda.com/item/45030000000044/400x400/Utah_Jazz_logo.png";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Washington Wizards":

                            pictureBox.Show();
                            pictureBox.ImageLocation = "http://cdn.mse.psddev.com/dims4/SPORTS/6d4b737/2147483647/resize/618x410%3E/quality/75/?url=http%3A%2F%2Fcdn.mse.psddev.com%2Ffd%2Ffe%2Fd4eb58a24521af7d2875c45dfb85%2F150421-washingtonwizards-primary.gif";
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Arizona Diamondbacks":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Arizona_Diamondbacks;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Atlanta Braves":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Atlanta_Braves;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Baltimore Orioles":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.BaltimoreOrioles;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Boston RedSox":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Boston_Red_Sox;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Chicago Cubs":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Chicago_Cubs;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Chicago WhiteSox":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Chicago_White_Sox;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Cincinnati Reds":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Cincinnati_Reds;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Cleveland Indians":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Cleveland_Indians;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Colorado Rockies":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Colorado_Rockies;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Detroit Tigers":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Detroit_Tigers;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Houston Astros":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Houston_Astros;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "KC Royals":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Kansas_City_Royals;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "LosAngeles Angels":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Los_Angeles_Angels;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "LosAngeles Dodgers":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Los_Angeles_Dodgers;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Miami Marlins":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Miami_Marlins;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Milwaukee Brewers":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Milwaukee_Brewers;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Minnesota Twins":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Minnesota_Twins;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "NewYork Mets":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.New_York_Mets;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "NewYork Yankees":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.New_York_Yankees;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Oakland Athletics":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Oakland_Atletics;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Phi Phillies":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Philadelphia_Phillies;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Pittsburgh Pirates":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Pittsburgh_Pirates;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "SanDiego Padres":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.San_Diego_Padres;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "SanFrancisco Giants":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.San_Francisco_Giants;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "Seattle Mariners":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Seattle_Mariners;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;


                        case "St.Louis Cardinals":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.St_Louis_Cardinals;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;

                        case "TampaBay Rays":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Tampa_Bay_Rays;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;
                        case "Texas Rangers":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Texas_Rangers;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;
                        case "Toronto BlueJays":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Toronto_Blue_Jays;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;
                        case "Washington Nationals":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Washington_Nationals;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;
                        case "2.Zwer23 Bets":

                            pictureBox.Show();
                            pictureBox.Image = Oklade.Properties.Resources.Zwer23Logo;
                            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
                            break;
                    }
                });
            }
        }
    }
}

﻿using System;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Oklade.DALEntityFramework;
using System.Messaging;
using System.Windows.Forms;

namespace Oklade.SharedMethodsCollection
{
    public class ParkFactors
    {
        List<string> _parkFactors = new List<string>();
        List<string> _teams = new List<string>();
        private HtmlAgilityPack.HtmlDocument _doc;
        int _i;

        public void GetParkFactor()
        {
            _i = 0;
            LoadHTML("http://www.espn.com/mlb/stats/parkfactor");

            var queryParkFactors = from table in _doc.DocumentNode.SelectNodes("//table").Cast<HtmlNode>()
                                   from row in table.SelectNodes("tr[contains(@class, 'row')]").Cast<HtmlNode>()
                                   from cell in row.SelectNodes("th|td").Cast<HtmlNode>()
                                     select new
                                     {
                                         Table = table.Id,
                                         CellText = cell.InnerText.Trim()
                                     };

            using (OkladesEntities context = new OkladesEntities())
            {
                foreach (var cell in queryParkFactors)
                {
                    _parkFactors.Add(cell.CellText);
                    _i++;

                    if(_i == 8)
                    {
                        _i = 0;
                        string parkName = _parkFactors[1].Substring(0, 8);
                        var getTeam = context.Klubovis.SingleOrDefault(k => k.ParkName.Contains(parkName));

                        if (getTeam != null)
                        {
                            getTeam.ParkFactor = Convert.ToDecimal(_parkFactors[2].Replace('.',','));
                        }
                        context.SaveChanges();

                        _parkFactors.Clear();
                    }
                };
                MessageBox.Show("Park Factors Updated!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        public void LoadHTML(string url)
        {
            using (var webClient = new WebClient())
            {
                var html = webClient.DownloadString(url);
                _doc = new HtmlAgilityPack.HtmlDocument();
                _doc.LoadHtml(html);
            }
        }
    }
}

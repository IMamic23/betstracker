﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;

namespace Oklade.SharedMethodsCollection
{
    public class SharedMethods
    {
        private string _response;
        public void UpdateSetting(string key, string value)
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save();

            ConfigurationManager.RefreshSection("appSettings");
        }

        public string GetStringSqlResponse(string connectionString, string sqlQuery)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var cmd = new SqlCommand(sqlQuery, connection))
                {
                    if (connection.State.Equals(ConnectionState.Closed))
                    {
                        connection.Open();
                    }
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            _response = dr[0].ToString();
                        }
                    }
                }
            }
            return _response;
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
    }
}

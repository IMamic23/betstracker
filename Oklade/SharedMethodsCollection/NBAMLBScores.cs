﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Oklade.SharedMethodsCollection
{

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class NBAMLBScores
    {
        private List<string> _coversTeamsList = new List<string>();
        private List<string> _coversGameTimeList = new List<string>();
        private List<string> _coversGameStatusList = new List<string>();
        private List<string> _coversHomePointsList = new List<string>();
        private List<string> _coversAwayPointsList = new List<string>();
        private List<string> _coversTotalList = new List<string>();
        private List<string> _coversLineList = new List<string>();
        private List<string> _coversStarterList = new List<string>();
        public List<string> _coversMatchupsLinkList = new List<string>();
        public List<string> _coversBoxScoreLinkList = new List<string>();
        private HtmlAgilityPack.HtmlDocument _doc;

        string[] _coversrow = new string[36];
        int _noofteams;
        private string _league;
        int _coversbsno;
        int j;
        int i;
        int k;
        string html;

        public void LoadHTML(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36";
            request.Timeout = 10000; //Timeout after 10000 ms
                using (var stream = request.GetResponse().GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                    _doc = new HtmlAgilityPack.HtmlDocument();
                    _doc.LoadHtml(html);
                }
        }

        public void LiveScores(DataGridView matchupsDataGridView, RadioButton nbaCheckBox, RadioButton mlbCheckBox, RadioButton bestBetsButton)
        {
            if (nbaCheckBox.Checked || mlbCheckBox.Checked || bestBetsButton.Checked)
            {
                try
                {
                    _coversTeamsList.Clear();
                    _coversGameTimeList.Clear();
                    _coversGameStatusList.Clear();
                    _coversHomePointsList.Clear();
                    _coversTotalList.Clear();
                    _coversLineList.Clear();
                    _coversAwayPointsList.Clear();
                    _coversStarterList.Clear();
                    _coversMatchupsLinkList.Clear();
                    _coversBoxScoreLinkList.Clear();


                    DateTime dt = DateTime.Now;
                    if (dt.Month < 6 || dt.Month > 9)
                    {
                        _league = "NBA";
                    }
                    else if (dt.Month > 5 && dt.Month < 10)
                    {
                        _league = "MLB";
                    }

                    var matchupsBox = _doc.DocumentNode.SelectNodes(".//div[@class='cmg_matchup_game_box']");
                    if (matchupsBox != null)
                    {
                        foreach (HtmlNode matchup in matchupsBox)
                        {

                            var varTeams = matchup.SelectNodes(".//div[@class='cmg_team_name']");
                            if (varTeams != null)
                                foreach (HtmlNode team in varTeams)
                                {
                                    _coversTeamsList.Add(Regex.Replace(team.InnerText.ToString(), @"[^A-Z]*", ""));
                                }

                            var varMatchupStatus = matchup.SelectNodes(".//div[@class='cmg_matchup_list_status']");
                            if (varMatchupStatus != null)
                                foreach (HtmlNode matchupstatus in varMatchupStatus)
                                {
                                    _coversGameStatusList.Add(Regex.Replace(matchupstatus.InnerText.ToString(), @"[^1-9:^A-Z^a-z]*", ""));
                                }
                            else
                                _coversGameStatusList.Add("-");

                            var varGameTime = matchup.SelectNodes(".//div[@class='cmg_game_time']");
                            if (varGameTime != null)
                            {
                                foreach (HtmlNode gametime in varGameTime)
                                {
                                    _coversGameTimeList.Add(Regex.Replace(gametime.InnerText.ToString(), @"[^0-9:]*", ""));
                                }
                            }
                            else
                            {
                                _coversGameTimeList.Add("-");
                            }

                            var varHomePoints = matchup.SelectNodes(".//div[contains(@class, 'cmg_matchup_list_score_home')]");
                            if (varHomePoints != null)
                                foreach (HtmlNode homepoints in varHomePoints)
                                {
                                    _coversHomePointsList.Add(Regex.Replace(homepoints.InnerText.ToString(), @"[^0-9]*", ""));
                                }
                            else
                                _coversHomePointsList.Add("-");

                            var varAwayPoints = matchup.SelectNodes(".//div[contains(@class, 'cmg_matchup_list_score_away')]");
                            if (varAwayPoints != null)
                            {
                                foreach (HtmlNode awaypoints in varAwayPoints)
                                {
                                    _coversAwayPointsList.Add(Regex.Replace(awaypoints.InnerText.ToString(), @"[^0-9]*", ""));
                                }
                            }
                            else
                            {
                                _coversAwayPointsList.Add("-");
                            }

                            if (mlbCheckBox.Checked || bestBetsButton.Checked && _league == "MLB")
                            {
                                var gameStarters = matchup.SelectNodes(".//div[contains(@class, 'cmg_team_starting_pitcher')]");
                                if (gameStarters != null)
                                {
                                    foreach (HtmlNode starter in gameStarters)
                                    {
                                        _coversStarterList.Add(Regex.Replace(starter.InnerText.ToString(), @"Probable[^()^A-Z ^a-z]*", "").Trim());
                                    }
                                }
                                else
                                {
                                    _coversStarterList.Add("-");
                                    _coversStarterList.Add("-");
                                }
                            }

                            var varLiveOdds = matchup.SelectNodes(".//div[@class='cmg_team_live_odds']");
                            if (varLiveOdds != null)
                            {
                                foreach (HtmlNode liveodds in varLiveOdds)
                                {
                                    var liveOddsSpanNode = liveodds.SelectNodes("./span");
                                    _coversTotalList.Add(Regex.Replace(liveOddsSpanNode[1].InnerText.ToString(), @"[^0-9.]*", ""));
                                    _coversLineList.Add(Regex.Replace(liveOddsSpanNode[2].InnerText.ToString(), @"[^-+0-9.]*", ""));
                                }
                            }
                            else
                            {
                                _coversTotalList.Add("-");
                                _coversLineList.Add("-");
                            }

                            var varMatchupList = matchup.SelectNodes(".//div[@class='cmg_l_row cmg_matchup_list_gamebox']");
                            if (varMatchupList != null)
                            {
                                foreach (HtmlNode matchup2 in varMatchupList)
                                {
                                    var matchupANode = matchup2.SelectNodes(".//a");
                                    if (matchupANode.Count == 6)
                                    {
                                        _coversMatchupsLinkList.Add(matchupANode[1].GetAttributeValue("Href", "").Trim());
                                    }
                                    else if (matchupANode.Count == 4)
                                    {
                                        _coversMatchupsLinkList.Add(matchupANode[0].GetAttributeValue("Href", "").Trim());
                                        if (matchupANode[1].InnerText.Contains("Boxscore"))
                                        {
                                            _coversBoxScoreLinkList.Add(matchupANode[1].GetAttributeValue("Href", "").Trim());
                                        }
                                        else
                                        {
                                            _coversBoxScoreLinkList.Add("-");
                                        }
                                    }
                                }
                            }
                        }
                    }

                    int saveRow = 0;
                    if (matchupsDataGridView.Rows.Count > 0)
                        saveRow = matchupsDataGridView.FirstDisplayedCell.RowIndex;


                    formatGDV(matchupsDataGridView, nbaCheckBox, mlbCheckBox, bestBetsButton, _league);

                    i = 0;
                    j = 1;
                    k = 0;
                    while (i < _coversTeamsList.Count)
                    {
                        if (nbaCheckBox.Checked || bestBetsButton.Checked && _league == "NBA")
                        {
                            _coversrow = new string[]
                                {
                                    "" + _coversTeamsList[i] + "", "" + _coversTeamsList[j] + "",
                                    "" + _coversGameStatusList[k] + "", "" + _coversAwayPointsList[k] + "",
                                    "" + _coversHomePointsList[k] + "", "" + _coversTotalList[k] + "", "" + _coversLineList[k] + ""
                                };
                        }
                        else if (mlbCheckBox.Checked || bestBetsButton.Checked && _league == "MLB")
                        {
                            _coversrow = new string[]
                                {
                                    "" + _coversTeamsList[i] + "", "" + _coversTeamsList[j] + "",
                                    "" + _coversGameStatusList[k] + "", "" + _coversStarterList[i] + "", "" + _coversStarterList[j] + "", "" + _coversAwayPointsList[k] + "",
                                    "" + _coversHomePointsList[k] + "", "" + _coversTotalList[k] + "", "" + _coversLineList[k] + ""
                                };
                        }

                        matchupsDataGridView.Rows.Add(_coversrow);
                        i++; i++;
                        j++; j++;
                        k++;
                    }

                    _noofteams = _coversTeamsList.Count / 2;

                    j = 0;
                    for (i = 0; i < _noofteams; i++)
                    {
                        if ((string)matchupsDataGridView.Rows[i].Cells["Game Status"].Value == "-")
                        {
                            matchupsDataGridView.Rows[i].Cells["Game Status"].Value = _coversGameTimeList[i];
                            matchupsDataGridView.Rows[i].Cells["Total"].Value = _coversTotalList[i];
                            matchupsDataGridView.Rows[i].Cells["Line"].Value = _coversLineList[i];
                            j++;
                        }
                    }

                    _coversbsno = 0; //////////////////////////////////////////////////////////// /
                    for (i = 0; i < _noofteams; i++)
                    {
                        if (matchupsDataGridView.Rows[i].Cells["Away Score"].Value.ToString() != "-")
                        {
                            matchupsDataGridView.Rows[i].Cells["Box / Matchup"].Value = @"Box Score";
                            matchupsDataGridView.Rows[i].Cells["Box / Matchup"].Style.ForeColor = Color.Blue;
                            matchupsDataGridView.Rows[i].Cells["Box / Matchup"].Style.Font = new System.Drawing.Font("Ariel", 9,
                                FontStyle.Underline);
                        }
                        else
                        {
                            matchupsDataGridView.Rows[i].Cells["Box / Matchup"].Value = @"Matchup";
                            matchupsDataGridView.Rows[i].Cells["Box / Matchup"].Style.ForeColor = Color.Blue;
                            matchupsDataGridView.Rows[i].Cells["Box / Matchup"].Style.Font = new System.Drawing.Font("Ariel", 9,
                                FontStyle.Underline);
                        }
                        if (matchupsDataGridView.Rows[i].Cells["Away Score"].Value.ToString() != "-" &&
                            matchupsDataGridView.Rows[i].Cells["Game Status"].Value.ToString().Contains("Final") != true)
                        {
                            _coversbsno++;
                        }
                    }

                    if (saveRow != 0 && saveRow < matchupsDataGridView.Rows.Count)
                        matchupsDataGridView.FirstDisplayedScrollingRowIndex = saveRow;


                    System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                    boldStyle.Font = new System.Drawing.Font("Arial Narrow", 11F, System.Drawing.FontStyle.Bold);
                    boldStyle.ForeColor = Color.Navy;

                    System.Windows.Forms.DataGridViewCellStyle boldStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
                    boldStyle2.Font = new System.Drawing.Font("Arial Narrow", 9.5F, System.Drawing.FontStyle.Bold);
                    boldStyle2.ForeColor = Color.Indigo;

                    matchupsDataGridView.Columns[0].DefaultCellStyle = boldStyle;
                    matchupsDataGridView.Columns[1].DefaultCellStyle = boldStyle;
                    var dataGridViewColumn = matchupsDataGridView.Columns["Home Starter"];
                    if (dataGridViewColumn != null)
                        dataGridViewColumn.DefaultCellStyle = boldStyle2;
                    var gridViewColumn = matchupsDataGridView.Columns["Away Starter"];
                    if (gridViewColumn != null)
                        gridViewColumn.DefaultCellStyle = boldStyle2;
                    matchupsDataGridView.Columns["Home Score"].DefaultCellStyle = boldStyle;
                    matchupsDataGridView.Columns["Away Score"].DefaultCellStyle = boldStyle;


                    matchupsDataGridView.Rows[0].Cells[0].Selected = false;
                }

                catch (Exception ex)
                {
                   // MessageBox.Show(ex.ToString());
                   // MessageBox.Show(@"Cant connect to covers.com", @"Warning!", MessageBoxButtons.OK);
                }
            }
        }

        public void formatGDV(DataGridView matchupsDataGridView, RadioButton nbaCheckBox, RadioButton mlbCheckBox, RadioButton bestBetsButton, string league)
        {
            if (matchupsDataGridView.DataSource != null)
            {
                matchupsDataGridView.DataSource = null;
            }
            else
            {
                matchupsDataGridView.Rows.Clear();
            }

            matchupsDataGridView.Refresh();

            if (nbaCheckBox.Checked || bestBetsButton.Checked && league == "NBA")
            {
                matchupsDataGridView.ColumnCount = 8;
                matchupsDataGridView.Columns[0].Name = "Away Team";
                matchupsDataGridView.Columns[0].Width = 44;
                matchupsDataGridView.Columns[1].Name = "Home Team";
                matchupsDataGridView.Columns[1].Width = 44;
                matchupsDataGridView.Columns[2].Name = "Game Status";
                matchupsDataGridView.Columns[2].Width = 88;
                matchupsDataGridView.Columns[3].Name = "Away Score";
                matchupsDataGridView.Columns[3].Width = 44;
                matchupsDataGridView.Columns[4].Name = "Home Score";
                matchupsDataGridView.Columns[4].Width = 44;
                matchupsDataGridView.Columns[5].Name = "Total";
                matchupsDataGridView.Columns[5].Width = 55;
                matchupsDataGridView.Columns[6].Name = "Line";
                matchupsDataGridView.Columns[6].Width = 55;
                matchupsDataGridView.Columns[7].Name = "Box / Matchup";
                matchupsDataGridView.Columns[7].Width = 70;
            }
            else if (mlbCheckBox.Checked || bestBetsButton.Checked && league == "MLB")
            {
                matchupsDataGridView.ColumnCount = 10;
                matchupsDataGridView.Columns[0].Name = "Away Team";
                matchupsDataGridView.Columns[0].Width = 44;
                matchupsDataGridView.Columns[1].Name = "Home Team";
                matchupsDataGridView.Columns[1].Width = 44;
                matchupsDataGridView.Columns[2].Name = "Game Status";
                matchupsDataGridView.Columns[2].Width = 66;
                matchupsDataGridView.Columns[3].Name = "Away Starter";
                matchupsDataGridView.Columns[3].Width = 147;
                matchupsDataGridView.Columns[4].Name = "Home Starter";
                matchupsDataGridView.Columns[4].Width = 147;
                matchupsDataGridView.Columns[5].Name = "Away Score";
                matchupsDataGridView.Columns[5].Width = 44;
                matchupsDataGridView.Columns[6].Name = "Home Score";
                matchupsDataGridView.Columns[6].Width = 44;
                matchupsDataGridView.Columns[7].Name = "Total";
                matchupsDataGridView.Columns[7].Width = 44;
                matchupsDataGridView.Columns[8].Name = "Line";
                matchupsDataGridView.Columns[8].Width = 44;
                matchupsDataGridView.Columns[9].Name = "Box / Matchup";
                matchupsDataGridView.Columns[9].Width = 70;
            }
        }
    }
}

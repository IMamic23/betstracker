﻿using Oklade.DataGridViews;
using Oklade.SharedMethodsCollection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using Oklade.AnalyzerFolder;
using Oklade.DALEntityFramework;
using System.Threading.Tasks;
using Oklade.Models;

namespace Oklade
{
    public partial class Analyzer : Form
    {
        Task atl10;
        Task htl10;
        DgvActions _dgva = new DgvActions();
        SharedMethods _sharedmethods = new SharedMethods();
        NBAMLBScores _nbamlbScores = new NBAMLBScores();
        AnalyzerActions _analyzerActions = new AnalyzerActions();
        TeamLogos _teamLogos = new TeamLogos();
        ParkFactors _parkFactors = new ParkFactors();
        GetTeamsAndStartersGames getTeamsAndStartersGames = new GetTeamsAndStartersGames();
        Klubovi _awayTeam = null;
        Klubovi _homeTeam = null;
        object _varResults = null;
        private List<NBAResult> _varNbaResults = null;
        private List<MLBResult> _varMlbResults = null;
        BindingSource _resultsBindingSource = new BindingSource();
        private int _addGames;
        private string _homeStarterFirstName;
        private string _homeStarterLastName;
        private string _awayStarterFirstName;
        private string _awayStarterLastName;
        private int _lastXGames;
        object lockA = new object();
        object lockB = new object();


        private string _strRankingsAway;
        private string _strRankingsHome;
        private double _varpacehome;
        private double _varpaceaway;
        private double _varpacehomelow;
        private double _varpacehomehigh;
        private double _varpaceawaylow;
        private double _varpaceawayhigh;
        private string _strpacehomelow;
        private string _strpacehomehigh;
        private string _strpaceawaylow;
        private string _strpaceawayhigh;
        private string _pacecbaway = "";
        private string _pacecbhome = "";
        private string _dgvHomePace;
        private string _dgvAwayPace;
        int seton;
        int ToBetOn;
        int BetProcessed;
        int Edited;
        int IDO2;
        string paceaway;
        string pacehome;
        string offeffaway;
        string offeffhome;
        string deffeffaway;
        string deffeffhome;
        double avg1vsDIV;
        double avg2vsDIV;
        int countSU2vsDIV;
        int countSU1vsDIV;
        int countSU1svDIV;
        string countU1vsDIV;
        string CountO1vsDIV;
        int countSU3vsDIV;
        int countSU4vsDIV;
        string CountO2vsDIV;
        string countU2vsDIV;
        double avg3vsDIV;
        double avg4vsDIV;
        double totaloffer;
        string updateSUr;
        string updateOUr;
        string updateATSr;
        string dateforupdate;
        private string timenoww;
        int IDOkl11;
        int Top1IDOklade;
        string varProvjera;
        decimal? _parkFactor;
        string LeagueNBAMLB;
        int counterstarter;
        int ManualChange = 0;
        int SeasonNBAMLB;
        int COUNTGAMES1;
        int COUNTGAMES2;
        string AwayStarterHand;
        string HomeStarterHand;
        string AwayStarterVar;
        string HomeStarterVar;
        string MLBHStarter;
        string MLBAStarter;
        string NBAMLBResultsVar;
        string NBAMLBBetsVar;
        string AllNBAMLBResultsVar;
        string query4PB14;
        string queryy2Total;
        string queryy3Hand;
        string Sql43Last5;
        string Sql33Last10;
        string Sql34Last10;
        string str1Delete;
        string str2Delete;
        string query4PB7;
        string query5PB7;
        string query4Update;
        string query4AP;
        string query5AP;
        string sql4AP;
        string query6AutoProcess;
        string query5AutoProcess;
        string Sqldgv2;
        string queryyResults;
        string _connectionString;
        SqlConnection _connection;
        string Sql2;
        string Sql3;
        string idkluba1;
        DataSet ds;
        DataSet ds2;
        DataSet ds4;
        DataSet ds5;
        DataSet dsstartersstats;
        ToolTip t1 = new ToolTip();
        SqlDataAdapter adapter;
        SqlDataAdapter adapter2;
        SqlDataAdapter adapter3;
        SqlDataAdapter adapter4;
        SqlDataAdapter adapter5;
        SqlDataAdapter adapterMain;
        SqlDataAdapter adapterStandings;
        DataGridView grv;
        string ID;
        public string _awayTeamValue;
        public int IDH;
        public int IDA;
        public int IDO;
        string HA;
        string tot;
        string hand;
        int? _homeTil1;
        int? _awayTil2;
        public int _isOver = 0;
        public int _isUnder = 0;
        public int _hAway = 0;
        public int _hHome = 0;
        int duplikati = 0;
        int duplikatiindex = 0;
        int? _awaytil22;
        int? _awaytil23;
        int? _hometil12;
        int? _hometil13;
        int? _awaytil24;
        int? _hometil14;
        int countSU1;
        int countSU2;
        int countSU3;
        int countSU4;
        int countSU1vsHand;
        int countSU2vsHand;
        int countSU3vsHand;
        int countSU4vsHand;
        string IDOkl2;
        string HorA;
        public string _homeTeamValue;
        double totall;
        double hendii;
        string HAteam22;
        double cellv;
        string version;
        double? avg1;
        double avg1vsHand;
        double avg2;
        double avg2vsHand;
        double avg3;
        double avg3vsHand;
        double avg4;
        double avg4vsHand;
        double avg5;
        double avg6;
        double avg7;
        double avg8;
        string kratkoime5;
        string kratkoime6;
        string ATS1;
        string ATS2;
        string OU1;
        string OU2;
        string CountO1;
        string CountO1vsHand;
        string CountO2;
        string CountO2vsHand;
        string countU1;
        string countU2;
        string countU1vsHand;
        string countU2vsHand;
        string countATS1;
        string countATS2;
        string countATS3;
        string countATS4;
        double _hAvg;
        double _aAvg;
        private int _season1;
        private int _season2;
        string _imeHKluba;
        string _imeAKluba;
        string IDKlubaInj;
        string htmlString = "";
        string _teamOuh;
        private string _teamOua;
        string link1;
        string link2;
        string link3;
        string link4;
        string link5;
        string link6;
        string link7;
        string link11;
        string link12;
        string link13;
        string link14;
        string link15;
        string link16;
        string link17;
        int index;
        int index2;
        int index3;
        int index4;
        int index5;
        int index6;
        int index7;
        int index8;
        int index9;
        int position;
        string linkat;
        string linkht;
        string[] pinnrow;
        string PinnAwayTeam;
        string PinnHomeTeam;
        string PinnHandi;
        string PinnTotal;
        int numbering;
        string[] coversboxscore = new string[36];
        string[] coversMatchup = new string[36];
        string[] coversrow = new string[36];
        string[] coversrow2 = new string[36];
        string[] coversrow3 = new string[36];
        string _coversAwayStarter;
        string _coversHomeStarter;
        int coversbsno;
        int j;
        int i;
        int k;
        int arraycount;
        List<string> result;

        public Analyzer()
        {
            SplashFormOpenProgram.ShowSplashScreenOpen();
            InitializeComponent();
            _connectionString = ConfigurationManager.ConnectionStrings["MyDatabase"].ToString();
            _connection = new SqlConnection(_connectionString);

            CultureInfo culture = new CultureInfo(ConfigurationManager.AppSettings["DefaultCulture"]);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            DateTime dtCheck = DateTime.Today;
            if (dtCheck.Month < 6 || dtCheck.Month > 10)
            {
                radioBtnNBA.Checked = true;
                label6.Hide();
                label7.Hide();
                awayStarterCB.Hide();
                homeStarterCB.Hide();
            }
            else
            {
                radioBtnMLB.Checked = true;
            }

            WebClient wc = new WebClient();
            htmlString = wc.DownloadString("http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/injury/injuries.html");

            ///// CLOSE LOADING SCREEN ////
            SplashFormOpenProgram.CloseForm();
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        private void UpdateFont()
        {
            //Change cell font
            foreach (DataGridViewColumn c in teamsDetailedStatsDGV.Columns)
            {
                c.DefaultCellStyle.Font = new System.Drawing.Font("Arial Narrow", 14.2F);
            }
        }

        private void wbhtmlinjuries(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                this.SuspendLayout();

                connection.Open();

                Task loadTask01 = Task.Run(() =>
                { 
                wbhtmlinjuries(sender, e);
                UpdateFont();
                });

                LastXgamesCB.Text = ConfigurationManager.AppSettings["LastXGames"];

                dateTimePicker1.CustomFormat = "dd.MM.yyyy.";

                DateTime dtCheck = DateTime.Today;

                AStarterHandLabel.Text = "";
                HStarterHandLabel.Text = "";

                TotalBox.Enabled = false;
                HandicapBox.Enabled = false;

                if (dtCheck.Month > 8)
                {
                    lastSeasonCB.Text = (dtCheck.Year - 1) + "/" + dtCheck.Year;
                    thisSeasonCB.Text = dtCheck.Year + "/" + (dtCheck.Year + 1);
                }
                else
                {
                    lastSeasonCB.Text = (dtCheck.Year - 2) + "/" + (dtCheck.Year - 1);
                    thisSeasonCB.Text = (dtCheck.Year - 1) + "/" + dtCheck.Year;
                }

                if (radioBtnNBA.Checked == true)
                {
                    AllNBAMLBResultsVar = "AllNBAResults";
                    NBAMLBBetsVar = "NBABets";
                    NBAMLBResultsVar = "NBAResults";
                    LeagueNBAMLB = "NBA";

                    if (dtCheck.Month < 11)
                    {
                        SeasonNBAMLB = dtCheck.Year - 1;
                    }
                    else
                    {
                        SeasonNBAMLB = dtCheck.Year;
                    }

                    DateTime dtPlayoffsStart = new DateTime(dtCheck.Year, 4, 12);
                    DateTime dtPlayoffsEnd = new DateTime(dtCheck.Year, 6, 12);


                    if (dtCheck.Date >= dtPlayoffsStart && dtCheck.Date < dtPlayoffsEnd)
                    {
                        logoPB.Image = Oklade.Properties.Resources.NBAPlayoffs;
                    }

                }
                else if (radioBtnMLB.Checked == true)
                {
                    AllNBAMLBResultsVar = "AllMLBResults";
                    NBAMLBBetsVar = "MLBBets";
                    NBAMLBResultsVar = "MLBResults";
                    LeagueNBAMLB = "MLB";
                    awayStarterCB.Enabled = false;
                    homeStarterCB.Enabled = false;
                    if (dtCheck.Month == 4 && dtCheck.Day < 16)
                    {
                        SeasonNBAMLB = dtCheck.Year - 1;
                    }
                    else
                    {
                        SeasonNBAMLB = dtCheck.Year;
                    }
                }


                DateTime dt2 = DateTime.Today;
                string Datum2 = dt2.ToString("yyyy-MM-dd");
                dateTimePicker2.Text = Datum2;

                ds = new DataSet();

                if (dtCheck.Month >= 11 && dtCheck.Month <= 12)
                {
                    lastSeasonCB.Checked = true;
                    thisSeasonCB.Checked = true;
                }
                if (dtCheck.Month >= 1 && dtCheck.Month < 11)
                {
                    lastSeasonCB.Checked = false;
                    thisSeasonCB.Checked = true;
                }

                L47.Text = "";
                label19.Text = "";
                label21.Text = "";
                label22.Text = "";
                label23.Text = "";
                label24.Text = "";
                label25.Text = "";
                label27.Text = "";
                label28.Text = "";
                label33.Text = "";
                label34.Text = "";
                labelAwayAvg.Text = "";
                labelHomeAvg.Text = "";
                labelTotalAvg.Text = "";
                richTextBox1.Text = "";
                richTextBox3.Text = "";
                label43.Text = "";

                // this.tabControlInjuries.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControlInjuries_DrawItem);

                /////////////////////////////// RESIZE FOR SMALLER RESOLUTION ////////////////////////
                this.Location = new System.Drawing.Point(0, 0);
                int vertsize = Screen.PrimaryScreen.WorkingArea.Height;
                if (vertsize < this.Height)
                {
                    this.Size = Screen.PrimaryScreen.WorkingArea.Size;
                }

                //////////////////////////////////VERSION//////////////////////////////

                if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
                {
                    version = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
                }
                else
                {
                    version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                }

                label13.Text = @"Copyright © 2014 IMS-Studio  v" + version;


                

                Sql2 = @"select ImeKluba from Klubovi where League = '" + LeagueNBAMLB + "' order by ImeKluba";


                try
                {
                    Task loadTeams = Task.Run(() =>
                      {
                          using (SqlConnection connection2 = new SqlConnection(_connectionString))
                          {
                              connection2.Open();
                              DataSet myDataSet = new DataSet();

                              adapter2 = new SqlDataAdapter(Sql2, connection2);

                              adapter2.Fill(myDataSet, "ImeKluba");

                              hteamradio.Checked = true;
                              System.Data.DataTable myDataTable = myDataSet.Tables[0];

                              DataRow tempRow = null;

                              foreach (DataRow tempRowVariable in myDataTable.Rows)
                              {
                                  tempRow = tempRowVariable;
                              }


                              queryyResults = "Select TOP 1 IDOklade from " + NBAMLBResultsVar + " order by IDOklade desc";

                              SqlCommand cmdd = new SqlCommand(queryyResults, connection2);
                              SqlDataReader readd = cmdd.ExecuteReader();

                              while (readd.Read())
                              {
                                  IDOkl2 = (readd["IDOklade"].ToString());
                              }
                              readd.Close();
                              int IDOkl3 = Convert.ToInt32(IDOkl2);
                              IDoklBox.Text = (IDOkl3 + 1).ToString();

                              ////ComboBox1
                              AwayTeamCB.InvokeIfRequired(c =>
                              {
                                  AwayTeamCB.Items.Clear();
                              
                              

                                  DataSet myDataSet3 = new DataSet();

                                  adapter2 = new SqlDataAdapter(Sql2, connection2);

                                  adapter2.Fill(myDataSet3, "ImeKluba");

                                  System.Data.DataTable myDataTable3 = myDataSet3.Tables[0];

                                  DataRow tempRow3 = null;

                                  foreach (DataRow tempRow3_Variable in myDataTable3.Rows)
                                  {

                                      tempRow3 = tempRow3_Variable;

                                      AwayTeamCB.Items.Add((tempRow3["ImeKluba"]));
                                  }
                                  AwayTeamCB.Focus();
                              });
                              ////ComboBox2
                              HomeTeamCB.InvokeIfRequired(c =>
                             {
                                 HomeTeamCB.Items.Clear();

                                 DataSet myDataSet4 = new DataSet();

                                 adapter3 = new SqlDataAdapter(Sql2, connection2);


                                 adapter3.Fill(myDataSet4, "ImeKluba");


                                 DataTable myDataTable4 = myDataSet4.Tables[0];


                                 DataRow tempRow4 = null;

                                 foreach (DataRow tempRow4Variable in myDataTable4.Rows)
                                 {

                                     tempRow4 = tempRow4Variable;

                                     HomeTeamCB.Items.Add((tempRow4["ImeKluba"]));

                                 }
                             });
                          }  
                      });

                    IDoklBox.SelectionStart = 0;

                    this.ActiveControl = label1;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                this.ResumeLayout();
            }
        }

        private void Aresult_Enter(object sender, EventArgs e)
        {

        }

        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {

                connection.Open();


                if (_awayTeamValue == "")
                {
                    _awayTeamValue = _awayTeamValue;
                }
                else
                {
                    HomeTeamCB.Text = _awayTeamValue;
                }

                String queryy33 = "Select ID from Klubovi where ImeKluba = '" + _awayTeamValue + "'";
                SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                SqlDataReader readd33 = cmdd33.ExecuteReader();
                while (readd33.Read())
                {
                    idkluba1 = (readd33["ID"].ToString());
                }
                readd33.Close();
                connection.Close();
            }
        }

        private void Hresult_Enter(object sender, EventArgs e)
        {

        }

        private void Aresult_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                //    Hresult.Focus();   //enter key is down
            }
        }

        private void Dgv2(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                _hAvg = _hAvg / (DGVAnalysisResults.Rows.Count - 1);
                _aAvg = _aAvg / (DGVAnalysisResults.Rows.Count - 1);
                _hAvg = Math.Round(_hAvg, 2);
                _aAvg = Math.Round(_aAvg, 2);
                double total11 = _hAvg + _aAvg;
                labelAwayAvg.Text = _aAvg.ToString();
                labelHomeAvg.Text = _hAvg.ToString();
                labelTotalAvg.Text = total11.ToString();


                Sqldgv2 = "select IsOver, IsUnder,HomeHendi,AwayHendi,Total,Handicap,Datum from " + NBAMLBBetsVar + " where IDOklade = '" + IDO + "'";

                ds2 = new DataSet();
                adapter4 = new SqlDataAdapter(Sqldgv2, connection);
                ds2.Reset();
                adapter4.Fill(ds2);
                dataGridView2.DataSource = ds2.Tables[0];

                dataGridView2.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView2.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView2.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[6].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Rows[0].Cells[0].Selected = false;

                for (int intCount2 = 0; intCount2 < ds2.Tables[0].Rows.Count; intCount2++)
                {
                    double is1 = Convert.ToDouble(ds2.Tables[0].Rows[intCount2][0]);
                    double is2 = Convert.ToDouble(ds2.Tables[0].Rows[intCount2][1]);
                    double is3 = is1 - is2;
                    double is4 = Convert.ToDouble(ds2.Tables[0].Rows[intCount2][2]);
                    double is5 = Convert.ToDouble(ds2.Tables[0].Rows[intCount2][3]);
                    double is6 = is4 - is5;

                    DataGridViewCellStyle style2 = new DataGridViewCellStyle();
                    style2.Font = new System.Drawing.Font("Arial Narrow", 13.75F, FontStyle.Bold);

                    if (is3 >= 2 & is3 <= 4)
                    {
                        dataGridView2.Rows[intCount2].Cells["IsOver"].Style.BackColor = Color.PaleGreen;
                        dataGridView2.Rows[intCount2].Cells["IsOver"].Style.ForeColor = Color.Black;

                    }
                    else if (is3 >= 5)
                    {
                        dataGridView2.Rows[intCount2].Cells["IsOver"].Style.BackColor = Color.LawnGreen;
                        dataGridView2.Rows[intCount2].Cells["IsOver"].Style.ForeColor = Color.Black;
                        dataGridView2.Columns["IsOver"].DefaultCellStyle = style2;

                    }
                    else if (is3 <= -2 & is3 >= -4)
                    {
                        dataGridView2.Rows[intCount2].Cells["IsUnder"].Style.BackColor = Color.PaleGreen;
                        dataGridView2.Rows[intCount2].Cells["IsUnder"].Style.ForeColor = Color.Black;
                    }
                    else if (is3 <= -5)
                    {
                        dataGridView2.Rows[intCount2].Cells["IsUnder"].Style.BackColor = Color.LawnGreen;
                        dataGridView2.Rows[intCount2].Cells["IsUnder"].Style.ForeColor = Color.Black;
                        dataGridView2.Columns["IsUnder"].DefaultCellStyle = style2;
                    }

                    if (is6 >= 2 & is6 <= 4)
                    {
                        dataGridView2.Rows[intCount2].Cells["HomeHendi"].Style.BackColor = Color.PaleGreen;
                        dataGridView2.Rows[intCount2].Cells["HomeHendi"].Style.ForeColor = Color.Black;
                    }
                    else if (is6 > 5)
                    {
                        dataGridView2.Rows[intCount2].Cells["HomeHendi"].Style.BackColor = Color.LawnGreen;
                        dataGridView2.Rows[intCount2].Cells["HomeHendi"].Style.ForeColor = Color.Black;
                        dataGridView2.Columns["HomeHendi"].DefaultCellStyle = style2;
                    }
                    else if (is6 <= -2 & is6 >= -4)
                    {
                        dataGridView2.Rows[intCount2].Cells["AwayHendi"].Style.BackColor = Color.PaleGreen;
                        dataGridView2.Rows[intCount2].Cells["AwayHendi"].Style.ForeColor = Color.Black;
                    }
                    else if (is6 <= -5)
                    {
                        dataGridView2.Rows[intCount2].Cells["AwayHendi"].Style.BackColor = Color.LawnGreen;
                        dataGridView2.Rows[intCount2].Cells["AwayHendi"].Style.ForeColor = Color.Black;
                        dataGridView2.Columns["AwayHendi"].DefaultCellStyle = style2;
                    }

                    double Operc = (is1 / (is1 + is2)) * 100;
                    double Uperc = (is2 / (is1 + is2)) * 100;
                    Operc = Math.Round(Operc, 2);
                    Uperc = Math.Round(Uperc, 2);
                    String Operc1 = Operc + "%";
                    String Uperc1 = Uperc + "%";
                    label27.Text = Operc1;
                    label28.Text = Uperc1;
                    double AHperc = (is5 / (is4 + is5)) * 100;
                    double HHperc = (is4 / (is4 + is5)) * 100;
                    AHperc = Math.Round(AHperc, 2);
                    HHperc = Math.Round(HHperc, 2);
                    String AHperc1 = AHperc + "%";
                    String HHperc1 = HHperc + "%";
                    label33.Text = AHperc1;
                    label34.Text = HHperc1;

                    TotalBox.Text = dataGridView2.Rows[0].Cells[4].Value.ToString();
                    HandicapBox.Text = dataGridView2.Rows[0].Cells[5].Value.ToString();
                }
            }
        }

        private void Hresult_KeyDown(object sender, KeyEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (e.KeyCode == Keys.Enter)
                {
                    e.Handled = true;
                    e.SuppressKeyPress = true;
                    IDO = Convert.ToInt32(IDoklBox.Text);
                    String query1 = "Select ID,ParkFactor from Klubovi where ImeKluba = '" + _homeTeamValue + "'";
                    String query2 = "Select ID from Klubovi where ImeKluba = '" + _awayTeamValue + "'";
                    SqlCommand cmd = new SqlCommand(query1, connection);
                    SqlDataReader read = cmd.ExecuteReader();
                    try
                    {
                        while (read.Read())
                        {
                            IDH = Convert.ToInt32(read["ID"]);
                            _parkFactor = Convert.ToDecimal(read["ParkFactor"]);
                        }
                        read.Close();

                        SqlCommand cmd2 = new SqlCommand(query2, connection);
                        SqlDataReader read2 = cmd2.ExecuteReader();
                        while (read2.Read())
                        {
                            IDA = Convert.ToInt32(read2["ID"]);
                        }
                        read2.Close();

                        if (hteamradio.Checked == true)
                        {
                            HA = "Home";
                            HAteam22 = _homeTeamValue.Split(' ')[1];
                        }
                        else if (ateamradio.Checked == true)
                        {
                            HA = "Away";
                            HAteam22 = _awayTeamValue.Split(' ')[1];
                        }


                        _dgva.ColorDgv1RowsAndCalculate(DGVAnalysisResults, TotalBox, HandicapBox, out _isOver, out _isUnder, out _hAway, out _hHome, out _hAvg, out _aAvg);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        private void finishBtn_Click(object sender, EventArgs e)
        {

        }

        private void linkk(object sender, EventArgs e)
        {
            homecheckBox1.Checked = false;
            awaycheckBox2.Checked = false;

            if (hteamradio.Checked == true)
            {
                HA = "Home";
                HAteam22 = _homeTeamValue.Split(' ')[1];
            }
            else if (ateamradio.Checked == true)
            {
                HA = "Away";
                HAteam22 = _awayTeamValue.Split(' ')[1];
            }

            if (hteamradio.Checked == true)
            {
                double tott = Convert.ToDouble(TotalBox.Text);
                double hendii = Convert.ToDouble(HandicapBox.Text);
                lineBox1.Text = (hendii + 3).ToString();
                lineBox2.Text = (hendii - 3).ToString();
                teamBox.Text = HAteam22;
                homecheckBox1.Checked = true;
            }
            else
            {
                double tott = Convert.ToDouble(TotalBox.Text);
                double hendii = Convert.ToDouble(HandicapBox.Text);
                lineBox2.Text = ((hendii * (-1)) - 3).ToString();
                lineBox1.Text = ((hendii * (-1)) + 3).ToString();
                teamBox.Text = HAteam22;
                awaycheckBox2.Checked = true;
            }
        }

        private void getGamesBtn_Click(object sender, EventArgs e)
        {
            PickGamesBtn.Enabled = true;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    { DGVAnalysisResults.Columns.Remove("chk2"); }

                }
                String team = teamBox.Text;
                if (homecheckBox1.Checked == true)
                {
                    HA = "H";

                    DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
                    chk.HeaderText = "Pick";
                    chk.Name = "chk";
                    DGVAnalysisResults.Columns.Add(chk);

                    Sql3 = "select ID,Month,Dayofmonth,Year,Team,Opp,Hscore,Ascore,Line,Total,Season from " + AllNBAMLBResultsVar + " where Team = '" + team +
                        "' and Line < '" + lineBox1.Text.ToString().Replace(",", ".") + "' and Line > '" + lineBox2.Text.ToString().Replace(",", ".") + "' order by ID desc";

                    ds = new DataSet();
                    adapter = new SqlDataAdapter(Sql3, connection);
                    ds.Reset();
                    adapter.Fill(ds);
                    DGVAnalysisResults.DataSource = ds.Tables[0];
                    DGVAnalysisResults.Columns[1].Width = 40;
                    DGVAnalysisResults.Columns[2].Width = 40;
                    DGVAnalysisResults.Columns[3].Width = 40;
                }
                else if (awaycheckBox2.Checked == true)
                {
                    HA = "A";
                    DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
                    chk.HeaderText = "Pick";
                    chk.Name = "chk";
                    DGVAnalysisResults.Columns.Add(chk);

                    double line1 = Convert.ToDouble(lineBox1.Text);
                    line1 = line1 * (-1);
                    double line2 = Convert.ToDouble(lineBox2.Text);
                    line2 = line2 * (-1);
                    Sql3 = "select ID,Month,Dayofmonth,Year,Team,Opp,Hscore,Ascore,Line,Total,Season from " + AllNBAMLBResultsVar + " where Opp = '" + team +
                        "' and Line <= " + line2.ToString().Replace(",", ".") + " and Line >= " + line1.ToString().Replace(",", ".") + " order by ID desc";

                    ds = new DataSet();
                    adapter = new SqlDataAdapter(Sql3, connection);
                    ds.Reset();
                    adapter.Fill(ds);
                    DGVAnalysisResults.DataSource = ds.Tables[0];
                    DGVAnalysisResults.Columns[1].Width = 40;
                    DGVAnalysisResults.Columns[2].Width = 40;
                    DGVAnalysisResults.Columns[3].Width = 40;
                }

                //String str1 = "http://sportsdatabase.com/nba/query?output=default&sdql=season%3D2013+and+team%3D" + team +
                //    "+and+" + HA + "+and+line%3C%3D" + lineBox1.Text.ToString().Replace(",", ".") + "+and+line%3E%3D" + lineBox2.Text.ToString().Replace(",", ".") +
                //    "+and+total%3C%3D" + totalBox1.Text.ToString().Replace(",", ".") + "+and+total%3E%3D" + totalBox2.Text.ToString().Replace(",", ".") + "&submit=++S+D+Q+L+%21++";
                //System.Diagnostics.Process.Start(str1);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            linkk(sender, e);
            getGamesBtn.Enabled = true;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            NewGameBtn_Click(sender, e);
            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }
            if (DGVAnalysisResults.Columns.Contains("chk"))
            {
                DGVAnalysisResults.Columns.Remove("chk");
                if (DGVAnalysisResults.Columns.Contains("chk2"))
                { DGVAnalysisResults.Columns.Remove("chk2"); }

            }

            DGVAnalysisResults.DataSource = null;

            String Sql5 = "select ID, IDOklade, Datum,ImeHKluba,ImeAKluba,Total,Handicap,IsOver,IsUnder,HomeHendi,AwayHendi,BetProcessed from " + NBAMLBBetsVar + " where Datum = '"
                + dateTimePicker1.Text + "'";

            try
            {
                ds = new DataSet();
                adapter5 = new SqlDataAdapter(Sql5, _connection);

                ds.Reset();
                adapter5.Fill(ds);
                //nevalja
                DGVAnalysisResults.DataSource = ds.Tables[0];

                DGVAnalysisResults.Columns.Remove("ID");

                DGVAnalysisResults.Columns[0].HeaderText = "IDOkl";
                DGVAnalysisResults.Columns[0].Width = 46;
                DGVAnalysisResults.Columns[2].HeaderText = "Home Team";
                DGVAnalysisResults.Columns[1].Width = 70;
                DGVAnalysisResults.Columns[3].HeaderText = "Away Team";
                DGVAnalysisResults.Columns[6].HeaderText = "Over";
                DGVAnalysisResults.Columns[7].HeaderText = "Under";
                DGVAnalysisResults.Columns[8].HeaderText = "HomeH";
                DGVAnalysisResults.Columns[9].HeaderText = "AwayH";
                DGVAnalysisResults.Columns[10].HeaderText = "Processed";
                DGVAnalysisResults.Columns[2].Width = 102;
                DGVAnalysisResults.Columns[3].Width = 102;
                DGVAnalysisResults.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                DGVAnalysisResults.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                DGVAnalysisResults.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
                DGVAnalysisResults.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
                DGVAnalysisResults.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
                DGVAnalysisResults.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
                DGVAnalysisResults.Columns[6].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[7].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[8].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[9].DefaultCellStyle.BackColor = Color.WhiteSmoke;

                label19.Text = "";
                label21.Text = "";
                label22.Text = "";
                label23.Text = "";
                label24.Text = "";
                label25.Text = "";
                label27.Text = "";
                label28.Text = "";
                label33.Text = "";
                label34.Text = "";
                labelAwayAvg.Text = "";
                labelHomeAvg.Text = "";
                labelTotalAvg.Text = "";
                label43.Text = "";

                for (int intCount2 = 0; intCount2 < ds.Tables[0].Rows.Count; intCount2++)
                {
                    string imehkluba = DGVAnalysisResults.Rows[intCount2].Cells["ImeHKluba"].Value.ToString();
                    string imeakluba = DGVAnalysisResults.Rows[intCount2].Cells["ImeAKluba"].Value.ToString();
                    imehkluba = imehkluba.Split(' ')[1];
                    imeakluba = imeakluba.Split(' ')[1];

                    DGVAnalysisResults.Rows[intCount2].Cells["ImeHKluba"].Value = imehkluba;
                    DGVAnalysisResults.Rows[intCount2].Cells["ImeAKluba"].Value = imeakluba;

                    double is1 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][7]);
                    double is2 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][8]);
                    double is3 = is1 - is2;
                    double is4 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][9]);
                    double is5 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][10]);
                    double is6 = is4 - is5;

                    if (is3 >= 2 & is3 <= 4)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsOver"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is3 >= 5)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsOver"].Style.BackColor = Color.LawnGreen;
                    }
                    else if (is3 <= -2 & is3 >= -4)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsUnder"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is3 <= -5)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsUnder"].Style.BackColor = Color.LawnGreen;
                    }

                    if (is6 >= 2 & is6 <= 4)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["HomeHendi"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is6 >= 5)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["HomeHendi"].Style.BackColor = Color.LawnGreen;
                    }
                    else if (is6 <= -2 & is6 >= -4)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["AwayHendi"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is6 <= -5)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["AwayHendi"].Style.BackColor = Color.LawnGreen;
                    }

                }

                DGVAnalysisResults.CurrentCell.Selected = false;
                dataGridView2.DataSource = null;
                TotalBox.Text = "";
                HandicapBox.Text = "";
                teamBox.Text = "";
                lineBox1.Text = "";
                lineBox2.Text = "";

            }
            catch (Exception ex)
            {
                //    MessageBox.Show("No Data for this date!","Important Message");
            }
            _connection.Close();
        }


        private void GetTeam_Click(object sender, EventArgs e)
        {
            if (DGVAnalysisResults.Columns.Contains("chk"))
            {
                DGVAnalysisResults.Columns.Remove("chk");
                if (DGVAnalysisResults.Columns.Contains("chk2"))
                { DGVAnalysisResults.Columns.Remove("chk2"); }

            }
            DGVAnalysisResults.DataSource = "";
            dataGridView2.DataSource = "";
            ATeamL50DGV.DataSource = "";
            HTeamL50DGV.DataSource = "";
            dataGridView5.DataSource = "";
            AwayTeamCB.Text = "";
            // comboBox2.Text = "";
            //  ImeHKluba = "";
            _imeAKluba = "";
            awayTeamLogoPB.Image = null;
            homeTeamLogoPB.Image = null;
            _awayTeamValue = "";
            _homeTeamValue = "";
            TotalBox.Text = "";
            HandicapBox.Text = "";
            teamBox.Text = "";
            lineBox1.Text = "";
            lineBox2.Text = "";
            awayTeamLogoPB.Hide();
            homeTeamLogoPB.Hide();
            label19.Text = "";
            label21.Text = "";
            label22.Text = "";
            label23.Text = "";
            label24.Text = "";
            label25.Text = "";
            label27.Text = "";
            label28.Text = "";
            label33.Text = "";
            label34.Text = "";
            labelAwayAvg.Text = "";
            labelHomeAvg.Text = "";
            labelTotalAvg.Text = "";
            label12.Text = "";
            label42.Text = "";
            label43.Text = "";
            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            if (_connection != null && _connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }
            String Sql5 = "select ID,IDOklade,Datum,ImeHKluba,ImeAKluba,Total,Handicap,IsOver,IsUnder, HomeHendi,AwayHendi,TotWL, HenWL from " + NBAMLBBetsVar + " where IDHKluba = '" + idkluba1 +
                "' or IDAKluba = '" + idkluba1 + "' order by IDOklade desc";
            try
            {
                ds = new DataSet();
                SqlCommand cmd223 = new SqlCommand(Sql5, _connection);
                adapter5 = new SqlDataAdapter(cmd223);
                //  adapter = new SqlDataAdapter(Sql5, connection);

                ds.Reset();
                adapter5.Fill(ds);

                DGVAnalysisResults.DataSource = ds.Tables[0];

                DGVAnalysisResults.Columns.Remove("ID");

                DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
                chk.HeaderText = "Tot Win";
                chk.Name = "chk";
                DGVAnalysisResults.Columns.Add(chk);
                DGVAnalysisResults.Columns.Remove("TotWL");

                DataGridViewCheckBoxColumn chk2 = new DataGridViewCheckBoxColumn();
                chk2.HeaderText = "H Win";
                chk2.Name = "chk2";
                DGVAnalysisResults.Columns.Add(chk2);
                DGVAnalysisResults.Columns.Remove("HenWL");

                for (int intCount = 0; intCount < ds.Tables[0].Rows.Count; intCount++)
                {
                    if (ds.Tables[0].Rows[intCount][11].ToString() == "1")
                    {
                        DGVAnalysisResults.Rows[intCount].Cells["chk"].Value = true;

                    }
                    else if (ds.Tables[0].Rows[intCount][11].ToString() == "0")
                    {
                        DGVAnalysisResults.Rows[intCount].Cells["chk"].Value = false;
                    }
                }
                for (int intCount = 0; intCount < ds.Tables[0].Rows.Count; intCount++)
                {
                    if (ds.Tables[0].Rows[intCount][12].ToString() == "1")
                    {
                        DGVAnalysisResults.Rows[intCount].Cells["chk2"].Value = true;
                    }
                    else if (ds.Tables[0].Rows[intCount][12].ToString() == "0")
                    {
                        DGVAnalysisResults.Rows[intCount].Cells["chk2"].Value = false;
                    }

                }
                DGVAnalysisResults.Columns[0].HeaderText = "ID Okl";
                DGVAnalysisResults.Columns[0].Width = 42;
                DGVAnalysisResults.Columns[1].Width = 72;
                DGVAnalysisResults.Columns[2].Width = 130;
                DGVAnalysisResults.Columns[2].HeaderText = "Home Team";
                DGVAnalysisResults.Columns[3].Width = 130;
                DGVAnalysisResults.Columns[3].HeaderText = "Away Team";
                DGVAnalysisResults.Columns[5].HeaderText = "Hnd";
                DGVAnalysisResults.Columns[6].HeaderText = "O";
                DGVAnalysisResults.Columns[7].HeaderText = "U";
                DGVAnalysisResults.Columns[8].HeaderText = "HH";
                DGVAnalysisResults.Columns[9].HeaderText = "AH";
                DGVAnalysisResults.Columns[4].Width = 52;
                DGVAnalysisResults.Columns[5].Width = 46;
                DGVAnalysisResults.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                DGVAnalysisResults.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                DGVAnalysisResults.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
                DGVAnalysisResults.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
                DGVAnalysisResults.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
                DGVAnalysisResults.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
                DGVAnalysisResults.Columns[6].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[7].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[8].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[9].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns["chk"].DefaultCellStyle.BackColor = Color.DarkGray;
                DGVAnalysisResults.Columns["chk2"].DefaultCellStyle.BackColor = Color.DarkGray;

                DGVAnalysisResults.CurrentCell.Selected = false;

                dataGridView2.DataSource = null;


                for (int intCount2 = 0; intCount2 < ds.Tables[0].Rows.Count; intCount2++)
                {
                    double is1 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][7]);
                    double is2 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][8]);
                    double is3 = is1 - is2;
                    double is4 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][9]);
                    double is5 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][10]);
                    double is6 = is4 - is5;

                    if (is3 > 2)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsOver"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is3 < -2)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsUnder"].Style.BackColor = Color.PaleGreen;
                    }

                    if (is6 > 2)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["HomeHendi"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is6 < -2)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["AwayHendi"].Style.BackColor = Color.PaleGreen;
                    }

                }

                //      value = "";
                //     _homeTeamValue = "";
                TotalBox.Text = "";
                HandicapBox.Text = "";
                teamBox.Text = "";
                lineBox1.Text = "";
                lineBox2.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            _connection.Close();

        }

        private void button10_Click(object sender, EventArgs e)
        {

            _connection.Open();
            if (DGVAnalysisResults.DataSource != null)
            {
                SqlCommandBuilder cmd224 = new SqlCommandBuilder(adapter5);

                DataGridViewRow row = new DataGridViewRow();
                for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
                {
                    row = DGVAnalysisResults.Rows[i];
                    if (Convert.ToBoolean(row.Cells["chk"].Value) == true)
                    {
                        ds.Tables[0].Rows[i][11] = "1";

                    }
                    else if (Convert.ToBoolean(row.Cells["chk"].Value) == false)
                    {
                        ds.Tables[0].Rows[i][11] = "0";

                    }
                }
                for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
                {
                    row = DGVAnalysisResults.Rows[i];
                    if (Convert.ToBoolean(row.Cells["chk2"].Value) == true)
                    {
                        ds.Tables[0].Rows[i][12] = "1";

                    }
                    else if (Convert.ToBoolean(row.Cells["chk2"].Value) == false)
                    {
                        ds.Tables[0].Rows[i][12] = "0";

                    }
                }

                try
                {
                    adapter5.Update(ds);
                    MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

            _connection.Close();
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            getGamesBtn_Click(sender, e);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Today.AddDays(-1);
            String datum1 = dt.ToString("yyyyMMdd");
            String str1 = "http://sportsdatabase.com/nba/query?output=default&sdql=season+%3D+2013+and+H+and+date%3D" + datum1 + "&submit=++S+D+Q+L+%21++";
            System.Diagnostics.Process.Start(str1);
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        public void RemoveDuplicate(object sender, EventArgs e)
        {
            duplikati = 0;
            duplikatiindex = -1;
            DataGridViewRow row = new DataGridViewRow();
            for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
            {
                for (int j = i; j < DGVAnalysisResults.Rows.Count - 1; j++)
                {
                    if (j != i)
                    {
                        if (DGVAnalysisResults.Rows[i].Cells[2].Value.Equals(DGVAnalysisResults.Rows[j].Cells[2].Value))
                        {
                            if (DGVAnalysisResults.Rows[i].Cells[3].Value.Equals(DGVAnalysisResults.Rows[j].Cells[3].Value))
                            {
                                if (DGVAnalysisResults.Rows[i].Cells[5].Value.Equals(DGVAnalysisResults.Rows[j].Cells[5].Value))
                                {
                                    if (DGVAnalysisResults.Rows[i].Cells[6].Value.Equals(DGVAnalysisResults.Rows[j].Cells[6].Value))
                                    {
                                        if (DGVAnalysisResults.Rows[i].Cells[8].Value.Equals(DGVAnalysisResults.Rows[j].Cells[8].Value))
                                        {
                                            if (DGVAnalysisResults.Rows[i].Cells[9].Value.Equals(DGVAnalysisResults.Rows[j].Cells[9].Value))
                                            {
                                                duplikati++;
                                                duplikatiindex = i;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                if (duplikati > 0)
                {
                    DGVAnalysisResults.Rows.Remove(DGVAnalysisResults.Rows[duplikatiindex]);
                    DGVAnalysisResults.Refresh();
                }
            }
        }

        public void button12_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void comboBox2_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void CalculateAveragesSuouAwayTeam()
        {
            var awayTeamStatsQueries = new StatsQueries();
            var awayTeamStats = new TeamStats();

            Invoke((MethodInvoker)delegate
             {
                 using (OkladesEntities context = new OkladesEntities())
                 {
                     if (_connection.State == ConnectionState.Closed)
                     {
                         _connection.Open();
                     }

                     if (_awayTeam == null)
                     {
                         _awayTeam = _analyzerActions.FillTeams(_awayTeamValue);
                     }

                     awayTeamStatsQueries.AvgPointsQuery = "SELECT AVG(Cast(AScore as decimal)) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";

                     if (lastXgamesAvgCB.Checked)
                     {
                         awayTeamStatsQueries.AvgPointsQuery = "SELECT AVG(Cast(AScore as decimal)) FROM (SELECT TOP " + lastXCB.Text + " AScore FROM " + AllNBAMLBResultsVar + " Where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg";
                     }

                     if(radioBtnNBA.Checked) {
                     avg1 = context.AllNBAResults.Where(p => p.Opp == _awayTeam.KratkoIme &&
                                                      p.Season == SeasonNBAMLB)
                                                      .Average(a => a.Ascore);
                     avg1 = Math.Round((double)avg1, 2);
                     } else {
                     avg1 = context.AllMLBResults.Where(p => p.Opp == _awayTeam.KratkoIme &&
                                                      p.Season == SeasonNBAMLB)
                                                      .Average(a => a.Ascore);
                      avg1 = Math.Round((double)avg1, 2);
                     }

                     //avg1 = AnalyzerActions.GetDataFromSQL<double>(awayTeamStatsQueries.AvgPointsQuery, _connection, true, 2);

                     awayTeamStats.AvgPoints = AnalyzerActions.GetDataFromSQL<double>(awayTeamStatsQueries.AvgPointsQuery, _connection, true, 2);
                     //////////MLB AVG VS HAND

                     if (radioBtnMLB.Checked == true)
                     {
                         //SELECT AVG(Cast(AScore as decimal)) FROM (SELECT top 5 AScore FROM AllMLBResults Where Opp = 'Orioles' and season = '2017' order by id desc) as avg

                         awayTeamStatsQueries.AvgPointsVsHandQuery = @"SELECT AVG(Cast(AScore as decimal)) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB +
                             "' and StarterHHand = '" + HomeStarterHand + "' ";
                         if(lastXgamesAvgCB.Checked) {
                             awayTeamStatsQueries.AvgPointsVsHandQuery = @"SELECT AVG(Cast(AScore as decimal)) FROM (SELECT TOP " + lastXCB.Text + " AScore FROM " + AllNBAMLBResultsVar + " Where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB +
                               "' and StarterHHand = '" + HomeStarterHand + "' order by id desc) as avg ";
                         }

                         avg1vsHand = AnalyzerActions.GetDataFromSQL<double>(awayTeamStatsQueries.AvgPointsVsHandQuery, _connection, true, 2);

                         awayTeamStats.AvgPointsVsHand = AnalyzerActions.GetDataFromSQL<double>(awayTeamStatsQueries.AvgPointsVsHandQuery, _connection, true, 2);

                     }

                     awayTeamStatsQueries.OversQuery = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'O' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                     awayTeamStatsQueries.UndersQuery = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'U' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";

                     if (lastXgamesAvgCB.Checked)
                     {
                         awayTeamStatsQueries.OversQuery = "SELECT COUNT(OUr) FROM (SELECT TOP " + lastXCB.Text + " OUr FROM " + AllNBAMLBResultsVar + " where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where OUr = 'O'";
                         awayTeamStatsQueries.UndersQuery = "SELECT COUNT(OUr) FROM (SELECT TOP " + lastXCB.Text + " OUr FROM " + AllNBAMLBResultsVar + " where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where OUr = 'U'";
                     }

                     CountO1 = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.OversQuery, _connection);

                     countU1 = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.UndersQuery, _connection);

                     awayTeamStats.Overs = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.OversQuery, _connection);
                     awayTeamStats.Unders = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.UndersQuery, _connection);

                     ///////////////////////////////////////////////////////////////////////////////
                     if (radioBtnMLB.Checked == true)
                     {
                         awayTeamStatsQueries.OversVsHandQuery = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'O' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                         awayTeamStatsQueries.UndersVsHandQuery = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'U' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";

                         if (lastXgamesAvgCB.Checked)
                         {
                             awayTeamStatsQueries.OversVsHandQuery = "SELECT COUNT(OUr) FROM (SELECT TOP "+ lastXCB.Text + " OUr " +
                                                                     "FROM " + AllNBAMLBResultsVar + " where Opp = '" + _awayTeam.KratkoIme + "' " +
                                                                     "and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "' " +
                                                                     "order by ID desc) as avg where OUr = 'O'";
                             awayTeamStatsQueries.UndersVsHandQuery = "SELECT COUNT(OUr) FROM (SELECT TOP "+ lastXCB.Text + " OUr " +
                                                                      "FROM " + AllNBAMLBResultsVar + " where Opp = '" + _awayTeam.KratkoIme + "' " +
                                                                      "and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "' " +
                                                                      "order by ID desc) as avg where OUr = 'U'";

                             var overs = context.AllMLBResults.Where(w => w.Opp == _awayTeam.KratkoIme && 
                                                                     w.Season == SeasonNBAMLB && 
                                                                     w.StarterHHand == HomeStarterHand)
                                                                     .OrderByDescending(s => s.ID)
                                                                     .Take(Convert.ToInt32(lastXCB.Text))
                                                                     .Where(w => w.OUr == "O")
                                                                     .Count();

                             var unders = context.AllMLBResults.Where(w => w.Opp == _awayTeam.KratkoIme &&
                                                                      w.Season == SeasonNBAMLB &&
                                                                      w.StarterHHand == HomeStarterHand)
                                                                      .OrderByDescending(s => s.ID)
                                                                      .Take(Convert.ToInt32(lastXCB.Text))
                                                                      .Where(w => w.OUr == "U")
                                                                      .Count();
                         }

                         CountO1vsHand = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.OversVsHandQuery, _connection);

                         countU1vsHand = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.UndersVsHandQuery, _connection);

                         awayTeamStats.OversVsHand = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.OversVsHandQuery, _connection);
                         awayTeamStats.UndersVsHand = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.UndersVsHandQuery, _connection);

                     }

                     ///////////////////////////////////////////////////////////////////////////////
                     if (radioBtnNBA.Checked == true)
                     {
                         awayTeamStatsQueries.ATSWinQuery = "SELECT COUNT(ATSr) FROM " + AllNBAMLBResultsVar + " where ATSr = 'L' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                         awayTeamStatsQueries.ATSLossQuery = "SELECT COUNT(ATSr) FROM " + AllNBAMLBResultsVar + " where ATSr = 'W' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                         if (lastXgamesAvgCB.Checked)
                         {
                             awayTeamStatsQueries.ATSWinQuery = "SELECT COUNT(ATSr) FROM (SELECT TOP " + lastXCB.Text + " ATSr FROM " + AllNBAMLBResultsVar + " where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where ATSr = 'L'";
                             awayTeamStatsQueries.ATSLossQuery = "SELECT COUNT(ATSr) FROM (SELECT TOP " + lastXCB.Text + " ATSr FROM " + AllNBAMLBResultsVar + " where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where ATSr = 'W'";
                         }

                         countATS1 = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.ATSWinQuery, _connection);
                         countATS2 = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.ATSLossQuery, _connection);

                         awayTeamStats.ATSWin = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.ATSWinQuery, _connection);
                         awayTeamStats.ATSLoss = AnalyzerActions.GetDataFromSQL<string>(awayTeamStatsQueries.ATSLossQuery, _connection);
                         // label22.Text = countATS1 + " - " + countATS2;
                     }
                     else if (radioBtnMLB.Checked == true)
                     {
                         awayTeamStatsQueries.SUWinQuery = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                         awayTeamStatsQueries.SULossQuery = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                         if (lastXgamesAvgCB.Checked)
                         {
                             awayTeamStatsQueries.SUWinQuery = "SELECT COUNT(SUr) FROM (SELECT TOP " + lastXCB.Text + " SUr FROM " + AllNBAMLBResultsVar + " where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where SUr = 'L'";
                             awayTeamStatsQueries.SULossQuery = "SELECT COUNT(SUr) FROM (SELECT TOP " + lastXCB.Text + " SUr FROM " + AllNBAMLBResultsVar + " where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where SUr = 'W'";
                         }

                         countSU1 = AnalyzerActions.GetDataFromSQL<int>(awayTeamStatsQueries.SUWinQuery, _connection);
                         countSU2 = AnalyzerActions.GetDataFromSQL<int>(awayTeamStatsQueries.SULossQuery, _connection);
                       
                         awayTeamStats.SUWin = AnalyzerActions.GetDataFromSQL<int>(awayTeamStatsQueries.SUWinQuery, _connection);
                         awayTeamStats.SULoss = AnalyzerActions.GetDataFromSQL<int>(awayTeamStatsQueries.SULossQuery, _connection);
                         
                     }
                     /////////////SUr vs HAND
                     if (radioBtnMLB.Checked == true)
                     {
                         awayTeamStatsQueries.SUWinVsHandQuery = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                         awayTeamStatsQueries.SULossVsHandQuery = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                         if (lastXgamesAvgCB.Checked)
                         {
                             awayTeamStatsQueries.SUWinVsHandQuery = "SELECT COUNT(SUr) FROM (SELECT TOP "+ lastXCB.Text + " SUr FROM " + AllNBAMLBResultsVar + " where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "' order by ID desc) as avg where SUr = 'L'";
                             awayTeamStatsQueries.SULossVsHandQuery = "SELECT COUNT(SUr) FROM (SELECT TOP "+ lastXCB.Text + " SUr FROM " + AllNBAMLBResultsVar + " where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "' order by ID desc) as avg where SUr = 'W'";
                         }

                         countSU1vsHand = AnalyzerActions.GetDataFromSQL<int>(awayTeamStatsQueries.SUWinVsHandQuery, _connection);
                         countSU2vsHand = AnalyzerActions.GetDataFromSQL<int>(awayTeamStatsQueries.SULossVsHandQuery, _connection);

                         awayTeamStats.SUWinVsHand = AnalyzerActions.GetDataFromSQL<int>(awayTeamStatsQueries.SUWinVsHandQuery, _connection);
                         awayTeamStats.SULossVsHand = AnalyzerActions.GetDataFromSQL<int>(awayTeamStatsQueries.SULossVsHandQuery, _connection);
                     }
                     //////////////////////////////////////////////////////////////////////////////

                     awayTeamStatsQueries.AvgPointsOppQuery = "SELECT AVG(Cast(HScore as decimal)) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                     if (lastXgamesAvgCB.Checked)
                     {
                         awayTeamStatsQueries.AvgPointsOppQuery = @"SELECT AVG(Cast(HScore as decimal)) FROM (SELECT TOP " + lastXCB.Text + " HScore FROM " + AllNBAMLBResultsVar + " Where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg";
                     }

                     avg2 = AnalyzerActions.GetDataFromSQL<double>(awayTeamStatsQueries.AvgPointsOppQuery, _connection, true, 2);
                     awayTeamStats.AvgPointsOpp = AnalyzerActions.GetDataFromSQL<double>(awayTeamStatsQueries.AvgPointsOppQuery, _connection, true, 2);

                     /////////////  AVERAGE VS HAND L/R - MLB

                     if (radioBtnMLB.Checked == true)
                     {
                         awayTeamStatsQueries.AvgPointsVsHandOppQuery = @"SELECT AVG(Cast(HScore as decimal)) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB +
                             "' and StarterHHand = '" + HomeStarterHand + "' ";
                         if (lastXgamesAvgCB.Checked)
                         {
                             awayTeamStatsQueries.AvgPointsVsHandOppQuery = @"SELECT AVG(Cast(HScore as decimal)) FROM (SELECT TOP " + lastXCB.Text + " HScore FROM " + AllNBAMLBResultsVar + " Where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB +
                             "' and StarterHHand = '" + HomeStarterHand + "' order by ID desc) as avg ";
                         }

                         avg2vsHand = AnalyzerActions.GetDataFromSQL<double>(awayTeamStatsQueries.AvgPointsVsHandOppQuery, _connection, true, 2);
                         awayTeamStats.AvgPointsVsHandOpp = AnalyzerActions.GetDataFromSQL<double>(awayTeamStatsQueries.AvgPointsVsHandOppQuery, _connection, true, 2);
                     }
                 }
             });
        }

        private void CalculateAveragesSuouHomeTeam()
        {
            var homeTeamStatsQueries = new StatsQueries();
            var homeTeamStats = new TeamStats();

            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }
            homeTeamStatsQueries.AvgPointsQuery = "SELECT AVG(Cast(HScore as decimal)) FROM " + AllNBAMLBResultsVar + " Where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
            if (lastXgamesAvgCB.Checked)
            {
                homeTeamStatsQueries.AvgPointsQuery = @"SELECT AVG(Cast(HScore as decimal)) FROM (SELECT TOP " + lastXCB.Text + " HScore FROM " + AllNBAMLBResultsVar + " Where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg";
            }

            avg3 = AnalyzerActions.GetDataFromSQL<double>(homeTeamStatsQueries.AvgPointsQuery, _connection, true, 2);

            homeTeamStats.AvgPoints = AnalyzerActions.GetDataFromSQL<double>(homeTeamStatsQueries.AvgPointsQuery, _connection, true, 2);

            ////////////// MLB AVG VS HAND

            if (radioBtnMLB.Checked == true)
            {
                homeTeamStatsQueries.AvgPointsVsHandQuery = @"SELECT AVG(Cast(HScore as decimal)) FROM " + AllNBAMLBResultsVar + " Where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB +
                    "' and StarterAHand = '" + AwayStarterHand + "'";

                if (lastXgamesAvgCB.Checked)
                {
                    homeTeamStatsQueries.AvgPointsVsHandQuery = @"SELECT AVG(Cast(HScore as decimal)) FROM (SELECT TOP " +lastXCB.Text+ " HScore FROM " + AllNBAMLBResultsVar + " Where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB +
                    "' and StarterAHand = '" + AwayStarterHand + "' order by ID desc) as avg";
                }

                avg3vsHand = AnalyzerActions.GetDataFromSQL<double>(homeTeamStatsQueries.AvgPointsVsHandQuery, _connection, true, 2);

                homeTeamStats.AvgPointsVsHand = AnalyzerActions.GetDataFromSQL<double>(homeTeamStatsQueries.AvgPointsVsHandQuery, _connection, true, 2);
            }
            ////////////////////////////////////

            homeTeamStatsQueries.AvgPointsOppQuery = "SELECT AVG(Cast(AScore as decimal)) FROM " + AllNBAMLBResultsVar + " Where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
            if (lastXgamesAvgCB.Checked)
            {
                homeTeamStatsQueries.AvgPointsOppQuery = @"SELECT AVG(Cast(AScore as decimal)) FROM (SELECT TOP " + lastXCB.Text + " AScore FROM " + AllNBAMLBResultsVar + " Where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg";
            }

            avg4 = AnalyzerActions.GetDataFromSQL<double>(homeTeamStatsQueries.AvgPointsOppQuery, _connection, true, 2);

            homeTeamStats.AvgPointsOpp = AnalyzerActions.GetDataFromSQL<double>(homeTeamStatsQueries.AvgPointsOppQuery, _connection, true, 2);
            ////////////// MLB AVG VS HAND

            if (radioBtnMLB.Checked == true)
            {

                homeTeamStatsQueries.AvgPointsVsHandOppQuery = @"SELECT AVG(Cast(AScore as decimal)) FROM " + AllNBAMLBResultsVar + " Where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "'";

                if (lastXgamesAvgCB.Checked)
                {
                    homeTeamStatsQueries.AvgPointsVsHandOppQuery = @"SELECT AVG(Cast(AScore as decimal)) FROM (SELECT TOP " +lastXCB.Text+ " AScore FROM " + AllNBAMLBResultsVar + " Where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "' order by ID desc) as avg";
                }

                avg4vsHand = AnalyzerActions.GetDataFromSQL<double>(homeTeamStatsQueries.AvgPointsVsHandOppQuery, _connection, true, 2);

                homeTeamStats.AvgPointsVsHandOpp = AnalyzerActions.GetDataFromSQL<double>(homeTeamStatsQueries.AvgPointsVsHandOppQuery, _connection, true, 2);
            }

            homeTeamStatsQueries.OversQuery = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'O' and Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
            homeTeamStatsQueries.UndersQuery = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'U' and Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
            if (lastXgamesAvgCB.Checked)
            {
                homeTeamStatsQueries.OversQuery = "SELECT COUNT(OUr) FROM (SELECT TOP " + lastXCB.Text + " OUr FROM " + AllNBAMLBResultsVar + " where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where OUr = 'O'";
                homeTeamStatsQueries.UndersQuery = "SELECT COUNT(OUr) FROM (SELECT TOP " + lastXCB.Text + " OUr FROM " + AllNBAMLBResultsVar + " where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where OUr = 'U'";
            }
           
                CountO2 = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.OversQuery, _connection);

                countU2 = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.UndersQuery, _connection);

                homeTeamStats.Overs = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.OversQuery, _connection);

                homeTeamStats.Unders = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.UndersQuery, _connection);

            ///////////////////////////////////////////////////////////////////////////////
            if (radioBtnMLB.Checked == true)
            {
                homeTeamStatsQueries.OversVsHandQuery = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'O' and Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "'";
                homeTeamStatsQueries.UndersVsHandQuery = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'U' and Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "'";
                if (lastXgamesAvgCB.Checked)
                {
                    homeTeamStatsQueries.OversVsHandQuery = "SELECT COUNT(OUr) FROM (SELECT TOP " + lastXCB.Text + " OUr FROM " + AllNBAMLBResultsVar + " where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "' order by ID desc) as avg where OUr = 'O'";
                    homeTeamStatsQueries.UndersVsHandQuery = "SELECT COUNT(OUr) FROM (SELECT TOP " + lastXCB.Text + " OUr FROM " + AllNBAMLBResultsVar + " where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "' order by ID desc) as avg where OUr = 'U'";
                }

                CountO2vsHand = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.OversVsHandQuery, _connection);

                countU2vsHand = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.UndersVsHandQuery, _connection);

                homeTeamStats.OversVsHand = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.OversVsHandQuery, _connection);

                homeTeamStats.UndersVsHand = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.UndersVsHandQuery, _connection);
            }
            ///////////////////////////////////////////////////////////////////////////////
            if (radioBtnNBA.Checked == true)
            {
                homeTeamStatsQueries.ATSWinQuery = "SELECT COUNT(ATSr) FROM " + AllNBAMLBResultsVar + " where ATSr = 'W' and Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                homeTeamStatsQueries.ATSLossQuery = "SELECT COUNT(ATSr) FROM " + AllNBAMLBResultsVar + " where ATSr = 'L' and Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                if (lastXgamesAvgCB.Checked)
                {
                    homeTeamStatsQueries.ATSWinQuery = "SELECT COUNT(ATSr) FROM (SELECT TOP " + lastXCB.Text + " ATSr FROM " + AllNBAMLBResultsVar + " where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where ATSr = 'W'";
                    homeTeamStatsQueries.ATSLossQuery = "SELECT COUNT(ATSr) FROM (SELECT TOP " + lastXCB.Text + " ATSr FROM " + AllNBAMLBResultsVar + " where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where ATSr = 'L'";
                }

                countATS3 = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.ATSWinQuery, _connection);

                countATS4 = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.ATSLossQuery, _connection);

                homeTeamStats.ATSWin = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.ATSWinQuery, _connection);

                homeTeamStats.ATSLoss = AnalyzerActions.GetDataFromSQL<string>(homeTeamStatsQueries.ATSLossQuery, _connection);

            }
            else if (radioBtnMLB.Checked == true)
            {
                homeTeamStatsQueries.SUWinQuery = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                homeTeamStatsQueries.SULossQuery = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                if (lastXgamesAvgCB.Checked)
                {
                    homeTeamStatsQueries.SUWinQuery = "SELECT COUNT(SUr) FROM (SELECT TOP " + lastXCB.Text + " SUr FROM " + AllNBAMLBResultsVar + " where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where SUr = 'W'";
                    homeTeamStatsQueries.SULossQuery = "SELECT COUNT(SUr) FROM (SELECT TOP " + lastXCB.Text + " SUr FROM " + AllNBAMLBResultsVar + " where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' order by ID desc) as avg where SUr = 'L'";
                }

                    countSU3 = AnalyzerActions.GetDataFromSQL<int>(homeTeamStatsQueries.SUWinQuery, _connection);

                    countSU4 = AnalyzerActions.GetDataFromSQL<int>(homeTeamStatsQueries.SULossQuery, _connection);

                    homeTeamStats.SUWin = AnalyzerActions.GetDataFromSQL<int>(homeTeamStatsQueries.SUWinQuery, _connection);

                    homeTeamStats.SULoss = AnalyzerActions.GetDataFromSQL<int>(homeTeamStatsQueries.SULossQuery, _connection);


            }
            //////////SUr VS HAND
            if (radioBtnMLB.Checked == true)
            {
                homeTeamStatsQueries.SUWinVsHandQuery = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "'";
                homeTeamStatsQueries.SULossVsHandQuery = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "'";
                if (lastXgamesAvgCB.Checked)
                {
                    homeTeamStatsQueries.SUWinVsHandQuery = "SELECT COUNT(SUr) FROM (SELECT TOP " + lastXCB.Text + " SUr FROM " + AllNBAMLBResultsVar + " where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "' order by ID desc) as avg where SUr = 'W'";
                    homeTeamStatsQueries.SULossVsHandQuery = "SELECT COUNT(SUr) FROM (SELECT TOP " + lastXCB.Text + " SUr FROM " + AllNBAMLBResultsVar + " where Team = '" + _homeTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "' order by ID desc) as avg where SUr = 'L'";
                }

                countSU3vsHand = AnalyzerActions.GetDataFromSQL<int>(homeTeamStatsQueries.SUWinVsHandQuery, _connection);

                countSU4vsHand = AnalyzerActions.GetDataFromSQL<int>(homeTeamStatsQueries.SULossVsHandQuery, _connection);

                homeTeamStats.SUWinVsHand = AnalyzerActions.GetDataFromSQL<int>(homeTeamStatsQueries.SUWinVsHandQuery, _connection);

                homeTeamStats.SULossVsHand = AnalyzerActions.GetDataFromSQL<int>(homeTeamStatsQueries.SULossVsHandQuery, _connection);
            }

        }

        private void showAveragesinDGV()
        {
            if (TeamsStatsDGV.DataSource != null)
            {
                TeamsStatsDGV.InvokeIfRequired(t =>
                    {
                        TeamsStatsDGV.DataSource = null;
                    });
            }
            else
            {
                TeamsStatsDGV.InvokeIfRequired(t =>
                    {
                        TeamsStatsDGV.Rows.Clear();
                    });
            }

            //  dataGridView9.Refresh();
            TeamsStatsDGV.InvokeIfRequired(t =>
                {
                    TeamsStatsDGV.ColumnCount = 3;


                    coversrow = new string[] { avg1.ToString(), "PPG", avg3.ToString() };
                    TeamsStatsDGV.Rows.Add(coversrow);
                    coversrow = new string[] { avg2.ToString(), "OPPG", avg4.ToString() };
                    TeamsStatsDGV.Rows.Add(coversrow);
                    if (radioBtnNBA.Checked == true)
                    {
                        coversrow = new string[] { countATS1 + " - " + countATS2, "ATS", countATS3 + " - " + countATS4 };
                        TeamsStatsDGV.Rows.Add(coversrow);
                    }
                    else if (radioBtnMLB.Checked == true)
                    {
                        coversrow = new string[] { countSU1 + " - " + countSU2, "SU", countSU3 + " - " + countSU4 };
                        TeamsStatsDGV.Rows.Add(coversrow);
                    }
                    coversrow = new string[] { CountO1 + " - " + countU1, "O/U", CountO2 + " - " + countU2 };
                    TeamsStatsDGV.Rows.Add(coversrow);

                    for (i = 0; i < 4; i++)
                    {
                        TeamsStatsDGV.Rows[i].Cells[1].Style.ForeColor = Color.White;
                    }

                    TeamsStatsDGV.Rows[0].Cells[0].Selected = false;


                    /////////////////////////////////////////////////////////

                    if (radioBtnMLB.Checked == true)
                    {
                        teamsDetailedStatsDGV.InvokeIfRequired(t2 =>
                            {


                                if (teamsDetailedStatsDGV.DataSource != null)
                                {
                                    teamsDetailedStatsDGV.DataSource = null;
                                }
                                else
                                {
                                    teamsDetailedStatsDGV.Rows.Clear();
                                }

                                //  dataGridView10.Refresh();

                                teamsDetailedStatsDGV.ColumnCount = 3;

                                coversrow = new string[] { AwayTeamCB.Text.Split(' ')[1].Replace("Diamondbacks", "D'backs") + Environment.NewLine.ToString() + " vs. " +
                                    HomeStarterHand, "Team", HomeTeamCB.Text.Split(' ')[1].Replace("Diamondbacks", "D'backs") +  Environment.NewLine + " vs. " + AwayStarterHand };
                                teamsDetailedStatsDGV.Rows.Add(coversrow);
                                coversrow = new string[] { avg1vsHand.ToString(), "PPG", avg3vsHand.ToString() };
                                teamsDetailedStatsDGV.Rows.Add(coversrow);
                                coversrow = new string[] { avg2vsHand.ToString(), "OPPG", avg4vsHand.ToString() };
                                teamsDetailedStatsDGV.Rows.Add(coversrow);

                                coversrow = new string[] { countSU1vsHand + " - " + countSU2vsHand, "SU", countSU3vsHand + " - " + countSU4vsHand };
                                teamsDetailedStatsDGV.Rows.Add(coversrow);

                                coversrow = new string[] { CountO1vsHand + " - " + countU1vsHand, "O/U", CountO2vsHand + " - " + countU2vsHand };
                                teamsDetailedStatsDGV.Rows.Add(coversrow);

                                DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
                                boldStyle.Font = new Font("Arial Narrow", 12.0F, FontStyle.Bold);
                                boldStyle.ForeColor = Color.White;

                                DataGridViewCellStyle greenboldStyle = new DataGridViewCellStyle();
                                greenboldStyle.Font = new Font("Arial Narrow", 13F, FontStyle.Bold);
                                greenboldStyle.ForeColor = Color.LightGreen;

                                DataGridViewCellStyle redboldStyle = new DataGridViewCellStyle();
                                redboldStyle.Font = new Font("Arial Narrow", 13F, FontStyle.Bold);
                                redboldStyle.ForeColor = Color.Red;

                                teamsDetailedStatsDGV.Columns[0].Width = 110;
                                teamsDetailedStatsDGV.Columns[1].Width = 70;
                                teamsDetailedStatsDGV.Columns[2].Width = 110;
                                teamsDetailedStatsDGV.Rows[0].Cells[0].Style = boldStyle;
                                teamsDetailedStatsDGV.Rows[0].Cells[2].Style = boldStyle;

                                if(avg1vsHand > avg2vsHand && avg1vsHand - avg2vsHand <= 1)
                                {
                                    teamsDetailedStatsDGV.Rows[1].Cells[0].Style.ForeColor = Color.LightGreen;
                                    teamsDetailedStatsDGV.Rows[2].Cells[0].Style.ForeColor = Color.LightGreen;
                                }
                                else if (avg1vsHand > avg2vsHand && avg1vsHand - avg2vsHand >= 1)
                                {
                                    teamsDetailedStatsDGV.Rows[1].Cells[0].Style = greenboldStyle;
                                    teamsDetailedStatsDGV.Rows[2].Cells[0].Style.ForeColor = Color.LightGreen;
                                }
                                else if (avg1vsHand < avg2vsHand && avg1vsHand - avg2vsHand >= -1)
                                {
                                    teamsDetailedStatsDGV.Rows[1].Cells[0].Style.ForeColor = Color.Red;
                                    teamsDetailedStatsDGV.Rows[2].Cells[0].Style.ForeColor = Color.Red;
                                }
                                else if (avg1vsHand < avg2vsHand && avg1vsHand - avg2vsHand <= -1)
                                {
                                    teamsDetailedStatsDGV.Rows[1].Cells[0].Style.ForeColor = Color.Red;
                                    teamsDetailedStatsDGV.Rows[2].Cells[0].Style = redboldStyle;
                                }

                                if (avg3vsHand > avg4vsHand && avg3vsHand - avg4vsHand <= 1)
                                {
                                    teamsDetailedStatsDGV.Rows[1].Cells[2].Style.ForeColor = Color.LightGreen;
                                    teamsDetailedStatsDGV.Rows[2].Cells[2].Style.ForeColor = Color.LightGreen;
                                }
                                else if (avg3vsHand > avg4vsHand && avg3vsHand - avg4vsHand >= 1)
                                {
                                    teamsDetailedStatsDGV.Rows[1].Cells[2].Style = greenboldStyle;
                                    teamsDetailedStatsDGV.Rows[2].Cells[2].Style.ForeColor = Color.LightGreen;
                                }
                                else if (avg3vsHand < avg4vsHand && avg3vsHand - avg4vsHand >= -1)
                                {
                                    teamsDetailedStatsDGV.Rows[1].Cells[2].Style.ForeColor = Color.Red;
                                    teamsDetailedStatsDGV.Rows[2].Cells[2].Style.ForeColor = Color.Red;
                                }
                                else if (avg3vsHand < avg4vsHand && avg3vsHand - avg4vsHand <= -1)
                                {
                                    teamsDetailedStatsDGV.Rows[1].Cells[2].Style.ForeColor = Color.Red;
                                    teamsDetailedStatsDGV.Rows[2].Cells[2].Style = redboldStyle;
                                }

                                if (countSU1vsHand > countSU2vsHand)
                                {
                                    teamsDetailedStatsDGV.Rows[3].Cells[0].Style.ForeColor = Color.LightGreen;
                                }
                                else
                                {
                                    teamsDetailedStatsDGV.Rows[3].Cells[0].Style.ForeColor = Color.Red;
                                }

                                if (countSU3vsHand > countSU4vsHand)
                                {
                                    teamsDetailedStatsDGV.Rows[3].Cells[2].Style.ForeColor = Color.LightGreen;
                                }
                                else
                                {
                                    teamsDetailedStatsDGV.Rows[3].Cells[2].Style.ForeColor = Color.Red;
                                }

                                if (Convert.ToDouble(CountO1vsHand) > Convert.ToDouble(countU1vsHand) &&
                                Convert.ToDouble(CountO1vsHand) - Convert.ToDouble(countU1vsHand) < 8)
                                {
                                    teamsDetailedStatsDGV.Rows[4].Cells[0].Style.ForeColor = Color.Red;
                                }
                                else if (Convert.ToDouble(CountO1vsHand) > Convert.ToDouble(countU1vsHand) &&
                                Convert.ToDouble(CountO1vsHand) - Convert.ToDouble(countU1vsHand) >= 8)
                                {
                                    teamsDetailedStatsDGV.Rows[4].Cells[0].Style = redboldStyle;
                                }
                                else if(Convert.ToDouble(CountO1vsHand) < Convert.ToDouble(countU1vsHand) &&
                                Convert.ToDouble(CountO1vsHand) - Convert.ToDouble(countU1vsHand) > -8)
                                {
                                    teamsDetailedStatsDGV.Rows[4].Cells[0].Style.ForeColor = Color.LightGreen;
                                }
                                else if (Convert.ToDouble(CountO1vsHand) < Convert.ToDouble(countU1vsHand) &&
                               Convert.ToDouble(CountO1vsHand) - Convert.ToDouble(countU1vsHand) <= -8)
                                {
                                    teamsDetailedStatsDGV.Rows[4].Cells[0].Style = greenboldStyle;
                                }

                                if (Convert.ToDouble(CountO2vsHand) > Convert.ToDouble(countU2vsHand) &&
                                Convert.ToDouble(CountO2vsHand) - Convert.ToDouble(countU2vsHand) < 8)
                                {
                                    teamsDetailedStatsDGV.Rows[4].Cells[2].Style.ForeColor = Color.Red;
                                }
                                else if (Convert.ToDouble(CountO2vsHand) > Convert.ToDouble(countU2vsHand) &&
                                Convert.ToDouble(CountO2vsHand) - Convert.ToDouble(countU2vsHand) >= 8)
                                {
                                    teamsDetailedStatsDGV.Rows[4].Cells[2].Style = redboldStyle;
                                }
                                else if (Convert.ToDouble(CountO2vsHand) < Convert.ToDouble(countU2vsHand) &&
                                Convert.ToDouble(CountO2vsHand) - Convert.ToDouble(countU2vsHand) > -8)
                                {
                                    teamsDetailedStatsDGV.Rows[4].Cells[2].Style.ForeColor = Color.LightGreen;
                                }
                                else if (Convert.ToDouble(CountO2vsHand) < Convert.ToDouble(countU2vsHand) &&
                               Convert.ToDouble(CountO2vsHand) - Convert.ToDouble(countU2vsHand) <= -8)
                                {
                                    teamsDetailedStatsDGV.Rows[4].Cells[2].Style = greenboldStyle;
                                }

                                for (i = 0; i < 5; i++)
                                {
                                    teamsDetailedStatsDGV.Rows[i].Cells[1].Style.ForeColor = Color.White;
                                }

                                teamsDetailedStatsDGV.Rows[0].Cells[0].Selected = false;
                            });
                    }
                });
        }

        private void showAVGvsDIV()
        {
            if (radioBtnMLB.Checked == true)
            {
                String queryyvsdiv = @"SELECT AVG(Cast(AScore as decimal)) FROM " + AllNBAMLBResultsVar + " as AMR, Klubovi as KL where AMR.Opp = '" + _awayTeam.KratkoIme + "' and ARM.season = '" + SeasonNBAMLB +
                    "' and AMR.Team = KL.KratkoIme and KL.Division = (Select Division from Klubovi where KratkoIme = '" + _awayTeam.KratkoIme + "')";

                SqlCommand cmddvsDIV2 = new SqlCommand(queryyvsdiv, _connection);
                SqlDataReader readdvsDIV2 = cmddvsDIV2.ExecuteReader();

                while (readdvsDIV2.Read())
                {
                    avg1vsDIV = (Convert.ToDouble(readdvsDIV2[0]));
                }
                readdvsDIV2.Close();
                avg1vsDIV = Math.Round(avg1vsDIV, 2);
            }
            ///////////////////////////////////////////////////////////////////////////////
            if (radioBtnMLB.Checked == true)
            {
                String queryy700vsH = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'O' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                String queryy701vsH = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'U' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                SqlCommand cmdd700vsH = new SqlCommand(queryy700vsH, _connection);
                SqlDataReader readd700vsH = cmdd700vsH.ExecuteReader();

                while (readd700vsH.Read())
                {
                    CountO1vsDIV = readd700vsH[0].ToString();
                }
                readd700vsH.Close();

                SqlCommand cmdd701vsH = new SqlCommand(queryy701vsH, _connection);
                SqlDataReader readd701vsH = cmdd701vsH.ExecuteReader();

                while (readd701vsH.Read())
                {
                    countU1vsDIV = readd701vsH[0].ToString();
                }
                readd701vsH.Close();
            }
            ///////////////////////////////////////////////////////////////////////////////

            else if (radioBtnMLB.Checked == true)
            {
                String queryySU1 = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";
                String queryySU2 = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "'";

                SqlCommand cmdd702 = new SqlCommand(queryySU1, _connection);
                SqlDataReader readd702 = cmdd702.ExecuteReader();

                while (readd702.Read())
                {
                    countSU1svDIV = Convert.ToInt16(readd702[0]);
                }
                readd702.Close();

                SqlCommand cmdd703 = new SqlCommand(queryySU2, _connection);
                SqlDataReader readd703 = cmdd703.ExecuteReader();

                while (readd703.Read())
                {
                    countSU2vsDIV = Convert.ToInt16(readd703[0]);
                }
                readd703.Close();
            }
            /////////////SUr vs HAND
            if (radioBtnMLB.Checked == true)
            {
                String queryySU1vHand = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                String queryySU2vHand = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";

                SqlCommand cmdd702vsH = new SqlCommand(queryySU1vHand, _connection);
                SqlDataReader readd702vsH = cmdd702vsH.ExecuteReader();

                while (readd702vsH.Read())
                {
                    countSU1vsDIV = Convert.ToInt16(readd702vsH[0]);
                }
                readd702vsH.Close();

                SqlCommand cmdd703vsH = new SqlCommand(queryySU2vHand, _connection);
                SqlDataReader readd703vsH = cmdd703vsH.ExecuteReader();

                while (readd703vsH.Read())
                {
                    countSU2vsDIV = Convert.ToInt16(readd703vsH[0]);
                }
                readd703vsH.Close();

            }

            /////////////  AVERAGE VS HAND L/R - MLB

            if (radioBtnMLB.Checked == true)
            {
                String queryyvsDIV = @"SELECT AVG(Cast(HScore as decimal)) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + _awayTeam.KratkoIme + "' and season = '" + SeasonNBAMLB +
                    "' and StarterHHand = '" + HomeStarterHand + "' ";

                SqlCommand cmddvsDIV = new SqlCommand(queryyvsDIV, _connection);
                SqlDataReader readdvsDIV = cmddvsDIV.ExecuteReader();

                while (readdvsDIV.Read())
                {
                    avg2vsDIV = (Convert.ToDouble(readdvsDIV[0]));
                }
                readdvsDIV.Close();
                avg2vsDIV = Math.Round(avg2vsDIV, 2);
            }

            if (vsDIVDGV.DataSource != null)
            {
                vsDIVDGV.DataSource = null;
            }
            else
            {
                vsDIVDGV.Rows.Clear();
            }

            //  vsDIVDGV.Refresh();

            vsDIVDGV.ColumnCount = 3;


            coversrow = new string[] { avg1.ToString(), "PPG", avg3.ToString() };
            vsDIVDGV.Rows.Add(coversrow);
            coversrow = new string[] { avg2.ToString(), "OPPG", avg4.ToString() };
            vsDIVDGV.Rows.Add(coversrow);
            if (radioBtnNBA.Checked == true)
            {
                coversrow = new string[] { countATS1 + " - " + countATS2, "ATS", countATS3 + " - " + countATS4 };
                vsDIVDGV.Rows.Add(coversrow);
            }
            else if (radioBtnMLB.Checked == true)
            {
                coversrow = new string[] { countSU1 + " - " + countSU2, "SU", countSU3 + " - " + countSU4 };
                vsDIVDGV.Rows.Add(coversrow);
            }
            coversrow = new string[] { CountO1 + " - " + countU1, "O/U", CountO2 + " - " + countU2 };
            vsDIVDGV.Rows.Add(coversrow);

            for (i = 0; i < 4; i++)
            {
                vsDIVDGV.Rows[i].Cells[1].Style.ForeColor = Color.White;
            }

            vsDIVDGV.Rows[0].Cells[0].Selected = false;


            /////////////////////////////////////////////////////////

            if (radioBtnMLB.Checked == true)
            {
                if (vsnoDIVDGV.DataSource != null)
                {
                    vsnoDIVDGV.DataSource = null;
                }
                else
                {
                    vsnoDIVDGV.Rows.Clear();
                }

                //  vsnoDIVDGV.Refresh();

                vsnoDIVDGV.ColumnCount = 3;


                coversrow = new string[] { avg1vsDIV.ToString(), "PPG", avg3vsDIV.ToString() };
                vsnoDIVDGV.Rows.Add(coversrow);
                coversrow = new string[] { avg2vsDIV.ToString(), "OPPG", avg4vsDIV.ToString() };
                vsnoDIVDGV.Rows.Add(coversrow);
                if (radioBtnMLB.Checked == true)
                {
                    coversrow = new string[] { countSU1vsDIV + " - " + countSU2vsDIV, "SU", countSU3vsDIV + " - " + countSU4vsDIV };
                    vsnoDIVDGV.Rows.Add(coversrow);
                }
                coversrow = new string[] { CountO1vsDIV + " - " + countU1vsDIV, "O/U", CountO2vsDIV + " - " + countU2vsDIV };
                vsnoDIVDGV.Rows.Add(coversrow);

                for (i = 0; i < 4; i++)
                {
                    vsnoDIVDGV.Rows[i].Cells[1].Style.ForeColor = Color.White;
                }

                vsnoDIVDGV.Rows[0].Cells[0].Selected = false;
                label47.Text = AwayTeamCB.Text.Split(' ')[1].Replace("Diamondbacks", "D'backs") + " vs. " + HomeStarterHand;
                label48.Text = HomeTeamCB.Text.Split(' ')[1].Replace("Diamondbacks", "D'backs") + " vs. " + AwayStarterHand;
            }
        }


        private void awayStarterCB_TextChanged(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                if (awayStarterCB.Text.Length > 1)
                {
                    AwayStarterHand = null;
                    String query133 = "Select Hand from MLBStarters where FirstName = '" + awayStarterCB.Text.Split(' ')[0] + "' and LastName = '" + awayStarterCB.Text.Split(' ')[1] + "'";
                    SqlCommand cmd133 = new SqlCommand(query133, connection);
                    SqlDataReader read133 = cmd133.ExecuteReader();
                    while (read133.Read())
                    {
                        AwayStarterHand = (read133["Hand"].ToString());
                    }
                    read133.Close();

                    if (AwayStarterHand == null || AwayStarterHand.Contains('?'))
                    {
                        AwayStarterHand = matchupsAndScoresDGV.Rows[matchupsAndScoresDGV.CurrentCell.RowIndex].Cells[5].Value.ToString().Split(' ')[2].Replace("(", "");
                        AwayStarterHand = AwayStarterHand.Replace(")", "");
                    }

                    AStarterHandLabel.Text = "(" + AwayStarterHand + ")";

                    if (AwayTeamTabControl.SelectedIndex == 1)
                    {
                        AwayStarterL50();
                    }
                }
            }
        }

        private void AwayTeamCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (OkladesEntities context = new OkladesEntities())
            {
                try
                {
                    _awayTeamValue = (string)AwayTeamCB.SelectedItem;
                    _awayTeam = _analyzerActions.FillTeams(_awayTeamValue);

                    if (awayTeamLogoPB.Visible == true)
                    {
                        awayTeamLogoPB.Image = null;
                    }
                    label22.Text = null;
                    label23.Text = null;
                    ATS1 = null;
                    OU1 = null;

                    ///// LOAD TEAM LOGO /////
                    var tL = Task.Run(() =>
                    {
                        _teamLogos.LoadTeamsLogos(awayTeamLogoPB, _awayTeamValue);
                    });

                    awayStarterCB.Items.Clear();

                    var tas = Task.Run(() =>
                    {
                        using (OkladesEntities context2 = new OkladesEntities())
                        {
                            var awayStarters = context2.MLBStarters.Where(s => s.Team == _awayTeam.KratkoIme)
                                .OrderBy(s => s.LastName)
                                .Select(s => new
                                {
                                    s.FirstName,
                                    s.LastName
                                }).ToList();

                            foreach (var starter in awayStarters)
                            {
                                awayStarterCB.InvokeIfRequired(a =>
                                {
                                    awayStarterCB.Items.Add((starter.FirstName + " " + starter.LastName));
                                });
                            }
                        }
                    });

                    idkluba1 = _awayTeam.ID.ToString();
                    linkat = _awayTeam.CoversID.ToString();

                    label12.Text = " " + _awayTeam.KratkoIme + " last 50 games";

                    // -------------------------------------------AWAY TEAM LAST 10-------------------------------------
                    atl10 = Task.Run(() =>
                    {
                        lock (lockA)
                        {
                            getTeamsAndStartersGames.GetGames(radioBtnNBA, radioBtnMLB, _awayTeam, SeasonNBAMLB, awayStarterCB, ATeamL50DGV);
                            _dgva.FormatLast50Games(ATeamL50DGV, _awayTeam, radioBtnNBA);
                        }
                    });
                    // -------------------------------------------AWAY TEAM LAST 10-------------------------------------

                    if (radioBtnNBA.Checked)
                    {
                        Task calcAvgA = Task.Run(() =>
                        {
                            CalculateAveragesSuouAwayTeam();
                        }).ContinueWith((prevTask) =>
                        {
                            showAveragesinDGV();
                        });
                        var tI = Task.Run(() =>
                        {
                            
                        });
                        InjuryAway(sender, e);
                    }

                    ActiveControl = label1;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
        }

        private void HomeTeamCB_SelectedValueChanged(object sender, EventArgs e)
        {
            using (OkladesEntities context = new OkladesEntities())
            {
                object homeTeamLast10 = null;
                _homeTeamValue = (string)HomeTeamCB.SelectedItem;
                _homeTeam = _analyzerActions.FillTeams(_homeTeamValue);

                if (homeTeamLogoPB.Visible)
                {
                    homeTeamLogoPB.Image = null;
                }

                TotalBox.Enabled = true;
                HandicapBox.Enabled = true;

                label24.Text = null;
                label25.Text = null;

                ATS2 = null;
                OU2 = null;

                var tL = Task.Run(() =>
                {
                    _teamLogos.LoadTeamsLogos(homeTeamLogoPB, _homeTeamValue);
                });

                homeStarterCB.Items.Clear();
                var tas = Task.Run(() =>
                     {
                         using (OkladesEntities context2 = new OkladesEntities())
                         {
                             var homeStarters = context2.MLBStarters.Where(s => s.Team == _homeTeam.KratkoIme)
                               .OrderBy(s => s.LastName)
                               .Select(s => new
                               {
                                   s.FirstName,
                                   s.LastName
                               }).ToList();

                             foreach (var starter in homeStarters)
                             {
                                 homeStarterCB.InvokeIfRequired(a =>
                                    {
                                        homeStarterCB.Items.Add((starter.FirstName + " " + starter.LastName));
                                    });
                             }
                         }
                     });
                idkluba1 = _homeTeam.ID.ToString();
                linkht = _homeTeam.CoversID.ToString();

                if (radioBtnMLB.Checked == true)
                {
                    _parkFactor = _homeTeam.ParkFactor;

                    L47.Text = _parkFactor.ToString();
                }

                if (AwayTeamCB.Text != "")
                {
                    TotalBox.BackColor = Color.OrangeRed;
                    HandicapBox.BackColor = Color.OrangeRed;
                }

                label42.Text = " " + _homeTeam.KratkoIme + " last 50 games";

                // -------------------------------------------HOME TEAM LAST 10-------------------------------------

                htl10 = Task.Run(() =>
                        {
                            lock (lockA)
                            {
                                getTeamsAndStartersGames.GetGames(radioBtnNBA, radioBtnMLB, _homeTeam, SeasonNBAMLB, homeStarterCB, HTeamL50DGV);
                                _dgva.FormatLast50Games(HTeamL50DGV, _homeTeam, radioBtnNBA);
                            }
                        });
                // -------------------------------------------HOME TEAM LAST 10-------------------------------------

                if (radioBtnNBA.Checked == true)
                {
                    Task calcAvgH = Task.Run(() =>
                     {
                         
                     }).ContinueWith((prevTask) =>
                     {
                         
                     });
                    CalculateAveragesSuouHomeTeam();
                    showAveragesinDGV();

                    var tI = Task.Run(() =>
                    {
                        pace_eff();
                        
                    });
                    InjuryHome(sender, e);
                }
                ActiveControl = label1;
            }
        }

        private void TotalBox_Validating(object sender, CancelEventArgs e)
        {
            if (TotalBox.Text.Length >= 1)
            {
                string input = TotalBox.Text;
                double input1 = Convert.ToDouble(input);
                if (radioBtnNBA.Checked == true)
                {
                    if (input1 <= 250 && input1 >= 160)
                    {
                        TotalBox.BackColor = Color.WhiteSmoke;
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Incorrect data in Total Box", "Important Message", MessageBoxButtons.OK,
                 MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    if (input1 <= 15 && input1 >= 4)
                    {
                        TotalBox.BackColor = Color.WhiteSmoke;
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Incorrect data in Total Box", "Important Message", MessageBoxButtons.OK,
                 MessageBoxIcon.Exclamation);
                    }
                }

            }


        }

        private void HandicapBox_Validating(object sender, CancelEventArgs e)
        {
            if (HandicapBox.Text != "")
            {
                string input = HandicapBox.Text;
                double input1 = Convert.ToDouble(input);
                if (radioBtnNBA.Checked == true)
                {
                    if (input1 <= 30 && input1 >= -30)
                    {
                        HandicapBox.BackColor = Color.WhiteSmoke;
                        button3_Click(sender, e);
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Incorrect data in Handicap Box", "Important Message", MessageBoxButtons.OK,
                 MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    if (input1 <= 3 && input1 >= -3)
                    {
                        HandicapBox.BackColor = Color.WhiteSmoke;
                        button3_Click(sender, e);
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Incorrect data in Handicap Box", "Important Message", MessageBoxButtons.OK,
                 MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DGVAnalysisResults.Rows[e.RowIndex].Cells[0].Selected)
            {


                IDoklBox.Text = DGVAnalysisResults.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                _connection.Open();

                IDOkl11 = Convert.ToInt32(IDoklBox.Text);

                _connection.Close();

                GetTeamsandStarters();

                TotalBox.BackColor = Color.White;
                HandicapBox.BackColor = Color.White;


                LoadGameBtn_Click(sender, e);

            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                IDO = Convert.ToInt32(IDoklBox.Text);
                if (radioBtnMLB.Checked == true)
                {
                    String query3 = "Select ID,ParkFactor from Klubovi where ImeKluba = '" + _homeTeamValue + "'";

                    SqlCommand cmd3 = new SqlCommand(query3, connection);
                    SqlDataReader read3 = cmd3.ExecuteReader();

                    while (read3.Read())
                    {
                        IDH = Convert.ToInt32(read3["ID"]);
                        _parkFactor = Convert.ToDecimal(read3["ParkFactor"]);
                    }
                    read3.Close();
                }
                else if (radioBtnNBA.Checked == true)
                {
                    String query3 = "Select ID from Klubovi where ImeKluba = '" + _homeTeamValue + "'";

                    SqlCommand cmd3 = new SqlCommand(query3, connection);
                    SqlDataReader read3 = cmd3.ExecuteReader();

                    while (read3.Read())
                    {
                        IDH = Convert.ToInt32(read3["ID"]);
                    }
                    read3.Close();
                }

                String query4 = "Select ID from Klubovi where ImeKluba = '" + _awayTeamValue + "'";
                SqlCommand cmd4 = new SqlCommand(query4, connection);
                SqlDataReader read4 = cmd4.ExecuteReader();
                while (read4.Read())
                {
                    IDA = Convert.ToInt32(read4["ID"]);
                }
                read4.Close();

                foreach (DataGridViewRow row in DGVAnalysisResults.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["chk"].Value) == true)
                    {
                        int rez1;
                        int rez2;
                        rez1 = Convert.ToInt32(row.Cells["HScore"].Value);
                        rez2 = Convert.ToInt32(row.Cells["AScore"].Value);
                        int totrez = rez1 + rez2;
                        int handirez = rez2 - rez1;
                        String query5 = @"INSERT INTO NBAResults(ImeHKluba, ImeAKluba,HResult,AResult, IDOklade, TotalResult, HandicapResult, Season, Line, Total)
                                VALUES ('" + row.Cells["Team"].Value.ToString() + "', '" + row.Cells["Opp"].Value.ToString() +
                       "', '" + row.Cells["HScore"].Value.ToString() + "','" + row.Cells["AScore"].Value.ToString() + "','" + IDO + "', '" + totrez +
                       "', '" + handirez + "'," + row.Cells["Season"].Value.ToString() + "," + row.Cells["Line"].Value.ToString().Replace(",", ".") +
                       "," + row.Cells["Total"].Value.ToString().Replace(",", ".") + ")";

                        SqlCommand cmd111 = new SqlCommand(query5, connection);

                        cmd111.ExecuteNonQuery();
                    }

                }
                button19_Click(sender, e);
                finishandCalcPB.Enabled = true;
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {

                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    { DGVAnalysisResults.Columns.Remove("chk2"); }

                }
                DGVAnalysisResults.DataSource = null;

                IDO = Convert.ToInt32(IDoklBox.Text);
                connection.Open();
                Sql3 = "select ID,ImeHKluba,ImeAKluba, HResult,AResult,TotalResult,HandicapResult, Season, Line, Total, HomePace, AwayPace from " + NBAMLBResultsVar + " where IDOklade = '" + IDO + "'";
                ds = new DataSet();
                adapter = new SqlDataAdapter(Sql3, connection);
                ds.Reset();
                adapter.Fill(ds);
                DGVAnalysisResults.DataSource = ds.Tables[0];
                DGVAnalysisResults.Columns[0].Width = 53;


                _dgva.ColorDgv1RowsAndCalculate(DGVAnalysisResults, TotalBox, HandicapBox, out _isOver, out _isUnder, out _hAway, out _hHome, out _hAvg, out _aAvg);

            }
        }

        private void dateTimePicker1_MouseDown(object sender, MouseEventArgs e)
        {
            //dateTimePicker1_ValueChanged(sender, e);

        }


        private void results1_Click(object sender, EventArgs e)
        {



        }

        private void results2_Click(object sender, EventArgs e)
        {
            if (HomeTeamCB.Text.Length > 1)
            {
                ResultUpdater frm3 = new ResultUpdater();
                frm3.value = HomeTeamCB.Text.Split(' ')[1];
                frm3.Show();
                frm3.comboBox1.Text = HomeTeamCB.Text;
                frm3.button7_Click(sender, e);
            }
        }

        private void results1_MouseHover(object sender, EventArgs e)
        {
            results1.Cursor = Cursors.Hand;
            results1.Image = Oklade.Properties.Resources.statistics2;
        }

        private void results1_MouseLeave(object sender, EventArgs e)
        {
            results1.Image = Oklade.Properties.Resources.statistics1;

        }

        private void results2_MouseHover(object sender, EventArgs e)
        {
            results2.Cursor = Cursors.Hand;
            results2.Image = Oklade.Properties.Resources.statistics2;

        }

        private void results2_MouseLeave(object sender, EventArgs e)
        {
            results2.Image = Oklade.Properties.Resources.statistics1;

        }

        private void AutoProcess_Click_1(object sender, EventArgs e)
        {
            _pacecbaway = "";
            _pacecbhome = "";

            using (OkladesEntities context = new OkladesEntities())
            {
                _awayTil2 = _awayTeam.TeamIsLike;
                _homeTil1 = _homeTeam.TeamIsLike;

                if (TotalBox.Text.Length < 1)
                {
                    return;
                }
                else
                    if (DGVAnalysisResults.Columns.Contains("chk"))
                    {
                        DGVAnalysisResults.Columns.Remove("chk");
                        if (DGVAnalysisResults.Columns.Contains("chk2"))
                        { DGVAnalysisResults.Columns.Remove("chk2"); }

                    }
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    IDO = Convert.ToInt32(IDoklBox.Text);

                    if (hteamradio.Checked == true)
                    {
                        HA = "Home";
                        HAteam22 = _homeTeamValue.Split(' ')[1];
                    }
                    else if (ateamradio.Checked == true)
                    {
                        HA = "Away";
                        HAteam22 = _awayTeamValue.Split(' ')[1];
                    }

                    if (radioBtnMLB.Checked == true)
                    {
                        IDH = _homeTeam.ID;
                        _teamOuh = _homeTeam.TeamOUH;
                        _parkFactor = Convert.ToDecimal(_homeTeam.ParkFactor);
                        IDA = _awayTeam.ID;
                        _teamOua = _awayTeam.TeamOUA;
                    }
                    else if (radioBtnNBA.Checked == true)
                    {
                        IDH = _homeTeam.ID;
                        _teamOuh = _homeTeam.TeamOUH;
                        _varpacehome = Convert.ToDouble(_homeTeam.PaceHome);
                        IDA = _awayTeam.ID;
                        _teamOua = _awayTeam.TeamOUA;
                        _varpaceaway = Convert.ToDouble(_awayTeam.PaceAway);
                    }

                    _awaytil22 = _awayTil2;
                    _awaytil23 = _awaytil22 + 1;
                    _awaytil24 = _awaytil22 - 1;

                    _hometil12 = _homeTil1;
                    _hometil13 = _hometil12 + 1;
                    _hometil14 = _hometil12 - 1;

                    if (lastSeasonCB.Checked & thisSeasonCB.Checked == false)
                    {
                        _season1 = SeasonNBAMLB - 1;
                        _season2 = SeasonNBAMLB - 1;
                    }
                    if (lastSeasonCB.Checked & thisSeasonCB.Checked)
                    {
                        var dateMonth = DateTime.Now.Month;
                        if (dateMonth > 9 && dateMonth < 11)
                        {
                            _season2 = SeasonNBAMLB;
                            _season1 = SeasonNBAMLB + 1;
                        }
                        else
                        {
                            _season1 = SeasonNBAMLB - 1;
                            _season2 = SeasonNBAMLB;
                        }
                    }
                    if (lastSeasonCB.Checked == false & thisSeasonCB.Checked)
                    {
                            _season2 = SeasonNBAMLB;
                            _season1 = SeasonNBAMLB;
                    }
                    if (ouCheckbox.Checked == false)
                    {
                        _teamOuh = "[o-u]%";
                        _teamOua = "[o-u]%";
                    }

                    if (paceCB.Checked)
                    {
                        if (_varpacehome >= 97.5)
                        {
                            _varpacehomelow = _varpacehome - 3;
                            _strpacehomelow = _varpacehomelow.ToString().Replace(",", ".");
                            _varpacehomehigh = _varpacehome + 1.8;
                            _strpacehomehigh = _varpacehomehigh.ToString().Replace(",", ".");
                        }
                        else if (_varpacehome <= 92)
                        {
                            _varpacehomelow = _varpacehome - 1.8;
                            _strpacehomelow = _varpacehomelow.ToString().Replace(",", ".");
                            _varpacehomehigh = _varpacehome + 2.6;
                            _strpacehomehigh = _varpacehomehigh.ToString().Replace(",", ".");
                        }
                        else
                        {
                            _varpacehomelow = _varpacehome - 1.8;
                            _strpacehomelow = _varpacehomelow.ToString().Replace(",", ".");
                            _varpacehomehigh = _varpacehome + 1.8;
                            _strpacehomehigh = _varpacehomehigh.ToString().Replace(",", ".");
                        }
                        _pacecbhome = "and Klubovi.PaceHome between " + _strpacehomelow + " and " + _strpacehomehigh + "";

                        if (_varpaceaway >= 97.5)
                        {
                            _varpaceawaylow = _varpaceaway - 3;
                            _strpaceawaylow = _varpaceawaylow.ToString().Replace(",", ".");
                            _varpaceawayhigh = _varpaceaway + 1.8;
                            _strpaceawayhigh = _varpaceawayhigh.ToString().Replace(",", ".");
                        }
                        else if (_varpaceaway <= 92)
                        {
                            _varpaceawaylow = _varpaceaway - 1.8;
                            _strpaceawaylow = _varpaceawaylow.ToString().Replace(",", ".");
                            _varpaceawayhigh = _varpaceaway + 2.6;
                            _strpaceawayhigh = _varpaceawayhigh.ToString().Replace(",", ".");
                        }
                        else
                        {
                            _varpaceawaylow = _varpaceaway - 1.8;
                            _strpaceawaylow = _varpaceawaylow.ToString().Replace(",", ".");
                            _varpaceawayhigh = _varpaceaway + 1.8;
                            _strpaceawayhigh = _varpaceawayhigh.ToString().Replace(",", ".");
                        }
                        _pacecbaway = "and Klubovi.PaceHome between " + _strpaceawaylow + " and " + _strpaceawayhigh + "";
                    }

                    if (rankingsCB.Checked == true)
                    {
                        _strRankingsAway = " HisLike IN ('" + _homeTil1 + "','" + _hometil13 + "','" + _hometil14 + "') and";
                        _strRankingsHome = " AisLike IN ('" + _awayTil2 + "','" + _awaytil23 + "','" + _awaytil24 + "') and";
                    }
                    else
                    {
                        _strRankingsAway = "";
                        _strRankingsHome = "";
                    }
                    _lastXGames = Convert.ToInt32(LastXgamesCB.Text);

                    if (radioBtnNBA.Checked == true)
                    {
                        //// GET HOME TEAM TESULTS ////
                        var tilList2 = new int?[] { _awayTil2, _awaytil23, _awaytil24 };
                        _analyzerActions.InsertNbaResultsHome(_teamOua, _season1, _season2, tilList2, _homeTeam, _awayTeam, _lastXGames, IDO, ouCheckbox, paceCB, rankingsCB);
                        //// GET AWAY TEAM RESULTS ////
                        var tilList = new int?[] { _homeTil1, _hometil13, _hometil14 };
                        _analyzerActions.InsertNbaResultsAway(_teamOuh, _season1, _season2, tilList, _awayTeam, _homeTeam, _lastXGames, IDO, ouCheckbox, paceCB, rankingsCB);

                    }
                    else if (radioBtnMLB.Checked == true)
                    {
                        //// GET HOME TEAM TESULTS ////

                        _analyzerActions.InsertMlbResultsHome(_teamOua, _season1, _season2, useFullNameCB, _lastXGames, _homeStarterFirstName, _homeStarterLastName, IDO);

                        if (_analyzerActions.HomeStarterGames < 4)
                        {
                            _addGames = _lastXGames - _analyzerActions.HomeStarterGames;
                            _analyzerActions.InsertMlbExtraGames(_homeTeam, _awayTeam, _teamOua, _season1, _season2, _addGames, IDO, AwayStarterHand, _homeStarterLastName, _awayStarterLastName, "Home");
                        }

                        //// GET AWAY TEAM TESULTS ////

                        _analyzerActions.InsertMlbResultsAway(_teamOuh, _season1, _season2, useFullNameCB, _lastXGames, _awayStarterFirstName, _awayStarterLastName, _homeStarterLastName, IDO);

                        if (_analyzerActions.AwayStarterGames < 4)
                        {
                            _addGames = _lastXGames - _analyzerActions.AwayStarterGames;
                            _analyzerActions.InsertMlbExtraGames(_homeTeam, _awayTeam, _teamOuh, _season1, _season2, _addGames, IDO, HomeStarterHand, _awayStarterLastName, _homeStarterLastName, "Away");
                        }
                    }

                    decimal total = Convert.ToDecimal(TotalBox.Text);
                    decimal line = Convert.ToDecimal(HandicapBox.Text);

                    if (radioBtnMLB.Checked == true)
                    {
                        _isOver = context.MLBResults.Count(m => m.IDOklade == IDO && m.HResult + m.AResult > total);
                        _isUnder = context.MLBResults.Count(m => m.IDOklade == IDO && m.HResult + m.AResult < total);
                        _hHome = context.MLBResults.Count(m => m.IDOklade == IDO && m.AResult - m.HResult < line);
                        _hAway = context.MLBResults.Count(m => m.IDOklade == IDO && m.AResult - m.HResult > line);
                    }
                    else if (radioBtnNBA.Checked == true)
                    {
                        _isOver = context.NBAResults.Count(m => m.IDOklade == IDO && m.HResult + m.AResult > total);
                        _isUnder = context.NBAResults.Count(m => m.IDOklade == IDO && m.HResult + m.AResult < total);
                        _hHome = context.NBAResults.Count(m => m.IDOklade == IDO && m.AResult - m.HResult < line);
                        _hAway = context.NBAResults.Count(m => m.IDOklade == IDO && m.AResult - m.HResult > line);
                    }

                    finishandCalcPB_Click(sender, e);

                    HandicapBox.BackColor = Color.White;
                    TotalBox.BackColor = Color.White;
                    linkk(sender, e);
                    this.ActiveControl = null;
                }
            }
        }

        private void autoProcess_MouseHover_1(object sender, EventArgs e)
        {
            AutoProcessBtn.Cursor = Cursors.Hand;
            AutoProcessBtn.Image = Oklade.Properties.Resources.marketing_automation_icon2;

        }

        private void autoProcess_MouseLeave_1(object sender, EventArgs e)
        {
            AutoProcessBtn.Image = Oklade.Properties.Resources.marketing_automation_icon1;

        }

        private void updateResultsPB_MouseHover(object sender, EventArgs e)
        {
            updateResultsPB.Cursor = Cursors.Hand;
            updateResultsPB.Image = Oklade.Properties.Resources.update2;

        }

        private void updateResultsPB_MouseLeave(object sender, EventArgs e)
        {
            updateResultsPB.Image = Oklade.Properties.Resources.update;

        }

        private void updateResultsPB_Click(object sender, EventArgs e)
        {

            if (TotalBox.Text.Length < 1)
            {
                return;
            }
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                if (TotalBox.Text != " ")
                    if (DGVAnalysisResults.DataSource != null)
                    {
                        {
                            _dgva.ColorDgv1RowsAndCalculate(DGVAnalysisResults, TotalBox, HandicapBox, out _isOver, out _isUnder, out _hAway, out _hHome, out _hAvg, out _aAvg);

                            if (radioBtnNBA.Checked == true)
                            {
                                query4Update = @"UPDATE NBABets SET Total ='" + TotalBox.Text.ToString().Replace(",", ".") + "' ,Handicap = '" +
                                                                        HandicapBox.Text.ToString().Replace(",", ".") + "', IsOver = '" + _isOver + "',IsUnder = '" + _isUnder +
                                                                        "', HomeHendi = '" + _hHome + "', AwayHendi = '" + _hAway + "' WHERE IDOklade = '" + IDoklBox.Text + "'";
                            }
                            else
                            {
                                query4Update = @"UPDATE MLBBets SET Total ='" + TotalBox.Text.ToString().Replace(",", ".") + "' ,Handicap = '" +
                                                                            HandicapBox.Text.ToString().Replace(",", ".") + "', IsOver = '" + _isOver + "',IsUnder = '" + _isUnder +
                                                                            "', HomeHendi = '" + _hHome + "', AwayHendi = '" + _hAway + "' WHERE IDOklade = '" + IDoklBox.Text + "'";
                            }
                            SqlCommand cmd112 = new SqlCommand(query4Update, connection);

                            cmd112.ExecuteNonQuery();

                            Dgv2(sender, e);

                            this.ActiveControl = null;
                        }
                    }
            }
        }

        private void nextGamePB_Click(object sender, EventArgs e)
        {
            this.SuspendLayout();
            _imeHKluba = "";
            _imeAKluba = "";

            _awayTeamValue = "";
            _homeTeamValue = "";

            IDOkl11 = Convert.ToInt32(IDoklBox.Text);
            IDOkl11 = IDOkl11 + 1;

            provjera2();
            GetTeamsandStarters();

            while (_imeAKluba == "" && IDOkl11 < Top1IDOklade)
            {
                IDOkl11 = IDOkl11 + 1;
                GetTeamsandStarters();
            }

            if (_imeAKluba == "")
            {
                return;
            }

            IDoklBox.Text = IDOkl11.ToString();

            TotalBox.BackColor = Color.White;
            HandicapBox.BackColor = Color.White;

            LoadGameBtn_Click(sender, e);
            _dgva.ColorDgv1RowsAndCalculate(DGVAnalysisResults, TotalBox, HandicapBox, out _isOver, out _isUnder, out _hAway, out _hHome, out _hAvg, out _aAvg);
            Thread.Sleep(100);
            //   updateResultsPB_Click(sender, e);
            this.ResumeLayout();
        }

        private void GetTeamsandStarters()
        {
            using (OkladesEntities context = new OkladesEntities())
            {
                if (radioBtnMLB.Checked == true)
                {
                    var teamsAndStarters = context.MLBBETS.FirstOrDefault(t => t.IDOklade == IDOkl11);
                    if (teamsAndStarters != null)
                    {
                        _imeHKluba = teamsAndStarters.ImeHKluba;
                        _imeAKluba = teamsAndStarters.ImeAKluba;
                    }
                    if (teamsAndStarters != null)
                    {
                        HomeStarterVar = teamsAndStarters.HomeStarter;
                        AwayStarterVar = teamsAndStarters.AwayStarter;
                    }
                }
                else if (radioBtnNBA.Checked == true)
                {
                    var teamsAndStarters = context.NBABETS.FirstOrDefault(t => t.IDOklade == IDOkl11);
                    if (teamsAndStarters != null)
                    {
                        _imeHKluba = teamsAndStarters.ImeHKluba;
                        _imeAKluba = teamsAndStarters.ImeAKluba;
                    }
                }

                if (_imeAKluba == "")
                {
                    return;
                }
                else
                {
                    _awayTeamValue = _imeAKluba;
                    _homeTeamValue = _imeHKluba;
                }

                AwayTeamCB.Text = _awayTeamValue;
                HomeTeamCB.Text = _homeTeamValue;
                awayStarterCB.Text = AwayStarterVar;
                homeStarterCB.Text = HomeStarterVar;
            }
        }

        private void previousGamePB_Click(object sender, EventArgs e)
        {
            cleanitup(sender, e);
            this.SuspendLayout();

            IDOkl11 = Convert.ToInt32(IDoklBox.Text);
            IDOkl11 = IDOkl11 - 1;
            varProvjera = "";

            provjera();

            while (varProvjera.Length < 1)
            {
                IDOkl11 = IDOkl11 - 1;
                provjera();
            }

            IDoklBox.Text = IDOkl11.ToString();

            GetTeamsandStarters();

            TotalBox.BackColor = Color.White;
            HandicapBox.BackColor = Color.White;

            LoadGameBtn_Click(sender, e);
            _dgva.ColorDgv1RowsAndCalculate(DGVAnalysisResults, TotalBox, HandicapBox, out _isOver, out _isUnder, out _hAway, out _hHome, out _hAvg, out _aAvg);
            // updateResultsPB_Click(sender, e);
            this.ResumeLayout();
        }

        private void provjera()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                String queryy = "Select TOP 1 ImeHKluba from " + NBAMLBBetsVar + " where IDOklade = '" + IDOkl11 + "'";
                SqlCommand cmdd = new SqlCommand(queryy, connection);
                SqlDataReader read22 = cmdd.ExecuteReader();

                while (read22.Read())
                {
                    varProvjera = (read22["ImeHKluba"].ToString());
                }
                read22.Close();
            }
        }

        private void provjera2()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                query4PB7 = "Select TOP 1 IDOklade from " + NBAMLBBetsVar + " order by IDOklade desc";

                SqlCommand cmd3 = new SqlCommand(query4PB7, connection);
                SqlDataReader read3 = cmd3.ExecuteReader();

                while (read3.Read())
                {
                    Top1IDOklade = Convert.ToInt32(read3["IDOklade"]);
                }
                read3.Close();
            }
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            nextGamePB.Cursor = Cursors.Hand;
            nextGamePB.Image = Oklade.Properties.Resources.arrow_right2;
        }

        private void pictureBox6_MouseLeave(object sender, EventArgs e)
        {
            nextGamePB.Image = Oklade.Properties.Resources.arrow_right1;
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            previousGamePB.Cursor = Cursors.Hand;
            previousGamePB.Image = Oklade.Properties.Resources.back_arrow2;
        }

        private void pictureBox7_MouseLeave(object sender, EventArgs e)
        {
            previousGamePB.Image = Oklade.Properties.Resources.back_arrow1;
        }


        private void deleteBtn_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                DialogResult d = MessageBox.Show("Are you sure you want to delete this game?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                try
                {

                    if (d == DialogResult.Yes)
                    {

                        str1Delete = "DELETE from " + NBAMLBBetsVar + " where IDOklade=" + IDoklBox.Text + "";

                        SqlCommand cmd = new SqlCommand(str1Delete, connection);
                        cmd.ExecuteNonQuery();


                        str2Delete = "DELETE from " + NBAMLBResultsVar + " where IDOklade=" + IDoklBox.Text + "";

                        SqlCommand cmd2 = new SqlCommand(str2Delete, connection);
                        cmd2.ExecuteNonQuery();
                        DGVAnalysisResults.DataSource = "";
                        dataGridView2.DataSource = "";
                        dataGridView5.DataSource = "";
                        AwayTeamCB.Text = "";
                        HomeTeamCB.Text = "";
                        TotalBox.Text = "";
                        HandicapBox.Text = "";
                        awayTeamLogoPB.Hide();
                        homeTeamLogoPB.Hide();
                        label19.Text = "";
                        label21.Text = "";
                        label22.Text = "";
                        label23.Text = "";
                        label24.Text = "";
                        label25.Text = "";
                        label27.Text = "";
                        label28.Text = "";
                        label33.Text = "";
                        label34.Text = "";
                        labelAwayAvg.Text = "";
                        labelHomeAvg.Text = "";
                        labelTotalAvg.Text = "";
                        label12.Text = "";
                        label42.Text = "";
                        ATeamL50DGV.DataSource = "";
                        HTeamL50DGV.DataSource = "";

                        cleanitup(sender, e);
                    }
                    else if (d == DialogResult.No)
                    {

                        return;

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
        }

        private void pictureBox9_MouseHover(object sender, EventArgs e)
        {
            pictureBox9.Cursor = Cursors.Hand;
            pictureBox9.Image = Oklade.Properties.Resources.Delete2;

        }

        private void pictureBox9_MouseLeave(object sender, EventArgs e)
        {
            pictureBox9.Image = Oklade.Properties.Resources.Delete;

        }

        private void EditTeamsbtn_Click(object sender, EventArgs e)
        {
            cleanitup(sender, e);
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                String Sql5 = "select ID, ImeKluba, TeamIsLike, TeamOUH, TeamOUA, PaceHome, PaceAway, OeffHome, OeffAway,DeffHome,DeffAway from Klubovi where League = '" + LeagueNBAMLB + "' order by ImeKluba";
                ds = new DataSet();
                adapter = new SqlDataAdapter(Sql5, connection);
                ds.Reset();
                adapter.Fill(ds);
                DGVAnalysisResults.DataSource = ds.Tables[0];

                DGVAnalysisResults.Columns[0].Width = 25;
                DGVAnalysisResults.Columns[1].Width = 140;
            }
        }

        private void pictureBox11_MouseHover(object sender, EventArgs e)
        {
            pictureBox11.Cursor = Cursors.Hand;
            pictureBox11.Image = Oklade.Properties.Resources.Edit2;
        }

        private void pictureBox11_MouseLeave(object sender, EventArgs e)
        {
            pictureBox11.Image = Oklade.Properties.Resources.Edit1;

        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                String Sql5 = "select ID, ImeKluba, TeamIsLike, TeamOUH, TeamOUA, PaceHome, PaceAway, OeffHome, OeffAway,DeffHome,DeffAway from Klubovi where League = '" + LeagueNBAMLB + "' order by ImeKluba";
                adapter = new SqlDataAdapter(Sql5, connection);
                SqlCommandBuilder cmd228 = new SqlCommandBuilder(adapter);

                try
                {
                    adapter.Update(ds);
                    MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                NewGameBtn_Click(sender, e);
            }
        }

        private void pictureBox12_MouseHover(object sender, EventArgs e)
        {
            pictureBox12.Cursor = Cursors.Hand;
            pictureBox12.Image = Oklade.Properties.Resources.floppysave2;

        }

        private void pictureBox12_MouseLeave(object sender, EventArgs e)
        {
            pictureBox12.Image = Oklade.Properties.Resources.floppysave1;

        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            ResultUpdater frm3 = new ResultUpdater();
            frm3.Show();
        }

        private void pictureBox13_MouseHover(object sender, EventArgs e)
        {
            pictureBox13.Cursor = Cursors.Hand;
            pictureBox13.Image = Oklade.Properties.Resources.scoreboard2;

        }

        private void pictureBox13_MouseLeave(object sender, EventArgs e)
        {
            pictureBox13.Image = Oklade.Properties.Resources.scoreboard1;

        }

        private void finishandCalcPB_Click(object sender, EventArgs e)
        {
            getGamesBtn.Enabled = false;
            PickGamesBtn.Enabled = false;

            using (OkladesEntities context = new OkladesEntities())
            {

                if (IDH == 0)
                {
                    IDH = _homeTeam.ID;
                    _parkFactor = _homeTeam.ParkFactor;

                    IDA = _awayTeam.ID;

                }


                timenoww = DateTime.Now.ToString("dd.MM.yyyy.");
                string timenow = DateTime.Now.ToShortTimeString();
                timenow = Regex.Replace(timenow, ":", "").Trim();
                int timenow3 = Convert.ToInt16(timenow);

                if (timenow3 < 300)
                {
                    timenoww = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy.");
                }

                try
                {
                    if (radioBtnNBA.Checked == true)
                    {

                        _analyzerActions.InsertNbaBets(IDH, IDA, IDO, _homeTeamValue, _awayTeamValue, TotalBox, HandicapBox, _isOver, _isUnder, _hHome, _hAway, timenoww);

                    }
                    else if (radioBtnMLB.Checked == true)
                    {

                        _analyzerActions.InsertMlbBets(IDH, homeStarterCB.Text, IDA, awayStarterCB.Text, IDO, _homeTeamValue, _awayTeamValue,
                                                        TotalBox, HandicapBox, _isOver, _isUnder, _hHome, _hAway, timenoww, _parkFactor);
                    }



                    LoadGameBtn_Click(sender, e);

                    _isOver = 0;
                    _isUnder = 0;
                    _hAway = 0;
                    _hHome = 0;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
        }

        private void pictureBox14_MouseHover(object sender, EventArgs e)
        {
            finishandCalcPB.Cursor = Cursors.Hand;
            finishandCalcPB.Image = Oklade.Properties.Resources.finishcalc2;

        }

        private void pictureBox14_MouseLeave(object sender, EventArgs e)
        {
            finishandCalcPB.Image = Oklade.Properties.Resources.finishcalc1;

        }

        private void cleanitup(object sender, EventArgs e)
        {
            this.SuspendLayout();
            getGamesBtn.Enabled = false;
            PickGamesBtn.Enabled = false;

            StartersStatsDGV.DataSource = null;
            DGVAnalysisResults.DataSource = null;
            dataGridView2.DataSource = null;
            ATeamL50DGV.DataSource = null;
            HTeamL50DGV.DataSource = null;
            dataGridView5.DataSource = null;
            TeamsStatsDGV.DataSource = null;
            teamsDetailedStatsDGV.DataSource = null;
            AStarterL50DGV.DataSource = null;
            TeamsStatsDGV.Rows.Clear();
            teamsDetailedStatsDGV.Rows.Clear();

            if (ToBetOnCB.Checked == true)
            {
                ToBetOnCB.Checked = false;
            }
            if (BetProcessedCB.Checked == true)
            {
                BetProcessedCB.Checked = false;
            }
            if (editedCB.Checked == true)
            {
                editedCB.Checked = false;
            }

            TotalBox.Enabled = false;
            HandicapBox.Enabled = false;

            if (AwayTeamCB.Text.Length > 1)
            {
                AwayTeamCB.Text = "";
                HomeTeamCB.Text = "";
            }

            AStarterHandLabel.Text = "";
            HStarterHandLabel.Text = "";

            _awayTeamValue = "";
            _homeTeamValue = "";
            TotalBox.Text = "";
            HandicapBox.Text = "";
            teamBox.Text = "";
            lineBox1.Text = "";
            lineBox2.Text = "";
            label19.Text = "";
            label21.Text = "";
            label22.Text = "";
            label23.Text = "";
            label24.Text = "";
            label25.Text = "";
            label27.Text = "";
            label28.Text = "";
            label33.Text = "";
            label34.Text = "";
            labelAwayAvg.Text = "";
            labelHomeAvg.Text = "";
            labelTotalAvg.Text = "";
            label12.Text = "";
            label42.Text = "";
            label43.Text = "";
            L47.Text = "";
            richTextBox1.Text = "";
            richTextBox3.Text = "";

            if (radioBtnMLB.Checked == true)
            {
                awayStarterCB.Text = "";
                awayStarterCB.Enabled = false;
                homeStarterCB.Text = "";
                homeStarterCB.Enabled = false;
            }
            if (richTextBox3.Controls.Contains(linkLabel1))
            {
                richTextBox3.Controls.Remove(linkLabel1);
            }
            if (richTextBox3.Controls.Contains(linkLabel2))
            {
                richTextBox3.Controls.Remove(linkLabel2);
            }
            if (richTextBox3.Controls.Contains(linkLabel3))
            {
                richTextBox3.Controls.Remove(linkLabel3);
            }
            if (richTextBox3.Controls.Contains(linkLabel4))
            {
                richTextBox3.Controls.Remove(linkLabel4);
            }
            if (richTextBox3.Controls.Contains(linkLabel5))
            {
                richTextBox3.Controls.Remove(linkLabel5);
            }
            if (richTextBox3.Controls.Contains(linkLabel6))
            {
                richTextBox3.Controls.Remove(linkLabel6);
            }
            if (richTextBox3.Controls.Contains(linkLabel7))
            {
                richTextBox3.Controls.Remove(linkLabel7);
            }
            if (richTextBox1.Controls.Contains(linkLabel8))
            {
                richTextBox1.Controls.Remove(linkLabel8);
            }
            if (richTextBox1.Controls.Contains(linkLabel9))
            {
                richTextBox1.Controls.Remove(linkLabel9);
            }
            if (richTextBox1.Controls.Contains(linkLabel10))
            {
                richTextBox1.Controls.Remove(linkLabel10);
            }
            if (richTextBox1.Controls.Contains(linkLabel11))
            {
                richTextBox1.Controls.Remove(linkLabel11);
            }
            if (richTextBox1.Controls.Contains(linkLabel12))
            {
                richTextBox1.Controls.Remove(linkLabel12);
            }
            if (richTextBox1.Controls.Contains(linkLabel13))
            {
                richTextBox1.Controls.Remove(linkLabel13);
            }
            if (richTextBox1.Controls.Contains(linkLabel14))
            {
                richTextBox1.Controls.Remove(linkLabel14);
            }
            this.ResumeLayout();

        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            cleanitup(sender, e);

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    { DGVAnalysisResults.Columns.Remove("chk2"); }

                }

                string Datum2 = dateTimePicker1.Value.Date.AddDays(-1).ToShortDateString();
                dateTimePicker1.Text = Datum2;

            }
        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            cleanitup(sender, e);

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    { DGVAnalysisResults.Columns.Remove("chk2"); }

                }

                string Datum2 = dateTimePicker1.Value.Date.AddDays(+1).ToShortDateString();
                dateTimePicker1.Text = Datum2;


            }
        }

        private void pictureBox16_MouseHover(object sender, EventArgs e)
        {
            prevPB.Cursor = Cursors.Hand;
            prevPB.Image = Oklade.Properties.Resources.back_arrow2;
        }

        private void pictureBox16_MouseLeave(object sender, EventArgs e)
        {
            prevPB.Image = Oklade.Properties.Resources.back_arrow1;
        }

        private void pictureBox17_MouseHover(object sender, EventArgs e)
        {
            nextPB.Cursor = Cursors.Hand;
            nextPB.Image = Oklade.Properties.Resources.arrow_right2;
        }

        private void pictureBox17_MouseLeave(object sender, EventArgs e)
        {
            nextPB.Image = Oklade.Properties.Resources.arrow_right1;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            string linkawayteam = "http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/teams/team" + linkat + ".html";
            Process.Start(linkawayteam);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            string linkhometeam = "http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/teams/team" + linkht + ".html";
            Process.Start(linkhometeam);
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            awayTeamLogoPB.Cursor = Cursors.Hand;
            awayTeamLogoPB.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            homeTeamLogoPB.Cursor = Cursors.Hand;
            homeTeamLogoPB.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            awayTeamLogoPB.BorderStyle = BorderStyle.None;
        }

        private void pictureBox3_MouseLeave(object sender, EventArgs e)
        {
            homeTeamLogoPB.BorderStyle = BorderStyle.None;
        }
        private void InjuryAway(object sender, EventArgs e)
        {
            string teamname;

            if (radioBtnNBA.Checked == true)
            {
                    teamname = AwayTeamCB.Text;
                
                if (teamname == "LA Lakers")
                {
                    teamname = "Los Angeles Lakers";
                }
                else if (teamname == "LA Clippers")
                {
                    teamname = "Los Angeles Clippers";
                }
                else if (teamname == "OKC Thunder")
                {
                    teamname = "Oklahoma City";
                }
                else if (teamname == "GS Warriors")
                {
                    teamname = "Golden State";
                }
                else if (teamname == "NewOrleans Pelicans")
                {
                    teamname = "New Orleans";
                }
                else if (teamname == "NewYork Knicks")
                {
                    teamname = "New York";
                }
                else if (teamname == "SanAntonio Spurs")
                {
                    teamname = "San Antonio";
                }
                else if (teamname == "Min Timberwolves")
                {
                    teamname = "Minnesota";
                }
                else if (teamname == "Phi Seventysixers")
                {
                    teamname = "Philadelphia";
                }
                else if (teamname == "Por Trailblazers")
                {
                    teamname = "Portland";
                }

                string teamname2 = "";

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();


                    string queryy339 = "Select ID from KluboviForInjuries where ImeKluba = '" + AwayTeamCB.Text + "'";
                    SqlCommand cmdd339 = new SqlCommand(queryy339, connection);
                    SqlDataReader readd339 = cmdd339.ExecuteReader();
                    while (readd339.Read())
                    {
                        IDKlubaInj = (readd339["ID"].ToString());
                    }
                    readd339.Close();

                    int IDKlubaInj2 = Convert.ToInt16(IDKlubaInj);
                    IDKlubaInj2 = IDKlubaInj2 + 1;

                    string queryy33 = "Select ImeKluba from KluboviForInjuries where ID = '" + IDKlubaInj2 + "'";
                    SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                    SqlDataReader readd33 = cmdd33.ExecuteReader();
                    while (readd33.Read())
                    {
                        teamname2 = (readd33["ImeKluba"].ToString());
                    }
                    readd33.Close();

                    if (IDKlubaInj2 == 31)
                    {
                        teamname2 = "Washington Wizards";
                    }

                    //  teamname2 = teamname2.Split(' ')[1];
                    if (teamname2 == "LA Lakers")
                    {
                        teamname2 = "Los Angeles Lakers";
                    }
                    else if (teamname2 == "LA Clippers")
                    {
                        teamname2 = "Los Angeles Clippers";
                    }
                    else if (teamname2 == "OKC Thunder")
                    {
                        teamname2 = "Oklahoma City";
                    }
                    else if (teamname2 == "GS Warriors")
                    {
                        teamname2 = "Golden State";
                    }
                    else if (teamname2 == "NewOrleans Pelicans")
                    {
                        teamname2 = "New Orleans";
                    }
                    else if (teamname2 == "NewYork Knicks")
                    {
                        teamname2 = "New York";
                    }
                    else if (teamname2 == "SanAntonio Spurs")
                    {
                        teamname2 = "San Antonio";
                    }
                    else if (teamname2 == "Min Timberwolves")
                    {
                        teamname2 = "Minnesota";
                    }
                    else if (teamname2 == "Phi Seventysixers")
                    {
                        teamname2 = "Philadelphia";
                    }
                    else if (teamname2 == "Por Trailblazers")
                    {
                        teamname2 = "Portland";
                    }

                    int i = 1;
                    string team = "";
                    string team2 = "";

                    if (htmlString.Length > 1)
                    {                       

                        Match mTeam = Regex.Match(htmlString, @"" + teamname + "(.*?)</td>", RegexOptions.Singleline);
                        if (mTeam.Success)
                        {
                            team = mTeam.Groups[i].Value;
                        }
                        index = mTeam.Index;

                        int duzinastringa = htmlString.Length;
                        duzinastringa = duzinastringa - index;

                        if (IDKlubaInj2 != 31)
                        {
                            Match mTeam2 = Regex.Match(htmlString.Substring(index, duzinastringa), @"" + teamname2 + "(.*?)</tr>", RegexOptions.Singleline);
                            {
                                team2 = mTeam2.Groups[i].Value;
                            }
                            index2 = mTeam2.Index;
                        }

                        if (IDKlubaInj2 == 31)
                        {
                            index2 = duzinastringa;
                        }

                        string playername = "";
                        string playername2 = "";
                        string playername3 = "";
                        string playername4 = "";
                        string playername5 = "";
                        string playername6 = "";
                        string playername7 = "";
                        Match mPlayer = Regex.Match(htmlString.Substring(index, index2), @".html"">(.*?)</a>", RegexOptions.Singleline);
                        if (mPlayer.Success)
                        {
                            playername = mPlayer.Groups[1].Value;
                            // //Console.WriteLine(playername);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        playername = Regex.Replace(playername, "(.*?)>", "").Trim();
                        int index3 = mPlayer.Index;
                        //Console.WriteLine("Index of match: " + index3);
                        //Console.WriteLine("Player 1:" + playername);


                        ////////////////////////////////////////////////////////////////

                        if (playername.Length > 1)
                        {
                            mPlayer = mPlayer.NextMatch();
                            if (mPlayer.Success)
                            {
                                playername2 = mPlayer.Groups[1].Value;
                                playername2 = Regex.Replace(playername2, "(.*?)>", "").Trim();
                                index4 = mPlayer.Index;
                                ////Console.WriteLine("Index of match: " + index4);
                                ////Console.WriteLine("Player 2:" + playername2);
                            }
                            if (playername2.Length > 1)
                            {
                                mPlayer = mPlayer.NextMatch();
                                if (mPlayer.Success)
                                {
                                    playername3 = mPlayer.Groups[1].Value;
                                    playername3 = Regex.Replace(playername3, "(.*?)>", "").Trim();
                                    index5 = mPlayer.Index;
                                    ////Console.WriteLine("Index of match: " + index5);
                                    ////Console.WriteLine("Player 3:" + playername3);
                                }
                                if (playername3.Length > 1)
                                {
                                    mPlayer = mPlayer.NextMatch();
                                    if (mPlayer.Success)
                                    {
                                        playername4 = mPlayer.Groups[1].Value;
                                        playername4 = Regex.Replace(playername4, "(.*?)>", "").Trim();

                                        index6 = mPlayer.Index;
                                        ////Console.WriteLine("Index of match: " + index6);
                                        ////Console.WriteLine("Player 4:" + playername4);
                                    }
                                    if (playername4.Length > 1)
                                    {
                                        mPlayer = mPlayer.NextMatch();
                                        if (mPlayer.Success)
                                        {
                                            playername5 = mPlayer.Groups[1].Value;
                                            playername5 = Regex.Replace(playername5, "(.*?)>", "").Trim();

                                            index7 = mPlayer.Index;
                                            //Console.WriteLine("Index of match: " + index7);
                                            //Console.WriteLine("Player 5:" + playername5);
                                        }
                                        if (playername5.Length > 1)
                                        {
                                            mPlayer = mPlayer.NextMatch();
                                            if (mPlayer.Success)
                                            {
                                                playername6 = mPlayer.Groups[1].Value;
                                                playername6 = Regex.Replace(playername6, "(.*?)>", "").Trim();

                                                index8 = mPlayer.Index;
                                                //Console.WriteLine("Index of match: " + index8);
                                                //Console.WriteLine("Player 6:" + playername6);
                                            }

                                            if (playername6.Length > 1)
                                            {
                                                mPlayer = mPlayer.NextMatch();
                                                if (mPlayer.Success)
                                                {
                                                    playername7 = mPlayer.Groups[1].Value;
                                                    playername7 = Regex.Replace(playername7, "(.*?)>", "").Trim();

                                                    int index9 = mPlayer.Index;
                                                    //Console.WriteLine("Index of match: " + index9);
                                                    //Console.WriteLine("Player 7:" + playername7);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        if (IDKlubaInj2 == 31)
                        {
                            index2 = duzinastringa - index4;
                        }

                        if (playername.Length > 1)
                        {
                            Match mLink = Regex.Match(htmlString.Substring(index, index2), @"<a href=""(.*?)"">" + playername + "", RegexOptions.Singleline);
                            if (mLink.Success)
                            {
                                link11 = mLink.Groups[1].Value;
                                //Console.WriteLine(link11);
                            }
                            link11 = "http://www.covers.com" + link11;

                            if (playername2.Length > 1)
                            {
                                if (IDKlubaInj2 == 31)
                                {
                                    index2 = duzinastringa - index4;
                                }
                                position = index + index4 - 90;
                                Match mLink2 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername2 + "", RegexOptions.Singleline);
                                if (mLink2.Success)
                                {
                                    link12 = mLink2.Groups[1].Value;
                                    //Console.WriteLine(link12);
                                }
                                link12 = "http://www.covers.com" + link12;

                                if (playername3.Length > 1)
                                {
                                    if (IDKlubaInj2 == 31)
                                    {
                                        index2 = duzinastringa - index5;
                                    }
                                    position = index + index5 - 90;
                                    Match mLink3 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername3 + "", RegexOptions.Singleline);
                                    if (mLink3.Success)
                                    {
                                        link13 = mLink3.Groups[1].Value;
                                        //Console.WriteLine(link13);
                                    }
                                    link13 = "http://www.covers.com" + link13;

                                    if (playername4.Length > 1)
                                    {
                                        if (IDKlubaInj2 == 31)
                                        {
                                            index2 = duzinastringa - index6;
                                        }
                                        position = index + index6 - 90;
                                        Match mLink4 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername4 + "", RegexOptions.Singleline);
                                        if (mLink4.Success)
                                        {
                                            link14 = mLink4.Groups[1].Value;
                                            //Console.WriteLine(link14);
                                        }
                                        link14 = "http://www.covers.com" + link14;

                                        if (playername5.Length > 1)
                                        {
                                            if (IDKlubaInj2 == 31)
                                            {
                                                index2 = duzinastringa - index7;
                                            }
                                            position = index + index7 - 90;
                                            Match mLink5 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername5 + "", RegexOptions.Singleline);
                                            if (mLink5.Success)
                                            {
                                                link15 = mLink5.Groups[1].Value;
                                                //Console.WriteLine(link15);
                                            }
                                            link15 = "http://www.covers.com" + link15;

                                            if (playername6.Length > 1)
                                            {
                                                if (IDKlubaInj2 == 31)
                                                {
                                                    index2 = duzinastringa - index8;
                                                }
                                                position = index + index8 - 90;
                                                Match mLink6 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername6 + "", RegexOptions.Singleline);
                                                if (mLink6.Success)
                                                {
                                                    link16 = mLink6.Groups[1].Value;
                                                    //Console.WriteLine(link16);
                                                }
                                                link16 = "http://www.covers.com" + link16;

                                                if (playername7.Length > 1)
                                                {
                                                    if (IDKlubaInj2 == 31)
                                                    {
                                                        index2 = duzinastringa - index9;
                                                    }
                                                    position = index + index9 - 90;
                                                    Match mLink7 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername7 + "", RegexOptions.Singleline);
                                                    if (mLink7.Success)
                                                    {
                                                        link17 = mLink7.Groups[1].Value;
                                                        //Console.WriteLine(link17);
                                                    }
                                                    link17 = "http://www.covers.com" + link17;
                                                }

                                            }
                                        }
                                    }
                                }
                            }

                        }


                        /////////////////////////////////////////////////////////////////////////
                        string injuryreason = "";
                        string injuryreason2 = "";
                        string injuryreason3 = "";
                        string injuryreason4 = "";
                        string injuryreason5 = "";
                        string injuryreason6 = "";
                        string injuryreason7 = "";
                        Match mInjury = Regex.Match(htmlString.Substring(index, index2), @"<strong>(.*?)</td>", RegexOptions.Singleline);
                        if (mInjury.Success)
                        {
                            injuryreason = mInjury.Groups[1].Value;
                            // //Console.WriteLine(playername);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        injuryreason = Regex.Replace(injuryreason, "</strong>", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, "/+", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, @"[\d]", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, @"&#;", "'");
                        if (injuryreason.Length > 1)
                        {
                            int duzina = injuryreason.Length - 2;
                            injuryreason = injuryreason.Substring(0, duzina);
                        }
                        //Console.WriteLine("Injury reason of player 1:" + injuryreason);

                        if (injuryreason.Length > 1)
                        {
                            mInjury = mInjury.NextMatch();
                            if (mInjury.Success)
                            {
                                injuryreason2 = mInjury.Groups[1].Value;
                                injuryreason2 = Regex.Replace(injuryreason2, "</strong>", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, "/+", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, @"[\d]", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, @"&#;", "'");
                                int duzina2 = injuryreason2.Length - 2;
                                injuryreason2 = injuryreason2.Substring(0, duzina2);
                                //Console.WriteLine("Injury reason of player 2:" + injuryreason2);
                            }
                            if (injuryreason2.Length > 1)
                            {
                                mInjury = mInjury.NextMatch();
                                if (mInjury.Success)
                                {
                                    injuryreason3 = mInjury.Groups[1].Value;
                                    injuryreason3 = Regex.Replace(injuryreason3, "</strong>", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, "/+", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, @"[\d]", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, @"&#;", "'");
                                    int duzina3 = injuryreason3.Length - 2;
                                    injuryreason3 = injuryreason3.Substring(0, duzina3);

                                    //Console.WriteLine("Injury reason of player 3:" + injuryreason3);
                                }

                                if (injuryreason3.Length > 1)
                                {
                                    mInjury = mInjury.NextMatch();
                                    if (mInjury.Success)
                                    {
                                        injuryreason4 = mInjury.Groups[1].Value;
                                        injuryreason4 = Regex.Replace(injuryreason4, "</strong>", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, "/+", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, @"[\d]", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, @"&#;", "'");
                                        int duzina4 = injuryreason4.Length - 2;
                                        injuryreason4 = injuryreason4.Substring(0, duzina4);

                                        //Console.WriteLine("Injury reason of player 4:" + injuryreason4);
                                    }

                                    if (injuryreason4.Length > 1)
                                    {
                                        mInjury = mInjury.NextMatch();
                                        if (mInjury.Success)
                                        {
                                            injuryreason5 = mInjury.Groups[1].Value;
                                            injuryreason5 = Regex.Replace(injuryreason5, "</strong>", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, "/+", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, @"[\d]", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, @"&#;", "'");
                                            int duzina5 = injuryreason5.Length - 2;
                                            injuryreason5 = injuryreason5.Substring(0, duzina5);

                                            //Console.WriteLine("Injury reason of player 5:" + injuryreason5);
                                        }

                                        if (injuryreason5.Length > 1)
                                        {
                                            mInjury = mInjury.NextMatch();
                                            if (mInjury.Success)
                                            {
                                                injuryreason6 = mInjury.Groups[1].Value;
                                                injuryreason6 = Regex.Replace(injuryreason6, "</strong>", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, "/+", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, @"[\d]", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, @"&#;", "'");
                                                int duzina6 = injuryreason6.Length - 2;
                                                injuryreason6 = injuryreason6.Substring(0, duzina6);

                                                //Console.WriteLine("Injury reason of player 4:" + injuryreason6);
                                            }

                                            if (injuryreason6.Length > 1)
                                            {
                                                mInjury = mInjury.NextMatch();
                                                if (mInjury.Success)
                                                {
                                                    injuryreason7 = mInjury.Groups[1].Value;
                                                    injuryreason7 = Regex.Replace(injuryreason7, "</strong>", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, "/+", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, @"[\d]", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, @"&#;", "'");
                                                    int duzina7 = injuryreason7.Length - 2;
                                                    injuryreason7 = injuryreason7.Substring(0, duzina7);

                                                    //Console.WriteLine("Injury reason of player 7:" + injuryreason7);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }





                        if (richTextBox1.Controls.Contains(linkLabel8))
                        {
                            richTextBox1.Controls.Remove(linkLabel8);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel9))
                        {
                            richTextBox1.Controls.Remove(linkLabel9);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel10))
                        {
                            richTextBox1.Controls.Remove(linkLabel10);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel11))
                        {
                            richTextBox1.Controls.Remove(linkLabel11);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel12))
                        {
                            richTextBox1.Controls.Remove(linkLabel12);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel13))
                        {
                            richTextBox1.Controls.Remove(linkLabel13);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel14))
                        {
                            richTextBox1.Controls.Remove(linkLabel14);
                        }

                        if (playername.Length < 1)
                        {
                            richTextBox1.Text = "No injuries to report.";
                        }

                        if (playername.Length > 1 && playername != "Advertising")
                        {
                            richTextBox1.Text = "    " + AwayTeamCB.Text.Split(' ')[1] + " Injuries:\n=>";
                            linkLabel8.Text = playername;
                            linkLabel8.AutoSize = true;
                            linkLabel8.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                            richTextBox1.Controls.Add(linkLabel8);
                            richTextBox1.AppendText(linkLabel8.Text + "\n  " + injuryreason + "\n");
                            richTextBox1.SelectionStart = richTextBox1.TextLength;

                            if (playername2.Length > 1 && playername2 != "Advertising")
                            {
                                linkLabel9.Text = playername2;
                                richTextBox1.AppendText("=>");
                                linkLabel9.AutoSize = true;
                                linkLabel9.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                richTextBox1.Controls.Add(linkLabel9);
                                richTextBox1.AppendText("\n  " + injuryreason2 + "\n");
                                richTextBox1.SelectionStart = richTextBox1.TextLength;

                                if (playername3.Length > 1 && playername3 != "Advertising")
                                {
                                    linkLabel10.Text = playername3;
                                    richTextBox1.AppendText("=>");
                                    linkLabel10.AutoSize = true;
                                    linkLabel10.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                    richTextBox1.Controls.Add(linkLabel10);
                                    richTextBox1.AppendText("\n  " + injuryreason3 + "\n");
                                    richTextBox1.SelectionStart = richTextBox1.TextLength;

                                    if (playername4.Length > 1 && playername4 != "Advertising")
                                    {
                                        linkLabel11.Text = playername4;
                                        richTextBox1.AppendText("=>");
                                        linkLabel11.AutoSize = true;
                                        linkLabel11.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                        richTextBox1.Controls.Add(linkLabel11);
                                        richTextBox1.AppendText("\n  " + injuryreason4 + "\n");
                                        richTextBox1.SelectionStart = richTextBox1.TextLength;

                                        if (playername5.Length > 1 && playername5 != "Advertising")
                                        {
                                            linkLabel12.Text = playername5;
                                            richTextBox1.AppendText("=>");
                                            linkLabel12.AutoSize = true;
                                            linkLabel12.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                            richTextBox1.Controls.Add(linkLabel12);
                                            richTextBox1.AppendText("\n  " + injuryreason5 + "\n");
                                            richTextBox1.SelectionStart = richTextBox1.TextLength;

                                            if (playername6.Length > 1 && playername6 != "Advertising")
                                            {
                                                linkLabel13.Text = playername6;
                                                richTextBox1.AppendText("=>");
                                                linkLabel13.AutoSize = true;
                                                linkLabel13.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                                richTextBox1.Controls.Add(linkLabel13);
                                                richTextBox1.AppendText("\n  " + injuryreason6 + "\n");
                                                richTextBox1.SelectionStart = richTextBox1.TextLength;

                                                if (playername7.Length > 1 && playername7 != "Advertising")
                                                {
                                                    linkLabel14.Text = playername7;
                                                    richTextBox1.AppendText("=>");
                                                    linkLabel14.AutoSize = true;
                                                    linkLabel14.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                                    richTextBox1.Controls.Add(linkLabel14);
                                                    richTextBox1.AppendText("\n  " + injuryreason7 + "\n");
                                                    richTextBox1.SelectionStart = richTextBox1.TextLength;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        richTextBox1.Text = Regex.Replace(richTextBox1.Text, @"=>Advertising", "");

                    }

                    else
                    {
                        richTextBox1.Text = "Can't retreve injuries information.\nCheck your internet connection!";
                    }

                }
            }
        }
        private void InjuryHome(object sender, EventArgs e)
        {
            if (radioBtnNBA.Checked == true)
            {
                string teamname = HomeTeamCB.Text;
                if (teamname == "LA Lakers")
                {
                    teamname = "Los Angeles Lakers";
                }
                else if (teamname == "LA Clippers")
                {
                    teamname = "Los Angeles Clippers";
                }
                else if (teamname == "OKC Thunder")
                {
                    teamname = "Oklahoma City";
                }
                else if (teamname == "GS Warriors")
                {
                    teamname = "Golden State";
                }
                else if (teamname == "NewOrleans Pelicans")
                {
                    teamname = "New Orleans";
                }
                else if (teamname == "NewYork Knicks")
                {
                    teamname = "New York";
                }
                else if (teamname == "SanAntonio Spurs")
                {
                    teamname = "San Antonio";
                }
                else if (teamname == "Min Timberwolves")
                {
                    teamname = "Minnesota";
                }
                else if (teamname == "Phi Seventysixers")
                {
                    teamname = "Philadelphia";
                }
                else if (teamname == "Por Trailblazers")
                {
                    teamname = "Portland";
                }

                string teamname2 = "";

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();


                    String queryy339 = "Select ID from KluboviForInjuries where ImeKluba = '" + HomeTeamCB.Text + "'";
                    SqlCommand cmdd339 = new SqlCommand(queryy339, connection);
                    SqlDataReader readd339 = cmdd339.ExecuteReader();
                    while (readd339.Read())
                    {
                        IDKlubaInj = (readd339["ID"].ToString());
                    }
                    readd339.Close();

                    int IDKlubaInj2 = Convert.ToInt16(IDKlubaInj);
                    IDKlubaInj2 = IDKlubaInj2 + 1;

                    String queryy33 = "Select ImeKluba from KluboviForInjuries where ID = '" + IDKlubaInj2 + "'";
                    SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                    SqlDataReader readd33 = cmdd33.ExecuteReader();
                    while (readd33.Read())
                    {
                        teamname2 = (readd33["ImeKluba"].ToString());
                    }
                    readd33.Close();

                    if (IDKlubaInj2 == 31)
                    {
                        teamname2 = "Washington Wizards";
                    }

                    //  teamname2 = teamname2.Split(' ')[0];
                    if (teamname2 == "LA Lakers")
                    {
                        teamname2 = "Los Angeles Lakers";
                    }
                    else if (teamname2 == "LA Clippers")
                    {
                        teamname2 = "Los Angeles Clippers";
                    }
                    else if (teamname2 == "OKC Thunder")
                    {
                        teamname2 = "Oklahoma City";
                    }
                    else if (teamname2 == "GS Warriors")
                    {
                        teamname2 = "Golden State";
                    }
                    else if (teamname2 == "NewOrleans Pelicans")
                    {
                        teamname2 = "New Orleans";
                    }
                    else if (teamname2 == "NewYork Knicks")
                    {
                        teamname2 = "New York";
                    }
                    else if (teamname2 == "SanAntonio Spurs")
                    {
                        teamname2 = "San Antonio";
                    }
                    else if (teamname2 == "Min Timberwolves")
                    {
                        teamname2 = "Minnesota";
                    }
                    else if (teamname2 == "Phi Seventysixers")
                    {
                        teamname2 = "Philadelphia";
                    }
                    else if (teamname2 == "Por Trailblazers")
                    {
                        teamname2 = "Portland";
                    }

                    WebClient wc = new WebClient();
                    string htmlString = wc.DownloadString("http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/injury/injuries.html");
                    int i = 1;
                    string team = "";
                    string team2 = "";


                    if (htmlString.Length > 1)
                    {
                        Match mTeam = Regex.Match(htmlString, @"" + teamname + "(.*?)</td>", RegexOptions.Singleline);
                        if (mTeam.Success)
                        {
                            team = mTeam.Groups[i].Value;
                            //   //Console.WriteLine(team);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        team = Regex.Replace(team, "(.*?)>", "").Trim();
                        index = mTeam.Index;

                        int duzinastringa = htmlString.Length;
                        duzinastringa = duzinastringa - index;

                        if (IDKlubaInj2 != 31)
                        {
                            Match mTeam2 = Regex.Match(htmlString.Substring(index, duzinastringa), @"" + teamname2 + "(.*?)</tr>", RegexOptions.Singleline);
                            if (mTeam2.Success)
                            {
                                team2 = mTeam2.Groups[i].Value;
                                //   //Console.WriteLine(team);
                            }
                            //Console.WriteLine("***************************************************************");
                            //manicure the pattern string
                            //Replace everything before the last (greater than) sign with and empty string
                            team2 = Regex.Replace(team2, "(.*?)>", "").Trim();
                            index2 = mTeam2.Index;
                        }

                        if (IDKlubaInj2 == 31)
                        {
                            index2 = duzinastringa;
                        }
                        string playername = "";
                        string playername2 = "";
                        string playername3 = "";
                        string playername4 = "";
                        string playername5 = "";
                        string playername6 = "";
                        string playername7 = "";
                        link1 = "";

                        Match mPlayer = Regex.Match(htmlString.Substring(index, index2), @".html"">(.*?)</a>", RegexOptions.Singleline);
                        if (mPlayer.Success)
                        {
                            playername = mPlayer.Groups[1].Value;
                            // //Console.WriteLine(playername);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        playername = Regex.Replace(playername, "(.*?)>", "").Trim();
                        index3 = mPlayer.Index;

                        //---------------------------------------------------------------------------------------------------------------///



                        if (playername.Length > 1)
                        {
                            mPlayer = mPlayer.NextMatch();
                            if (mPlayer.Success)
                            {
                                playername2 = mPlayer.Groups[1].Value;
                                playername2 = Regex.Replace(playername2, "(.*?)>", "").Trim();
                                index4 = mPlayer.Index;
                            }
                            if (playername2.Length > 1)
                            {
                                mPlayer = mPlayer.NextMatch();
                                if (mPlayer.Success)
                                {
                                    playername3 = mPlayer.Groups[1].Value;
                                    playername3 = Regex.Replace(playername3, "(.*?)>", "").Trim();
                                    index5 = mPlayer.Index;
                                }
                                if (playername3.Length > 1)
                                {
                                    mPlayer = mPlayer.NextMatch();
                                    if (mPlayer.Success)
                                    {
                                        playername4 = mPlayer.Groups[1].Value;
                                        playername4 = Regex.Replace(playername4, "(.*?)>", "").Trim();

                                        index6 = mPlayer.Index;
                                    }
                                    if (playername4.Length > 1)
                                    {
                                        mPlayer = mPlayer.NextMatch();
                                        if (mPlayer.Success)
                                        {
                                            playername5 = mPlayer.Groups[1].Value;
                                            playername5 = Regex.Replace(playername5, "(.*?)>", "").Trim();

                                            index7 = mPlayer.Index;
                                        }
                                        if (playername5.Length > 1)
                                        {
                                            mPlayer = mPlayer.NextMatch();
                                            if (mPlayer.Success)
                                            {
                                                playername6 = mPlayer.Groups[1].Value;
                                                playername6 = Regex.Replace(playername6, "(.*?)>", "").Trim();

                                                index8 = mPlayer.Index;
                                            }

                                            if (playername6.Length > 1)
                                            {
                                                mPlayer = mPlayer.NextMatch();
                                                if (mPlayer.Success)
                                                {
                                                    playername7 = mPlayer.Groups[1].Value;
                                                    playername7 = Regex.Replace(playername7, "(.*?)>", "").Trim();

                                                    int index9 = mPlayer.Index;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        //---------------------------------------------------------------------------------------------------------------///

                        if (playername.Length > 1)
                        {
                            Match mLink = Regex.Match(htmlString.Substring(index, index2), @"href=""(.*?)"">" + playername + "", RegexOptions.Singleline);
                            if (mLink.Success)
                            {
                                link1 = mLink.Groups[1].Value;
                                //Console.WriteLine(link1);
                            }
                            link1 = "http://www.covers.com" + link1;

                            if (playername2.Length > 1)
                            {
                                if (IDKlubaInj2 == 31)
                                {
                                    index2 = duzinastringa - index4;
                                }

                                position = index + index4 - 90;
                                Match mLink2 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername2 + "", RegexOptions.Singleline);
                                if (mLink2.Success)
                                {
                                    link2 = mLink2.Groups[1].Value;
                                    //Console.WriteLine(link2);
                                }
                                link2 = "http://www.covers.com" + link2;

                                if (playername3.Length > 1)
                                {
                                    if (IDKlubaInj2 == 31)
                                    {
                                        index2 = duzinastringa - index5;
                                    }

                                    position = index + index5 - 90;
                                    Match mLink3 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername3 + "", RegexOptions.Singleline);
                                    if (mLink3.Success)
                                    {
                                        link3 = mLink3.Groups[1].Value;
                                        //Console.WriteLine(link3);
                                    }
                                    link3 = "http://www.covers.com" + link3;

                                    if (playername4.Length > 1)
                                    {
                                        if (IDKlubaInj2 == 31)
                                        {
                                            index2 = duzinastringa - index6;
                                        }

                                        position = index + index6 - 90;
                                        Match mLink4 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername4 + "", RegexOptions.Singleline);
                                        if (mLink4.Success)
                                        {
                                            link4 = mLink4.Groups[1].Value;
                                            //Console.WriteLine(link4);
                                        }
                                        link4 = "http://www.covers.com" + link4;

                                        if (playername5.Length > 1)
                                        {
                                            if (IDKlubaInj2 == 31)
                                            {
                                                index2 = duzinastringa - index7;
                                            }

                                            position = index + index7 - 90;
                                            Match mLink5 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername5 + "", RegexOptions.Singleline);
                                            if (mLink5.Success)
                                            {
                                                link5 = mLink5.Groups[1].Value;
                                                //Console.WriteLine(link5);
                                            }
                                            link5 = "http://www.covers.com" + link5;

                                            if (playername6.Length > 1)
                                            {
                                                if (IDKlubaInj2 == 31)
                                                {
                                                    index2 = duzinastringa - index8;
                                                }

                                                position = index + index8 - 90;
                                                Match mLink6 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername6 + "", RegexOptions.Singleline);
                                                if (mLink6.Success)
                                                {
                                                    link6 = mLink6.Groups[1].Value;
                                                    //Console.WriteLine(link6);
                                                }
                                                link6 = "http://www.covers.com" + link6;

                                                if (playername7.Length > 1)
                                                {
                                                    if (IDKlubaInj2 == 31)
                                                    {
                                                        index2 = duzinastringa - index9;
                                                    }

                                                    position = index + index9 - 90;
                                                    Match mLink7 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername7 + "", RegexOptions.Singleline);
                                                    if (mLink7.Success)
                                                    {
                                                        link7 = mLink7.Groups[1].Value;
                                                        //Console.WriteLine(link7);
                                                    }
                                                    link7 = "http://www.covers.com" + link7;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        /////////////////////////////////////////////////////////////////////////
                        string injuryreason = "";
                        string injuryreason2 = "";
                        string injuryreason3 = "";
                        string injuryreason4 = "";
                        string injuryreason5 = "";
                        string injuryreason6 = "";
                        string injuryreason7 = "";
                        Match mInjury = Regex.Match(htmlString.Substring(index, index2), @"<strong>(.*?)</td>", RegexOptions.Singleline);
                        if (mInjury.Success)
                        {
                            injuryreason = mInjury.Groups[1].Value;
                            // //Console.WriteLine(playername);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        injuryreason = Regex.Replace(injuryreason, "</strong>", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, "/+", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, @"[\d]", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, @"&#;", "'");
                        if (injuryreason.Length > 1)
                        {
                            int duzina = injuryreason.Length - 2;
                            injuryreason = injuryreason.Substring(0, duzina);
                        }
                        index7 = mInjury.Index;
                        //Console.WriteLine("Index of match: " + index7);
                        //Console.WriteLine("Injury reason of player 1:" + injuryreason);


                        if (injuryreason.Length > 1)
                        {
                            mInjury = mInjury.NextMatch();
                            if (mInjury.Success)
                            {
                                injuryreason2 = mInjury.Groups[1].Value;
                                injuryreason2 = Regex.Replace(injuryreason2, "</strong>", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, "/+", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, @"[\d]", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, @"&#;", "'");
                                int duzina2 = injuryreason2.Length - 2;
                                injuryreason2 = injuryreason2.Substring(0, duzina2);
                                int index8 = mInjury.Index;
                                //Console.WriteLine("Index of match: " + index8);
                                //Console.WriteLine("Injury reason of player 2:" + injuryreason2);
                            }
                            if (injuryreason2.Length > 1)
                            {
                                mInjury = mInjury.NextMatch();
                                if (mInjury.Success)
                                {
                                    injuryreason3 = mInjury.Groups[1].Value;
                                    injuryreason3 = Regex.Replace(injuryreason3, "</strong>", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, "/+", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, @"[\d]", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, @"&#;", "'");
                                    int duzina3 = injuryreason3.Length - 2;
                                    injuryreason3 = injuryreason3.Substring(0, duzina3);

                                    //Console.WriteLine("Injury reason of player 3:" + injuryreason3);
                                }

                                if (injuryreason3.Length > 1)
                                {
                                    mInjury = mInjury.NextMatch();
                                    if (mInjury.Success)
                                    {
                                        injuryreason4 = mInjury.Groups[1].Value;
                                        injuryreason4 = Regex.Replace(injuryreason4, "</strong>", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, "/+", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, @"[\d]", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, @"&#;", "'");
                                        int duzina4 = injuryreason4.Length - 2;
                                        injuryreason4 = injuryreason4.Substring(0, duzina4);

                                        //Console.WriteLine("Injury reason of player 4:" + injuryreason4);
                                    }

                                    if (injuryreason4.Length > 1)
                                    {
                                        mInjury = mInjury.NextMatch();
                                        if (mInjury.Success)
                                        {
                                            injuryreason5 = mInjury.Groups[1].Value;
                                            injuryreason5 = Regex.Replace(injuryreason5, "</strong>", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, "/+", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, @"[\d]", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, @"&#;", "'");
                                            int duzina5 = injuryreason5.Length - 2;
                                            injuryreason5 = injuryreason5.Substring(0, duzina5);

                                            //Console.WriteLine("Injury reason of player 5:" + injuryreason5);
                                        }
                                        if (injuryreason5.Length > 1)
                                        {
                                            mInjury = mInjury.NextMatch();
                                            if (mInjury.Success)
                                            {
                                                injuryreason6 = mInjury.Groups[1].Value;
                                                injuryreason6 = Regex.Replace(injuryreason6, "</strong>", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, "/+", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, @"[\d]", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, @"&#;", "'");
                                                int duzina6 = injuryreason6.Length - 2;
                                                injuryreason6 = injuryreason6.Substring(0, duzina6);

                                                //Console.WriteLine("Injury reason of player 4:" + injuryreason6);
                                            }
                                            if (injuryreason6.Length > 1)
                                            {
                                                mInjury = mInjury.NextMatch();
                                                if (mInjury.Success)
                                                {
                                                    injuryreason7 = mInjury.Groups[1].Value;
                                                    injuryreason7 = Regex.Replace(injuryreason7, "</strong>", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, "/+", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, @"[\d]", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, @"&#;", "'");
                                                    int duzina7 = injuryreason7.Length - 2;
                                                    injuryreason7 = injuryreason7.Substring(0, duzina7);

                                                    //Console.WriteLine("Injury reason of player 7:" + injuryreason7);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (playername.Length < 1)
                        {
                            richTextBox3.Text = "No injuries to report.";
                        }

                        if (richTextBox3.Controls.Contains(linkLabel1))
                        {
                            richTextBox3.Controls.Remove(linkLabel1);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel2))
                        {
                            richTextBox3.Controls.Remove(linkLabel2);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel3))
                        {
                            richTextBox3.Controls.Remove(linkLabel3);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel4))
                        {
                            richTextBox3.Controls.Remove(linkLabel4);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel5))
                        {
                            richTextBox3.Controls.Remove(linkLabel5);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel6))
                        {
                            richTextBox3.Controls.Remove(linkLabel6);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel7))
                        {
                            richTextBox3.Controls.Remove(linkLabel7);
                        }

                        richTextBox3.Text = "";

                        if (playername.Length < 1)
                        {
                            richTextBox3.Text = "No injuries to report.";
                        }

                        if (playername.Length > 1 && playername != "Advertising")
                        {
                            richTextBox3.Text = "    " + HomeTeamCB.Text.Split(' ')[1] + " Injuries:\n=>";
                            linkLabel1.Text = playername;
                            linkLabel1.AutoSize = true;
                            linkLabel1.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                            richTextBox3.Controls.Add(linkLabel1);
                            richTextBox3.AppendText(linkLabel1.Text + "\n  " + injuryreason + "\n");
                            richTextBox3.SelectionStart = richTextBox3.TextLength;

                            if (playername2.Length > 1 && playername2 != "Advertising")
                            {
                                linkLabel2.Text = playername2;
                                richTextBox3.AppendText("=>");
                                linkLabel2.AutoSize = true;
                                linkLabel2.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                richTextBox3.Controls.Add(linkLabel2);
                                richTextBox3.AppendText("\n  " + injuryreason2 + "\n");
                                richTextBox3.SelectionStart = richTextBox3.TextLength;

                                if (playername3.Length > 1 && playername3 != "Advertising")
                                {
                                    linkLabel3.Text = playername3;
                                    richTextBox3.AppendText("=>");
                                    linkLabel3.AutoSize = true;
                                    linkLabel3.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                    richTextBox3.Controls.Add(linkLabel3);
                                    richTextBox3.AppendText("\n  " + injuryreason3 + "\n");
                                    richTextBox3.SelectionStart = richTextBox3.TextLength;
                                    if (playername4.Length > 1 && playername4 != "Advertising")
                                    {
                                        linkLabel4.Text = playername4;
                                        richTextBox3.AppendText("=>");
                                        linkLabel4.AutoSize = true;
                                        linkLabel4.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                        richTextBox3.Controls.Add(linkLabel4);
                                        richTextBox3.AppendText("\n  " + injuryreason4 + "\n");
                                        richTextBox3.SelectionStart = richTextBox3.TextLength;

                                        if (playername5.Length > 1 && playername5 != "Advertising")
                                        {
                                            linkLabel5.Text = playername5;
                                            richTextBox3.AppendText("=>");
                                            linkLabel5.AutoSize = true;
                                            linkLabel5.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                            richTextBox3.Controls.Add(linkLabel5);
                                            richTextBox3.AppendText("\n  " + injuryreason5 + "\n");
                                            richTextBox3.SelectionStart = richTextBox3.TextLength;

                                            if (playername6.Length > 1 && playername6 != "Advertising")
                                            {
                                                linkLabel6.Text = playername6;
                                                richTextBox3.AppendText("=>");
                                                linkLabel6.AutoSize = true;
                                                linkLabel6.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                                richTextBox3.Controls.Add(linkLabel6);
                                                richTextBox3.AppendText("\n  " + injuryreason6 + "\n");
                                                richTextBox3.SelectionStart = richTextBox3.TextLength;
                                                if (playername7.Length > 1 && playername7 != "Advertising")
                                                {
                                                    linkLabel7.Text = playername7;
                                                    richTextBox3.AppendText("=>");
                                                    linkLabel7.AutoSize = true;
                                                    linkLabel7.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                                    richTextBox3.Controls.Add(linkLabel7);
                                                    richTextBox3.AppendText("\n  " + injuryreason7 + "\n");
                                                    richTextBox3.SelectionStart = richTextBox3.TextLength;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        richTextBox3.Text = Regex.Replace(richTextBox3.Text, @"=> Advertising", "");
                    }
                    else
                    {
                        richTextBox3.Text = "Can't retreve injuries information.\nCheck your internet connection!";
                    }
                }
            }
        }
        private void ll_LinkClicked(object sender, MouseEventArgs e)
        {
            MessageBox.Show("Press here!");
        }

        private void TotalBox_MouseClick(object sender, MouseEventArgs e)
        {
            TotalBox.Text = "";
        }

        private void HandicapBox_MouseClick(object sender, MouseEventArgs e)
        {
            HandicapBox.Text = "";
        }
        private void Dgv5(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (dataGridView5.Columns.Contains("Total Points"))
                {
                    dataGridView5.Columns.Remove("Total Points");
                    if (dataGridView5.Columns.Contains(@"Hendicap Points"))
                    { dataGridView5.Columns.Remove(@"Hendicap Points"); }

                }
                if (HomeTeamCB.Text.Length > 3)
                {

                    if (radioBtnNBA.Checked == true)
                    {
                        Sql43Last5 = "select TOP 5 Dayofmonth,Month,Year,Team,Opp,Line,Total,Hscore,Ascore, ATSr, OUr from " + AllNBAMLBResultsVar + " where Team = '" + HomeTeamCB.Text.Split(' ')[1] +
                             "' and Opp = '" + AwayTeamCB.Text.Split(' ')[1] + "' or Opp = '" + HomeTeamCB.Text.Split(' ')[1] +
                             "' and Team = '" + AwayTeamCB.Text.Split(' ')[1] + "' order by ID desc";
                    }
                    else
                    {
                        Sql43Last5 = "select TOP 5 Dayofmonth,Month,Season,Team,StarterHLastName as 'HS', Opp,StarterALastName as 'AS',Line,Total,Hscore,Ascore, Hits, OUr from " + AllNBAMLBResultsVar + " where Team = '" + HomeTeamCB.Text.Split(' ')[1] +
                                "' and Opp = '" + AwayTeamCB.Text.Split(' ')[1] + "' or Opp = '" + HomeTeamCB.Text.Split(' ')[1] +
                                "' and Team = '" + AwayTeamCB.Text.Split(' ')[1] + "' order by ID desc";
                    }

                    ds5 = new DataSet();
                    adapterMain = new SqlDataAdapter(Sql43Last5, connection);
                    ds5.Reset();
                    adapterMain.Fill(ds5);
                    dataGridView5.DataSource = ds5.Tables[0];
                    dataGridView5.Columns[0].Width = 53;

                    dataGridView5.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                    dataGridView5.Columns[0].HeaderText = "Day";
                    dataGridView5.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                    dataGridView5.Columns[1].HeaderText = "Month";
                    dataGridView5.Columns[1].Width = 50;
                    dataGridView5.Columns[2].DefaultCellStyle.BackColor = Color.DarkGray;
                    dataGridView5.Columns[2].HeaderText = "Year";
                    dataGridView5.Columns[2].Width = 42;
                    dataGridView5.Columns["Team"].DefaultCellStyle.BackColor = Color.LightGray;
                    dataGridView5.Columns["Team"].HeaderText = "Home Team";
                    dataGridView5.Columns["Team"].Width = 130;
                    dataGridView5.Columns["Opp"].DefaultCellStyle.BackColor = Color.LightGray;
                    dataGridView5.Columns["Opp"].Width = 130;
                    dataGridView5.Columns["Opp"].HeaderText = "Away Team";

                    dataGridView5.Columns["Line"].DefaultCellStyle.BackColor = Color.Silver;
                    dataGridView5.Columns["Line"].HeaderText = "Line";
                    dataGridView5.Columns["Total"].DefaultCellStyle.BackColor = Color.Silver;
                    dataGridView5.Columns["Total"].HeaderText = "Total";
                    dataGridView5.Columns["Hscore"].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    dataGridView5.Columns["Hscore"].HeaderText = "Hs";
                    dataGridView5.Columns["Ascore"].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    dataGridView5.Columns["Ascore"].HeaderText = "As";
                    if (radioBtnMLB.Checked == true)
                    {
                        dataGridView5.Columns["HS"].Width = 110;
                        dataGridView5.Columns["AS"].Width = 110;
                    }

                    System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                    boldStyle.Font = new System.Drawing.Font("Ariel", 9.25F, System.Drawing.FontStyle.Bold);


                    var col3 = new DataGridViewTextBoxColumn();
                    var col4 = new DataGridViewTextBoxColumn();

                    col3.HeaderText = "Total Points";
                    col3.Name = "Total Points";

                    col4.HeaderText = "Hand. Points";
                    col4.Name = "Hendicap Points";

                    dataGridView5.Columns.AddRange(new DataGridViewColumn[] { col3, col4 });

                    int totall;
                    double hendikk;
                    DataGridViewRow row = new DataGridViewRow();
                    for (int i = 0; i < dataGridView5.Rows.Count - 1; i++)
                    {
                        if (dataGridView5.Rows[i].Cells["Team"].Value.ToString() == HomeTeamCB.Text.Split(' ')[1])
                        {
                            dataGridView5.Rows[i].Cells["Team"].Style = boldStyle;
                        }
                        if (dataGridView5.Rows[i].Cells["Opp"].Value.ToString() == AwayTeamCB.Text.Split(' ')[1])
                        {
                            dataGridView5.Rows[i].Cells["Opp"].Style = boldStyle;
                        }
                        if (dataGridView5.Rows[i].Cells["Team"].Value.ToString() == AwayTeamCB.Text.Split(' ')[1])
                        {
                            dataGridView5.Rows[i].Cells["Team"].Style = boldStyle;
                        }
                        if (dataGridView5.Rows[i].Cells["Opp"].Value.ToString() == HomeTeamCB.Text.Split(' ')[1])
                        {
                            dataGridView5.Rows[i].Cells["Opp"].Style = boldStyle;
                        }
                        totall = Convert.ToInt16(dataGridView5.Rows[i].Cells["Hscore"].Value) + Convert.ToInt16(dataGridView5.Rows[i].Cells["Ascore"].Value);
                        dataGridView5.Rows[i].Cells["Total Points"].Value = totall;
                        hendikk = Convert.ToInt16(dataGridView5.Rows[i].Cells["Ascore"].Value) - Convert.ToInt16(dataGridView5.Rows[i].Cells["Hscore"].Value);
                        dataGridView5.Rows[i].Cells["Hendicap Points"].Value = hendikk;
                    }
                    dataGridView5.Rows[0].Cells[0].Selected = false;

                    label43.Text = "Last 5 games between " + AwayTeamCB.Text.Split(' ')[1] + " and " + HomeTeamCB.Text.Split(' ')[1] + "";
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link2);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link1);
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link3);
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link4);
        }

        private void richTextBox3_VScroll(object sender, EventArgs e)
        {

        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link5);
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link6);
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link7);
        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link11);
        }

        private void linkLabel9_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link12);
        }
        private void linkLabel10_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link13);
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link14);
        }

        private void linkLabel12_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link15);
        }

        private void linkLabel13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link16);
        }

        private void linkLabel14_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link17);
        }

        private void HandicapBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                //enter key is down
            }
        }

        private void GetTodaysGames(object sender, EventArgs e)
        {
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //  GetTodaysGames(sender, e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (tabControlInjuries.SelectedIndex == 3 && matchupsAndScoresDGV.Rows.Count > 0 && matchupsAndScoresDGV.Rows[0].Cells["Away Score"].Value.ToString() != "-")
            {
                if (coversbsno > 0)
                {
                    Scores(sender, e);
                }
            }
        }

        private void tabControlInjuries_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SuspendLayout();
            if (tabControlInjuries.SelectedIndex == 3)
            {
               
                dateTimePicker2.Format = DateTimePickerFormat.Custom;
                dateTimePicker2.CustomFormat = "yyyy-MM-dd";
                Scores(sender, e);
               
            }
            if (tabControlInjuries.SelectedIndex == 4)
            {
                _connection.Open();

                if (DGVAnalysisResults.Columns.Contains("Col0"))
                {
                    DGVAnalysisResults.Columns.Remove("Col0");
                }

                dataGridView7.DataSource = null;

                string sql16 = @"SELECT
                   [TeamName]
                  ,[SUTotWin]
                  ,[SUTotLoss]
                  ,[ATSTotWin]
                  ,[ATSTotLoss]
                  ,[OTotal]
                  ,[UTotal]
                   FROM [NBATeamsStatistics] order by Conference,SUTotWin desc";


                adapterStandings = new SqlDataAdapter(sql16, _connection);
                ds.Tables.Add("Table2");
                adapterStandings.Fill(ds.Tables["Table2"]);

                var col0 = new DataGridViewTextBoxColumn();
                col0.HeaderText = "NO.";
                col0.Name = "Col0";
                dataGridView7.Columns.AddRange(new DataGridViewColumn[] { col0 });

                dataGridView7.DataSource = ds.Tables["Table2"];

                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                boldStyle.Font = new System.Drawing.Font("Ariel", 9.75F, System.Drawing.FontStyle.Bold);

                dataGridView7.Columns[1].DefaultCellStyle = boldStyle;

                dataGridView7.Columns[0].Width = 30;
                dataGridView7.Columns[1].Width = 110;
                dataGridView7.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView7.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;

                dataGridView7.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView7.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView7.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView7.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView7.Columns[6].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView7.Columns[7].DefaultCellStyle.BackColor = Color.Silver;

                dataGridView7.Columns[1].HeaderText = "Team";
                dataGridView7.Columns[2].HeaderText = "SU Win";
                dataGridView7.Columns[3].HeaderText = "SU Loss";
                dataGridView7.Columns[4].HeaderText = "ATS Win";
                dataGridView7.Columns[5].HeaderText = "ATS Loss";
                dataGridView7.Columns[6].HeaderText = "O's";
                dataGridView7.Columns[7].HeaderText = "U's";

                numbering = 1;
                DataGridViewRow row = new DataGridViewRow();
                for (int i = 0; i < 15; i++)
                {
                    dataGridView7.Rows[numbering - 1].Cells[0].Value = numbering;
                    if (numbering < 9)
                    {
                        dataGridView7.Rows[numbering - 1].Cells[0].Style.ForeColor = Color.Green;
                    }
                    numbering++;
                }
                numbering = 1;

                for (int i = 15; i < 30; i++)
                {
                    dataGridView7.Rows[numbering + 14].Cells[0].Value = numbering;
                    if (numbering < 9)
                    {
                        dataGridView7.Rows[numbering + 14].Cells[0].Style.ForeColor = Color.Green;
                    }
                    numbering++;
                }

                _connection.Close();

                dataGridView7.CurrentCell.Selected = false;
            }
            this.ResumeLayout();
        }

        private void dataGridView6_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void Renameteams()
        {
            switch (PinnHomeTeam)
            {
                case "Philadelphia 76ers":
                    {
                        PinnHomeTeam = "Phi Seventysixers";
                        break;
                    }
                case "Los Angeles Clippers":
                    {
                        PinnHomeTeam = "LA Clippers";
                        break;
                    }
                case "San Antonio Spurs":
                    {
                        PinnHomeTeam = "SanAntonio Spurs";
                        break;
                    }
                case "Golden State Warriors":
                    {
                        PinnHomeTeam = "GS Warriors";
                        break;
                    }
                case "New Orleans Pelicans":
                    {
                        PinnHomeTeam = "NewOrleans Pelicans";
                        break;
                    }
                case "Portland Trail Blazers":
                    {
                        PinnHomeTeam = "Por Trailblazers";
                        break;
                    }
            }

            switch (PinnAwayTeam)
            {
                case "Philadelphia 76ers":
                    {
                        PinnAwayTeam = "Phi Seventysixers";
                        break;
                    }
                case "Los Angeles Clippers":
                    {
                        PinnAwayTeam = "LA Clippers";
                        break;
                    }
                case "San Antonio Spurs":
                    {
                        PinnAwayTeam = "SanAntonio Spurs";
                        break;
                    }
                case "Golden State Warriors":
                    {
                        PinnAwayTeam = "GS Warriors";
                        break;
                    }
                case "New Orleans Pelicans":
                    {
                        PinnAwayTeam = "NewOrleans Pelicans";
                        break;
                    }
                case "Portland Trail Blazers":
                    {
                        PinnAwayTeam = "Por Trailblazers";
                        break;
                    }
            }
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            PastResults(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Scores(sender, e);
        }

        private void PastResults(object sender, EventArgs e)
        {
            if (tabControlInjuries.SelectedIndex == 3)
            {
                try
                {
                    if (SharedMethods.CheckForInternetConnection())
                    {
                        DateTime dt = DateTime.Today.AddDays(-1);
                        String datum1 = dt.ToString("yyyy-MM-dd");

                        if (radioBtnNBA.Checked == true)
                        {
                            string url = string.Format(@"http://www.covers.com/Sports/NBA/Matchups?selectedDate={0}", datum1);
                            _nbamlbScores.LoadHTML(url);
                        }
                        else
                        {
                            string url = string.Format(@"http://www.covers.com/Sports/MLB/Matchups?selectedDate={0}", datum1);
                            _nbamlbScores.LoadHTML(url);
                        }

                        if (radioBtnNBA.Checked == true)
                        {
                            _nbamlbScores.LiveScores(matchupsAndScoresDGV, radioBtnNBA, radioBtnMLB, bestBetBtn);
                        }
                        else if (radioBtnMLB.Checked == true)
                        {
                            _nbamlbScores.LiveScores(matchupsAndScoresDGV, radioBtnNBA, radioBtnMLB, bestBetBtn);
                        }
                    }
                }
                catch (Exception ex)
                {
                    matchupsAndScoresDGV.ColumnCount = 1;
                    matchupsAndScoresDGV.RowCount = 1;
                    matchupsAndScoresDGV.Columns[0].Name = "covers.com Info";
                    matchupsAndScoresDGV.Rows[0].Cells[0].Value = ex.ToString();
                }
            }
        }

        public void Scores(object sender, EventArgs e)
        {
            if (tabControlInjuries.SelectedIndex == 3)
            {
                try
                {
                    if (SharedMethods.CheckForInternetConnection())
                    {
                        if (radioBtnNBA.Checked)
                        {
                            string url = string.Format(@"http://www.covers.com/Sports/NBA/Matchups");
                            _nbamlbScores.LoadHTML(url);
                        }
                        else
                        {
                            string url = string.Format(@"http://www.covers.com/Sports/MLB/Matchups");
                            _nbamlbScores.LoadHTML(url);
                        }

                        if (radioBtnNBA.Checked)
                        {
                            _nbamlbScores.LiveScores(matchupsAndScoresDGV, radioBtnNBA, radioBtnMLB, bestBetBtn);
                        }
                        else if (radioBtnMLB.Checked)
                        {
                            _nbamlbScores.LiveScores(matchupsAndScoresDGV, radioBtnNBA, radioBtnMLB, bestBetBtn);
                        }
                    }
                }
                catch (Exception ex)
                {
                    matchupsAndScoresDGV.ColumnCount = 1;
                    matchupsAndScoresDGV.RowCount = 1;
                    matchupsAndScoresDGV.Columns[0].Name = "covers.com Info";
                    matchupsAndScoresDGV.Rows[0].Cells[0].Value = ex.ToString();
                }
            }
        }

        private void matchupsAndScoresDGV_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            var dataGridView = (sender as DataGridView);
            //Check the condition as per the requirement casting the cell value to the appropriate type
            var colIndex = 0;
            if (radioBtnNBA.Checked)
            {
                colIndex = 7;
            }
            else if (radioBtnMLB.Checked)
            {
                colIndex = 9;
            }
            dataGridView.Cursor = e.ColumnIndex == colIndex ? Cursors.Hand : Cursors.Arrow;
        }

        private void matchupsAndScoresDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Box / Matchup"].Selected)
            {
                int clicked = e.RowIndex;
                if (matchupsAndScoresDGV.Rows[clicked].Cells["Box / Matchup"].Value.ToString() == "Box Score")
                {
                    System.Diagnostics.Process.Start(_nbamlbScores._coversBoxScoreLinkList[clicked]);
                }
                else if (matchupsAndScoresDGV.Rows[clicked].Cells["Box / Matchup"].Value.ToString() == "Matchup")
                {
                    System.Diagnostics.Process.Start(_nbamlbScores._coversMatchupsLinkList[clicked]);
                }
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (SharedMethods.CheckForInternetConnection())
                {
                    string datum1 = dateTimePicker2.Text;
                    WebClient wc = new WebClient();

                    if (radioBtnNBA.Checked)
                    {
                        string url = string.Format(@"http://www.covers.com/Sports/NBA/Matchups?selectedDate={0}", datum1);
                        _nbamlbScores.LoadHTML(url);
                    }
                    else
                    {
                        string url = string.Format(@"http://www.covers.com/Sports/MLB/Matchups?selectedDate={0}", datum1);
                        _nbamlbScores.LoadHTML(url);
                    }

                    if (radioBtnNBA.Checked)
                    {
                        _nbamlbScores.LiveScores(matchupsAndScoresDGV, radioBtnNBA, radioBtnMLB, bestBetBtn);
                    }
                    else if (radioBtnMLB.Checked)
                    {
                        _nbamlbScores.LiveScores(matchupsAndScoresDGV, radioBtnNBA, radioBtnMLB, bestBetBtn);
                    }
                }
            }
            catch (Exception ex)
            {
                matchupsAndScoresDGV.ColumnCount = 1;
                matchupsAndScoresDGV.RowCount = 1;
                matchupsAndScoresDGV.Columns[0].Name = "covers.com Info";
                matchupsAndScoresDGV.Rows[0].Cells[0].Value = ex.ToString();
            }
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            DateTime dt2 = dateTimePicker2.Value.Date.AddDays(-1);
            string Datum2 = dt2.ToString("yyyy-MM-dd");
            dateTimePicker2.Text = Datum2;
        }

        private void pictureBox18_Click(object sender, EventArgs e)
        {
            DateTime dt2 = dateTimePicker2.Value.Date.AddDays(+1);
            string Datum2 = dt2.ToString("yyyy-MM-dd");
            dateTimePicker2.Text = Datum2;
        }

        private void pictureBox1_MouseHover_2(object sender, EventArgs e)
        {
            pictureBox1.Cursor = Cursors.Hand;
            pictureBox1.Image = Oklade.Properties.Resources.back_arrow2;
        }

        private void pictureBox1_MouseLeave_2(object sender, EventArgs e)
        {
            pictureBox1.Image = Oklade.Properties.Resources.back_arrow1;
        }

        private void pictureBox18_MouseHover(object sender, EventArgs e)
        {
            pictureBox18.Cursor = Cursors.Hand;
            pictureBox18.Image = Oklade.Properties.Resources.arrow_right2;
        }

        private void pictureBox18_MouseLeave(object sender, EventArgs e)
        {
            pictureBox18.Image = Oklade.Properties.Resources.arrow_right1;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                DateTime dt3 = dateTimePicker2.Value.Date;
                dateforupdate = dt3.ToString("dd.M.yyyy.");

                for (int i = 0; i < matchupsAndScoresDGV.Rows.Count; i++)
                {
                    if (matchupsAndScoresDGV.Rows[i].Cells[2].Value.ToString().Contains("Final") == true)
                    {
                        int totalscore = Convert.ToInt16(matchupsAndScoresDGV.Rows[i].Cells["Home Score"].Value) + Convert.ToInt16(matchupsAndScoresDGV.Rows[i].Cells["Away Score"].Value);

                        string gettotal = @"SELECT [Total] FROM [LiveScoresUpdates] where Date = '" + dateforupdate + "' " +
                      " and AwayTeam = '" + matchupsAndScoresDGV.Rows[i].Cells["Away Team"].Value + "'";

                        SqlCommand cmdtotal = new SqlCommand(gettotal, connection);
                        SqlDataReader readtotal = cmdtotal.ExecuteReader();

                        while (readtotal.Read())
                        {
                            totaloffer = (Convert.ToDouble(readtotal["Total"]));
                        }
                        readtotal.Close();

                        if (totalscore > totaloffer)
                        {
                            updateOUr = "O";
                        }
                        else if (totalscore == totaloffer)
                        {
                            updateOUr = "P";
                        }
                        else if (totalscore < totaloffer)
                        {
                            updateOUr = "U";
                        }

                        if (Convert.ToInt16(matchupsAndScoresDGV.Rows[i].Cells["Home Score"].Value) > Convert.ToInt16(matchupsAndScoresDGV.Rows[i].Cells["Away Score"].Value))
                        {
                            updateSUr = "W";
                        }
                        else if (Convert.ToInt16(matchupsAndScoresDGV.Rows[i].Cells["Home Score"].Value) < Convert.ToInt16(matchupsAndScoresDGV.Rows[i].Cells["Away Score"].Value))
                        {
                            updateSUr = "L";
                        }

                        string query4 = @"UPDATE LiveScoresUpdates set HomeScore = " + matchupsAndScoresDGV.Rows[i].Cells["Home Score"].Value + ", " +
                      "  AwayScore = " + matchupsAndScoresDGV.Rows[i].Cells["Away Score"].Value + ", OUr = '" + updateOUr + "', SUr = '" + updateSUr + "' where Date = '" + dateforupdate + "' " +
                      " and AwayTeam = '" + matchupsAndScoresDGV.Rows[i].Cells["Away Team"].Value + "' ";
                        SqlCommand cmd114 = new SqlCommand(query4, connection);
                        cmd114.ExecuteNonQuery();
                    }
                }
                MessageBox.Show(@"Game Results Updated!", @"Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void radioBtnNBA_CheckedChanged(object sender, EventArgs e)
        {
            //ManualChange = 1;
            DateTime dtCheck = DateTime.Today;
            DateTime dtPlayoffsStart = new DateTime(dtCheck.Year, 4, 14);
            DateTime dtPlayoffsEnd = new DateTime(dtCheck.Year, 6, 14);

            if (dtCheck.Date >= dtPlayoffsStart && dtCheck.Date < dtPlayoffsEnd)
            {
                logoPB.ImageLocation = "http://2.bp.blogspot.com/-fUNKupJA9bE/VJyYeZh3e3I/AAAAAAAAH1A/iRCcaHioIbE/w1200-h630-p-nu/Logo%2BNBA_Playoffs.png";

            }
            else
            {
                logoPB.Image = Properties.Resources.mlb2;
            }

            label6.Hide();
            label7.Hide();
            awayStarterCB.Hide();
            homeStarterCB.Hide();

            Form2_Load(sender, e);
            NewGameBtn_Click(sender, e);
        }

        private void radioBtnMLB_CheckedChanged(object sender, EventArgs e)
        {
            //ManualChange = 1;
            logoPB.Image = Properties.Resources.mlb2;
            awayStarterCB.Show();
            homeStarterCB.Show();
            label6.Show();
            label7.Show();

            Form2_Load(sender, e);
            NewGameBtn_Click(sender, e);
        }

        private void HomeStarterL50()
        {
            // -------------------------------------------home STARTER LAST 10-------------------------------------
            if (radioBtnMLB.Checked)
            {
                var t = Task.Run(() =>
                 {
                     getTeamsAndStartersGames.GetStartersGames(HStarterL50DGV, _homeStarterFirstName, _homeStarterLastName);
                     _dgva.FormatLast50StarterGames(HStarterL50DGV, _homeStarterLastName);
                 }).ContinueWith((prevTask) =>
                     {

                     });
            }
        }

        private void AwayStarterL50()
        {
            // -------------------------------------------AWAY STARTER LAST 10-------------------------------------
            if (radioBtnMLB.Checked)
            {
                var t2 = Task.Run(() =>
                 {
                     getTeamsAndStartersGames.GetStartersGames(AStarterL50DGV, _awayStarterFirstName, _awayStarterLastName);
                     _dgva.FormatLast50StarterGames(AStarterL50DGV, _awayStarterLastName);
                 }).ContinueWith((prevTask) =>
                     {

                     });
            }
        }

        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                if (radioBtnMLB.Checked == true)
                {
                    awayStarterCB.Focus();
                }
                else
                {
                    HomeTeamCB.Focus();
                }
                AwayTeamCB.SelectionStart = 0;
                AwayTeamCB.SelectionLength = 0;
            }
        }

        private void awayStarterCB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;

                HomeTeamCB.Focus();

                awayStarterCB.SelectionLength = 0;
            }
        }

        private void comboBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                if (radioBtnMLB.Checked == true)
                {
                    homeStarterCB.Focus();
                }
                else
                {
                    TotalBox.Focus();
                }
                HomeTeamCB.SelectionLength = 0;
            }
        }

        private void homeStarterCB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;

                TotalBox.Focus();

                homeStarterCB.SelectionLength = 0;
            }
        }

        private void TotalBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;

                HandicapBox.Focus();

                if (radioBtnMLB.Checked == true)
                {
                    HandicapBox.Text = "0";
                }

                AutoProcessBtn.Focus();

                TotalBox.SelectionLength = 0;
            }
        }

        private void Analyzer_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Control && e.KeyCode == Keys.A)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                AutoProcess_Click_1(sender, e);
            }
            else if (e.Control && e.KeyCode == Keys.D)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                deleteBtn_Click(sender, e);
            }
            else if (e.Control && e.KeyCode == Keys.C)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                NewGameBtn_Click(sender, e);
            }
        }

        private void dataGridView8_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //   try
            //  {
            if (radioBtnMLB.Checked == true)
            {
                NewGameBtn_Click(sender, e);

                if (matchupsAndScoresDGV.Rows[e.RowIndex].Cells[0].Selected)
                {
                    _coversAwayStarter = matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Away Starter"].Value.ToString().Replace(" (R)", "");
                    _coversHomeStarter = matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Home Starter"].Value.ToString().Replace(" (R)", "");
                    PinnAwayTeam = matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Away Team"].Value.ToString();
                    PinnHomeTeam = matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Home Team"].Value.ToString();

                    PinnHandi = matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Line"].Value.ToString();
                    PinnTotal = matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Total"].Value.ToString().Replace(".", ",");

                    if (PinnHandi == "Off" || PinnHandi == "" || PinnHandi == null)
                    {
                        PinnHandi = "0";
                        MessageBox.Show(@"There was an 'Off' or empty offer, Hendi is set to 0!", @"Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    if (PinnTotal == "Off" || PinnHandi == "" || PinnHandi == null)
                    {
                        PinnTotal = "8";
                        MessageBox.Show(@"There was an 'Off' or empty offer, Total is set to 8!", @"Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        connection.Open();
                        String queryy33 = "Select ImeKluba from Klubovi where ShortForCovers = '" + PinnAwayTeam + "' and League = '" + LeagueNBAMLB + "'";
                        SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                        SqlDataReader readd33 = cmdd33.ExecuteReader();
                        while (readd33.Read())
                        {
                            PinnAwayTeam = (readd33["ImeKluba"].ToString());
                        }
                        readd33.Close();

                        String queryy34 = "Select ImeKluba from Klubovi where ShortForCovers = '" + PinnHomeTeam + "' and League = '" + LeagueNBAMLB + "'";
                        SqlCommand cmdd34 = new SqlCommand(queryy34, connection);
                        SqlDataReader readd34 = cmdd34.ExecuteReader();
                        while (readd34.Read())
                        {
                            PinnHomeTeam = (readd34["ImeKluba"].ToString());
                        }
                        readd34.Close();
                    }
                    Renameteams();

                    double PinnH = Convert.ToDouble(PinnHandi);
                    if (PinnH >= 130 && PinnH <= 170)
                    {
                        PinnHandi = "+1";
                    }
                    else if (PinnH < -125 && PinnH >= -170)
                    {
                        PinnHandi = "-1";
                    }
                    else if ((PinnH >= -125 && PinnH < 130))
                    {
                        PinnHandi = "0";
                    }
                    else if (PinnH > 170 && PinnH < 220)
                    {
                        PinnHandi = "+1,5";
                    }
                    else if (PinnH < -170 && PinnH > -220)
                    {
                        PinnHandi = "-1,5";
                    }
                    else if (PinnH >= 220)
                    {
                        PinnHandi = "+2";
                    }
                    else if (PinnH <= -220)
                    {
                        PinnHandi = "-2";
                    }


                    _coversAwayStarter = _coversAwayStarter.Replace("O&#39;Sullivan", "OSullivan");
                    _coversHomeStarter = _coversHomeStarter.Replace("O&#39;Sullivan", "OSullivan");
                    _coversAwayStarter = _coversAwayStarter.Replace("De La Rosa", "DeLaRosa");
                    _coversHomeStarter = _coversHomeStarter.Replace("De La Rosa", "DeLaRosa");
                    _coversAwayStarter = _coversAwayStarter.Replace("Jonathon Niese", "Jon Niese");
                    _coversHomeStarter = _coversHomeStarter.Replace("Jonathon Niese", "Jon Niese");
                    _coversAwayStarter = _coversAwayStarter.Replace("R.A.", "RA");
                    _coversHomeStarter = _coversHomeStarter.Replace("R.A.", "RA");
                    _coversAwayStarter = _coversAwayStarter.Replace("J.A.", "JA");
                    _coversHomeStarter = _coversHomeStarter.Replace("J.A.", "JA");
                    _coversAwayStarter = _coversAwayStarter.Replace("T.J.", "TJ");
                    _coversHomeStarter = _coversHomeStarter.Replace("T.J.", "TJ");
                    _coversAwayStarter = _coversAwayStarter.Replace("A.J.", "AJ");
                    _coversHomeStarter = _coversHomeStarter.Replace("A.J.", "AJ");
                    _coversAwayStarter = _coversAwayStarter.Replace("Chi Chi", "Chi-Chi");
                    _coversHomeStarter = _coversHomeStarter.Replace("Chi Chi", "Chi-Chi");
                    _coversAwayStarter = _coversAwayStarter.Replace(" (L)", "");
                    _coversHomeStarter = _coversHomeStarter.Replace(" (L)", "");

                    HomeStarterHand = matchupsAndScoresDGV.Rows[matchupsAndScoresDGV.CurrentCell.RowIndex].Cells["Home Starter"].Value.ToString().Split(' ')[2].Replace("(", "");
                    HomeStarterHand = HomeStarterHand.Replace(")", "");

                    AwayStarterHand = matchupsAndScoresDGV.Rows[matchupsAndScoresDGV.CurrentCell.RowIndex].Cells["Away Starter"].Value.ToString().Split(' ')[2].Replace("(", "");
                    AwayStarterHand = HomeStarterHand.Replace(")", "");
                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        connection.Open();
                        string queryawaystarter = @"IF NOT EXISTS (Select ID from MLBStarters where FirstName = '" + _coversAwayStarter.Split(' ')[0] + "' and LastName = '" + _coversAwayStarter.Split(' ')[1] + "') " +
                                                  " BEGIN " +
                                                  " INSERT INTO MLBStarters (FirstName, LastName, Hand,Team) " +
                                                  " VALUES('" + _coversAwayStarter.Split(' ')[0] + "','" + _coversAwayStarter.Split(' ')[1] + "' " +
                                                  " , '" + AwayStarterHand + "', (Select KratkoIme from Klubovi where ImeKluba = '" + PinnAwayTeam + "')) end";

                        SqlCommand cmdawaystarter = new SqlCommand(queryawaystarter, connection);
                        cmdawaystarter.ExecuteNonQuery();

                        string queryhomestarter = @"IF NOT EXISTS (Select ID from MLBStarters where FirstName = '" + _coversHomeStarter.Split(' ')[0] + "' and LastName = '" + _coversHomeStarter.Split(' ')[1] + "') " +
                                                  " BEGIN " +
                                                  " INSERT INTO MLBStarters (FirstName, LastName, Hand,Team) " +
                                                  " VALUES('" + _coversHomeStarter.Split(' ')[0] + "','" + _coversHomeStarter.Split(' ')[1] + "' " +
                                                  " , '" + HomeStarterHand + "', (Select KratkoIme from Klubovi where ImeKluba = '" + PinnHomeTeam + "')) end";

                        SqlCommand cmdhomestarter = new SqlCommand(queryhomestarter, connection);
                        cmdhomestarter.ExecuteNonQuery();
                    }
                    AwayTeamCB.Text = PinnAwayTeam;
                    HomeTeamCB.Text = PinnHomeTeam;
                    awayStarterCB.Text = _coversAwayStarter;
                    homeStarterCB.Text = _coversHomeStarter;
                    TotalBox.Text = PinnTotal;
                    HandicapBox.Text = PinnHandi;
                    TotalBox.Focus();
                    TotalBox.Refresh();
                    HandicapBox.Focus();
                    HandicapBox.Refresh();

                    awayStarterCB.Enabled = true;
                    homeStarterCB.Enabled = true;
                    TotalBox.Enabled = true;
                    HandicapBox.Enabled = true;
                    AutoProcessBtn.Focus();
                }
            }
            else if (radioBtnNBA.Checked == true)
            {
                NewGameBtn_Click(sender, e);

                if (matchupsAndScoresDGV.Rows[e.RowIndex].Cells[0].Selected)
                {

                    PinnAwayTeam = matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Away Team"].Value.ToString();
                    PinnHomeTeam = matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Home Team"].Value.ToString();
                    PinnHandi = matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Line"].Value.ToString();
                    PinnTotal = matchupsAndScoresDGV.Rows[e.RowIndex].Cells["Total"].Value.ToString().Replace(".", ",");

                    if (PinnHandi == "")
                    {
                        PinnHandi = "0";
                        MessageBox.Show("There was an 'Off' or empty offer, Hendi is set to 0!", "Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    if (PinnTotal == "")
                    {
                        PinnTotal = "200";
                        MessageBox.Show("There was an 'Off' or empty offer, Total is set to 200!", "Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        connection.Open();
                        String queryy33 = "Select ImeKluba from Klubovi where ShortForCovers = '" + PinnAwayTeam + "' and League = '" + LeagueNBAMLB + "'";
                        SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                        SqlDataReader readd33 = cmdd33.ExecuteReader();
                        while (readd33.Read())
                        {
                            PinnAwayTeam = (readd33["ImeKluba"].ToString());
                        }
                        readd33.Close();

                        String queryy34 = "Select ImeKluba from Klubovi where ShortForCovers = '" + PinnHomeTeam + "' and League = '" + LeagueNBAMLB + "'";
                        SqlCommand cmdd34 = new SqlCommand(queryy34, connection);
                        SqlDataReader readd34 = cmdd34.ExecuteReader();
                        while (readd34.Read())
                        {
                            PinnHomeTeam = (readd34["ImeKluba"].ToString());
                        }
                        readd34.Close();
                    }

                    Renameteams();

                    double PinnH = Convert.ToDouble(PinnHandi.Replace(".", ","));
                    PinnHandi = PinnH.ToString();

                    AwayTeamCB.Text = PinnAwayTeam;
                    HomeTeamCB.Text = PinnHomeTeam;
                    TotalBox.Text = PinnTotal;
                    HandicapBox.Text = PinnHandi;
                    TotalBox.Focus();
                    TotalBox.Refresh();
                    HandicapBox.Focus();
                    HandicapBox.Refresh();
                    AutoProcessBtn.Focus();
                }
            }
            // }
            //   catch
            //  {
            //MessageBox.Show("Enter offer for off value!", "Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    }
        }


        private void UpdMLBStrBtn_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand UpdMLBStr = new SqlCommand("UPDATEMLBSTARTERS", connection);
                UpdMLBStr.CommandType = CommandType.StoredProcedure;
                UpdMLBStr.ExecuteNonQuery();
                MessageBox.Show("Starters Updated!", "Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void LoadGameBtn_Click(object sender, EventArgs e)
        {
            using (OkladesEntities context = new OkladesEntities())
            {
                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    {
                        DGVAnalysisResults.Columns.Remove("chk2");
                    }
                }

                DGVAnalysisResults.DataSource = null;

                IDO = Convert.ToInt32(IDoklBox.Text);

                // GET RESULTS FOR GAMES
                if (radioBtnMLB.Checked == true)
                {
                    _varMlbResults = _analyzerActions.FillResultsMlb(IDO, radioBtnMLB, radioBtnNBA);
                    _resultsBindingSource.DataSource = _varMlbResults;
                    DGVAnalysisResults.DataSource = _resultsBindingSource;
                }
                else if (radioBtnNBA.Checked == true)
                {
                    _varNbaResults = _analyzerActions.FillResultsNba(IDO, radioBtnMLB, radioBtnNBA);
                    _resultsBindingSource.DataSource = _varNbaResults;
                    DGVAnalysisResults.DataSource = _resultsBindingSource;
                }

                // RemoveDuplicate(sender, e);

                // Format DGVAnalysisResults
                _dgva.Dgv1Actions(DGVAnalysisResults, _homeTeam, _awayTeam, radioBtnNBA, radioBtnMLB, awayStarterCB, homeStarterCB);

                _dgva.ColorDgv1RowsAndCalculate(DGVAnalysisResults, TotalBox, HandicapBox, out _isOver, out _isUnder, out _hAway, out _hHome, out _hAvg, out _aAvg);
                Dgv2(sender, e);
                Dgv5(sender, e);
                finishandCalcPB.Enabled = false;
            }
        }

        private void NewGameBtn_Click(object sender, EventArgs e)
        {
            cleanitup(sender, e);

            awayTeamLogoPB.Image = null;
            homeTeamLogoPB.Image = null;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                String queryy = "Select TOP 1 IDOklade from " + NBAMLBResultsVar + " order by IDOklade desc";

                SqlCommand cmdd = new SqlCommand(queryy, connection);
                SqlDataReader readd = cmdd.ExecuteReader();

                while (readd.Read())
                {
                    IDOkl2 = (readd["IDOklade"].ToString());
                }
                readd.Close();
                int IDOkl3 = Convert.ToInt32(IDOkl2);
                IDoklBox.Text = (IDOkl3 + 1).ToString();

            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //EXIT BUTTON//
            DialogResult d = MessageBox.Show("Are you sure you want to exit, did you check if all bets are entered?", "Exit Program", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            try
            {
                if (d == DialogResult.Yes)
                {
                    System.Windows.Forms.Application.Exit();
                }
                else if (d == DialogResult.No)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SQDLTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                using (var connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    Sql3 = string.Format("select Month,Dayofmonth,Team,Opp,Hscore,Ascore,Total from {0} where {1} order by ID desc", AllNBAMLBResultsVar, SQDLTB.Text);

                    ds = new DataSet();
                    adapter = new SqlDataAdapter(Sql3, connection);
                    ds.Reset();
                    adapter.Fill(ds);
                    dataGridView6.DataSource = ds.Tables[0];
                    dataGridView6.Columns[0].Width = 40;
                    dataGridView6.Columns[1].Width = 40;
                }
            }
        }

        private void results1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void results1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && (ModifierKeys & Keys.Control) == Keys.Control)
            {
                global::Oklade.BetsTracker BT = new global::Oklade.BetsTracker();

                BT.TeamslistBox1.Focus();
                BT.TeamslistBox1.SelectedValue = AwayTeamCB.Text;

            }
            else
            {
                if (AwayTeamCB.Text.Length > 1)
                {
                    ResultUpdater frm3 = new ResultUpdater();
                    frm3.value = AwayTeamCB.Text.Split(' ')[1];
                    frm3.Show();
                    //frm3.tabControl1.SelectedIndex = 1;
                    frm3.comboBox1.Text = AwayTeamCB.Text;
                    frm3.button7_Click(sender, e);
                }
            }
        }

        private void AwayTeamTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AwayTeamTabControl.SelectedIndex == 1)
            {
                AwayStarterL50();
            }
        }

        private void HomeTeamTabControl_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (HomeTeamTabControl.SelectedIndex == 1)
            {
                HomeStarterL50();
            }
        }

        private void Starterstats()
        {
            using (OkladesEntities context = new OkladesEntities())
            {
                StartersStatsDGV.InvokeIfRequired(ssdgv =>
                  {
                      var awayStarterLN = _awayStarterLastName;
                      awayStarterLN = awayStarterLN.Replace("OSullivan", "O'Sullivan");
                      awayStarterLN = awayStarterLN.Replace("DeLaRosa", "De La Rosa");
                      awayStarterLN = awayStarterLN.Replace("DeLaCruz", "De La Cruz");
                      awayStarterLN = awayStarterLN.Replace("DeLeon", "De Leon");

                      var homeStarterLN = _homeStarterLastName;
                      homeStarterLN = homeStarterLN.Replace("OSullivan", "O'Sullivan");
                      homeStarterLN = homeStarterLN.Replace("DeLaRosa", "De La Rosa");
                      homeStarterLN = homeStarterLN.Replace("DeLaCruz", "De La Cruz");
                      homeStarterLN = homeStarterLN.Replace("DeLeon", "De Leon");

                      var startersStats =
                         context.MLBStartersStatsNEWs.Where(
                                                     p => p.PlayerName.Contains(awayStarterLN) &&
                                                          p.PlayerName.Substring(0, 1).Contains(_awayStarterFirstName.Substring(0, 1)) &&
                                                          p.Venue == "Away" &&
                                                          p.Team.Contains(_awayTeam.ShortForCovers.Replace("WAS", "WSH")) ||
                                                          p.PlayerName.Contains(homeStarterLN) &&
                                                          p.PlayerName.Substring(0, 1).Contains(_homeStarterFirstName.Substring(0, 1)) &&
                                                          p.Venue == "Home" &&
                                                          p.Team.Contains(_homeTeam.ShortForCovers.Replace("WAS", "WSH")))
                                                          .ToList();

                      StartersStatsDGV.DataSource = startersStats;

                      if (startersStats.Count > 0)
                      {
                          System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                          boldStyle.Font = new System.Drawing.Font("Ariel Narrow", 11F, System.Drawing.FontStyle.Bold);
                          boldStyle.ForeColor = Color.Navy;
                          System.Windows.Forms.DataGridViewCellStyle boldStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
                          boldStyle2.Font = new System.Drawing.Font("Ariel Narrow", 10.5F, System.Drawing.FontStyle.Bold);
                          boldStyle2.ForeColor = Color.Green;
                          System.Windows.Forms.DataGridViewCellStyle boldStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
                          boldStyle3.Font = new System.Drawing.Font("Ariel Narrow", 10.5F, System.Drawing.FontStyle.Bold);
                          boldStyle3.ForeColor = Color.DarkRed;
                          System.Windows.Forms.DataGridViewCellStyle boldStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
                          boldStyle4.Font = new System.Drawing.Font("Ariel Narrow", 10.5F, System.Drawing.FontStyle.Bold);
                          boldStyle4.ForeColor = Color.DarkOrange;

                          StartersStatsDGV.Columns.Remove("ID");
                          StartersStatsDGV.Columns.Remove("Venue");
                          StartersStatsDGV.Columns["PlayerName"].DefaultCellStyle = boldStyle;
                          StartersStatsDGV.Columns["Team"].DefaultCellStyle = boldStyle;
                          StartersStatsDGV.Columns["ER"].DefaultCellStyle = boldStyle;
                          StartersStatsDGV.Columns["W"].DefaultCellStyle = boldStyle2;
                          StartersStatsDGV.Columns["L"].DefaultCellStyle = boldStyle3;
                          StartersStatsDGV.Columns["PlayerName"].Width = 160;
                          StartersStatsDGV.Columns["Team"].Width = 45;
                          StartersStatsDGV.Columns["ERA"].Width = 45;


                          foreach (DataGridViewRow row in StartersStatsDGV.Rows)
                          {
                              if (Convert.ToDecimal(row.Cells["ERA"].Value) >= 4.5M)
                              {
                                  row.Cells["ERA"].Style = boldStyle3;
                              }
                              else if (Convert.ToDecimal(row.Cells["ERA"].Value) <= 3.5M)
                              {
                                  row.Cells["ERA"].Style = boldStyle2;
                              }
                              else if (Convert.ToDecimal(row.Cells["ERA"].Value) >= 3.5M && Convert.ToDecimal(row.Cells["ERA"].Value) <= 4.5M)
                              {
                                  row.Cells["ERA"].Style = boldStyle4;
                              }
                              if (Convert.ToDecimal(row.Cells["SO"].Value) <= 4.0M)
                              {
                                  row.Cells["SO"].Style = boldStyle3;
                              }
                              else if (Convert.ToDecimal(row.Cells["SO"].Value) >= 6.0M)
                              {
                                  row.Cells["SO"].Style = boldStyle2;
                              }
                              else if (Convert.ToDecimal(row.Cells["SO"].Value) >= 4.0M && Convert.ToDecimal(row.Cells["SO"].Value) <= 6.0M)
                              {
                                  row.Cells["SO"].Style = boldStyle4;
                              }
                          }

                          StartersStatsDGV.Rows[0].Cells[0].Selected = false;
                      }
                  });
            }
        }

        private void homeStarterCB_TextChanged(object sender, EventArgs e)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (homeStarterCB.Text != "")
                {
                    _homeStarterFirstName = homeStarterCB.Text.Split(' ')[0];
                    _homeStarterLastName = homeStarterCB.Text.Split(' ')[1];

                    if (awayStarterCB.Text != "")
                    {
                        _awayStarterFirstName = awayStarterCB.Text.Split(' ')[0];
                        _awayStarterLastName = awayStarterCB.Text.Split(' ')[1];
                    }

                    if (homeStarterCB.Text.Length > 1)
                    {
                        HomeStarterHand = null;
                        string query134 = string.Format("Select Hand from MLBStarters where FirstName = '{0}' and LastName = '{1}'", homeStarterCB.Text.Split(' ')[0], homeStarterCB.Text.Split(' ')[1]);
                        SqlCommand cmd134 = new SqlCommand(query134, connection);
                        SqlDataReader read134 = cmd134.ExecuteReader();
                        while (read134.Read())
                        {
                            HomeStarterHand = (read134["Hand"].ToString());
                        }
                        read134.Close();

                        if (HomeStarterHand == null || HomeStarterHand.Contains('?'))
                        {
                            HomeStarterHand = matchupsAndScoresDGV.Rows[matchupsAndScoresDGV.CurrentCell.RowIndex].Cells[6].Value.ToString().Split(' ')[2].Replace("(", "");
                            HomeStarterHand = HomeStarterHand.Replace(")", "");
                        }

                        HStarterHandLabel.Text = "(" + HomeStarterHand + ")";
                        if (radioBtnMLB.Checked == true)
                        {
                            var calcA = Task.Run(() =>
                           {
                               CalculateAveragesSuouAwayTeam();
                           }).ContinueWith((pastTask) =>
                           {
                               CalculateAveragesSuouHomeTeam();
                           }).ContinueWith((pastTask2) =>
                           {
                               showAveragesinDGV();
                           });

                        }
                        if (HomeTeamTabControl.SelectedIndex == 1)
                        {
                            HomeStarterL50();
                        }
                        var ss = Task.Run(() =>
                               {
                                   Starterstats();
                               });
                    }
                }

            }
        }

        private void ViewGamesAwayBtn_Click(object sender, EventArgs e)
        {
            tabControlInjuries.SelectedIndex = 2;

            SQDLTB.Text = string.Format("Opp = '{0}' and StarterHHand like '{1}'", AwayTeamCB.Text.Split(' ')[1], HomeStarterHand);
            SQDLTB.Text = SQDLTB.Text.Replace("L ", "L");
            SQDLTB.Text = SQDLTB.Text.Replace("R ", "R");
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                Sql3 = string.Format("select Month,Dayofmonth,Team,Opp,Hscore,Ascore,Total from {0} where {1} order by ID desc", AllNBAMLBResultsVar, SQDLTB.Text);

                ds = new DataSet();
                adapter = new SqlDataAdapter(Sql3, connection);
                ds.Reset();
                adapter.Fill(ds);
                dataGridView6.DataSource = ds.Tables[0];
                dataGridView6.Columns[0].Width = 40;
                dataGridView6.Columns[1].Width = 40;
                dataGridView6.Rows[0].Cells[0].Selected = false;
            }
        }

        private void ViewGamesHomeBtn_Click(object sender, EventArgs e)
        {
            tabControlInjuries.SelectedIndex = 2;

            //SQDLTB.Text = "Team = '" + HomeTeamCB.Text.Split(' ')[1] + "' and StarterAHand like '" + AwayStarterHand + "'";
            SQDLTB.Text = string.Format("Team = '{0}' and StarterAHand like '{1}'", HomeTeamCB.Text.Split(' ')[1], AwayStarterHand);
            SQDLTB.Text = SQDLTB.Text.Replace("L ", "L");
            SQDLTB.Text = SQDLTB.Text.Replace("R ", "R");
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                Sql3 = string.Format("select Month,Dayofmonth,Team,Opp,Hscore,Ascore,Total from {0} where {1} order by ID desc", AllNBAMLBResultsVar, SQDLTB.Text);

                ds = new DataSet();
                adapter = new SqlDataAdapter(Sql3, connection);
                ds.Reset();
                adapter.Fill(ds);
                dataGridView6.DataSource = ds.Tables[0];
                dataGridView6.Columns[0].Width = 40;
                dataGridView6.Columns[1].Width = 40;
                dataGridView6.Rows[0].Cells[0].Selected = false;
            }
        }


        private void button6_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                for (int i = 0; i < matchupsAndScoresDGV.Rows.Count; i++)
                {
                    if (matchupsAndScoresDGV.Rows[i].Cells["Total"].Value.ToString() == "Off")
                    {
                        if (matchupsAndScoresDGV.Rows[i].Cells["Line"].Value.ToString() == "")
                        {
                            matchupsAndScoresDGV.Rows[i].Cells["Line"].Value = 0;
                        }
                        matchupsAndScoresDGV.Rows[i].Cells["Total"].Value = 0;
                    }
                    //                        String query3 = @"INSERT INTO LiveScoresUpdates (Date, AwayTeam,AwayStarter,HomeTeam,HomeStarter,Total,Line,GameStatus)
                    //              VALUES ( '" + DateTime.Now.ToShortDateString() + "','" + dataGridView8.Rows[i].Cells["Away Team"].Value + "','" + dataGridView8.Rows[i].Cells["Away Starter"].Value +
                    //                          "','" + dataGridView8.Rows[i].Cells["Home Team"].Value + "','" + dataGridView8.Rows[i].Cells["Home Starter"].Value + "'," + dataGridView8.Rows[i].Cells["Total"].Value +
                    //                          "," + dataGridView8.Rows[i].Cells["Line"].Value + ",'" + dataGridView8.Rows[i].Cells["Game Status"].Value + "') ON DUPLICATE KEY UPDATE Total = " +
                    //                          dataGridView8.Rows[i].Cells["Total"].Value + ", Line = " + dataGridView8.Rows[i].Cells["Line"].Value + ";";

                    string query3 = @"if not exists " +
                                      "(select top 1 ID from LiveScoresUpdates where Date = '" + DateTime.Now.ToShortDateString() + "' and AwayTeam = '" + matchupsAndScoresDGV.Rows[i].Cells["Away Team"].Value + "')" +
                                      " begin " +
                                       " INSERT INTO LiveScoresUpdates (Date, AwayTeam,AwayStarter,HomeTeam,HomeStarter,Total,Line,GameStatus) " +
                                      " VALUES ( '" + DateTime.Now.ToShortDateString() + "','" + matchupsAndScoresDGV.Rows[i].Cells["Away Team"].Value + "','" + matchupsAndScoresDGV.Rows[i].Cells["Away Starter"].Value + "', " +
                                       " '" + matchupsAndScoresDGV.Rows[i].Cells["Home Team"].Value + "','" + matchupsAndScoresDGV.Rows[i].Cells["Home Starter"].Value + "'," + matchupsAndScoresDGV.Rows[i].Cells["Total"].Value + ", " +
                                       " " + matchupsAndScoresDGV.Rows[i].Cells["Line"].Value + ",'" + matchupsAndScoresDGV.Rows[i].Cells["Game Status"].Value + "') " +
                                      " end " +
                                      " else " +
                                      " begin " +
                                      " UPDATE LiveScoresUpdates set Total = " + matchupsAndScoresDGV.Rows[i].Cells["Total"].Value + ", " +
                                      "  Line = " + matchupsAndScoresDGV.Rows[i].Cells["Line"].Value + " where  Date = '" + DateTime.Now.ToShortDateString() + "' " +
                                      " and AwayTeam = '" + matchupsAndScoresDGV.Rows[i].Cells["Away Team"].Value + "' " +
                                      "end";

                    SqlCommand cmd111 = new SqlCommand(query3, connection);
                    cmd111.ExecuteNonQuery();



                }

                MessageBox.Show("Games Inserted/Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void SaveandRecalcbtn_Click(object sender, EventArgs e)
        {
            using (OkladesEntities context = new OkladesEntities())
            {
                int j = 0;
                int k = 0;
                for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
                {
                    int rezz1 = Convert.ToInt32(DGVAnalysisResults.Rows[i].Cells["HResult"].Value);
                    int rezz2 = Convert.ToInt32(DGVAnalysisResults.Rows[i].Cells["AResult"].Value);
                    int tot1 = rezz1 + rezz2;
                    int hen1 = rezz2 - rezz1;

                    DGVAnalysisResults.Rows[i].Cells["TotalResult"].Value = tot1;
                    DGVAnalysisResults.Rows[i].Cells["HandicapResult"].Value = hen1;
                }

                context.SaveChanges();
                updateResultsPB_Click(sender, e);
            }
        }
        private void pace_eff()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                teamsDetailedStatsDGV.InvokeIfRequired(t =>
                {
                    if (radioBtnNBA.Checked == true)
                    {
                        if (teamsDetailedStatsDGV.DataSource != null)
                        {
                            teamsDetailedStatsDGV.DataSource = null;
                        }
                        else
                        {
                            teamsDetailedStatsDGV.Rows.Clear();
                        }

                        teamsDetailedStatsDGV.ColumnCount = 3;

                        paceaway = _awayTeam.PaceAway.ToString();

                        pacehome = _homeTeam.PaceHome.ToString();

                        offeffaway = _awayTeam.OeffAway.ToString();

                        offeffhome = _homeTeam.OeffHome.ToString();

                        deffeffaway = _awayTeam.DeffAway.ToString();

                        deffeffhome = _homeTeam.DeffHome.ToString();

                        coversrow = new string[] { _awayTeam.KratkoIme, "Team", _homeTeam.KratkoIme };
                        teamsDetailedStatsDGV.Rows.Add(coversrow);

                        coversrow = new string[] { paceaway, "Pace", pacehome };
                        teamsDetailedStatsDGV.Rows.Add(coversrow);

                        coversrow2 = new string[] { offeffaway, "Off Eff", offeffhome };
                        teamsDetailedStatsDGV.Rows.Add(coversrow2);

                        coversrow3 = new string[] { deffeffaway, "Deff Eff", deffeffhome };
                        teamsDetailedStatsDGV.Rows.Add(coversrow3);

                        DataGridViewCellStyle boldStyle = new DataGridViewCellStyle();
                        boldStyle.Font = new Font("Arial Narrow", 12.0F, FontStyle.Bold);
                        boldStyle.ForeColor = Color.White;

                        teamsDetailedStatsDGV.Columns[0].Width = 110;
                        teamsDetailedStatsDGV.Columns[1].Width = 70;
                        teamsDetailedStatsDGV.Columns[2].Width = 110;
                        teamsDetailedStatsDGV.Rows[0].Cells[0].Style = boldStyle;
                        teamsDetailedStatsDGV.Rows[0].Cells[2].Style = boldStyle;

                        for (i = 0; i < 4; i++)
                        {
                            teamsDetailedStatsDGV.Rows[i].Cells[1].Style.ForeColor = Color.White;
                        }

                        teamsDetailedStatsDGV.Rows[0].Cells[0].Selected = false;
                    }
                });
            }
        }

        private void TotalPlusButton_Click(object sender, EventArgs e)
        {
            double totalNEW = Convert.ToDouble(TotalBox.Text);
            totalNEW += 0.5;
            TotalBox.Text = totalNEW.ToString();
        }

        private void TotalMinusButton_Click(object sender, EventArgs e)
        {
            double totalNEW = Convert.ToDouble(TotalBox.Text);
            totalNEW -= 0.5;
            TotalBox.Text = totalNEW.ToString();
        }

        private void HandicapPlusButton_Click(object sender, EventArgs e)
        {
            double totalNEW = Convert.ToDouble(HandicapBox.Text);
            totalNEW += 0.5;
            HandicapBox.Text = totalNEW.ToString();
        }

        private void HandicapMinusButton_Click(object sender, EventArgs e)
        {
            double totalNEW = Convert.ToDouble(HandicapBox.Text);
            totalNEW -= 0.5;
            HandicapBox.Text = totalNEW.ToString();
        }

        private void Checkboxes()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                ToBetOnCB.Checked = false;
                BetProcessedCB.Checked = false;
                editedCB.Checked = false;
                ToBetOn = 0;
                BetProcessed = 0;
                Edited = 0;


                string sqlqcheck = @"SELECT ToBetOn, BetProcessed, Edited from " + NBAMLBBetsVar + " where IDOklade = " + IDoklBox.Text + "";

                SqlCommand cmdsqlqcheck = new SqlCommand(sqlqcheck, connection);
                SqlDataReader readsqlqcheck = cmdsqlqcheck.ExecuteReader();

                while (readsqlqcheck.Read())
                {
                    if (!(readsqlqcheck["ToBetOn"] is DBNull))
                    {
                        ToBetOn = Convert.ToInt32(readsqlqcheck["ToBetOn"]);
                        BetProcessed = Convert.ToInt32(readsqlqcheck["BetProcessed"]);
                        Edited = Convert.ToInt32(readsqlqcheck["Edited"]);
                    }
                }
                readsqlqcheck.Close();

                if (ToBetOn == 1)
                {
                    ToBetOnCB.Checked = true;
                }
                else
                {
                    ToBetOnCB.Checked = false;
                }
                if (BetProcessed == 1)
                {
                    BetProcessedCB.Checked = true;
                }
                else
                {
                    BetProcessedCB.Checked = false;
                }
                if (Edited == 1)
                {
                    editedCB.Checked = true;
                }
                else
                {
                    editedCB.Checked = false;
                }
            }
        }

        private void IDoklBox_TextChanged(object sender, EventArgs e)
        {
            if (IDoklBox.Text != "")
            {
                Checkboxes();
            }
        }

        private void editedCB_MouseClick(object sender, MouseEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (editedCB.Checked == true)
                {
                    seton = 1;
                }
                else
                {
                    seton = 0;
                }
                string sqlqEdited = @"UPDATE " + NBAMLBBetsVar + " SET Edited = " + seton + " where IDOklade = " + IDoklBox.Text + "";

                SqlCommand cmd112 = new SqlCommand(sqlqEdited, connection);

                cmd112.ExecuteNonQuery();
            }
        }

        private void ToBetOnCB_MouseClick(object sender, MouseEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (ToBetOnCB.Checked == true)
                {
                    seton = 1;
                }
                else
                {
                    seton = 0;
                }
                string sqlqToBetOn = @"UPDATE " + NBAMLBBetsVar + " SET ToBetOn = " + seton + " where IDOklade = " + IDoklBox.Text + "";

                SqlCommand cmd112 = new SqlCommand(sqlqToBetOn, connection);

                cmd112.ExecuteNonQuery();
            }
        }

        private void BetProcessedCB_MouseClick(object sender, MouseEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (BetProcessedCB.Checked == true)
                {
                    seton = 1;
                }
                else
                {
                    seton = 0;
                }

                string sqlqBetProcessed = @"UPDATE " + NBAMLBBetsVar + " SET BetProcessed = " + seton + " where IDOklade = " + IDoklBox.Text + "";

                SqlCommand cmd112 = new SqlCommand(sqlqBetProcessed, connection);

                cmd112.ExecuteNonQuery();
            }
        }

        private void ateamradio_CheckedChanged(object sender, EventArgs e)
        {
            if (ateamradio.Checked == true)
            {
                homecheckBox1.Checked = false;
                awaycheckBox2.Checked = true;
                button3_Click(sender, e);
            }
            else
            {
                homecheckBox1.Checked = true;
                awaycheckBox2.Checked = false;
                button3_Click(sender, e);
            }
        }

        private void TotalBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
            (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == ',') && ((sender as System.Windows.Forms.TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
            if (Regex.IsMatch(TotalBox.Text, @"\,\d"))
            {
                e.Handled = true;
            }
        }

        private void HandicapBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
          (e.KeyChar != ',') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == ',') && ((sender as System.Windows.Forms.TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
            // only allow one minus
            if ((e.KeyChar == '-') && ((sender as System.Windows.Forms.TextBox).Text.IndexOf('-') > -1))
            {
                e.Handled = true;
            }
            if (Regex.IsMatch(HandicapBox.Text, @"\,\d"))
            {
                e.Handled = true;
            }
        }

        private void DelRowBtn_Click(object sender, EventArgs e)
        {

            DialogResult d = MessageBox.Show("Are you sure you want to delete this game?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            try
            {

                if (d == DialogResult.Yes)
                {

                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        connection.Open();

                        using (OkladesEntities context = new OkladesEntities())
                        {

                            Sql3 = "select ID,ImeHKluba,ImeAKluba, HResult,AResult,TotalResult,HandicapResult,Season,Line,Total, HomePace, AwayPace from " + NBAMLBResultsVar + " where IDOklade = '" + IDO2 + "'";

                            foreach (DataGridViewCell oneCell in DGVAnalysisResults.SelectedCells)
                            {
                                if (oneCell.Selected)
                                {
                                    NBAResult nbaResult = new NBAResult() { ID = Convert.ToInt32(DGVAnalysisResults.Rows[DGVAnalysisResults.CurrentCell.RowIndex].Cells[0].Value) };
                                    context.NBAResults.Attach(nbaResult);
                                    context.NBAResults.Remove(nbaResult);
                                    context.SaveChanges();
                                    DGVAnalysisResults.Rows.RemoveAt(oneCell.RowIndex);
                                    //var rowToDelete = context.NBAResults.FirstOrDefault(g => g.ID == Convert.ToInt32(oneCell.Value));
                                    //context.NBAResults.Attach(rowToDelete);
                                }          
                            }

                            //try
                            //{
                            //    adapterMain = new SqlDataAdapter(Sql3, connection);
                            //    SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapterMain);
                            //    adapterMain.Update(ds);
                            //    MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //}
                            //catch (Exception ex)
                            //{
                            //    MessageBox.Show(ex.ToString());
                            //}

                            updateResultsPB_Click(sender, e);
                        }
                    }
                }
                else if (d == DialogResult.No)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void AvsDivBTN_Click(object sender, EventArgs e)
        {
            string vsDIVvar = "";
            if (ateamradio.Checked == true)
            {
                vsDIVvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+o%3Adivision%3Ddivision&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                vsDIVvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+o%3Adivision%3Ddivision&submit=++S+D+Q+L+%21++";
            }
            Process.Start(vsDIVvar);
        }

        private void AafterLBTN_Click(object sender, EventArgs e)
        {
            string afterLvar = "";
            if (ateamradio.Checked == true)
            {
                afterLvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+p%3AL&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                afterLvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+p%3AL&submit=++S+D+Q+L+%21++";
            }
            Process.Start(afterLvar);
        }

        private void vsNODIVBTN_Click(object sender, EventArgs e)
        {
            string vsNODIVvar = "";
            if (ateamradio.Checked == true)
            {
                vsNODIVvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+o%3Adivision%21%3Ddivision&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                vsNODIVvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+o%3Adivision%21%3Ddivision&submit=++S+D+Q+L+%21++";
            }
            Process.Start(vsNODIVvar);
        }

        private void AfterWBTN_Click(object sender, EventArgs e)
        {
            string afterWvar = "";
            if (ateamradio.Checked == true)
            {
                afterWvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+p%3AW&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                afterWvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+p%3AW&submit=++S+D+Q+L+%21++";
            }
            Process.Start(afterWvar);
        }

        private void firstBtoBbtn_Click(object sender, EventArgs e)
        {
            string firstndofBtoB = "";
            if (ateamradio.Checked == true)
            {
                firstndofBtoB = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+n%3Arest%3D0&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                firstndofBtoB = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+n%3Arest%3D0&submit=++S+D+Q+L+%21++";
            }
            Process.Start(firstndofBtoB);
        }

        private void secondBtoBbtn_Click(object sender, EventArgs e)
        {
            string secondndofBtoB = "";
            if (ateamradio.Checked == true)
            {
                secondndofBtoB = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+rest%3D0&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                secondndofBtoB = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+rest%3D0&submit=++S+D+Q+L+%21++";
            }
            Process.Start(secondndofBtoB);
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void lastXGamesSetBtn_Click(object sender, EventArgs e)
        {
            _sharedmethods.UpdateSetting("LastXGames", LastXgamesCB.Text);
        }


        private void updateStartersBtn_Click(object sender, EventArgs e)
        {
            if (SharedMethods.CheckForInternetConnection())
            {
                GetStartersStats getStartersStats = new GetStartersStats();
                getStartersStats.UpdateStartersStats(progressLabel, updateStartersPB);
            }
            else
            {
                MessageBox.Show("No Internet available to proceed with request", "Info", MessageBoxButtons.OK);
            }
        }

        private void updatePFBtn_Click(object sender, EventArgs e)
        {
            _parkFactors.GetParkFactor();
        }

        private void lastXgamesAvgCB_CheckedChanged(object sender, EventArgs e)
        {
                CalculateAveragesSuouAwayTeam();
                CalculateAveragesSuouHomeTeam();
                showAveragesinDGV();
        }

        private void lastXCB_SelectedValueChanged(object sender, EventArgs e)
        {
            if (lastXgamesAvgCB.Checked) { 
                CalculateAveragesSuouAwayTeam();
                CalculateAveragesSuouHomeTeam();
                showAveragesinDGV();
            }
        }
    }
}

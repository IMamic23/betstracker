using Oklade.DataGridViews;
using Oklade.SharedMethodsCollection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Oklade
{
    public partial class Analyzer : Form
    {
        DGVActions dgva = new DGVActions();
        SharedMethods sharedmethods = new SharedMethods();

        string strRankingsAway;
        string strRankingsHome;
        double varpacehome;
        double varpaceaway;
        double varpacehomelow;
        double varpacehomehigh;
        double varpaceawaylow;
        double varpaceawayhigh;
        string strpacehomelow;
        string strpacehomehigh;
        string strpaceawaylow;
        string strpaceawayhigh;
        string pacecbaway = "";
        string pacecbhome = "";
        string dgvHomePace;
        string dgvAwayPace;
        int seton;
        int ToBetOn;
        int BetProcessed;
        int Edited;
        int IDO2;
        string paceaway;
        string pacehome;
        string offeffaway;
        string offeffhome;
        string deffeffaway;
        string deffeffhome;
        double avg1vsDIV;
        double avg2vsDIV;
        int countSU2vsDIV;
        int countSU1vsDIV;
        int countSU1svDIV;
        string countU1vsDIV;
        string CountO1vsDIV;
        int countSU3vsDIV;
        int countSU4vsDIV;
        string CountO2vsDIV;
        string countU2vsDIV;
        double avg3vsDIV;
        double avg4vsDIV;
        double totaloffer;
        string updateSUr;
        string updateOUr;
        string updateATSr;
        string dateforupdate;
        string timenoww;
        int IDOkl11;
        int Top1IDOklade;
        string varProvjera;
        double ParkFactor;
        string LeagueNBAMLB;
        int counterstarter;
        int ManualChange = 0;
        int SeasonNBAMLB;
        int COUNTGAMES1;
        int COUNTGAMES2;
        string AwayStarterHand;
        string HomeStarterHand;
        string AwayStarterVar;
        string HomeStarterVar;
        string MLBHStarter;
        string MLBAStarter;
        string NBAMLBResultsVar;
        string NBAMLBBetsVar;
        string AllNBAMLBResultsVar;
        string query4PB14;
        string queryy2Total;
        string queryy3Hand;
        string Sql43Last5;
        string Sql33Last10;
        string Sql34Last10;
        string str1Delete;
        string str2Delete;
        string query4PB7;
        string query5PB7;
        string query4Update;
        string query4AP;
        string query5AP;
        string sql4AP;
        string query6AutoProcess;
        string query5AutoProcess;
        string Sqldgv2;
        string queryyResults;
        string _connectionString;
        SqlConnection connection;
        string Sql2;
        string Sql3;
        string idkluba1;
        DataSet ds;
        DataSet ds2;
        DataSet ds4;
        DataSet ds5;
        DataSet dsstartersstats;
        ToolTip t1 = new ToolTip();
        SqlDataAdapter adapter;
        SqlDataAdapter adapter2;
        SqlDataAdapter adapter3;
        SqlDataAdapter adapter4;
        SqlDataAdapter adapter5;
        SqlDataAdapter adapterMain;
        SqlDataAdapter adapterStandings;
        DataGridView grv;
        string ID;
        string value;
        string IDH;
        string IDA;
        string IDO;
        string HA;
        string tot;
        string hand;
        string til1;
        string til2;
        int isOver = 0;
        int isUnder = 0;
        int hAway = 0;
        int hHome = 0;
        int duplikati = 0;
        int duplikatiindex = 0;
        int til22;
        int til23;
        int til12;
        int til13;
        int til24;
        int til14;
        int countSU1;
        int countSU2;
        int countSU3;
        int countSU4;
        int countSU1vsHand;
        int countSU2vsHand;
        int countSU3vsHand;
        int countSU4vsHand;
        string IDOkl2;
        string HorA;
        string value2;
        double totall;
        double hendii;
        string HAteam22;
        double cellv;
        string version;
        double avg1;
        double avg1vsHand;
        double avg2;
        double avg2vsHand;
        double avg3;
        double avg3vsHand;
        double avg4;
        double avg4vsHand;
        double avg5;
        double avg6;
        double avg7;
        double avg8;
        string kratkoime5;
        string kratkoime6;
        string ATS1;
        string ATS2;
        string OU1;
        string OU2;
        string CountO1;
        string CountO1vsHand;
        string CountO2;
        string CountO2vsHand;
        string countU1;
        string countU2;
        string countU1vsHand;
        string countU2vsHand;
        string CountO3;
        string CountO4;
        string countU3;
        string countU4;
        string CountO3vsHand;
        string CountO4vsHand;
        string countU3vsHand;
        string countU4vsHand;
        string countATS1;
        string countATS2;
        string countATS3;
        string countATS4;
        double HAvg;
        double AAvg;
        string season1;
        string season2;
        string ImeHKluba;
        string ImeAKluba;
        string IDKlubaInj;
        string htmlString = "";
        string pinnacleString = "";
        string TeamOUH;
        string TeamOUA;
        string link1;
        string link2;
        string link3;
        string link4;
        string link5;
        string link6;
        string link7;
        string link11;
        string link12;
        string link13;
        string link14;
        string link15;
        string link16;
        string link17;
        int index;
        int index2;
        int index3;
        int index4;
        int index5;
        int index6;
        int index7;
        int index8;
        int index9;
        int position;
        string linkat;
        string linkht;
        string[] pinnrow;
        string[] pinnteam = new string[32];
        string[] pinnhand = new string[32];
        string[] pinnhandodds = new string[32];
        string[] pinntotal = new string[32];
        string[] pinntotalodds = new string[32];
        string PinnAwayTeam;
        string PinnHomeTeam;
        string PinnHandi;
        string PinnTotal;
        int pinnindex1;
        int pinnindex2;
        int pinnlenght;
        int numbering;
        int noofteams = 0;
        string coversstring;
        string[] coversteam = new string[36];
        string[] coversgamestatus = new string[36];
        string[] coversgamestatus2 = new string[36];
        string[] covershpoints = new string[36];
        string[] covershpoints2 = new string[36];
        string[] coversapoints = new string[36];
        string[] coversapoints2 = new string[36];
        string[] coversboxscore = new string[36];
        string[] coversMatchup = new string[36];
        string[] coversInnings = new string[36];
        string[] coversrow = new string[36];
        string[] coversrow2 = new string[36];
        string[] coversrow3 = new string[36];
        string[] LineHistory = new string[36];
        string[] coversteamimage = new string[36];
        string[] coversdiamond = new string[36];
        string[] coversballs = new string[36];
        string[] coversstrikes = new string[36];
        string[] coversouts = new string[36];
        string[] coversPitcher = new string[36];
        string[] coversTotal = new string[36];
        string[] coversLine = new string[36];
        int[] coversindex = new int[36];
        int[] coversindex2 = new int[36];
        string CoversAwayStarter;
        string CoversHomeStarter;
        int coversbsno;
        int j;
        int i;
        int k;
        int arraycount;
        List<string> result;

        public Analyzer()
        {
            InitializeComponent();
            _connectionString = ConfigurationManager.ConnectionStrings["MyDatabase"].ToString();
            connection = new SqlConnection(_connectionString);

            CultureInfo culture = new CultureInfo(ConfigurationManager.AppSettings["DefaultCulture"]);
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            DateTime dtCheck = DateTime.Today;
            if (dtCheck.Month < 6 || dtCheck.Month > 10)
            {
                radioBtnNBA.Checked = true;
                label6.Hide();
                label7.Hide();
                awayStarterCB.Hide();
                homeStarterCB.Hide();
            }
            else
            {
                radioBtnMLB.Checked = true;
            }

            //WebClient wc = new WebClient();
            //htmlString = wc.DownloadString("http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/injury/injuries.html");

        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        private void UpdateFont()
        {
            //Change cell font
            foreach (DataGridViewColumn c in teamsDetailedStatsDGV.Columns)
            {
                c.DefaultCellStyle.Font = new System.Drawing.Font("Arial Narrow", 14.2F);
            }
        }

        private void wbhtmlinjuries(object sender, EventArgs e)
        {
            try
            {
                if (radioBtnNBA.Checked != true) return;
                bool stats;
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                {
                    stats = true;
                }
                else
                {
                    stats = false;
                }


                if (stats == true)
                {

                    WebClient wc = new WebClient();
                    htmlString = wc.DownloadString("http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/injury/injuries.html");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cant connect to covers.com", "Warning!", MessageBoxButtons.OK);
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                wbhtmlinjuries(sender, e);
                UpdateFont();

                LastXgamesCB.Text = ConfigurationManager.AppSettings["LastXGames"];

                dateTimePicker1.CustomFormat = "dd.MM.yyyy.";

                DateTime dtCheck = DateTime.Today;

                AStarterHandLabel.Text = "";
                HStarterHandLabel.Text = "";

                TotalBox.Enabled = false;
                HandicapBox.Enabled = false;

                if (dtCheck.Month > 8)
                {
                    checkBox1.Text = (dtCheck.Year - 1) + "/" + dtCheck.Year;
                    checkBox2.Text = dtCheck.Year + "/" + (dtCheck.Year + 1);
                }
                else
                {
                    checkBox1.Text = (dtCheck.Year - 2) + "/" + (dtCheck.Year - 1);
                    checkBox2.Text = (dtCheck.Year - 1) + "/" + dtCheck.Year;
                }
                //DateTime dtCheckStart = new DateTime(2015,6,1);
                //DateTime dtCheckEnd = new DateTime(2015, 10, 1);

                //if(ManualChange == 0)
                //{
                //    if (dtCheck.Month >= dtCheckStart.Month && dtCheck.Month < dtCheckEnd.Month)
                //    {
                //      radioBtnMLB.Checked = true;
                //      pictureBox4.Image = Oklade.Properties.Resources.mlb2;
                //    }
                //    else
                //    {
                //      radioBtnNBA.Checked = true;
                //    pictureBox4.Image = Oklade.Properties.Resources.NBALogo12;
                //    }
                //}

                if (radioBtnNBA.Checked == true)
                {
                    AllNBAMLBResultsVar = "AllNBAResults";
                    NBAMLBBetsVar = "NBABets";
                    NBAMLBResultsVar = "NBAResults";
                    LeagueNBAMLB = "NBA";

                    if (dtCheck.Month < 12)
                    {
                        SeasonNBAMLB = dtCheck.Year - 1;
                    }
                    else
                    {
                        SeasonNBAMLB = dtCheck.Year;
                    }

                    DateTime dtPlayoffsStart = new DateTime(dtCheck.Year, 4, 12);
                    DateTime dtPlayoffsEnd = new DateTime(dtCheck.Year, 6, 12);


                    if (dtCheck.Date >= dtPlayoffsStart && dtCheck.Date < dtPlayoffsEnd)
                    {
                        pictureBox4.Image = Oklade.Properties.Resources.NBAPlayoffs;
                    }

                }
                else if (radioBtnMLB.Checked == true)
                {
                    AllNBAMLBResultsVar = "AllMLBResults";
                    NBAMLBBetsVar = "MLBBets";
                    NBAMLBResultsVar = "MLBResults";
                    LeagueNBAMLB = "MLB";
                    awayStarterCB.Enabled = false;
                    homeStarterCB.Enabled = false;
                    if (dtCheck.Month == 4 && dtCheck.Day <= 16)
                    {
                        SeasonNBAMLB = dtCheck.Year - 1;
                    }
                    else
                    {
                        SeasonNBAMLB = dtCheck.Year;
                    }
                }


                DateTime dt2 = DateTime.Today;
                string Datum2 = dt2.ToString("yyyy-MM-dd");
                dateTimePicker2.Text = Datum2;

                ds = new DataSet();

                if (dtCheck.Month >= 10 && dtCheck.Month <= 12)
                {
                    checkBox1.Checked = true;
                    checkBox2.Checked = true;
                }
                if (dtCheck.Month >= 1 && dtCheck.Month < 10)
                {
                    checkBox1.Checked = false;
                    checkBox2.Checked = true;
                }

                label45.Text = "";
                label46.Text = "";
                L47.Text = "";
                label19.Text = "";
                label21.Text = "";
                label22.Text = "";
                label23.Text = "";
                label24.Text = "";
                label25.Text = "";
                label27.Text = "";
                label28.Text = "";
                label33.Text = "";
                label34.Text = "";
                label35.Text = "";
                label36.Text = "";
                label38.Text = "";
                richTextBox1.Text = "";
                richTextBox3.Text = "";
                label43.Text = "";

                // this.tabControlInjuries.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControlInjuries_DrawItem);

                /////////////////////////////// RESIZE FOR SMALLER RESOLUTION ////////////////////////
                this.Location = new System.Drawing.Point(0, 0);
                int vertsize = Screen.PrimaryScreen.WorkingArea.Height;
                if (vertsize < this.Height)
                {
                    this.Size = Screen.PrimaryScreen.WorkingArea.Size;
                }

                //////////////////////////////////VERSION//////////////////////////////

                if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
                {
                    version = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
                }
                else
                {
                    version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                }

                label13.Text = @"Copyright © 2014 IMS-Studio  v" + version;



                Sql2 = @"select ImeKluba from Klubovi where League = '" + LeagueNBAMLB + "' order by ImeKluba";


                try
                {
                    DataSet myDataSet = new DataSet();

                    adapter2 = new SqlDataAdapter(Sql2, connection);

                    adapter2.Fill(myDataSet, "ImeKluba");

                    hteamradio.Checked = true;
                    System.Data.DataTable myDataTable = myDataSet.Tables[0];

                    DataRow tempRow = null;

                    foreach (DataRow tempRowVariable in myDataTable.Rows)
                    {
                        tempRow = tempRowVariable;
                    }


                    queryyResults = "Select TOP 1 IDOklade from " + NBAMLBResultsVar + " order by IDOklade desc";

                    SqlCommand cmdd = new SqlCommand(queryyResults, connection);
                    SqlDataReader readd = cmdd.ExecuteReader();

                    while (readd.Read())
                    {
                        IDOkl2 = (readd["IDOklade"].ToString());
                    }
                    readd.Close();
                    int IDOkl3 = Convert.ToInt32(IDOkl2);
                    IDoklBox.Text = (IDOkl3 + 1).ToString();

                    ////ComboBox1
                    AwayTeamCB.Items.Clear();

                    DataSet myDataSet3 = new DataSet();

                    adapter2 = new SqlDataAdapter(Sql2, connection);

                    adapter2.Fill(myDataSet3, "ImeKluba");

                    System.Data.DataTable myDataTable3 = myDataSet3.Tables[0];

                    DataRow tempRow3 = null;

                    foreach (DataRow tempRow3_Variable in myDataTable3.Rows)
                    {

                        tempRow3 = tempRow3_Variable;

                        AwayTeamCB.Items.Add((tempRow3["ImeKluba"]));

                    }
                    ////ComboBox2
                    HomeTeamCB.Items.Clear();

                    DataSet myDataSet4 = new DataSet();

                    adapter3 = new SqlDataAdapter(Sql2, connection);


                    adapter3.Fill(myDataSet4, "ImeKluba");


                    System.Data.DataTable myDataTable4 = myDataSet4.Tables[0];


                    DataRow tempRow4 = null;

                    foreach (DataRow tempRow4Variable in myDataTable4.Rows)
                    {

                        tempRow4 = tempRow4Variable;

                        HomeTeamCB.Items.Add((tempRow4["ImeKluba"]));

                    }

                    IDoklBox.SelectionStart = 0;

                    this.ActiveControl = label1;

                    AwayTeamCB.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void Aresult_Enter(object sender, EventArgs e)
        {

        }

        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {

                connection.Open();


                if (value == "")
                {
                    value = value;
                }
                else
                {
                    HomeTeamCB.Text = value;
                }

                String queryy33 = "Select ID from Klubovi where ImeKluba = '" + value + "'";
                SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                SqlDataReader readd33 = cmdd33.ExecuteReader();
                while (readd33.Read())
                {
                    idkluba1 = (readd33["ID"].ToString());
                }
                readd33.Close();
                connection.Close();
            }
        }

        private void Hresult_Enter(object sender, EventArgs e)
        {

        }

        private void Aresult_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                //    Hresult.Focus();   //enter key is down
            }
        }

        private void colorrows(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                isOver = 0;
                isUnder = 0;
                hAway = 0;
                hHome = 0;
                // COLOR ROWS
                HAvg = 0;
                AAvg = 0;

                try
                {

                    foreach (DataGridViewRow row in DGVAnalysisResults.Rows)
                    {
                        if (row.Cells["TotalResult"].Value.ToString() == DBNull.Value.ToString())
                        {
                            DGVAnalysisResults.Rows.Remove(row);
                        }
                        else
                        {
                            cellv = Convert.ToDouble(row.Cells["TotalResult"].Value);

                            if (TotalBox.Text != "")
                            {
                                totall = float.Parse(TotalBox.Text);
                            }
                            else
                            {
                                totall = float.Parse(tot);
                            }

                            double cellv2 = Convert.ToDouble(row.Cells["HandicapResult"].Value);
                            if (HandicapBox.Text != "")
                            {
                                hendii = float.Parse(HandicapBox.Text);
                            }
                            else
                            {
                                hendii = float.Parse(hand);
                            }


                            HAvg = HAvg + Convert.ToDouble(row.Cells["HResult"].Value);
                            AAvg = AAvg + Convert.ToDouble(row.Cells["AResult"].Value);

                            if (cellv < totall) //if check ==0
                            {
                                isUnder++;
                                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                                boldStyle.Font = new System.Drawing.Font("Ariel", 8.25F, System.Drawing.FontStyle.Bold);
                                row.Cells["TotalResult"].Style = boldStyle;
                                //  row.Cells["TotalResult"].Style.BackColor = Color.Green;
                                row.Cells["TotalResult"].Style.ForeColor = Color.Green;

                            }
                            else if (cellv > totall) //if check ==0
                            {
                                isOver++;
                                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                                boldStyle.Font = new System.Drawing.Font("Ariel", 8.25F, System.Drawing.FontStyle.Bold);
                                row.Cells["TotalResult"].Style = boldStyle;
                                row.Cells["TotalResult"].Style.ForeColor = Color.Red;

                            }

                            else if (cellv.Equals(totall)) //if check ==0
                            {

                                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                                boldStyle.Font = new System.Drawing.Font("Ariel", 8.25F, System.Drawing.FontStyle.Bold);
                                row.Cells["TotalResult"].Style = boldStyle;
                                row.Cells["TotalResult"].Style.ForeColor = Color.Orange;

                            }

                            if (cellv2 < hendii) //if check ==0
                            {
                                hHome++;
                                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                                boldStyle.Font = new System.Drawing.Font("Ariel", 8.25F, System.Drawing.FontStyle.Bold);
                                row.Cells["HandicapResult"].Style = boldStyle;
                                row.Cells["HandicapResult"].Style.ForeColor = Color.Green;
                                // row.Cells["HandicapResult"].Style.ForeColor = Color.AntiqueWhite;

                            }
                            else if (cellv2 > hendii) //if check ==0
                            {
                                hAway++;
                                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                                boldStyle.Font = new System.Drawing.Font("Ariel", 8.25F, System.Drawing.FontStyle.Bold);
                                row.Cells["HandicapResult"].Style = boldStyle;
                                row.Cells["HandicapResult"].Style.ForeColor = Color.Red;


                            }
                            else if (cellv2.Equals(hendii)) //if check ==0
                            {
                                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                                boldStyle.Font = new System.Drawing.Font("Ariel", 8.25F, System.Drawing.FontStyle.Bold);
                                row.Cells["HandicapResult"].Style = boldStyle;
                                row.Cells["HandicapResult"].Style.ForeColor = Color.Orange;

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.ToString());
                }
            }
        }
        private void dgv2(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                HAvg = HAvg / (DGVAnalysisResults.Rows.Count - 1);
                AAvg = AAvg / (DGVAnalysisResults.Rows.Count - 1);
                HAvg = Math.Round(HAvg, 2);
                AAvg = Math.Round(AAvg, 2);
                double total11 = HAvg + AAvg;
                label35.Text = AAvg.ToString();
                label36.Text = HAvg.ToString();
                label38.Text = total11.ToString();


                Sqldgv2 = "select IsOver, IsUnder,HomeHendi,AwayHendi,Total,Handicap,Datum from " + NBAMLBBetsVar + " where IDOklade = '" + IDO + "'";

                ds2 = new DataSet();
                adapter4 = new SqlDataAdapter(Sqldgv2, connection);
                ds2.Reset();
                adapter4.Fill(ds2);
                dataGridView2.DataSource = ds2.Tables[0];

                dataGridView2.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView2.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView2.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView2.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Columns[6].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView2.Rows[0].Cells[0].Selected = false;

                for (int intCount2 = 0; intCount2 < ds2.Tables[0].Rows.Count; intCount2++)
                {
                    double is1 = Convert.ToDouble(ds2.Tables[0].Rows[intCount2][0]);
                    double is2 = Convert.ToDouble(ds2.Tables[0].Rows[intCount2][1]);
                    double is3 = is1 - is2;
                    double is4 = Convert.ToDouble(ds2.Tables[0].Rows[intCount2][2]);
                    double is5 = Convert.ToDouble(ds2.Tables[0].Rows[intCount2][3]);
                    double is6 = is4 - is5;

                    DataGridViewCellStyle style2 = new DataGridViewCellStyle();
                    style2.Font = new System.Drawing.Font("Arial Narrow", 13.75F, FontStyle.Bold);

                    if (is3 >= 2 & is3 <= 4)
                    {
                        dataGridView2.Rows[intCount2].Cells["IsOver"].Style.BackColor = Color.PaleGreen;
                        dataGridView2.Rows[intCount2].Cells["IsOver"].Style.ForeColor = Color.Black;

                    }
                    else if (is3 >= 5)
                    {
                        dataGridView2.Rows[intCount2].Cells["IsOver"].Style.BackColor = Color.LawnGreen;
                        dataGridView2.Rows[intCount2].Cells["IsOver"].Style.ForeColor = Color.Black;
                        dataGridView2.Columns["IsOver"].DefaultCellStyle = style2;

                    }
                    else if (is3 <= -2 & is3 >= -4)
                    {
                        dataGridView2.Rows[intCount2].Cells["IsUnder"].Style.BackColor = Color.PaleGreen;
                        dataGridView2.Rows[intCount2].Cells["IsUnder"].Style.ForeColor = Color.Black;
                    }
                    else if (is3 <= -5)
                    {
                        dataGridView2.Rows[intCount2].Cells["IsUnder"].Style.BackColor = Color.LawnGreen;
                        dataGridView2.Rows[intCount2].Cells["IsUnder"].Style.ForeColor = Color.Black;
                        dataGridView2.Columns["IsUnder"].DefaultCellStyle = style2;
                    }

                    if (is6 >= 2 & is6 <= 4)
                    {
                        dataGridView2.Rows[intCount2].Cells["HomeHendi"].Style.BackColor = Color.PaleGreen;
                        dataGridView2.Rows[intCount2].Cells["HomeHendi"].Style.ForeColor = Color.Black;
                    }
                    else if (is6 > 5)
                    {
                        dataGridView2.Rows[intCount2].Cells["HomeHendi"].Style.BackColor = Color.LawnGreen;
                        dataGridView2.Rows[intCount2].Cells["HomeHendi"].Style.ForeColor = Color.Black;
                        dataGridView2.Columns["HomeHendi"].DefaultCellStyle = style2;
                    }
                    else if (is6 <= -2 & is6 >= -4)
                    {
                        dataGridView2.Rows[intCount2].Cells["AwayHendi"].Style.BackColor = Color.PaleGreen;
                        dataGridView2.Rows[intCount2].Cells["AwayHendi"].Style.ForeColor = Color.Black;
                    }
                    else if (is6 <= -5)
                    {
                        dataGridView2.Rows[intCount2].Cells["AwayHendi"].Style.BackColor = Color.LawnGreen;
                        dataGridView2.Rows[intCount2].Cells["AwayHendi"].Style.ForeColor = Color.Black;
                        dataGridView2.Columns["AwayHendi"].DefaultCellStyle = style2;
                    }

                    double Operc = (is1 / (is1 + is2)) * 100;
                    double Uperc = (is2 / (is1 + is2)) * 100;
                    Operc = Math.Round(Operc, 2);
                    Uperc = Math.Round(Uperc, 2);
                    String Operc1 = Operc + "%";
                    String Uperc1 = Uperc + "%";
                    label27.Text = Operc1;
                    label28.Text = Uperc1;
                    double AHperc = (is5 / (is4 + is5)) * 100;
                    double HHperc = (is4 / (is4 + is5)) * 100;
                    AHperc = Math.Round(AHperc, 2);
                    HHperc = Math.Round(HHperc, 2);
                    String AHperc1 = AHperc + "%";
                    String HHperc1 = HHperc + "%";
                    label33.Text = AHperc1;
                    label34.Text = HHperc1;

                    TotalBox.Text = dataGridView2.Rows[0].Cells[4].Value.ToString();
                    HandicapBox.Text = dataGridView2.Rows[0].Cells[5].Value.ToString();
                }
            }
        }

        private void Hresult_KeyDown(object sender, KeyEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (e.KeyCode == Keys.Enter)
                {
                    e.Handled = true;
                    e.SuppressKeyPress = true;
                    IDO = IDoklBox.Text;
                    String query1 = "Select ID,ParkFactor from Klubovi where ImeKluba = '" + value2 + "'";
                    String query2 = "Select ID from Klubovi where ImeKluba = '" + value + "'";
                    SqlCommand cmd = new SqlCommand(query1, connection);
                    SqlDataReader read = cmd.ExecuteReader();
                    try
                    {
                        while (read.Read())
                        {
                            IDH = (read["ID"].ToString());
                            ParkFactor = Convert.ToDouble(read["ParkFactor"]);
                        }
                        read.Close();

                        SqlCommand cmd2 = new SqlCommand(query2, connection);
                        SqlDataReader read2 = cmd2.ExecuteReader();
                        while (read2.Read())
                        {
                            IDA = (read2["ID"].ToString());
                        }
                        read2.Close();

                        if (hteamradio.Checked == true)
                        {
                            HA = "Home";
                            HAteam22 = value2.Split(' ')[1];
                        }
                        else if (ateamradio.Checked == true)
                        {
                            HA = "Away";
                            HAteam22 = value.Split(' ')[1];
                        }

                        //                        int HRes = Convert.ToInt32(Hresult.Text);
                        //                        int ARes = Convert.ToInt32(Aresult.Text);
                        //                        int TotalRes = HRes + ARes;
                        //                        int HendiRes = ARes - HRes;
                        //                        String query3 = @"INSERT INTO NBAResults (IDHKluba, IDAKluba, IDOklade,ImeAKluba,ImeHKluba,HResult,AResult,TotalResult, HandicapResult, Datum, HomeAway)
                        //                                                      VALUES ( '" + IDH + "','" + IDA + "','" + IDO + "','"+value+"','"+value2+"','" + Hresult.Text + "','" + Aresult.Text +
                        //                                                                  "', '" + TotalRes.ToString() + "', '" + HendiRes.ToString() + "', '" +
                        //                                                                  DateTime.Now.ToShortDateString() + "', '" + HA + "');";

                        //SqlCommand cmd111 = new SqlCommand(query3, connection);

                        //cmd111.ExecuteNonQuery();



                        //Aresult.Text = "";
                        //Hresult.Text = "";
                        //Aresult.Focus();
                        colorrows(sender, e);
                        //    linkk(sender, e);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }

        private void finishBtn_Click(object sender, EventArgs e)
        {

        }

        private void linkk(object sender, EventArgs e)
        {
            homecheckBox1.Checked = false;
            awaycheckBox2.Checked = false;

            if (hteamradio.Checked == true)
            {
                HA = "Home";
                HAteam22 = value2.Split(' ')[1];
            }
            else if (ateamradio.Checked == true)
            {
                HA = "Away";
                HAteam22 = value.Split(' ')[1];
            }

            if (hteamradio.Checked == true)
            {
                double tott = Convert.ToDouble(TotalBox.Text);
                double hendii = Convert.ToDouble(HandicapBox.Text);
                lineBox1.Text = (hendii + 3).ToString();
                lineBox2.Text = (hendii - 3).ToString();
                teamBox.Text = HAteam22;
                homecheckBox1.Checked = true;
            }
            else
            {
                double tott = Convert.ToDouble(TotalBox.Text);
                double hendii = Convert.ToDouble(HandicapBox.Text);
                lineBox2.Text = ((hendii * (-1)) - 3).ToString();
                lineBox1.Text = ((hendii * (-1)) + 3).ToString();
                teamBox.Text = HAteam22;
                awaycheckBox2.Checked = true;
            }
        }

        private void getGamesBtn_Click(object sender, EventArgs e)
        {
            PickGamesBtn.Enabled = true;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    { DGVAnalysisResults.Columns.Remove("chk2"); }

                }
                String team = teamBox.Text;
                if (homecheckBox1.Checked == true)
                {
                    HA = "H";

                    DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
                    chk.HeaderText = "Pick";
                    chk.Name = "chk";
                    DGVAnalysisResults.Columns.Add(chk);

                    Sql3 = "select ID,Month,Dayofmonth,Year,Team,Opp,Hscore,Ascore,Line,Total,Season from " + AllNBAMLBResultsVar + " where Team = '" + team +
                        "' and Line < '" + lineBox1.Text.ToString().Replace(",", ".") + "' and Line > '" + lineBox2.Text.ToString().Replace(",", ".") + "' order by ID desc";

                    ds = new DataSet();
                    adapter = new SqlDataAdapter(Sql3, connection);
                    ds.Reset();
                    adapter.Fill(ds);
                    DGVAnalysisResults.DataSource = ds.Tables[0];
                    DGVAnalysisResults.Columns[1].Width = 40;
                    DGVAnalysisResults.Columns[2].Width = 40;
                    DGVAnalysisResults.Columns[3].Width = 40;
                }
                else if (awaycheckBox2.Checked == true)
                {
                    HA = "A";
                    DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
                    chk.HeaderText = "Pick";
                    chk.Name = "chk";
                    DGVAnalysisResults.Columns.Add(chk);

                    double line1 = Convert.ToDouble(lineBox1.Text);
                    line1 = line1 * (-1);
                    double line2 = Convert.ToDouble(lineBox2.Text);
                    line2 = line2 * (-1);
                    Sql3 = "select ID,Month,Dayofmonth,Year,Team,Opp,Hscore,Ascore,Line,Total,Season from " + AllNBAMLBResultsVar + " where Opp = '" + team +
                        "' and Line <= " + line2.ToString().Replace(",", ".") + " and Line >= " + line1.ToString().Replace(",", ".") + " order by ID desc";

                    ds = new DataSet();
                    adapter = new SqlDataAdapter(Sql3, connection);
                    ds.Reset();
                    adapter.Fill(ds);
                    DGVAnalysisResults.DataSource = ds.Tables[0];
                    DGVAnalysisResults.Columns[1].Width = 40;
                    DGVAnalysisResults.Columns[2].Width = 40;
                    DGVAnalysisResults.Columns[3].Width = 40;
                }

                //String str1 = "http://sportsdatabase.com/nba/query?output=default&sdql=season%3D2013+and+team%3D" + team +
                //    "+and+" + HA + "+and+line%3C%3D" + lineBox1.Text.ToString().Replace(",", ".") + "+and+line%3E%3D" + lineBox2.Text.ToString().Replace(",", ".") +
                //    "+and+total%3C%3D" + totalBox1.Text.ToString().Replace(",", ".") + "+and+total%3E%3D" + totalBox2.Text.ToString().Replace(",", ".") + "&submit=++S+D+Q+L+%21++";
                //System.Diagnostics.Process.Start(str1);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            linkk(sender, e);
            getGamesBtn.Enabled = true;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            NewGameBtn_Click(sender, e);
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            if (DGVAnalysisResults.Columns.Contains("chk"))
            {
                DGVAnalysisResults.Columns.Remove("chk");
                if (DGVAnalysisResults.Columns.Contains("chk2"))
                { DGVAnalysisResults.Columns.Remove("chk2"); }

            }

            DGVAnalysisResults.DataSource = null;

            //   String Sql5 = "select ID,ImeHKluba,ImeAKluba,Datum,IDOklade,Total,Handicap,IsOver,IsUnder, HomeHendi,AwayHendi,TotWL, HenWL from NBABets where Datum = '" +
            //       dateTimePicker1.Text + "'";
            String Sql5 = "select ID, IDOklade, Datum,ImeHKluba,ImeAKluba,Total,Handicap,IsOver,IsUnder,HomeHendi,AwayHendi,BetProcessed from " + NBAMLBBetsVar + " where Datum = '"
                + dateTimePicker1.Text + "'";

            try
            {
                ds = new DataSet();
                adapter5 = new SqlDataAdapter(Sql5, connection);

                ds.Reset();
                adapter5.Fill(ds);
                //nevalja
                DGVAnalysisResults.DataSource = ds.Tables[0];

                DGVAnalysisResults.Columns.Remove("ID");

                DGVAnalysisResults.Columns[0].HeaderText = "IDOkl";
                DGVAnalysisResults.Columns[0].Width = 46;
                DGVAnalysisResults.Columns[2].HeaderText = "Home Team";
                DGVAnalysisResults.Columns[1].Width = 70;
                DGVAnalysisResults.Columns[3].HeaderText = "Away Team";
                DGVAnalysisResults.Columns[6].HeaderText = "Over";
                DGVAnalysisResults.Columns[7].HeaderText = "Under";
                DGVAnalysisResults.Columns[8].HeaderText = "HomeH";
                DGVAnalysisResults.Columns[9].HeaderText = "AwayH";
                DGVAnalysisResults.Columns[10].HeaderText = "Processed";
                DGVAnalysisResults.Columns[2].Width = 102;
                DGVAnalysisResults.Columns[3].Width = 102;
                //dataGridView1.Columns[4].Width = 46;
                //dataGridView1.Columns[5].Width = 46;
                DGVAnalysisResults.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                DGVAnalysisResults.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                DGVAnalysisResults.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
                DGVAnalysisResults.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
                DGVAnalysisResults.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
                DGVAnalysisResults.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
                DGVAnalysisResults.Columns[6].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[7].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[8].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[9].DefaultCellStyle.BackColor = Color.WhiteSmoke;


                label45.Text = "";
                label46.Text = "";

                label19.Text = "";
                label21.Text = "";
                label22.Text = "";
                label23.Text = "";
                label24.Text = "";
                label25.Text = "";
                label27.Text = "";
                label28.Text = "";
                label33.Text = "";
                label34.Text = "";
                label35.Text = "";
                label36.Text = "";
                label38.Text = "";
                label43.Text = "";

                for (int intCount2 = 0; intCount2 < ds.Tables[0].Rows.Count; intCount2++)
                {
                    string imehkluba = DGVAnalysisResults.Rows[intCount2].Cells["ImeHKluba"].Value.ToString();
                    string imeakluba = DGVAnalysisResults.Rows[intCount2].Cells["ImeAKluba"].Value.ToString();
                    imehkluba = imehkluba.Split(' ')[1];
                    imeakluba = imeakluba.Split(' ')[1];

                    DGVAnalysisResults.Rows[intCount2].Cells["ImeHKluba"].Value = imehkluba;
                    DGVAnalysisResults.Rows[intCount2].Cells["ImeAKluba"].Value = imeakluba;

                    double is1 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][7]);
                    double is2 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][8]);
                    double is3 = is1 - is2;
                    double is4 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][9]);
                    double is5 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][10]);
                    double is6 = is4 - is5;

                    if (is3 >= 2 & is3 <= 4)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsOver"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is3 >= 5)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsOver"].Style.BackColor = Color.LawnGreen;
                    }
                    else if (is3 <= -2 & is3 >= -4)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsUnder"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is3 <= -5)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsUnder"].Style.BackColor = Color.LawnGreen;
                    }

                    if (is6 >= 2 & is6 <= 4)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["HomeHendi"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is6 >= 5)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["HomeHendi"].Style.BackColor = Color.LawnGreen;
                    }
                    else if (is6 <= -2 & is6 >= -4)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["AwayHendi"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is6 <= -5)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["AwayHendi"].Style.BackColor = Color.LawnGreen;
                    }

                }

                DGVAnalysisResults.CurrentCell.Selected = false;
                dataGridView2.DataSource = null;
                //dataGridView1.Columns["chk"].DefaultCellStyle.BackColor = Color.DarkGray;
                //dataGridView1.Columns["chk2"].DefaultCellStyle.BackColor = Color.DarkGray;
                //value = "";
                //   value2 = "";
                TotalBox.Text = "";
                HandicapBox.Text = "";
                teamBox.Text = "";
                lineBox1.Text = "";
                lineBox2.Text = "";

            }
            catch (Exception ex)
            {
                //    MessageBox.Show("No Data for this date!","Important Message");
            }

            // colorrows(sender, e);
            //}
            connection.Close();
        }


        private void GetTeam_Click(object sender, EventArgs e)
        {
            if (DGVAnalysisResults.Columns.Contains("chk"))
            {
                DGVAnalysisResults.Columns.Remove("chk");
                if (DGVAnalysisResults.Columns.Contains("chk2"))
                { DGVAnalysisResults.Columns.Remove("chk2"); }

            }
            DGVAnalysisResults.DataSource = "";
            dataGridView2.DataSource = "";
            ATeamL10DGV.DataSource = "";
            HTeamL10DGV.DataSource = "";
            dataGridView5.DataSource = "";
            AwayTeamCB.Text = "";
            // comboBox2.Text = "";
            //  ImeHKluba = "";
            ImeAKluba = "";
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            value = "";
            value2 = "";
            TotalBox.Text = "";
            HandicapBox.Text = "";
            teamBox.Text = "";
            lineBox1.Text = "";
            lineBox2.Text = "";
            pictureBox2.Hide();
            pictureBox3.Hide();
            label45.Text = "";
            label46.Text = "";
            label19.Text = "";
            label21.Text = "";
            label22.Text = "";
            label23.Text = "";
            label24.Text = "";
            label25.Text = "";
            label27.Text = "";
            label28.Text = "";
            label33.Text = "";
            label34.Text = "";
            label35.Text = "";
            label36.Text = "";
            label38.Text = "";
            label12.Text = "";
            label42.Text = "";
            label43.Text = "";
            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            if (connection != null && connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            String Sql5 = "select ID,IDOklade,Datum,ImeHKluba,ImeAKluba,Total,Handicap,IsOver,IsUnder, HomeHendi,AwayHendi,TotWL, HenWL from " + NBAMLBBetsVar + " where IDHKluba = '" + idkluba1 +
                "' or IDAKluba = '" + idkluba1 + "' order by IDOklade desc";
            try
            {
                ds = new DataSet();
                SqlCommand cmd223 = new SqlCommand(Sql5, connection);
                adapter5 = new SqlDataAdapter(cmd223);
                //  adapter = new SqlDataAdapter(Sql5, connection);

                ds.Reset();
                adapter5.Fill(ds);

                DGVAnalysisResults.DataSource = ds.Tables[0];

                DGVAnalysisResults.Columns.Remove("ID");

                DataGridViewCheckBoxColumn chk = new DataGridViewCheckBoxColumn();
                chk.HeaderText = "Tot Win";
                chk.Name = "chk";
                DGVAnalysisResults.Columns.Add(chk);
                DGVAnalysisResults.Columns.Remove("TotWL");

                DataGridViewCheckBoxColumn chk2 = new DataGridViewCheckBoxColumn();
                chk2.HeaderText = "H Win";
                chk2.Name = "chk2";
                DGVAnalysisResults.Columns.Add(chk2);
                DGVAnalysisResults.Columns.Remove("HenWL");

                for (int intCount = 0; intCount < ds.Tables[0].Rows.Count; intCount++)
                {
                    if (ds.Tables[0].Rows[intCount][11].ToString() == "1")
                    {
                        DGVAnalysisResults.Rows[intCount].Cells["chk"].Value = true;

                    }
                    else if (ds.Tables[0].Rows[intCount][11].ToString() == "0")
                    {
                        DGVAnalysisResults.Rows[intCount].Cells["chk"].Value = false;
                    }
                }
                for (int intCount = 0; intCount < ds.Tables[0].Rows.Count; intCount++)
                {
                    if (ds.Tables[0].Rows[intCount][12].ToString() == "1")
                    {
                        DGVAnalysisResults.Rows[intCount].Cells["chk2"].Value = true;
                    }
                    else if (ds.Tables[0].Rows[intCount][12].ToString() == "0")
                    {
                        DGVAnalysisResults.Rows[intCount].Cells["chk2"].Value = false;
                    }

                }
                DGVAnalysisResults.Columns[0].HeaderText = "ID Okl";
                DGVAnalysisResults.Columns[0].Width = 42;
                DGVAnalysisResults.Columns[1].Width = 72;
                DGVAnalysisResults.Columns[2].Width = 130;
                DGVAnalysisResults.Columns[2].HeaderText = "Home Team";
                DGVAnalysisResults.Columns[3].Width = 130;
                DGVAnalysisResults.Columns[3].HeaderText = "Away Team";
                DGVAnalysisResults.Columns[5].HeaderText = "Hnd";
                DGVAnalysisResults.Columns[6].HeaderText = "O";
                DGVAnalysisResults.Columns[7].HeaderText = "U";
                DGVAnalysisResults.Columns[8].HeaderText = "HH";
                DGVAnalysisResults.Columns[9].HeaderText = "AH";
                DGVAnalysisResults.Columns[4].Width = 52;
                DGVAnalysisResults.Columns[5].Width = 46;
                DGVAnalysisResults.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                DGVAnalysisResults.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                DGVAnalysisResults.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
                DGVAnalysisResults.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
                DGVAnalysisResults.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
                DGVAnalysisResults.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
                DGVAnalysisResults.Columns[6].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[7].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[8].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns[9].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                DGVAnalysisResults.Columns["chk"].DefaultCellStyle.BackColor = Color.DarkGray;
                DGVAnalysisResults.Columns["chk2"].DefaultCellStyle.BackColor = Color.DarkGray;

                DGVAnalysisResults.CurrentCell.Selected = false;

                dataGridView2.DataSource = null;


                for (int intCount2 = 0; intCount2 < ds.Tables[0].Rows.Count; intCount2++)
                {
                    double is1 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][7]);
                    double is2 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][8]);
                    double is3 = is1 - is2;
                    double is4 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][9]);
                    double is5 = Convert.ToDouble(ds.Tables[0].Rows[intCount2][10]);
                    double is6 = is4 - is5;

                    if (is3 > 2)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsOver"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is3 < -2)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["IsUnder"].Style.BackColor = Color.PaleGreen;
                    }

                    if (is6 > 2)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["HomeHendi"].Style.BackColor = Color.PaleGreen;
                    }
                    else if (is6 < -2)
                    {
                        DGVAnalysisResults.Rows[intCount2].Cells["AwayHendi"].Style.BackColor = Color.PaleGreen;
                    }

                }

                //      value = "";
                //     value2 = "";
                TotalBox.Text = "";
                HandicapBox.Text = "";
                teamBox.Text = "";
                lineBox1.Text = "";
                lineBox2.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            connection.Close();

        }

        private void button10_Click(object sender, EventArgs e)
        {

            connection.Open();
            if (DGVAnalysisResults.DataSource != null)
            {
                SqlCommandBuilder cmd224 = new SqlCommandBuilder(adapter5);

                DataGridViewRow row = new DataGridViewRow();
                for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
                {
                    row = DGVAnalysisResults.Rows[i];
                    if (Convert.ToBoolean(row.Cells["chk"].Value) == true)
                    {
                        ds.Tables[0].Rows[i][11] = "1";

                    }
                    else if (Convert.ToBoolean(row.Cells["chk"].Value) == false)
                    {
                        ds.Tables[0].Rows[i][11] = "0";

                    }
                }
                for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
                {
                    row = DGVAnalysisResults.Rows[i];
                    if (Convert.ToBoolean(row.Cells["chk2"].Value) == true)
                    {
                        ds.Tables[0].Rows[i][12] = "1";

                    }
                    else if (Convert.ToBoolean(row.Cells["chk2"].Value) == false)
                    {
                        ds.Tables[0].Rows[i][12] = "0";

                    }
                }

                try
                {
                    adapter5.Update(ds);
                    MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

            connection.Close();
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            getGamesBtn_Click(sender, e);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Today.AddDays(-1);
            String datum1 = dt.ToString("yyyyMMdd");
            String str1 = "http://sportsdatabase.com/nba/query?output=default&sdql=season+%3D+2013+and+H+and+date%3D" + datum1 + "&submit=++S+D+Q+L+%21++";
            System.Diagnostics.Process.Start(str1);
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        public void RemoveDuplicate(object sender, EventArgs e)
        {
            duplikati = 0;
            duplikatiindex = -1;
            DataGridViewRow row = new DataGridViewRow();
            for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
            {
                for (int j = i; j < DGVAnalysisResults.Rows.Count - 1; j++)
                {
                    if (j != i)
                    {
                        if (DGVAnalysisResults.Rows[i].Cells[1].Value.Equals(DGVAnalysisResults.Rows[j].Cells[1].Value))
                        {
                            if (DGVAnalysisResults.Rows[i].Cells[2].Value.Equals(DGVAnalysisResults.Rows[j].Cells[2].Value))
                            {
                                if (DGVAnalysisResults.Rows[i].Cells[3].Value.Equals(DGVAnalysisResults.Rows[j].Cells[3].Value))
                                {
                                    if (DGVAnalysisResults.Rows[i].Cells[4].Value.Equals(DGVAnalysisResults.Rows[j].Cells[4].Value))
                                    {
                                        duplikati++;
                                        duplikatiindex = i;
                                    }
                                }
                            }
                        }
                    }

                }

            }
            if (duplikati > 0)
            {
                DGVAnalysisResults.Rows.Remove(DGVAnalysisResults.Rows[duplikatiindex]);
                DGVAnalysisResults.Refresh();
            }

        }

        public void button12_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void comboBox2_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void calculateAveragesSUOUAwayTeam()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            String queryy66 = "SELECT AVG(AScore) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "'";

            SqlCommand cmdd66 = new SqlCommand(queryy66, connection);
            SqlDataReader readd66 = cmdd66.ExecuteReader();

            while (readd66.Read())
            {
                avg1 = (Convert.ToDouble(readd66[0]));
            }
            readd66.Close();
            avg1 = Math.Round(avg1, 1);

            //////////MLB AVG VS HAND

            if (radioBtnMLB.Checked == true)
            {
                String queryyvsHand2 = @"SELECT AVG(AScore) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB +
                    "' and StarterHHand = '" + HomeStarterHand + "' ";

                SqlCommand cmddvsHand2 = new SqlCommand(queryyvsHand2, connection);
                SqlDataReader readdvsHand2 = cmddvsHand2.ExecuteReader();

                while (readdvsHand2.Read())
                {
                    if (!(readdvsHand2[0] is DBNull))
                    {
                        avg1vsHand = (Convert.ToDouble(readdvsHand2[0]));
                    }
                }
                readdvsHand2.Close();
                avg1vsHand = Math.Round(avg1vsHand, 2);
            }

            String queryy700 = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'O' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "'";
            String queryy701 = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'U' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "'";
            SqlCommand cmdd700 = new SqlCommand(queryy700, connection);
            SqlDataReader readd700 = cmdd700.ExecuteReader();

            while (readd700.Read())
            {
                CountO1 = readd700[0].ToString();
            }
            readd700.Close();

            SqlCommand cmdd701 = new SqlCommand(queryy701, connection);
            SqlDataReader readd701 = cmdd701.ExecuteReader();

            while (readd701.Read())
            {
                countU1 = readd701[0].ToString();
            }
            readd701.Close();
            ///////////////////////////////////////////////////////////////////////////////
            if (radioBtnMLB.Checked == true)
            {
                String queryy700vsH = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'O' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                String queryy701vsH = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'U' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                SqlCommand cmdd700vsH = new SqlCommand(queryy700vsH, connection);
                SqlDataReader readd700vsH = cmdd700vsH.ExecuteReader();

                while (readd700vsH.Read())
                {
                    if (!(readd700vsH[0] is DBNull))
                    {
                        CountO1vsHand = readd700vsH[0].ToString();
                    }
                }
                readd700vsH.Close();

                SqlCommand cmdd701vsH = new SqlCommand(queryy701vsH, connection);
                SqlDataReader readd701vsH = cmdd701vsH.ExecuteReader();

                while (readd701vsH.Read())
                {
                    if (!(readd701vsH[0] is DBNull))
                    {
                        countU1vsHand = readd701vsH[0].ToString();
                    }
                }
                readd701vsH.Close();
            }
            ///////////////////////////////////////////////////////////////////////////////
            if (radioBtnNBA.Checked == true)
            {
                String queryy702 = "SELECT COUNT(ATSr) FROM " + AllNBAMLBResultsVar + " where ATSr = 'L' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "'";
                String queryy703 = "SELECT COUNT(ATSr) FROM " + AllNBAMLBResultsVar + " where ATSr = 'W' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "'";

                SqlCommand cmdd702 = new SqlCommand(queryy702, connection);
                SqlDataReader readd702 = cmdd702.ExecuteReader();

                while (readd702.Read())
                {
                    countATS1 = readd702[0].ToString();
                }
                readd702.Close();

                SqlCommand cmdd703 = new SqlCommand(queryy703, connection);
                SqlDataReader readd703 = cmdd703.ExecuteReader();

                while (readd703.Read())
                {
                    countATS2 = readd703[0].ToString();
                }
                readd703.Close();

                // label22.Text = countATS1 + " - " + countATS2;
            }
            else if (radioBtnMLB.Checked == true)
            {
                String queryySU1 = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "'";
                String queryySU2 = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "'";

                SqlCommand cmdd702 = new SqlCommand(queryySU1, connection);
                SqlDataReader readd702 = cmdd702.ExecuteReader();

                while (readd702.Read())
                {
                    countSU1 = Convert.ToInt16(readd702[0]);
                }
                readd702.Close();

                SqlCommand cmdd703 = new SqlCommand(queryySU2, connection);
                SqlDataReader readd703 = cmdd703.ExecuteReader();

                while (readd703.Read())
                {
                    countSU2 = Convert.ToInt16(readd703[0]);
                }
                readd703.Close();
            }
            /////////////SUr vs HAND
            if (radioBtnMLB.Checked == true)
            {
                String queryySU1vHand = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                String queryySU2vHand = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";

                SqlCommand cmdd702vsH = new SqlCommand(queryySU1vHand, connection);
                SqlDataReader readd702vsH = cmdd702vsH.ExecuteReader();

                while (readd702vsH.Read())
                {
                    if (!(readd702vsH[0] is DBNull))
                    {
                        countSU1vsHand = Convert.ToInt16(readd702vsH[0]);
                    }
                }
                readd702vsH.Close();

                SqlCommand cmdd703vsH = new SqlCommand(queryySU2vHand, connection);
                SqlDataReader readd703vsH = cmdd703vsH.ExecuteReader();

                while (readd703vsH.Read())
                {
                    if (!(readd703vsH[0] is DBNull))
                    {
                        countSU2vsHand = Convert.ToInt16(readd703vsH[0]);
                    }
                }
                readd703vsH.Close();

            }
            //////////////////////////////////////////////////////////////////////////////

            String queryy667 = "SELECT AVG(HScore) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "'";

            SqlCommand cmdd667 = new SqlCommand(queryy667, connection);
            SqlDataReader readd667 = cmdd667.ExecuteReader();

            while (readd667.Read())
            {
                avg2 = (Convert.ToDouble(readd667[0]));
            }
            readd667.Close();
            avg2 = Math.Round(avg2, 1);

            /////////////  AVERAGE VS HAND L/R - MLB

            if (radioBtnMLB.Checked == true)
            {
                String queryyvsHand = @"SELECT AVG(HScore) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB +
                    "' and StarterHHand = '" + HomeStarterHand + "' ";

                SqlCommand cmddvsHand = new SqlCommand(queryyvsHand, connection);
                SqlDataReader readdvsHand = cmddvsHand.ExecuteReader();

                while (readdvsHand.Read())
                {
                    if (!(readdvsHand[0] is DBNull))
                    {
                        avg2vsHand = (Convert.ToDouble(readdvsHand[0]));
                    }
                }
                readdvsHand.Close();
                avg2vsHand = Math.Round(avg2vsHand, 2);
            }
        }

        private void calculateAveragesSUOUHomeTeam()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            String queryy66 = "SELECT AVG(HScore) FROM " + AllNBAMLBResultsVar + " Where Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "'";

            SqlCommand cmdd66 = new SqlCommand(queryy66, connection);
            SqlDataReader readd66 = cmdd66.ExecuteReader();

            while (readd66.Read())
            {
                avg3 = (Convert.ToDouble(readd66[0]));
            }
            readd66.Close();
            avg3 = Math.Round(avg3, 1);

            ////////////// MLB AVG VS HAND

            if (radioBtnMLB.Checked == true)
            {
                String queryy66vsH = @"SELECT AVG(HScore) FROM " + AllNBAMLBResultsVar + " Where Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB +
                    "' and StarterAHand = '" + AwayStarterHand + "'";

                SqlCommand cmdd66vsH = new SqlCommand(queryy66vsH, connection);
                SqlDataReader readd66vsH = cmdd66vsH.ExecuteReader();

                while (readd66vsH.Read())
                {
                    if (!(readd66vsH[0] is DBNull))
                    {
                        avg3vsHand = (Convert.ToDouble(readd66vsH[0]));
                    }
                }
                readd66vsH.Close();
                avg3vsHand = Math.Round(avg3vsHand, 2);
            }
            ////////////////////////////////////

            String queryy667 = "SELECT AVG(AScore) FROM " + AllNBAMLBResultsVar + " Where Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "'";

            SqlCommand cmdd667 = new SqlCommand(queryy667, connection);
            SqlDataReader readd667 = cmdd667.ExecuteReader();

            while (readd667.Read())
            {
                avg4 = (Convert.ToDouble(readd667[0]));
            }
            readd667.Close();
            avg4 = Math.Round(avg4, 1);

            ////////////// MLB AVG VS HAND

            if (radioBtnMLB.Checked == true)
            {

                String queryy667vsHand = @"SELECT AVG(AScore) FROM " + AllNBAMLBResultsVar + " Where Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "'";

                SqlCommand cmdd667vsHand = new SqlCommand(queryy667vsHand, connection);
                SqlDataReader readd667vsHand = cmdd667vsHand.ExecuteReader();

                while (readd667vsHand.Read())
                {
                    if (!(readd667vsHand[0] is DBNull))
                    {
                        avg4vsHand = (Convert.ToDouble(readd667vsHand[0]));
                    }
                }
                readd667vsHand.Close();
                avg4vsHand = Math.Round(avg4vsHand, 2);
            }


            String queryy700 = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'O' and Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "'";
            String queryy701 = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'U' and Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "'";
            SqlCommand cmdd700 = new SqlCommand(queryy700, connection);
            SqlDataReader readd700 = cmdd700.ExecuteReader();

            while (readd700.Read())
            {
                CountO2 = readd700[0].ToString();
            }
            readd700.Close();

            SqlCommand cmdd701 = new SqlCommand(queryy701, connection);
            SqlDataReader readd701 = cmdd701.ExecuteReader();

            while (readd701.Read())
            {
                countU2 = readd701[0].ToString();
            }
            readd701.Close();
            ///////////////////////////////////////////////////////////////////////////////
            if (radioBtnMLB.Checked == true)
            {
                String queryy700vsH = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'O' and Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "'";
                String queryy701vsH = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'U' and Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "'";
                SqlCommand cmdd700vsH = new SqlCommand(queryy700vsH, connection);
                SqlDataReader readd700vsH = cmdd700vsH.ExecuteReader();

                while (readd700vsH.Read())
                {
                    if (!(readd700vsH[0] is DBNull))
                    {
                        CountO2vsHand = readd700vsH[0].ToString();
                    }
                }
                readd700vsH.Close();

                SqlCommand cmdd701vsH = new SqlCommand(queryy701vsH, connection);
                SqlDataReader readd701vsH = cmdd701vsH.ExecuteReader();

                while (readd701vsH.Read())
                {
                    if (!(readd701vsH[0] is DBNull))
                    {
                        countU2vsHand = readd701vsH[0].ToString();
                    }
                }
                readd701vsH.Close();
            }
            ///////////////////////////////////////////////////////////////////////////////
            if (radioBtnNBA.Checked == true)
            {
                String queryy702 = "SELECT COUNT(ATSr) FROM " + AllNBAMLBResultsVar + " where ATSr = 'W' and Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "'";
                String queryy703 = "SELECT COUNT(ATSr) FROM " + AllNBAMLBResultsVar + " where ATSr = 'L' and Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "'";

                SqlCommand cmdd702 = new SqlCommand(queryy702, connection);
                SqlDataReader readd702 = cmdd702.ExecuteReader();

                while (readd702.Read())
                {
                    countATS3 = readd702[0].ToString();
                }
                readd702.Close();

                SqlCommand cmdd703 = new SqlCommand(queryy703, connection);
                SqlDataReader readd703 = cmdd703.ExecuteReader();

                while (readd703.Read())
                {
                    countATS4 = readd703[0].ToString();
                }
                readd703.Close();

            }
            else if (radioBtnMLB.Checked == true)
            {
                String queryy702 = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "'";
                String queryy703 = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "'";

                SqlCommand cmdd702 = new SqlCommand(queryy702, connection);
                SqlDataReader readd702 = cmdd702.ExecuteReader();

                while (readd702.Read())
                {
                    countSU3 = Convert.ToInt16(readd702[0]);
                }
                readd702.Close();

                SqlCommand cmdd703 = new SqlCommand(queryy703, connection);
                SqlDataReader readd703 = cmdd703.ExecuteReader();

                while (readd703.Read())
                {
                    countSU4 = Convert.ToInt16(readd703[0]);
                }
                readd703.Close();
            }
            //////////SUr VS HAND
            if (radioBtnMLB.Checked == true)
            {
                String queryy702vsH = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "'";
                String queryy703vsH = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Team = '" + kratkoime6 + "' and season = '" + SeasonNBAMLB + "' and StarterAHand = '" + AwayStarterHand + "'";

                SqlCommand cmdd702vsH = new SqlCommand(queryy702vsH, connection);
                SqlDataReader readd702vsH = cmdd702vsH.ExecuteReader();

                while (readd702vsH.Read())
                {
                    if (!(readd702vsH[0] is DBNull))
                    {
                        countSU3vsHand = Convert.ToInt16(readd702vsH[0]);
                    }
                }
                readd702vsH.Close();

                SqlCommand cmdd703vsH = new SqlCommand(queryy703vsH, connection);
                SqlDataReader readd703vsH = cmdd703vsH.ExecuteReader();

                while (readd703vsH.Read())
                {
                    if (!(readd703vsH[0] is DBNull))
                    {
                        countSU4vsHand = Convert.ToInt16(readd703vsH[0]);
                    }
                }
                readd703vsH.Close();
            }

        }

        private void showAveragesinDGV()
        {
            if (TeamsStatsDGV.DataSource != null)
            {
                TeamsStatsDGV.DataSource = null;
            }
            else
            {
                TeamsStatsDGV.Rows.Clear();
            }

            //  dataGridView9.Refresh();

            TeamsStatsDGV.ColumnCount = 3;


            coversrow = new string[] { avg1.ToString(), "PPG", avg3.ToString() };
            TeamsStatsDGV.Rows.Add(coversrow);
            coversrow = new string[] { avg2.ToString(), "OPPG", avg4.ToString() };
            TeamsStatsDGV.Rows.Add(coversrow);
            if (radioBtnNBA.Checked == true)
            {
                coversrow = new string[] { countATS1 + " - " + countATS2, "ATS", countATS3 + " - " + countATS4 };
                TeamsStatsDGV.Rows.Add(coversrow);
            }
            else if (radioBtnMLB.Checked == true)
            {
                coversrow = new string[] { countSU1 + " - " + countSU2, "SU", countSU3 + " - " + countSU4 };
                TeamsStatsDGV.Rows.Add(coversrow);
            }
            coversrow = new string[] { CountO1 + " - " + countU1, "O/U", CountO2 + " - " + countU2 };
            TeamsStatsDGV.Rows.Add(coversrow);

            for (i = 0; i < 4; i++)
            {
                TeamsStatsDGV.Rows[i].Cells[1].Style.ForeColor = Color.White;
            }

            TeamsStatsDGV.Rows[0].Cells[0].Selected = false;


            /////////////////////////////////////////////////////////

            if (radioBtnMLB.Checked == true)
            {
                if (teamsDetailedStatsDGV.DataSource != null)
                {
                    teamsDetailedStatsDGV.DataSource = null;
                }
                else
                {
                    teamsDetailedStatsDGV.Rows.Clear();
                }

                //  dataGridView10.Refresh();

                teamsDetailedStatsDGV.ColumnCount = 3;


                coversrow = new string[] { avg1vsHand.ToString(), "PPG", avg3vsHand.ToString() };
                teamsDetailedStatsDGV.Rows.Add(coversrow);
                coversrow = new string[] { avg2vsHand.ToString(), "OPPG", avg4vsHand.ToString() };
                teamsDetailedStatsDGV.Rows.Add(coversrow);
                if (radioBtnMLB.Checked == true)
                {
                    coversrow = new string[] { countSU1vsHand + " - " + countSU2vsHand, "SU", countSU3vsHand + " - " + countSU4vsHand };
                    teamsDetailedStatsDGV.Rows.Add(coversrow);
                }
                coversrow = new string[] { CountO1vsHand + " - " + countU1vsHand, "O/U", CountO2vsHand + " - " + countU2vsHand };
                teamsDetailedStatsDGV.Rows.Add(coversrow);

                for (i = 0; i < 4; i++)
                {
                    teamsDetailedStatsDGV.Rows[i].Cells[1].Style.ForeColor = Color.White;
                }

                teamsDetailedStatsDGV.Rows[0].Cells[0].Selected = false;
                label45.Text = AwayTeamCB.Text.Split(' ')[1].Replace("Diamondbacks", "D'backs") + " vs. " + HomeStarterHand;
                label46.Text = HomeTeamCB.Text.Split(' ')[1].Replace("Diamondbacks", "D'backs") + " vs. " + AwayStarterHand;
            }
        }

        private void showAVGvsDIV()
        {
            if (radioBtnMLB.Checked == true)
            {
                String queryyvsdiv = @"SELECT AVG(AScore) FROM " + AllNBAMLBResultsVar + " as AMR, Klubovi as KL where AMR.Opp = '" + kratkoime5 + "' and ARM.season = '" + SeasonNBAMLB +
                    "' and AMR.Team = KL.KratkoIme and KL.Division = (Select Division from Klubovi where KratkoIme = '" + kratkoime5 + "')";

                SqlCommand cmddvsDIV2 = new SqlCommand(queryyvsdiv, connection);
                SqlDataReader readdvsDIV2 = cmddvsDIV2.ExecuteReader();

                while (readdvsDIV2.Read())
                {
                    avg1vsDIV = (Convert.ToDouble(readdvsDIV2[0]));
                }
                readdvsDIV2.Close();
                avg1vsDIV = Math.Round(avg1vsDIV, 2);
            }
            ///////////////////////////////////////////////////////////////////////////////
            if (radioBtnMLB.Checked == true)
            {
                String queryy700vsH = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'O' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                String queryy701vsH = "SELECT COUNT(OUr) FROM " + AllNBAMLBResultsVar + " where OUr = 'U' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                SqlCommand cmdd700vsH = new SqlCommand(queryy700vsH, connection);
                SqlDataReader readd700vsH = cmdd700vsH.ExecuteReader();

                while (readd700vsH.Read())
                {
                    CountO1vsDIV = readd700vsH[0].ToString();
                }
                readd700vsH.Close();

                SqlCommand cmdd701vsH = new SqlCommand(queryy701vsH, connection);
                SqlDataReader readd701vsH = cmdd701vsH.ExecuteReader();

                while (readd701vsH.Read())
                {
                    countU1vsDIV = readd701vsH[0].ToString();
                }
                readd701vsH.Close();
            }
            ///////////////////////////////////////////////////////////////////////////////

            else if (radioBtnMLB.Checked == true)
            {
                String queryySU1 = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "'";
                String queryySU2 = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "'";

                SqlCommand cmdd702 = new SqlCommand(queryySU1, connection);
                SqlDataReader readd702 = cmdd702.ExecuteReader();

                while (readd702.Read())
                {
                    countSU1svDIV = Convert.ToInt16(readd702[0]);
                }
                readd702.Close();

                SqlCommand cmdd703 = new SqlCommand(queryySU2, connection);
                SqlDataReader readd703 = cmdd703.ExecuteReader();

                while (readd703.Read())
                {
                    countSU2vsDIV = Convert.ToInt16(readd703[0]);
                }
                readd703.Close();
            }
            /////////////SUr vs HAND
            if (radioBtnMLB.Checked == true)
            {
                String queryySU1vHand = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'L' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";
                String queryySU2vHand = "SELECT COUNT(SUr) FROM " + AllNBAMLBResultsVar + " where SUr = 'W' and Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB + "' and StarterHHand = '" + HomeStarterHand + "'";

                SqlCommand cmdd702vsH = new SqlCommand(queryySU1vHand, connection);
                SqlDataReader readd702vsH = cmdd702vsH.ExecuteReader();

                while (readd702vsH.Read())
                {
                    countSU1vsDIV = Convert.ToInt16(readd702vsH[0]);
                }
                readd702vsH.Close();

                SqlCommand cmdd703vsH = new SqlCommand(queryySU2vHand, connection);
                SqlDataReader readd703vsH = cmdd703vsH.ExecuteReader();

                while (readd703vsH.Read())
                {
                    countSU2vsDIV = Convert.ToInt16(readd703vsH[0]);
                }
                readd703vsH.Close();

            }

            /////////////  AVERAGE VS HAND L/R - MLB

            if (radioBtnMLB.Checked == true)
            {
                String queryyvsDIV = @"SELECT AVG(HScore) FROM " + AllNBAMLBResultsVar + " Where Opp = '" + kratkoime5 + "' and season = '" + SeasonNBAMLB +
                    "' and StarterHHand = '" + HomeStarterHand + "' ";

                SqlCommand cmddvsDIV = new SqlCommand(queryyvsDIV, connection);
                SqlDataReader readdvsDIV = cmddvsDIV.ExecuteReader();

                while (readdvsDIV.Read())
                {
                    avg2vsDIV = (Convert.ToDouble(readdvsDIV[0]));
                }
                readdvsDIV.Close();
                avg2vsDIV = Math.Round(avg2vsDIV, 2);
            }

            if (vsDIVDGV.DataSource != null)
            {
                vsDIVDGV.DataSource = null;
            }
            else
            {
                vsDIVDGV.Rows.Clear();
            }

            //  vsDIVDGV.Refresh();

            vsDIVDGV.ColumnCount = 3;


            coversrow = new string[] { avg1.ToString(), "PPG", avg3.ToString() };
            vsDIVDGV.Rows.Add(coversrow);
            coversrow = new string[] { avg2.ToString(), "OPPG", avg4.ToString() };
            vsDIVDGV.Rows.Add(coversrow);
            if (radioBtnNBA.Checked == true)
            {
                coversrow = new string[] { countATS1 + " - " + countATS2, "ATS", countATS3 + " - " + countATS4 };
                vsDIVDGV.Rows.Add(coversrow);
            }
            else if (radioBtnMLB.Checked == true)
            {
                coversrow = new string[] { countSU1 + " - " + countSU2, "SU", countSU3 + " - " + countSU4 };
                vsDIVDGV.Rows.Add(coversrow);
            }
            coversrow = new string[] { CountO1 + " - " + countU1, "O/U", CountO2 + " - " + countU2 };
            vsDIVDGV.Rows.Add(coversrow);

            for (i = 0; i < 4; i++)
            {
                vsDIVDGV.Rows[i].Cells[1].Style.ForeColor = Color.White;
            }

            vsDIVDGV.Rows[0].Cells[0].Selected = false;


            /////////////////////////////////////////////////////////

            if (radioBtnMLB.Checked == true)
            {
                if (vsnoDIVDGV.DataSource != null)
                {
                    vsnoDIVDGV.DataSource = null;
                }
                else
                {
                    vsnoDIVDGV.Rows.Clear();
                }

                //  vsnoDIVDGV.Refresh();

                vsnoDIVDGV.ColumnCount = 3;


                coversrow = new string[] { avg1vsDIV.ToString(), "PPG", avg3vsDIV.ToString() };
                vsnoDIVDGV.Rows.Add(coversrow);
                coversrow = new string[] { avg2vsDIV.ToString(), "OPPG", avg4vsDIV.ToString() };
                vsnoDIVDGV.Rows.Add(coversrow);
                if (radioBtnMLB.Checked == true)
                {
                    coversrow = new string[] { countSU1vsDIV + " - " + countSU2vsDIV, "SU", countSU3vsDIV + " - " + countSU4vsDIV };
                    vsnoDIVDGV.Rows.Add(coversrow);
                }
                coversrow = new string[] { CountO1vsDIV + " - " + countU1vsDIV, "O/U", CountO2vsDIV + " - " + countU2vsDIV };
                vsnoDIVDGV.Rows.Add(coversrow);

                for (i = 0; i < 4; i++)
                {
                    vsnoDIVDGV.Rows[i].Cells[1].Style.ForeColor = Color.White;
                }

                vsnoDIVDGV.Rows[0].Cells[0].Selected = false;
                label47.Text = AwayTeamCB.Text.Split(' ')[1].Replace("Diamondbacks", "D'backs") + " vs. " + HomeStarterHand;
                label48.Text = HomeTeamCB.Text.Split(' ')[1].Replace("Diamondbacks", "D'backs") + " vs. " + AwayStarterHand;
            }
        }

        private void AwayTeamCB_SelectedValueChanged(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                if (pictureBox2.Visible == true)
                {
                    //pictureBox2.Hide();
                    pictureBox2.Image = null;
                }
                connection.Open();

                try
                {

                    //label18.Text = "";
                    //label19.Text = "";
                    label22.Text = null;
                    label23.Text = null;
                    ATS1 = null;
                    OU1 = null;

                    value = (string)this.AwayTeamCB.SelectedItem;

                    teamslogos();


                    Sql2 = "select FirstName,LastName from MLBStarters where Team = '" + AwayTeamCB.Text.Split(' ')[1] + "' order by LastName";

                    awayStarterCB.Items.Clear();

                    DataSet myDataSet4 = new DataSet();

                    adapter4 = new SqlDataAdapter(Sql2, connection);

                    // adapter4.Fill(myDataSet4, "LastName");
                    adapter4.Fill(myDataSet4);

                    System.Data.DataTable myDataTable4 = myDataSet4.Tables[0];

                    DataRow tempRow4 = null;

                    foreach (DataRow tempRow4_Variable in myDataTable4.Rows)
                    {

                        tempRow4 = tempRow4_Variable;

                        //  awayStarterCB.Items.Add((tempRow4["LastName"]));
                        awayStarterCB.Items.Add((tempRow4[0] + " " + tempRow4[1]));

                    }


                    String queryy33 = "Select ID,CoversID from Klubovi where ImeKluba = '" + value + "'";
                    SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                    SqlDataReader readd33 = cmdd33.ExecuteReader();
                    while (readd33.Read())
                    {
                        idkluba1 = (readd33["ID"].ToString());
                        linkat = (readd33["CoversID"].ToString());
                    }
                    readd33.Close();

                    String queryy334 = "Select KratkoIme from Klubovi where ImeKluba = '" + value + "'";
                    SqlCommand cmdd334 = new SqlCommand(queryy334, connection);
                    SqlDataReader readd334 = cmdd334.ExecuteReader();
                    while (readd334.Read())
                    {
                        kratkoime5 = (readd334["KratkoIme"].ToString());
                    }
                    readd334.Close();

                    label12.Text = " " + kratkoime5 + " last 10 games";



                    // -------------------------------------------AWAY TEAM LAST 10-------------------------------------

                    if (radioBtnNBA.Checked == true)
                    {
                        Sql33Last10 = "select TOP 10 Dayofmonth,Month,Team, Opp, Hscore, Ascore, Line, Total, SUr, ATSr, OUr from " + AllNBAMLBResultsVar + " where Team = '" + kratkoime5 +
                            "' or Opp = '" + kratkoime5 + "' order by ID desc";
                    }
                    else
                    {
                        awayStarterCB.Enabled = true;
                        Sql33Last10 = "select TOP 10 Dayofmonth,Month,Team, Opp, Hscore, Ascore, Line, Total, SUr, Hits, OUr from " + AllNBAMLBResultsVar + " where Team = '" + kratkoime5 +
                                "' or Opp = '" + kratkoime5 + "' order by ID desc";
                    }
                    ds4 = new DataSet();
                    adapter4 = new SqlDataAdapter(Sql33Last10, connection);
                    ds4.Reset();
                    adapter4.Fill(ds4);
                    ATeamL10DGV.DataSource = ds4.Tables[0];

                    ATeamL10DGV.CurrentCell.Selected = false;


                    System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                    boldStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
                    //  dataGridView3.Columns[0].DefaultCellStyle = boldStyle;
                    //  dataGridView3.Columns[1].DefaultCellStyle = boldStyle;
                    // dataGridView3.Columns[6].DefaultCellStyle = boldStyle;
                    // dataGridView3.Columns[7].DefaultCellStyle = boldStyle;
                    // dataGridView3.Columns[8].DefaultCellStyle = boldStyle;

                    ATeamL10DGV.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                    ATeamL10DGV.Columns[0].HeaderText = "Day";
                    ATeamL10DGV.Columns[0].Width = 35;
                    ATeamL10DGV.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                    ATeamL10DGV.Columns[1].HeaderText = "Month";
                    ATeamL10DGV.Columns[1].Width = 40;
                    ATeamL10DGV.Columns[2].DefaultCellStyle.BackColor = Color.DarkGray;
                    ATeamL10DGV.Columns[2].Width = 107;
                    ATeamL10DGV.Columns[2].HeaderText = "Home Team";
                    ATeamL10DGV.Columns[3].DefaultCellStyle.BackColor = Color.DarkGray;
                    ATeamL10DGV.Columns[3].Width = 107;
                    ATeamL10DGV.Columns[3].HeaderText = "Away Team";
                    ATeamL10DGV.Columns[4].DefaultCellStyle.BackColor = Color.Silver;
                    ATeamL10DGV.Columns[4].HeaderText = "Home Points";
                    ATeamL10DGV.Columns[5].DefaultCellStyle.BackColor = Color.Silver;
                    ATeamL10DGV.Columns[5].HeaderText = "Away Points";
                    ATeamL10DGV.Columns[6].DefaultCellStyle.BackColor = Color.LightGray;
                    ATeamL10DGV.Columns[7].DefaultCellStyle.BackColor = Color.LightGray;
                    ATeamL10DGV.Columns[8].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    ATeamL10DGV.Columns[8].DefaultCellStyle = boldStyle;
                    ATeamL10DGV.Columns[9].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    ATeamL10DGV.Columns[9].DefaultCellStyle = boldStyle;
                    ATeamL10DGV.Columns[10].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    ATeamL10DGV.Columns[10].DefaultCellStyle = boldStyle;

                    double line1;

                    DataGridViewRow row = new DataGridViewRow();
                    for (int i = 0; i < ATeamL10DGV.Rows.Count - 1; i++)
                    {

                        if (ATeamL10DGV.Rows[i].Cells[10].Value.ToString() == "U")
                        {
                            ATeamL10DGV.Rows[i].Cells[10].Style.ForeColor = Color.ForestGreen;
                        }
                        else if (ATeamL10DGV.Rows[i].Cells[10].Value.ToString() == "O")
                        {
                            ATeamL10DGV.Rows[i].Cells[10].Style.ForeColor = Color.DarkRed;
                        }

                        ////////////////////////////////////////////////////////////////////////////////////////////

                        if (kratkoime5 == ATeamL10DGV.Rows[i].Cells[2].Value.ToString())
                        {
                            ATeamL10DGV.Rows[i].Cells[2].Style = boldStyle;
                            if (ATeamL10DGV.Rows[i].Cells[8].Value.ToString() == "L")
                            {
                                ATeamL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.DarkRed;
                            }
                            if (ATeamL10DGV.Rows[i].Cells[8].Value.ToString() == "W")
                            {
                                ATeamL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.ForestGreen;
                            }
                            if (ATeamL10DGV.Rows[i].Cells[9].Value.ToString() == "L")
                            {
                                ATeamL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.DarkRed;
                            }
                            if (ATeamL10DGV.Rows[i].Cells[9].Value.ToString() == "W")
                            {
                                ATeamL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.ForestGreen;
                            }
                        }
                        if (kratkoime5 == ATeamL10DGV.Rows[i].Cells[3].Value.ToString())
                        {
                            ATeamL10DGV.Rows[i].Cells[3].Style = boldStyle;
                            if (ATeamL10DGV.Rows[i].Cells[8].Value.ToString() == "L")
                            {
                                ATeamL10DGV.Rows[i].Cells[8].Value = "W";
                                ATeamL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.ForestGreen;
                            }
                            else if (ATeamL10DGV.Rows[i].Cells[8].Value.ToString() == "W")
                            {
                                ATeamL10DGV.Rows[i].Cells[8].Value = "L";
                                ATeamL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.DarkRed;
                            }
                            if (ATeamL10DGV.Rows[i].Cells[9].Value.ToString() == "L")
                            {
                                ATeamL10DGV.Rows[i].Cells[9].Value = "W";
                                ATeamL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.ForestGreen;
                            }
                            else if (ATeamL10DGV.Rows[i].Cells[9].Value.ToString() == "W")
                            {
                                ATeamL10DGV.Rows[i].Cells[9].Value = "L";
                                ATeamL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.DarkRed;
                            }
                            if (ATeamL10DGV.Rows[i].Cells[6].Value.ToString().Length > 0)
                            {
                                line1 = Convert.ToDouble(ATeamL10DGV.Rows[i].Cells[6].Value.ToString().Replace(".", ","));
                                line1 = line1 * (-1);
                                ATeamL10DGV.Rows[i].Cells[6].Value = line1.ToString();
                            }
                        }
                    }


                    // -------------------------------------------AWAY TEAM LAST 10-------------------------------------
                    if (radioBtnNBA.Checked == true)
                    {
                        calculateAveragesSUOUAwayTeam();
                        showAveragesinDGV();
                    }

                    this.ActiveControl = label1;

                    InjuryAway(sender, e);

                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void HomeTeamCB_SelectedValueChanged(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {

                if (pictureBox3.Visible == true)
                {
                    //pictureBox3.Hide();
                    pictureBox3.Image = null;
                }
                connection.Open();

                TotalBox.Enabled = true;
                HandicapBox.Enabled = true;

                //label20.Text = "";
                //label21.Text = "";
                label24.Text = null;
                label25.Text = null;

                ATS2 = null;
                OU2 = null;

                value2 = (string)this.HomeTeamCB.SelectedItem;

                teamslogos();

                Sql2 = "select FirstName,LastName from MLBStarters where Team = '" + HomeTeamCB.Text.Split(' ')[1] + "' order by LastName";

                homeStarterCB.Items.Clear();

                DataSet myDataSet4 = new DataSet();

                adapter4 = new SqlDataAdapter(Sql2, connection);

                adapter4.Fill(myDataSet4);

                System.Data.DataTable myDataTable4 = myDataSet4.Tables[0];

                DataRow tempRow4 = null;

                foreach (DataRow tempRow4_Variable in myDataTable4.Rows)
                {

                    tempRow4 = tempRow4_Variable;

                    homeStarterCB.Items.Add((tempRow4[0] + " " + tempRow4[1]));

                }

                if (radioBtnMLB.Checked == true)
                {
                    String queryy33 = "Select ID, CoversID,ParkFactor from Klubovi where ImeKluba = '" + value2 + "'";
                    SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                    SqlDataReader readd33 = cmdd33.ExecuteReader();
                    while (readd33.Read())
                    {
                        idkluba1 = (readd33["ID"].ToString());
                        linkht = (readd33["CoversID"].ToString());
                        ParkFactor = Convert.ToDouble(readd33["ParkFactor"]);
                    }
                    readd33.Close();
                    L47.Text = ParkFactor.ToString();
                }
                else if (radioBtnNBA.Checked == true)
                {
                    String queryy33 = "Select ID, CoversID from Klubovi where ImeKluba = '" + value2 + "'";
                    SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                    SqlDataReader readd33 = cmdd33.ExecuteReader();
                    while (readd33.Read())
                    {
                        idkluba1 = (readd33["ID"].ToString());
                        linkht = (readd33["CoversID"].ToString());
                    }
                    readd33.Close();
                }


                if (AwayTeamCB.Text != "")
                {
                    TotalBox.BackColor = Color.OrangeRed;
                    HandicapBox.BackColor = Color.OrangeRed;
                }

                String queryy334 = "Select KratkoIme from Klubovi where ImeKluba = '" + value2 + "'";
                SqlCommand cmdd334 = new SqlCommand(queryy334, connection);
                SqlDataReader readd334 = cmdd334.ExecuteReader();
                while (readd334.Read())
                {
                    kratkoime6 = (readd334["KratkoIme"].ToString());
                }
                readd334.Close();

                label42.Text = " " + kratkoime6 + " last 10 games";


                //////////////////////////////////////////////////////////////////////////////

                // -------------------------------------------HOME TEAM LAST 10-------------------------------------
                if (radioBtnNBA.Checked == true)
                {
                    Sql34Last10 = "select TOP 10 Dayofmonth,Month,Team, Opp, Hscore, Ascore, Line, Total, SUr, ATSr, OUr from " + AllNBAMLBResultsVar + " where Team = '" + kratkoime6 +
                        "' or Opp = '" + kratkoime6 + "' order by ID desc";
                }
                else
                {
                    homeStarterCB.Enabled = true;
                    Sql34Last10 = "select TOP 10 Dayofmonth,Month,Team, Opp, Hscore, Ascore, Line, Total, SUr, Hits, OUr from " + AllNBAMLBResultsVar + " where Team = '" + kratkoime6 +
                            "' or Opp = '" + kratkoime6 + "' order by ID desc";
                }
                ds4 = new DataSet();
                adapter4 = new SqlDataAdapter(Sql34Last10, connection);
                ds4.Reset();
                adapter4.Fill(ds4);
                HTeamL10DGV.DataSource = ds4.Tables[0];

                HTeamL10DGV.CurrentCell.Selected = false;


                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                boldStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
                //  dataGridView3.Columns[0].DefaultCellStyle = boldStyle;
                //  dataGridView3.Columns[1].DefaultCellStyle = boldStyle;
                // dataGridView3.Columns[6].DefaultCellStyle = boldStyle;
                // dataGridView3.Columns[7].DefaultCellStyle = boldStyle;
                // dataGridView3.Columns[8].DefaultCellStyle = boldStyle;

                HTeamL10DGV.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                HTeamL10DGV.Columns[0].HeaderText = "Day";
                HTeamL10DGV.Columns[0].Width = 35;
                HTeamL10DGV.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                HTeamL10DGV.Columns[1].HeaderText = "Month";
                HTeamL10DGV.Columns[1].Width = 40;
                HTeamL10DGV.Columns[2].DefaultCellStyle.BackColor = Color.DarkGray;
                HTeamL10DGV.Columns[2].Width = 107;
                HTeamL10DGV.Columns[2].HeaderText = "Home Team";
                HTeamL10DGV.Columns[3].DefaultCellStyle.BackColor = Color.DarkGray;
                HTeamL10DGV.Columns[3].Width = 107;
                HTeamL10DGV.Columns[3].HeaderText = "Away Team";
                HTeamL10DGV.Columns[4].DefaultCellStyle.BackColor = Color.Silver;
                HTeamL10DGV.Columns[4].HeaderText = "Home Points";
                HTeamL10DGV.Columns[5].DefaultCellStyle.BackColor = Color.Silver;
                HTeamL10DGV.Columns[5].HeaderText = "Away Points";
                HTeamL10DGV.Columns[6].DefaultCellStyle.BackColor = Color.LightGray;
                HTeamL10DGV.Columns[7].DefaultCellStyle.BackColor = Color.LightGray;
                HTeamL10DGV.Columns[8].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                HTeamL10DGV.Columns[8].DefaultCellStyle = boldStyle;
                HTeamL10DGV.Columns[9].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                HTeamL10DGV.Columns[9].DefaultCellStyle = boldStyle;
                HTeamL10DGV.Columns[10].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                HTeamL10DGV.Columns[10].DefaultCellStyle = boldStyle;

                double line1;

                DataGridViewRow row = new DataGridViewRow();
                for (int i = 0; i < HTeamL10DGV.Rows.Count - 1; i++)
                {

                    if (HTeamL10DGV.Rows[i].Cells[10].Value.ToString() == "U")
                    {
                        HTeamL10DGV.Rows[i].Cells[10].Style.ForeColor = Color.ForestGreen;
                    }
                    else if (HTeamL10DGV.Rows[i].Cells[10].Value.ToString() == "O")
                    {
                        HTeamL10DGV.Rows[i].Cells[10].Style.ForeColor = Color.DarkRed;
                    }

                    ////////////////////////////////////////////////////////////////////////////////////////////

                    if (kratkoime6 == HTeamL10DGV.Rows[i].Cells[2].Value.ToString())
                    {
                        // dataGridView4.Rows[i].Cells[1].Style = boldStyle;
                        HTeamL10DGV.Rows[i].Cells[2].Style = boldStyle;


                        if (HTeamL10DGV.Rows[i].Cells[8].Value.ToString() == "L")
                        {
                            HTeamL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.DarkRed;
                        }
                        if (HTeamL10DGV.Rows[i].Cells[8].Value.ToString() == "W")
                        {
                            HTeamL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.ForestGreen;
                        }
                        if (HTeamL10DGV.Rows[i].Cells[9].Value.ToString() == "L")
                        {
                            HTeamL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.DarkRed;
                        }
                        if (HTeamL10DGV.Rows[i].Cells[9].Value.ToString() == "W")
                        {
                            HTeamL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.ForestGreen;
                        }
                    }

                    if (kratkoime6 == HTeamL10DGV.Rows[i].Cells[3].Value.ToString())
                    {
                        //dataGridView4.Rows[i].Cells[1].Style = boldStyle;
                        HTeamL10DGV.Rows[i].Cells[3].Style = boldStyle;


                        if (HTeamL10DGV.Rows[i].Cells[8].Value.ToString() == "L")
                        {
                            HTeamL10DGV.Rows[i].Cells[8].Value = "W";
                            HTeamL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.ForestGreen;
                        }
                        else if (HTeamL10DGV.Rows[i].Cells[8].Value.ToString() == "W")
                        {
                            HTeamL10DGV.Rows[i].Cells[8].Value = "L";
                            HTeamL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.DarkRed;
                        }
                        if (HTeamL10DGV.Rows[i].Cells[9].Value.ToString() == "L")
                        {
                            HTeamL10DGV.Rows[i].Cells[9].Value = "W";
                            HTeamL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.ForestGreen;
                        }
                        else if (HTeamL10DGV.Rows[i].Cells[9].Value.ToString() == "W")
                        {
                            HTeamL10DGV.Rows[i].Cells[9].Value = "L";
                            HTeamL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.DarkRed;
                        }
                        if (HTeamL10DGV.Rows[i].Cells[6].Value.ToString().Length > 0)
                        {
                            line1 = Convert.ToDouble(HTeamL10DGV.Rows[i].Cells[6].Value.ToString().Replace(".", ","));
                            line1 = line1 * (-1);
                            HTeamL10DGV.Rows[i].Cells[6].Value = line1.ToString();
                        }
                    }


                }

                if (radioBtnNBA.Checked == true)
                {
                    calculateAveragesSUOUHomeTeam();
                    showAveragesinDGV();
                    pace_eff();
                }

                this.ActiveControl = label1;

                InjuryHome(sender, e);

                connection.Close();
            }

        }

        private void teamslogos()
        {

            pictureBox2.Image = null;
            pictureBox3.Image = null;

            switch (value)
            {
                case "Atlanta Hawks":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/f/fe/Atlanta_Hawks_2015_Logo.svg/739px-Atlanta_Hawks_2015_Logo.svg.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Boston Celtics":

                    pictureBox2.Show();
                    // pictureBox2.Image = Oklade.Properties.Resources.Boston_Celtics_Logo;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/8/8f/Boston_Celtics.svg/250px-Boston_Celtics.svg.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Brooklyn Nets":

                    pictureBox2.Show();
                    //pictureBox2.Image = Oklade.Properties.Resources.Brooklyn_Nets_logo;
                    pictureBox2.ImageLocation = "http://cdn.bleacherreport.net/images/team_logos/164x164/brooklyn_nets.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Charlotte Hornets":

                    pictureBox2.Show();
                    //  pictureBox2.Image = Oklade.Properties.Resources.Charlotte_Hornets_new_logo1;
                    pictureBox2.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/cha.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Chicago Bulls":

                    pictureBox2.Show();
                    // pictureBox2.Image = Oklade.Properties.Resources.chicago_bulls_logo1;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/6/67/Chicago_Bulls_logo.svg/475px-Chicago_Bulls_logo.svg.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Cleveland Cavaliers":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://www.printyourbrackets.com/nba-logos/cleveland-cavaliers-logo.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Dallas Mavericks":

                    pictureBox2.Show();
                    // pictureBox2.Image = Oklade.Properties.Resources.Dallas_Mavericks_logo;
                    pictureBox2.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/dal.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Denver Nuggets":

                    pictureBox2.Show();
                    //   pictureBox2.Image = Oklade.Properties.Resources.Denver_Nuggets_logo;
                    pictureBox2.ImageLocation = "http://content.sportslogos.net/logos/6/229/full/xeti0fjbyzmcffue57vz5o1gl.gif";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Detroit Pistons":

                    pictureBox2.Show();
                    // pictureBox2.Image = Oklade.Properties.Resources.Detroit_Pistons_logo;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/1/1e/Detroit_Pistons_logo.svg/1229px-Detroit_Pistons_logo.svg.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "GS Warriors":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://img1.findthebest.com/sites/default/files/5881/media/images/_4173363.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Houston Rockets":

                    pictureBox2.Show();
                    //  pictureBox2.Image = Oklade.Properties.Resources.Houston_Rockets;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/2/28/Houston_Rockets.svg/1280px-Houston_Rockets.svg.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Indiana Pacers":

                    pictureBox2.Show();
                    //  pictureBox2.Image = Oklade.Properties.Resources.Indiana_Pacers_Logo;
                    pictureBox2.ImageLocation = "http://content.sportslogos.net/logos/6/224/full/3083.gif";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "LA Clippers":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/media/2.0/teamsites/clippers/clippers-primary-150617-v2.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "LA Lakers":

                    pictureBox2.Show();
                    // pictureBox2.Image = Oklade.Properties.Resources.Los_Angeles_Lakers_logo;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/LosAngeles_Lakers_logo.svg/360px-LosAngeles_Lakers_logo.svg.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Memphis Grizzlies":

                    pictureBox2.Show();
                    //  pictureBox2.Image = Oklade.Properties.Resources.Grizzlies_Logo1;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/it/6/6f/Memphis_Grizzlies_logo.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Miami Heat":

                    pictureBox2.Show();
                    //  pictureBox2.Image = Oklade.Properties.Resources.Miami_Heat_Logo;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/f/fb/Miami_Heat_logo.svg/741px-Miami_Heat_logo.svg.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Milwaukee Bucks":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/mil.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Min Timberwolves":

                    pictureBox2.Show();
                    //  pictureBox2.Image = Oklade.Properties.Resources.Minnesota_Timberwolves_logo;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/hr/5/57/Minnesota_Timberwolves.gif";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "NewOrleans Pelicans":

                    pictureBox2.Show();
                    // pictureBox2.Image = Oklade.Properties.Resources.New_Orleans_Pelicans;
                    pictureBox2.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/nop.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "NewYork Knicks":

                    pictureBox2.Show();
                    //  pictureBox2.Image = Oklade.Properties.Resources.new_york_knicks_logo;
                    pictureBox2.ImageLocation = "http://vignette1.wikia.nocookie.net/logopedia/images/8/80/NewYorkKnicks.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "OKC Thunder":

                    pictureBox2.Show();
                    //  pictureBox2.Image = Oklade.Properties.Resources.OKC;
                    pictureBox2.ImageLocation = "http://a3.espncdn.com/combiner/i?img=%2Fi%2Fteamlogos%2Fnba%2F500%2Fokc.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Orlando Magic":

                    pictureBox2.Show();
                    // pictureBox2.Image = Oklade.Properties.Resources.Orlando_magic_logo;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/8/85/Orlando_magic_logo.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Phi Seventysixers":

                    pictureBox2.Show();
                    //   pictureBox2.Image = Oklade.Properties.Resources.phi_logo;
                    pictureBox2.ImageLocation = "https://s-media-cache-ak0.pinimg.com/originals/36/8a/b9/368ab94dbe2732e963c9b94456e6b7c5.jpg";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Phoenix Suns":

                    pictureBox2.Show();
                    pictureBox2.Image = Oklade.Properties.Resources.Phoenix_Suns;
                    break;

                case "Por Trailblazers":

                    pictureBox2.Show();
                    //   pictureBox2.Image = Oklade.Properties.Resources.portland_trail_blazers_logo2;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/7/74/Portland_Trail_Blazers.svg/840px-Portland_Trail_Blazers.svg.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Sacramento Kings":

                    pictureBox2.Show();
                    // pictureBox2.Image = Oklade.Properties.Resources.Sacramento_Kings_Logo;
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/hr/a/a8/Sacramento_Kings.gif";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "SanAntonio Spurs":

                    pictureBox2.Show();
                    //  pictureBox2.Image = Oklade.Properties.Resources.San_Antonio_Spurs_logo;
                    pictureBox2.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/sas.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Toronto Raptors":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/tor.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Utah Jazz":

                    pictureBox2.Show();
                    //   pictureBox2.Image = Oklade.Properties.Resources.utahjazz;
                    pictureBox2.ImageLocation = "http://img.zanda.com/item/45030000000044/400x400/Utah_Jazz_logo.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Washington Wizards":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://cdn.mse.psddev.com/dims4/SPORTS/6d4b737/2147483647/resize/618x410%3E/quality/75/?url=http%3A%2F%2Fcdn.mse.psddev.com%2Ffd%2Ffe%2Fd4eb58a24521af7d2875c45dfb85%2F150421-washingtonwizards-primary.gif";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Arizona Diamondbacks":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://s7d2.scene7.com/is/image/Fathead/lgo_mlb_arizona_diamondbacks?layer=comp&fit=constrain&hei=280&wid=280&fmt=png-alpha&qlt=75,0&op_sharpen=1&resMode=bicub&op_usm=0.0,0.0,0,0&iccEmbed=0";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Atlanta Braves":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://www.clipartbest.com/cliparts/di6/ap4/di6ap4a9T.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Baltimore Orioles":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/c/cc/Orioles_new.PNG";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Boston RedSox":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://img2.wikia.nocookie.net/__cb20111229213234/mlbtheshow/images/9/9a/Boston_Red_Sox_Logo.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Chicago Cubs":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://bloximages.chicago2.vip.townnews.com/qctimes.com/content/tncms/assets/v3/editorial/8/97/8979e588-d2d5-11e4-ad61-4b71ac7962dc/551286ed10d24.image.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Chicago WhiteSox":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://www.printyourbrackets.com/mlb-logos/chicago-white-sox-logo.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;


                case "Cincinnati Reds":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://i.cdn.turner.com/dr/hln/www/release/sites/default/files/2013/10/10/cincinnati_reds_logo_0.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Colorado Rockies":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/3/31/Colorado_Rockies_logo.svg/800px-Colorado_Rockies_logo.svg.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Miami Marlins":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://www.printyourbrackets.com/mlb-logos/miami-marlins-logo.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;


                case "NewYork Mets":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://images.sportsworldreport.com/data/images/full/13810/new-york-mets-logo.gif";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "NewYork Yankees":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://i587.photobucket.com/albums/ss311/njaw69/New-York-Yankees-Logo-psd11640.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "SanDiego Padres":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://www.wagerminds.com/blog/wp-content/uploads/2012/04/San-Diego-Padres.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "SanFrancisco Giants":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "https://s3.amazonaws.com/rantsports.com/img/logos/mlb/san-francisco-giants.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Seattle Mariners":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://www.allsportstalk.net/wp-content/uploads/sea-mariners.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "St.Louis Cardinals":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://s7d2.scene7.com/is/image/Fathead/lgo_mlb_st_louis_cardinals?layer=comp&fit=constrain&hei=350&wid=350&fmt=png-alpha&qlt=75,0&op_sharpen=1&resMode=bicub&op_usm=0.0,0.0,0,0&iccEmbed=0";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "TampaBay Rays":

                    pictureBox2.Show();
                    pictureBox2.ImageLocation = "http://sportsteamhistory.com/images/mlblogos/tampa_bay_rays.png";
                    pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
                    break;


            }
            //////////////////////////////////////////////////////////////////////////

            switch (value2)
            {
                case "Atlanta Hawks":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/f/fe/Atlanta_Hawks_2015_Logo.svg/739px-Atlanta_Hawks_2015_Logo.svg.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Boston Celtics":

                    pictureBox3.Show();
                    //  pictureBox3.Image = Oklade.Properties.Resources.Boston_Celtics_Logo;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/8/8f/Boston_Celtics.svg/250px-Boston_Celtics.svg.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Brooklyn Nets":

                    pictureBox3.Show();
                    // pictureBox3.Image = Oklade.Properties.Resources.Brooklyn_Nets_logo;
                    pictureBox3.ImageLocation = "http://cdn.bleacherreport.net/images/team_logos/164x164/brooklyn_nets.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Charlotte Hornets":

                    pictureBox3.Show();
                    //   pictureBox3.Image = Oklade.Properties.Resources.Charlotte_Hornets_new_logo1;
                    pictureBox3.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/cha.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Chicago Bulls":

                    pictureBox3.Show();
                    //  pictureBox3.Image = Oklade.Properties.Resources.chicago_bulls_logo1;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/6/67/Chicago_Bulls_logo.svg/475px-Chicago_Bulls_logo.svg.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Cleveland Cavaliers":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://www.printyourbrackets.com/nba-logos/cleveland-cavaliers-logo.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Dallas Mavericks":

                    pictureBox3.Show();
                    //  pictureBox3.Image = Oklade.Properties.Resources.dallas_mavericks_logo1;
                    pictureBox3.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/dal.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Denver Nuggets":

                    pictureBox3.Show();
                    //     pictureBox3.Image = Oklade.Properties.Resources.Denver_Nuggets_logo;
                    pictureBox3.ImageLocation = "http://content.sportslogos.net/logos/6/229/full/xeti0fjbyzmcffue57vz5o1gl.gif";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Detroit Pistons":

                    pictureBox3.Show();
                    //   pictureBox3.Image = Oklade.Properties.Resources.Detroit_Pistons_logo;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/1/1e/Detroit_Pistons_logo.svg/1229px-Detroit_Pistons_logo.svg.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "GS Warriors":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://img1.findthebest.com/sites/default/files/5881/media/images/_4173363.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Houston Rockets":

                    pictureBox3.Show();
                    //  pictureBox3.Image = Oklade.Properties.Resources.Houston_Rockets;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/2/28/Houston_Rockets.svg/1280px-Houston_Rockets.svg.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Indiana Pacers":

                    pictureBox3.Show();
                    // pictureBox3.Image = Oklade.Properties.Resources.Indiana_Pacers_Logo;
                    pictureBox3.ImageLocation = "http://content.sportslogos.net/logos/6/224/full/3083.gif";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "LA Clippers":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/media/2.0/teamsites/clippers/clippers-primary-150617-v2.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "LA Lakers":

                    pictureBox3.Show();
                    // pictureBox3.Image = Oklade.Properties.Resources.Los_Angeles_Lakers_logo;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/LosAngeles_Lakers_logo.svg/360px-LosAngeles_Lakers_logo.svg.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Memphis Grizzlies":

                    pictureBox3.Show();
                    //    pictureBox3.Image = Oklade.Properties.Resources.Grizzlies_Logo1;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/it/6/6f/Memphis_Grizzlies_logo.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Miami Heat":

                    pictureBox3.Show();
                    //  pictureBox3.Image = Oklade.Properties.Resources.Miami_Heat_Logo;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/f/fb/Miami_Heat_logo.svg/741px-Miami_Heat_logo.svg.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Milwaukee Bucks":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/mil.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Min Timberwolves":

                    pictureBox3.Show();
                    //   pictureBox3.Image = Oklade.Properties.Resources.Minnesota_Timberwolves_logo;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/hr/5/57/Minnesota_Timberwolves.gif";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "NewOrleans Pelicans":

                    pictureBox3.Show();
                    // pictureBox3.Image = Oklade.Properties.Resources.New_Orleans_Pelicans;
                    pictureBox3.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/nop.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "NewYork Knicks":

                    pictureBox3.Show();
                    // pictureBox3.Image = Oklade.Properties.Resources.new_york_knicks_logo;
                    pictureBox3.ImageLocation = "http://vignette1.wikia.nocookie.net/logopedia/images/8/80/NewYorkKnicks.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "OKC Thunder":

                    pictureBox3.Show();
                    // pictureBox3.Image = Oklade.Properties.Resources.OKC;
                    pictureBox3.ImageLocation = "http://a3.espncdn.com/combiner/i?img=%2Fi%2Fteamlogos%2Fnba%2F500%2Fokc.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Orlando Magic":

                    pictureBox3.Show();
                    //  pictureBox3.Image = Oklade.Properties.Resources.Orlando_magic_logo;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/8/85/Orlando_magic_logo.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Phi Seventysixers":

                    pictureBox3.Show();
                    //   pictureBox3.Image = Oklade.Properties.Resources.phi_logo;
                    pictureBox3.ImageLocation = "https://s-media-cache-ak0.pinimg.com/originals/36/8a/b9/368ab94dbe2732e963c9b94456e6b7c5.jpg";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Phoenix Suns":

                    pictureBox3.Show();
                    pictureBox3.Image = Oklade.Properties.Resources.Phoenix_Suns;
                    break;

                case "Por Trailblazers":

                    pictureBox3.Show();
                    // pictureBox3.Image = Oklade.Properties.Resources.portland_trail_blazers_logo2;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/7/74/Portland_Trail_Blazers.svg/840px-Portland_Trail_Blazers.svg.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Sacramento Kings":

                    pictureBox3.Show();
                    //   pictureBox3.Image = Oklade.Properties.Resources.Sacramento_Kings_Logo;
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/hr/a/a8/Sacramento_Kings.gif";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "SanAntonio Spurs":

                    pictureBox3.Show();
                    //  pictureBox3.Image = Oklade.Properties.Resources.San_Antonio_Spurs_logo;
                    pictureBox3.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/sas.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Toronto Raptors":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://i.cdn.turner.com/nba/nba/.element/img/1.0/teamsites/logos/teamlogos_500x500/tor.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Utah Jazz":

                    pictureBox3.Show();
                    //   pictureBox3.Image = Oklade.Properties.Resources.utahjazz;
                    pictureBox3.ImageLocation = "http://img.zanda.com/item/45030000000044/400x400/Utah_Jazz_logo.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Washington Wizards":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://cdn.mse.psddev.com/dims4/SPORTS/6d4b737/2147483647/resize/618x410%3E/quality/75/?url=http%3A%2F%2Fcdn.mse.psddev.com%2Ffd%2Ffe%2Fd4eb58a24521af7d2875c45dfb85%2F150421-washingtonwizards-primary.gif";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Arizona Diamondbacks":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://s7d2.scene7.com/is/image/Fathead/lgo_mlb_arizona_diamondbacks?layer=comp&fit=constrain&hei=280&wid=280&fmt=png-alpha&qlt=75,0&op_sharpen=1&resMode=bicub&op_usm=0.0,0.0,0,0&iccEmbed=0";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Atlanta Braves":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://www.clipartbest.com/cliparts/di6/ap4/di6ap4a9T.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Baltimore Orioles":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/c/cc/Orioles_new.PNG";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Boston RedSox":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://img2.wikia.nocookie.net/__cb20111229213234/mlbtheshow/images/9/9a/Boston_Red_Sox_Logo.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Chicago Cubs":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://bloximages.chicago2.vip.townnews.com/qctimes.com/content/tncms/assets/v3/editorial/8/97/8979e588-d2d5-11e4-ad61-4b71ac7962dc/551286ed10d24.image.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Chicago WhiteSox":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://www.printyourbrackets.com/mlb-logos/chicago-white-sox-logo.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Cincinnati Reds":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://i.cdn.turner.com/dr/hln/www/release/sites/default/files/2013/10/10/cincinnati_reds_logo_0.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Colorado Rockies":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "https://upload.wikimedia.org/wikipedia/en/thumb/3/31/Colorado_Rockies_logo.svg/800px-Colorado_Rockies_logo.svg.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Miami Marlins":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://www.printyourbrackets.com/mlb-logos/miami-marlins-logo.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "NewYork Mets":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://images.sportsworldreport.com/data/images/full/13810/new-york-mets-logo.gif";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "NewYork Yankees":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://i587.photobucket.com/albums/ss311/njaw69/New-York-Yankees-Logo-psd11640.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "SanDiego Padres":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://www.wagerminds.com/blog/wp-content/uploads/2012/04/San-Diego-Padres.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "SanFrancisco Giants":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "https://s3.amazonaws.com/rantsports.com/img/logos/mlb/san-francisco-giants.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "Seattle Mariners":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://www.allsportstalk.net/wp-content/uploads/sea-mariners.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;


                case "St.Louis Cardinals":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://s7d2.scene7.com/is/image/Fathead/lgo_mlb_st_louis_cardinals?layer=comp&fit=constrain&hei=350&wid=350&fmt=png-alpha&qlt=75,0&op_sharpen=1&resMode=bicub&op_usm=0.0,0.0,0,0&iccEmbed=0";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

                case "TampaBay Rays":

                    pictureBox3.Show();
                    pictureBox3.ImageLocation = "http://sportsteamhistory.com/images/mlblogos/tampa_bay_rays.png";
                    pictureBox3.SizeMode = PictureBoxSizeMode.Zoom;
                    break;

            }
        }

        private void TotalBox_Validating(object sender, CancelEventArgs e)
        {
            if (TotalBox.Text.Length >= 1)
            {
                string input = TotalBox.Text;
                double input1 = Convert.ToDouble(input);
                if (radioBtnNBA.Checked == true)
                {
                    if (input1 <= 250 && input1 >= 160)
                    {
                        TotalBox.BackColor = Color.WhiteSmoke;
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Incorrect data in Total Box", "Important Message", MessageBoxButtons.OK,
                 MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    if (input1 <= 15 && input1 >= 4)
                    {
                        TotalBox.BackColor = Color.WhiteSmoke;
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Incorrect data in Total Box", "Important Message", MessageBoxButtons.OK,
                 MessageBoxIcon.Exclamation);
                    }
                }

            }


        }

        private void HandicapBox_Validating(object sender, CancelEventArgs e)
        {
            if (HandicapBox.Text != "")
            {
                string input = HandicapBox.Text;
                double input1 = Convert.ToDouble(input);
                if (radioBtnNBA.Checked == true)
                {
                    if (input1 <= 30 && input1 >= -30)
                    {
                        HandicapBox.BackColor = Color.WhiteSmoke;
                        button3_Click(sender, e);
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Incorrect data in Handicap Box", "Important Message", MessageBoxButtons.OK,
                 MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    if (input1 <= 3 && input1 >= -3)
                    {
                        HandicapBox.BackColor = Color.WhiteSmoke;
                        button3_Click(sender, e);
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Incorrect data in Handicap Box", "Important Message", MessageBoxButtons.OK,
                 MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DGVAnalysisResults.Rows[e.RowIndex].Cells[0].Selected)
            {


                IDoklBox.Text = DGVAnalysisResults.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                connection.Open();

                IDOkl11 = Convert.ToInt32(IDoklBox.Text);

                connection.Close();

                GetTeamsandStarters();

                TotalBox.BackColor = Color.White;
                HandicapBox.BackColor = Color.White;


                LoadGameBtn_Click(sender, e);

            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                IDO = IDoklBox.Text;
                if (radioBtnMLB.Checked == true)
                {
                    String query3 = "Select ID,ParkFactor from Klubovi where ImeKluba = '" + value2 + "'";

                    SqlCommand cmd3 = new SqlCommand(query3, connection);
                    SqlDataReader read3 = cmd3.ExecuteReader();

                    while (read3.Read())
                    {
                        IDH = (read3["ID"].ToString());
                        ParkFactor = Convert.ToDouble(read3["ParkFactor"]);
                    }
                    read3.Close();
                }
                else if (radioBtnNBA.Checked == true)
                {
                    String query3 = "Select ID from Klubovi where ImeKluba = '" + value2 + "'";

                    SqlCommand cmd3 = new SqlCommand(query3, connection);
                    SqlDataReader read3 = cmd3.ExecuteReader();

                    while (read3.Read())
                    {
                        IDH = (read3["ID"].ToString());
                    }
                    read3.Close();
                }

                String query4 = "Select ID from Klubovi where ImeKluba = '" + value + "'";
                SqlCommand cmd4 = new SqlCommand(query4, connection);
                SqlDataReader read4 = cmd4.ExecuteReader();
                while (read4.Read())
                {
                    IDA = (read4["ID"].ToString());
                }
                read4.Close();

                foreach (DataGridViewRow row in DGVAnalysisResults.Rows)
                {
                    if (Convert.ToBoolean(row.Cells["chk"].Value) == true)
                    {
                        int rez1;
                        int rez2;
                        rez1 = Convert.ToInt32(row.Cells["HScore"].Value);
                        rez2 = Convert.ToInt32(row.Cells["AScore"].Value);
                        int totrez = rez1 + rez2;
                        int handirez = rez2 - rez1;
                        String query5 = @"INSERT INTO NBAResults(ImeHKluba, ImeAKluba,HResult,AResult, IDOklade, IDHKluba, IDAKluba, TotalResult, HandicapResult, Season, Line, Total)
                                VALUES ('" + row.Cells["Team"].Value.ToString() + "', '" + row.Cells["Opp"].Value.ToString() +
                       "', '" + row.Cells["HScore"].Value.ToString() + "','" + row.Cells["AScore"].Value.ToString() + "','" + IDO + "','" + IDH + "','" + IDA +
                       "', '" + totrez + "', '" + handirez + "'," + row.Cells["Season"].Value.ToString() + "," + row.Cells["Line"].Value.ToString().Replace(",", ".") + "," + row.Cells["Total"].Value.ToString().Replace(",", ".") + ")";

                        SqlCommand cmd111 = new SqlCommand(query5, connection);

                        cmd111.ExecuteNonQuery();
                    }

                }
                button19_Click(sender, e);
                finishandCalcPB.Enabled = true;
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {

                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    { DGVAnalysisResults.Columns.Remove("chk2"); }

                }
                DGVAnalysisResults.DataSource = null;

                IDO = IDoklBox.Text;
                connection.Open();
                Sql3 = "select ID,ImeHKluba,ImeAKluba, HResult,AResult,TotalResult,HandicapResult, Season, Line, Total, HomePace, AwayPace from " + NBAMLBResultsVar + " where IDOklade = '" + IDO + "'";
                ds = new DataSet();
                adapter = new SqlDataAdapter(Sql3, connection);
                ds.Reset();
                adapter.Fill(ds);
                DGVAnalysisResults.DataSource = ds.Tables[0];
                DGVAnalysisResults.Columns[0].Width = 53;


                colorrows(sender, e);

            }
        }

        private void dateTimePicker1_MouseDown(object sender, MouseEventArgs e)
        {
            //dateTimePicker1_ValueChanged(sender, e);

        }


        private void results1_Click(object sender, EventArgs e)
        {



        }

        private void results2_Click(object sender, EventArgs e)
        {
            if (HomeTeamCB.Text.Length > 1)
            {
                ResultUpdater frm3 = new ResultUpdater();
                frm3.value = HomeTeamCB.Text.Split(' ')[1];
                frm3.Show();
                frm3.comboBox1.Text = HomeTeamCB.Text;
                frm3.button7_Click(sender, e);
            }
        }

        private void results1_MouseHover(object sender, EventArgs e)
        {
            results1.Cursor = Cursors.Hand;
            results1.Image = Oklade.Properties.Resources.statistics2;
        }

        private void results1_MouseLeave(object sender, EventArgs e)
        {
            results1.Image = Oklade.Properties.Resources.statistics1;

        }

        private void results2_MouseHover(object sender, EventArgs e)
        {
            results2.Cursor = Cursors.Hand;
            results2.Image = Oklade.Properties.Resources.statistics2;

        }

        private void results2_MouseLeave(object sender, EventArgs e)
        {
            results2.Image = Oklade.Properties.Resources.statistics1;

        }

        private void AutoProcess_Click_1(object sender, EventArgs e)
        {
            pacecbaway = "";
            pacecbhome = "";

            if (TotalBox.Text.Length < 1)
            {
                return;
            }
            else

                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    { DGVAnalysisResults.Columns.Remove("chk2"); }

                }
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                IDO = IDoklBox.Text;

                String HteamShort = value2.Split(' ')[1];
                String AteamShort = value.Split(' ')[1];

                String query1 = "Select TeamIsLike from Klubovi where ImeKluba = '" + value2 + "'";
                String query2 = "Select TeamIsLike from Klubovi where ImeKluba = '" + value + "'";
                SqlCommand cmd = new SqlCommand(query1, connection);
                SqlDataReader read = cmd.ExecuteReader();
                //   try
                //   {
                while (read.Read())
                {
                    til1 = (read["TeamIsLike"].ToString());
                }
                read.Close();

                SqlCommand cmd2 = new SqlCommand(query2, connection);
                SqlDataReader read2 = cmd2.ExecuteReader();
                while (read2.Read())
                {
                    til2 = (read2["TeamIsLike"].ToString());
                }
                read2.Close();

                if (hteamradio.Checked == true)
                {
                    HA = "Home";
                    HAteam22 = value2.Split(' ')[1];
                }
                else if (ateamradio.Checked == true)
                {
                    HA = "Away";
                    HAteam22 = value.Split(' ')[1];
                }

                if (radioBtnMLB.Checked == true)
                {
                    string query3 = "Select ID,TeamOUH,ParkFactor from Klubovi where ImeKluba = '" + value2 + "'";
                    SqlCommand cmd3 = new SqlCommand(query3, connection);
                    SqlDataReader read3 = cmd3.ExecuteReader();
                    //try
                    //{
                    while (read3.Read())
                    {
                        IDH = (read3["ID"].ToString());
                        TeamOUH = (read3["TeamOUH"].ToString());
                        ParkFactor = Convert.ToDouble(read3["ParkFactor"]);
                    }
                    read3.Close();
                }
                else if (radioBtnNBA.Checked == true)
                {
                    string query3 = "Select ID,TeamOUH,PaceHome from Klubovi where ImeKluba = '" + value2 + "'";
                    SqlCommand cmd3 = new SqlCommand(query3, connection);
                    SqlDataReader read3 = cmd3.ExecuteReader();
                    //try
                    //{
                    while (read3.Read())
                    {
                        IDH = (read3["ID"].ToString());
                        TeamOUH = (read3["TeamOUH"].ToString());
                        varpacehome = Convert.ToDouble(read3["PaceHome"]);
                    }
                    read3.Close();
                }
                if (radioBtnNBA.Checked == true)
                {
                    string query4 = "Select ID,TeamOUA,PaceAway from Klubovi where ImeKluba = '" + value + "'";
                    SqlCommand cmd4 = new SqlCommand(query4, connection);
                    SqlDataReader read4 = cmd4.ExecuteReader();
                    while (read4.Read())
                    {
                        IDA = (read4["ID"].ToString());
                        TeamOUA = (read4["TeamOUA"].ToString());
                        varpaceaway = Convert.ToDouble(read4["PaceAway"]);
                    }
                    read4.Close();
                }
                else if (radioBtnMLB.Checked == true)
                {
                    string query4 = "Select ID,TeamOUA from Klubovi where ImeKluba = '" + value + "'";
                    SqlCommand cmd4 = new SqlCommand(query4, connection);
                    SqlDataReader read4 = cmd4.ExecuteReader();
                    while (read4.Read())
                    {
                        IDA = (read4["ID"].ToString());
                        TeamOUA = (read4["TeamOUA"].ToString());
                    }
                    read4.Close();
                }
                til22 = Convert.ToInt32(til2);
                til23 = til22 + 1;
                til24 = til22 - 1;

                til12 = Convert.ToInt32(til1);
                til13 = til12 + 1;
                til14 = til12 - 1;

                //if (til22 == 3)
                //{
                //    til23 = til22;
                //    til24 = til22;
                //}
                //if (til12 == 3)
                //{
                //    til13 = til12;
                //    til14 = til12;
                //}
                if (checkBox1.Checked == true & checkBox2.Checked == false)
                {
                    season1 = "" + (SeasonNBAMLB - 1) + "";
                    season2 = "";
                }
                if (checkBox1.Checked == true & checkBox2.Checked == true)
                {
                    season1 = "" + (SeasonNBAMLB - 1) + "";
                    season2 = "" + SeasonNBAMLB + "";
                }
                if (checkBox1.Checked == false & checkBox2.Checked == true)
                {
                    season2 = "" + SeasonNBAMLB + "";
                    season1 = "";
                }
                if (checkBox3.Checked == false)
                {
                    TeamOUH = "[o-u]%";
                    TeamOUA = "[o-u]%";
                }

                if (paceCB.Checked == true)
                {
                    if (varpacehome >= 97.5)
                    {
                        varpacehomelow = varpacehome - 3;
                        strpacehomelow = varpacehomelow.ToString().Replace(",", ".");
                        varpacehomehigh = varpacehome + 1.8;
                        strpacehomehigh = varpacehomehigh.ToString().Replace(",", ".");
                    }
                    else if (varpacehome <= 92)
                    {
                        varpacehomelow = varpacehome - 1.8;
                        strpacehomelow = varpacehomelow.ToString().Replace(",", ".");
                        varpacehomehigh = varpacehome + 2.6;
                        strpacehomehigh = varpacehomehigh.ToString().Replace(",", ".");
                    }
                    else
                    {
                        varpacehomelow = varpacehome - 1.8;
                        strpacehomelow = varpacehomelow.ToString().Replace(",", ".");
                        varpacehomehigh = varpacehome + 1.8;
                        strpacehomehigh = varpacehomehigh.ToString().Replace(",", ".");
                    }
                    pacecbhome = "and Klubovi.PaceHome between " + strpacehomelow + " and " + strpacehomehigh + "";

                    if (varpaceaway >= 97.5)
                    {
                        varpaceawaylow = varpaceaway - 3;
                        strpaceawaylow = varpaceawaylow.ToString().Replace(",", ".");
                        varpaceawayhigh = varpaceaway + 1.8;
                        strpaceawayhigh = varpaceawayhigh.ToString().Replace(",", ".");
                    }
                    else if (varpaceaway <= 92)
                    {
                        varpaceawaylow = varpaceaway - 1.8;
                        strpaceawaylow = varpaceawaylow.ToString().Replace(",", ".");
                        varpaceawayhigh = varpaceaway + 2.6;
                        strpaceawayhigh = varpaceawayhigh.ToString().Replace(",", ".");
                    }
                    else
                    {
                        varpaceawaylow = varpaceaway - 1.8;
                        strpaceawaylow = varpaceawaylow.ToString().Replace(",", ".");
                        varpaceawayhigh = varpaceaway + 1.8;
                        strpaceawayhigh = varpaceawayhigh.ToString().Replace(",", ".");
                    }
                    pacecbaway = "and Klubovi.PaceHome between " + strpaceawaylow + " and " + strpaceawayhigh + "";
                }

                if (rankingsCB.Checked == true)
                {
                    strRankingsAway = " HisLike IN ('" + til1 + "','" + til13 + "','" + til14 + "') and";
                    strRankingsHome = " AisLike IN ('" + til2 + "','" + til23 + "','" + til24 + "') and";
                }
                else
                {
                    strRankingsAway = "";
                    strRankingsHome = "";
                }

                //               String query6 = @"INSERT INTO NBAResults(ImeHKluba, ImeAKluba,HResult,AResult, IDOklade, IDHKluba, IDAKluba)
                //         SELECT TOP 9 AllNbaResults.Team, AllNbaResults.Opp,AllNbaResults.Hscore, AllNbaResults.Ascore, '" + IDO + "', '" + IDH + "','" + IDA +
                //         "' FROM AllNBAResults,Klubovi WHERE HisLike ='" + til1 + "' and Opp ='" + AteamShort + "' and season = '" + season1 + "' or HisLike ='" + til1 +
                //         "' and Opp ='" + AteamShort + "' and season = '" + season2 + "' or  HisLike ='" + til13 + "' and Opp ='" + AteamShort +
                //         "' and season = '" + season1 + "' or HisLike ='" + til13 + "' and Opp ='" + AteamShort +
                //         "' and season = '" + season2 + "' or HisLike ='" + til14 + "' and Opp ='" + AteamShort + "' and season = '" + season1 +
                //         "' or HisLike ='" + til14 + "' and Opp ='" + AteamShort + "' and season = '" + season2 + "' order by AllNbaResults.ID desc;";
                //   TeamOUH = "O";
                if (radioBtnNBA.Checked == true)
                {
                    // AWAY TEAM //
                    query6AutoProcess = @"INSERT INTO NBAResults(Season, Line, Total, ImeHKluba, ImeAKluba,HResult,AResult, IDOklade, IDHKluba, IDAKluba)
                  SELECT TOP " + LastXgamesCB.Text + " AllNbaResults.Season,AllNbaResults.Line,AllNbaResults.Total,AllNbaResults.Team, AllNbaResults.Opp,AllNbaResults.Hscore, AllNbaResults.Ascore, '" + IDO + "', '" + IDH + "','" + IDA +
                "' FROM AllNBAResults,Klubovi WHERE " + strRankingsAway + " Opp ='" + AteamShort + "' and season IN ('" + season1 +
                "','" + season2 + "') and Klubovi.Kratkoime = AllNbaResults.Team and Klubovi.TeamOUH like '" + TeamOUH + "' " + pacecbhome + " order by AllNbaResults.ID desc;";
                }
                else
                {
                    query6AutoProcess = @"INSERT INTO MLBResults(ImeHKluba,HomeStarter,HomeStarterHand,ImeAKluba,AwayStarter,AwayStarterHand,HResult,AResult, IDOklade, IDHKluba, IDAKluba)
                  SELECT TOP " + LastXgamesCB.Text + " " + AllNBAMLBResultsVar + ".Team, " + AllNBAMLBResultsVar + ".StarterHLastName ," + AllNBAMLBResultsVar + ".StarterHHand, " + AllNBAMLBResultsVar + ".Opp, " + AllNBAMLBResultsVar + ".StarterALastName, " + AllNBAMLBResultsVar + ".StarterAHand,  " +
                          AllNBAMLBResultsVar + ".Hscore, " + AllNBAMLBResultsVar + ".Ascore, '" + IDO + "', '" + IDH + "','" + IDA +
                    "' FROM " + AllNBAMLBResultsVar + " WHERE season IN ('" + season1 + "','" + season2 + "') and " + AllNBAMLBResultsVar + ".StarterAFirstName like '" +
                    awayStarterCB.Text.Split(' ')[0] + "%' and " + AllNBAMLBResultsVar + ".StarterALastName = '" + awayStarterCB.Text.Split(' ')[1] +
                    "' order by " + AllNBAMLBResultsVar + ".ID desc;";


                    //////COUNT GAMES AND IF UNDER 4, ADD 5 GAMES WITH TEAM GAMES AGAINST OPPONENT HAND
                    string query6AutoProcessCount = @"SELECT COUNT(" + AllNBAMLBResultsVar + ".StarterHLastName) AS COUNTGAMES FROM " + AllNBAMLBResultsVar +
                        " WHERE season IN ('" + season1 + "','" + season2 + "') and " +
                    AllNBAMLBResultsVar + ".StarterAFirstName like '" + awayStarterCB.Text.Split(' ')[0] + "%' and " +
                    AllNBAMLBResultsVar + ".StarterALastName = '" + awayStarterCB.Text.Split(' ')[1] + "';";

                    SqlCommand cmdd700COUNT = new SqlCommand(query6AutoProcessCount, connection);
                    SqlDataReader readd700COUNT = cmdd700COUNT.ExecuteReader();

                    while (readd700COUNT.Read())
                    {
                        COUNTGAMES1 = Convert.ToInt16(readd700COUNT["COUNTGAMES"]);
                    }
                    readd700COUNT.Close();

                    if (COUNTGAMES1 < 4)
                    {
                        int addgames = Convert.ToInt32(LastXgamesCB.Text) - COUNTGAMES1;
                        string query6AutoProcessAfterCount = @"INSERT INTO MLBResults(ImeHKluba,HomeStarter,HomeStarterHand,ImeAKluba,AwayStarter,AwayStarterHand,HResult,AResult, IDOklade, IDHKluba, IDAKluba)
                                 SELECT TOP " + addgames + " " + AllNBAMLBResultsVar + ".Team, " + AllNBAMLBResultsVar + ".StarterHLastName , " + AllNBAMLBResultsVar + ".StarterHHand," + AllNBAMLBResultsVar + ".Opp, " + AllNBAMLBResultsVar + ".StarterALastName," + AllNBAMLBResultsVar + ".StarterAHand," +
                              AllNBAMLBResultsVar + ".Hscore, " + AllNBAMLBResultsVar + ".Ascore, '" + IDO + "', '" + IDH + "','" + IDA +
                        "' FROM " + AllNBAMLBResultsVar + ",Klubovi WHERE Klubovi.TeamIsLike IN ('" + til1 + "','" + til13 + "','" + til14 + "') and Opp ='" + AteamShort +
                        "' and season IN ('" + season1 + "','" + season2 + "') and Klubovi.Kratkoime = " + AllNBAMLBResultsVar + ".Team and Klubovi.TeamOUH = '" + TeamOUH + "' and " +
                        AllNBAMLBResultsVar + ".StarterHHand = '" + HomeStarterHand + "' order by " + AllNBAMLBResultsVar + ".ID desc;";
                        SqlCommand cmd112AfterCount = new SqlCommand(query6AutoProcessAfterCount, connection);

                        cmd112AfterCount.ExecuteNonQuery();
                    }

                }
                SqlCommand cmd112 = new SqlCommand(query6AutoProcess, connection);

                cmd112.ExecuteNonQuery();


                //               String query5 = @"INSERT INTO NBAResults(ImeHKluba, ImeAKluba,HResult,AResult, IDOklade, IDHKluba, IDAKluba)
                //         SELECT TOP 9 AllNbaResults.Team, AllNbaResults.Opp,AllNbaResults.Hscore, AllNbaResults.Ascore, '" + IDO + "', '" + IDH + "','" + IDA +
                //          "' FROM AllNBAResults, Klubovi WHERE Team ='" + HteamShort + "' and AIsLike ='" + til2 + "' and season = '" + season1 + "' or Team ='" + HteamShort +
                //          "' and AIsLike ='" + til2 + "' and season = '" + season2 + "' or Team ='" + HteamShort + "' and AIsLike = '" + til23 + "' and season = '" + season1 +
                //          "' or Team ='" + HteamShort + "' and AIsLike = '" + til23 + "' and season = '" + season2 + "'  or Team ='" + HteamShort +
                //          "' and AIsLike = '" + til24 + "' and season = '" + season1 + "' or Team ='" + HteamShort +
                //          "' and AIsLike = '" + til24 + "' and season = '" + season2 + "' order by AllNbaResults.ID desc;";
                //    TeamOUA = "O";

                //                {
                //                    query5AutoProcess = @"INSERT INTO MLBResults(ImeHKluba, HomeStarter, ImeAKluba,AwayStarter,HResult,AResult, IDOklade, IDHKluba, IDAKluba)
                //         SELECT TOP 10 " + AllNBAMLBResultsVar + ".Team, " + AllNBAMLBResultsVar + ".StarterHLastName , " + AllNBAMLBResultsVar + ".Opp," + AllNBAMLBResultsVar + ".StarterALastName, " +
                //                          AllNBAMLBResultsVar + ".Hscore, " + AllNBAMLBResultsVar + ".Ascore, '" + IDO + "', '" + IDH + "','" + IDA +
                //                 "' FROM " + AllNBAMLBResultsVar + ",Klubovi WHERE AisLike IN ('" + til2 + "','" + til23 + "','" + til24 + "') and Team ='" + HteamShort + "' and season IN ('" + season1 +
                //                 "','" + season2 + "') and Klubovi.Kratkoime = " + AllNBAMLBResultsVar + ".Opp and Klubovi.TeamOUH = '" + TeamOUA
                //                 + "' and " + AllNBAMLBResultsVar + ".StarterHLastName = '" + homeStarterCB.Text + "' order by " + AllNBAMLBResultsVar + ".ID desc;";
                //                }

                //HOME TEAM//
                if (radioBtnNBA.Checked == true)
                {
                    query5AutoProcess = @"INSERT INTO NBAResults(Season, Line, Total, ImeHKluba, ImeAKluba,HResult,AResult, IDOklade, IDHKluba, IDAKluba)
         SELECT TOP " + LastXgamesCB.Text + " AllNbaResults.Season,AllNbaResults.Line,AllNbaResults.Total,AllNbaResults.Team, AllNbaResults.Opp,AllNbaResults.Hscore, AllNbaResults.Ascore, '" + IDO + "', '" + IDH + "','" + IDA +
             "' FROM AllNBAResults,Klubovi WHERE " + strRankingsHome + " Team ='" + HteamShort + "' and season IN ('" + season1 +
             "','" + season2 + "') and Klubovi.Kratkoime = AllNbaResults.Opp and Klubovi.TeamOUA like '" + TeamOUA + "' " + pacecbaway + " order by AllNbaResults.ID desc;";
                }
                else
                {
                    query5AutoProcess = @"INSERT INTO MLBResults(ImeHKluba,HomeStarter,HomeStarterHand,ImeAKluba,AwayStarter,AwayStarterHand,HResult,AResult, IDOklade, IDHKluba, IDAKluba)
         SELECT TOP " + LastXgamesCB.Text + " " + AllNBAMLBResultsVar + ".Team, " + AllNBAMLBResultsVar + ".StarterHLastName ,  " + AllNBAMLBResultsVar + ".StarterHHand, " + AllNBAMLBResultsVar + ".Opp," + AllNBAMLBResultsVar + ".StarterALastName," + AllNBAMLBResultsVar + ".StarterAHand, " +
                          AllNBAMLBResultsVar + ".Hscore, " + AllNBAMLBResultsVar + ".Ascore, '" + IDO + "', '" + IDH + "','" + IDA +
                 "' FROM " + AllNBAMLBResultsVar + " WHERE season IN ('" + season1 + "','" + season2 + "') and " + AllNBAMLBResultsVar + ".StarterHFirstName like '" +
                 homeStarterCB.Text.Split(' ')[0] + "%' and " + AllNBAMLBResultsVar + ".StarterHLastName = '" +
                 homeStarterCB.Text.Split(' ')[1] + "' order by " + AllNBAMLBResultsVar + ".ID desc;";

                    //////COUNT GAMES AND IF UNDER 4, ADD 5 GAMES WITH TEAM GAMES AGAINST OPPONENT HAND

                    string query5AutoProcessCount = @"SELECT COUNT(" + AllNBAMLBResultsVar + ".StarterHLastName) AS COUNTGAMES FROM " + AllNBAMLBResultsVar +
                        " WHERE season IN ('" + season1 + "','" + season2 + "') and " +
                        AllNBAMLBResultsVar + ".StarterHFirstName like '" + homeStarterCB.Text.Split(' ')[0] + "%' and " +
                        AllNBAMLBResultsVar + ".StarterHLastName = '" + homeStarterCB.Text.Split(' ')[1] + "';";

                    SqlCommand cmdd700COUNT = new SqlCommand(query5AutoProcessCount, connection);
                    SqlDataReader readd700COUNT = cmdd700COUNT.ExecuteReader();

                    while (readd700COUNT.Read())
                    {
                        COUNTGAMES2 = Convert.ToInt16(readd700COUNT["COUNTGAMES"]);
                    }
                    readd700COUNT.Close();

                    if (COUNTGAMES2 < 4)
                    {
                        int addgames2 = Convert.ToInt32(LastXgamesCB.Text) - COUNTGAMES2;
                        string query5AutoProcessAfterCount = @"INSERT INTO MLBResults(ImeHKluba,HomeStarter,HomeStarterHand,ImeAKluba,AwayStarter,AwayStarterHand,HResult,AResult, IDOklade, IDHKluba, IDAKluba)
         SELECT TOP " + addgames2 + " " + AllNBAMLBResultsVar + ".Team, " + AllNBAMLBResultsVar + ".StarterHLastName ," + AllNBAMLBResultsVar + ".StarterHHand, " + AllNBAMLBResultsVar + ".Opp," + AllNBAMLBResultsVar + ".StarterALastName," + AllNBAMLBResultsVar + ".StarterAHand, " +
                         AllNBAMLBResultsVar + ".Hscore, " + AllNBAMLBResultsVar + ".Ascore, '" + IDO + "', '" + IDH + "','" + IDA +
                "' FROM " + AllNBAMLBResultsVar + ",Klubovi WHERE Klubovi.TeamIsLike IN ('" + til2 + "','" + til23 + "','" + til24 + "') and Team ='" + HteamShort + "' and season IN ('" + season1 +
                "','" + season2 + "') and Klubovi.Kratkoime = " + AllNBAMLBResultsVar + ".Opp and Klubovi.TeamOUA = '" + TeamOUA
                + "' and " + AllNBAMLBResultsVar + ".StarterAHand = '" + AwayStarterHand + "' order by " + AllNBAMLBResultsVar + ".ID desc;";

                        SqlCommand cmd112AfterCount = new SqlCommand(query5AutoProcessAfterCount, connection);

                        cmd112AfterCount.ExecuteNonQuery();
                    }
                }

                SqlCommand cmd111 = new SqlCommand(query5AutoProcess, connection);

                cmd111.ExecuteNonQuery();

                if (radioBtnNBA.Checked == true)
                {
                    sql4AP = "select ID, ImeHKluba,ImeAKluba,HResult,AResult,TotalResult,HandicapResult,Season,Line,Total, HomePace, AwayPace from " + NBAMLBResultsVar + " where IDOklade = '" + IDO + "'";
                }
                else
                {
                    sql4AP = "select ID, ImeHKluba,ImeAKluba,HResult,AResult,TotalResult,HandicapResult,HomeStarter,HomeStarterHand as HH,AwayStarter,AwayStarterHand as AH from " + NBAMLBResultsVar + " where IDOklade = '" + IDO + "'";
                }

                ds = new DataSet();
                adapterMain = new SqlDataAdapter(sql4AP, connection);
                ds.Reset();
                adapterMain.Fill(ds);
                DGVAnalysisResults.DataSource = ds.Tables[0];

                DGVAnalysisResults.Columns[0].Width = 80;

                //Comment and try out the call

                dgva.DGV1Actions(DGVAnalysisResults);


                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                boldStyle.Font = new System.Drawing.Font("Ariel", 9.25F, System.Drawing.FontStyle.Bold);


                DataGridViewRow row = new DataGridViewRow();
                for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
                {
                    if (radioBtnNBA.Checked == true)
                    {
                        string gethomepace = "Select PaceHome from Klubovi where KratkoIme = '" + DGVAnalysisResults.Rows[i].Cells[1].Value.ToString() + "'";
                        string getawaypace = "Select PaceAway from Klubovi where KratkoIme = '" + DGVAnalysisResults.Rows[i].Cells[2].Value.ToString() + "'";

                        SqlCommand cmdhomepace = new SqlCommand(gethomepace, connection);
                        SqlDataReader readhomepace = cmdhomepace.ExecuteReader();
                        while (readhomepace.Read())
                        {
                            dgvHomePace = (readhomepace["PaceHome"].ToString());
                        }
                        readhomepace.Close();

                        SqlCommand cmdawaypace = new SqlCommand(getawaypace, connection);
                        SqlDataReader read4awaypace = cmdawaypace.ExecuteReader();
                        while (read4awaypace.Read())
                        {
                            dgvAwayPace = (read4awaypace["PaceAway"].ToString());
                        }
                        read4awaypace.Close();
                        DGVAnalysisResults.Rows[i].Cells["HomePace"].Value = dgvHomePace;
                        DGVAnalysisResults.Rows[i].Cells["AwayPace"].Value = dgvAwayPace;
                    }
                    if (DGVAnalysisResults.Rows[i].Cells[1].Value.ToString() == HomeTeamCB.Text.Split(' ')[1])
                    {
                        DGVAnalysisResults.Rows[i].Cells[1].Style = boldStyle;
                    }
                    if (DGVAnalysisResults.Rows[i].Cells[2].Value.ToString() == AwayTeamCB.Text.Split(' ')[1])
                    {
                        DGVAnalysisResults.Rows[i].Cells[2].Style = boldStyle;
                    }
                }


                //     dataGridView1.CurrentCell.Selected = false;
                // DataGridViewRow row = new DataGridViewRow();
                for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
                {
                    //String rez1 = dataGridView1.Rows[i].Cells[3].Value.ToString();
                    //String rez2 = dataGridView1.Rows[i].Cells[4].Value.ToString();
                    String rez1 = ds.Tables[0].Rows[i][3].ToString();
                    String rez2 = ds.Tables[0].Rows[i][4].ToString();
                    Double rezz1 = Convert.ToDouble(rez1);
                    Double rezz2 = Convert.ToDouble(rez2);
                    Double tot1 = rezz1 + rezz2;
                    Double hen1 = rezz2 - rezz1;
                    ds.Tables[0].Rows[i][5] = tot1.ToString();
                    ds.Tables[0].Rows[i][6] = hen1.ToString();
                    //dataGridView1.Rows[i].Cells[5].Value = tot1.ToString();
                    //dataGridView1.Rows[i].Cells[6].Value = hen1.ToString();

                }

                for (Int32 i = 0; i <= duplikati; i++)
                {
                    RemoveDuplicate(sender, e);
                }

                SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapterMain);
                adapterMain.Update(ds);

                colorrows(sender, e);

                finishandCalcPB_Click(sender, e);
                //    MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DGVAnalysisResults.Refresh();
                //  button1_Click(sender, e);
                dgv5(sender, e);
                //   Aresult.Text = "";
                //   Hresult.Text = "";
                //   Aresult.Focus();
                HandicapBox.BackColor = Color.White;
                TotalBox.BackColor = Color.White;
                linkk(sender, e);
                this.ActiveControl = null;
                //}
                //catch (Exception ex)
                //{
                //  //  MessageBox.Show(ex.ToString());
                //}
            }
        }

        private void autoProcess_MouseHover_1(object sender, EventArgs e)
        {
            AutoProcessBtn.Cursor = Cursors.Hand;
            AutoProcessBtn.Image = Oklade.Properties.Resources.marketing_automation_icon2;

        }

        private void autoProcess_MouseLeave_1(object sender, EventArgs e)
        {
            AutoProcessBtn.Image = Oklade.Properties.Resources.marketing_automation_icon1;

        }

        private void updateResultsPB_MouseHover(object sender, EventArgs e)
        {
            updateResultsPB.Cursor = Cursors.Hand;
            updateResultsPB.Image = Oklade.Properties.Resources.update2;

        }

        private void updateResultsPB_MouseLeave(object sender, EventArgs e)
        {
            updateResultsPB.Image = Oklade.Properties.Resources.update;

        }

        private void updateResultsPB_Click(object sender, EventArgs e)
        {

            if (TotalBox.Text.Length < 1)
            {
                return;
            }
            else

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();
                    if (TotalBox.Text != " ")
                        if (DGVAnalysisResults.DataSource != null)
                        {
                            {
                                colorrows(sender, e);

                                if (radioBtnNBA.Checked == true)
                                {
                                    query4Update = @"UPDATE NBABets SET Total ='" + TotalBox.Text.ToString().Replace(",", ".") + "' ,Handicap = '" +
                                                                            HandicapBox.Text.ToString().Replace(",", ".") + "', IsOver = '" + isOver + "',IsUnder = '" + isUnder +
                                                                            "', HomeHendi = '" + hHome + "', AwayHendi = '" + hAway + "' WHERE IDOklade = '" + IDoklBox.Text + "'";
                                }
                                else
                                {
                                    query4Update = @"UPDATE MLBBets SET Total ='" + TotalBox.Text.ToString().Replace(",", ".") + "' ,Handicap = '" +
                                                                                HandicapBox.Text.ToString().Replace(",", ".") + "', IsOver = '" + isOver + "',IsUnder = '" + isUnder +
                                                                                "', HomeHendi = '" + hHome + "', AwayHendi = '" + hAway + "' WHERE IDOklade = '" + IDoklBox.Text + "'";
                                }
                                SqlCommand cmd112 = new SqlCommand(query4Update, connection);

                                cmd112.ExecuteNonQuery();

                                dgv2(sender, e);

                                this.ActiveControl = null;
                            }
                        }


                }
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {

            //comboBox1.Text = "";
            //comboBox2.Text = "";
            ImeHKluba = "";
            ImeAKluba = "";
            //awayStarterCB.Text = "";
            //homeStarterCB.Text = "";
            //pictureBox2.Image = null;
            //pictureBox3.Image = null;
            value = "";
            value2 = "";
            //TotalBox.Text = "";
            //HandicapBox.Text = "";
            //teamBox.Text = "";
            //lineBox1.Text = "";
            //lineBox2.Text = "";
            //label27.Text = "";
            //label28.Text = "";
            //label33.Text = "";
            //label34.Text = "";
            //label35.Text = "";
            //label36.Text = "";
            //label38.Text = "";
            //label43.Text = "";




            IDOkl11 = Convert.ToInt32(IDoklBox.Text);
            IDOkl11 = IDOkl11 + 1;

            provjera2();
            GetTeamsandStarters();

            while (ImeAKluba == "" && IDOkl11 < Top1IDOklade)
            {
                IDOkl11 = IDOkl11 + 1;
                GetTeamsandStarters();
            }

            if (ImeAKluba == "")
            {
                return;
            }

            //dataGridView1.DataSource = "";
            //dataGridView2.DataSource = "";
            //dataGridView3.DataSource = "";
            //dataGridView4.DataSource = "";
            //dataGridView5.DataSource = "";

            IDoklBox.Text = IDOkl11.ToString();

            TotalBox.BackColor = Color.White;
            HandicapBox.BackColor = Color.White;

            LoadGameBtn_Click(sender, e);
            colorrows(sender, e);
            Thread.Sleep(500);
            updateResultsPB_Click(sender, e);
        }

        private void provjera2()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                query4PB7 = "Select TOP 1 IDOklade from " + NBAMLBBetsVar + " order by IDOklade desc";

                SqlCommand cmd3 = new SqlCommand(query4PB7, connection);
                SqlDataReader read3 = cmd3.ExecuteReader();

                while (read3.Read())
                {
                    Top1IDOklade = Convert.ToInt32(read3["IDOklade"]);
                }
                read3.Close();
            }
        }

        private void GetTeamsandStarters()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            query4PB7 = "Select ImeHKluba,ImeAKluba from " + NBAMLBBetsVar + " where IDOklade = '" + IDOkl11 + "'";


            SqlCommand cmd3 = new SqlCommand(query4PB7, connection);
            SqlDataReader read3 = cmd3.ExecuteReader();

            while (read3.Read())
            {
                ImeHKluba = (read3["ImeHKluba"].ToString());
                ImeAKluba = (read3["ImeAKluba"].ToString());
            }
            read3.Close();

            if (radioBtnMLB.Checked == true)
            {
                query5PB7 = "Select HomeStarter,AwayStarter from " + NBAMLBBetsVar + " where IDOklade = '" + IDOkl11 + "'";

                SqlCommand cmd4 = new SqlCommand(query5PB7, connection);
                SqlDataReader read4 = cmd4.ExecuteReader();

                while (read4.Read())
                {
                    HomeStarterVar = (read4["HomeStarter"].ToString());
                    AwayStarterVar = (read4["AwayStarter"].ToString());
                }
                read4.Close();
            }
            value = ImeAKluba;
            value2 = ImeHKluba;

            if (ImeAKluba == "")
            {
                connection.Close();
                return;
            }

            AwayTeamCB.Text = value;
            HomeTeamCB.Text = value2;
            awayStarterCB.Text = AwayStarterVar;
            homeStarterCB.Text = HomeStarterVar;


            connection.Close();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            DGVAnalysisResults.DataSource = "";
            dataGridView2.DataSource = "";
            ATeamL10DGV.DataSource = "";
            HTeamL10DGV.DataSource = "";
            dataGridView5.DataSource = "";
            AwayTeamCB.Text = "";
            HomeTeamCB.Text = "";
            ImeHKluba = "";
            ImeAKluba = "";
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            value = "";
            value2 = "";
            TotalBox.Text = "";
            HandicapBox.Text = "";
            teamBox.Text = "";
            lineBox1.Text = "";
            lineBox2.Text = "";
            label27.Text = "";
            label28.Text = "";
            label33.Text = "";
            label34.Text = "";
            label35.Text = "";
            label36.Text = "";
            label38.Text = "";
            label43.Text = "";

            IDOkl11 = Convert.ToInt32(IDoklBox.Text);
            IDOkl11 = IDOkl11 - 1;
            varProvjera = "";

            provjera();

            while (varProvjera.Length < 1)
            {
                IDOkl11 = IDOkl11 - 1;
                provjera();
            }

            IDoklBox.Text = IDOkl11.ToString();

            GetTeamsandStarters();

            TotalBox.BackColor = Color.White;
            HandicapBox.BackColor = Color.White;

            LoadGameBtn_Click(sender, e);

            Thread.Sleep(200);
            updateResultsPB_Click(sender, e);
        }

        private void provjera()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                String queryy = "Select TOP 1 ImeHKluba from " + NBAMLBBetsVar + " where IDOklade = '" + IDOkl11 + "'";
                SqlCommand cmdd = new SqlCommand(queryy, connection);
                SqlDataReader read22 = cmdd.ExecuteReader();

                while (read22.Read())
                {
                    varProvjera = (read22["ImeHKluba"].ToString());
                }
                read22.Close();
            }
        }
        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            pictureBox6.Cursor = Cursors.Hand;
            pictureBox6.Image = Oklade.Properties.Resources.arrow_right2;
        }

        private void pictureBox6_MouseLeave(object sender, EventArgs e)
        {
            pictureBox6.Image = Oklade.Properties.Resources.arrow_right1;
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            pictureBox7.Cursor = Cursors.Hand;
            pictureBox7.Image = Oklade.Properties.Resources.back_arrow2;
        }

        private void pictureBox7_MouseLeave(object sender, EventArgs e)
        {
            pictureBox7.Image = Oklade.Properties.Resources.back_arrow1;
        }


        private void deleteBtn_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                DialogResult d = MessageBox.Show("Are you sure you want to delete this game?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                try
                {

                    if (d == DialogResult.Yes)
                    {

                        str1Delete = "DELETE from " + NBAMLBBetsVar + " where IDOklade=" + IDoklBox.Text + "";

                        SqlCommand cmd = new SqlCommand(str1Delete, connection);
                        cmd.ExecuteNonQuery();


                        str2Delete = "DELETE from " + NBAMLBResultsVar + " where IDOklade=" + IDoklBox.Text + "";

                        SqlCommand cmd2 = new SqlCommand(str2Delete, connection);
                        cmd2.ExecuteNonQuery();
                        DGVAnalysisResults.DataSource = "";
                        dataGridView2.DataSource = "";
                        dataGridView5.DataSource = "";
                        AwayTeamCB.Text = "";
                        HomeTeamCB.Text = "";
                        TotalBox.Text = "";
                        HandicapBox.Text = "";
                        pictureBox2.Hide();
                        pictureBox3.Hide();
                        label45.Text = "";
                        label46.Text = "";
                        label19.Text = "";
                        label21.Text = "";
                        label22.Text = "";
                        label23.Text = "";
                        label24.Text = "";
                        label25.Text = "";
                        label27.Text = "";
                        label28.Text = "";
                        label33.Text = "";
                        label34.Text = "";
                        label35.Text = "";
                        label36.Text = "";
                        label38.Text = "";
                        label12.Text = "";
                        label42.Text = "";
                        ATeamL10DGV.DataSource = "";
                        HTeamL10DGV.DataSource = "";

                        cleanitup(sender, e);
                    }
                    else if (d == DialogResult.No)
                    {

                        return;

                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

            }
        }

        private void pictureBox9_MouseHover(object sender, EventArgs e)
        {
            pictureBox9.Cursor = Cursors.Hand;
            pictureBox9.Image = Oklade.Properties.Resources.Delete2;

        }

        private void pictureBox9_MouseLeave(object sender, EventArgs e)
        {
            pictureBox9.Image = Oklade.Properties.Resources.Delete;

        }

        private void EditTeamsbtn_Click(object sender, EventArgs e)
        {
            cleanitup(sender, e);
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                String Sql5 = "select ID, ImeKluba, TeamIsLike, TeamOUH, TeamOUA, PaceHome, PaceAway, OeffHome, OeffAway,DeffHome,DeffAway from Klubovi where League = '" + LeagueNBAMLB + "' order by ImeKluba";
                ds = new DataSet();
                adapter = new SqlDataAdapter(Sql5, connection);
                ds.Reset();
                adapter.Fill(ds);
                DGVAnalysisResults.DataSource = ds.Tables[0];

                DGVAnalysisResults.Columns[0].Width = 25;
                DGVAnalysisResults.Columns[1].Width = 140;
            }
        }

        private void pictureBox11_MouseHover(object sender, EventArgs e)
        {
            pictureBox11.Cursor = Cursors.Hand;
            pictureBox11.Image = Oklade.Properties.Resources.Edit2;
        }

        private void pictureBox11_MouseLeave(object sender, EventArgs e)
        {
            pictureBox11.Image = Oklade.Properties.Resources.Edit1;

        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                String Sql5 = "select ID, ImeKluba, TeamIsLike, TeamOUH, TeamOUA, PaceHome, PaceAway, OeffHome, OeffAway,DeffHome,DeffAway from Klubovi where League = '" + LeagueNBAMLB + "' order by ImeKluba";
                adapter = new SqlDataAdapter(Sql5, connection);
                SqlCommandBuilder cmd228 = new SqlCommandBuilder(adapter);

                try
                {
                    adapter.Update(ds);
                    MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                NewGameBtn_Click(sender, e);
            }
        }

        private void pictureBox12_MouseHover(object sender, EventArgs e)
        {
            pictureBox12.Cursor = Cursors.Hand;
            pictureBox12.Image = Oklade.Properties.Resources.floppysave2;

        }

        private void pictureBox12_MouseLeave(object sender, EventArgs e)
        {
            pictureBox12.Image = Oklade.Properties.Resources.floppysave1;

        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            ResultUpdater frm3 = new ResultUpdater();
            frm3.Show();
        }

        private void pictureBox13_MouseHover(object sender, EventArgs e)
        {
            pictureBox13.Cursor = Cursors.Hand;
            pictureBox13.Image = Oklade.Properties.Resources.scoreboard2;

        }

        private void pictureBox13_MouseLeave(object sender, EventArgs e)
        {
            pictureBox13.Image = Oklade.Properties.Resources.scoreboard1;

        }

        private void finishandCalcPB_Click(object sender, EventArgs e)
        {
            getGamesBtn.Enabled = false;
            PickGamesBtn.Enabled = false;

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (IDH == null)
                {
                    String query3 = "Select ID,ParkFactor from Klubovi where ImeKluba = '" + value2 + "'";
                    String query4 = "Select ID from Klubovi where ImeKluba = '" + value + "'";
                    SqlCommand cmd3 = new SqlCommand(query3, connection);
                    SqlDataReader read3 = cmd3.ExecuteReader();
                    //try
                    //{
                    while (read3.Read())
                    {
                        IDH = (read3["ID"].ToString());
                        ParkFactor = Convert.ToDouble(read3["ParkFactor"]);
                    }
                    read3.Close();

                    SqlCommand cmd4 = new SqlCommand(query4, connection);
                    SqlDataReader read4 = cmd4.ExecuteReader();
                    while (read4.Read())
                    {
                        IDA = (read4["ID"].ToString());
                    }
                    read4.Close();
                }


                timenoww = DateTime.Now.ToString("dd.MM.yyyy.");
                string timenow = DateTime.Now.ToShortTimeString();
                string timenow2 = Regex.Replace(timenow, ":", "").Trim();
                int timenow3 = Convert.ToInt16(timenow2);

                if (timenow3 < 300)
                {
                    timenoww = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy.");
                }

                try
                {
                    if (radioBtnNBA.Checked == true)
                    {
                        query4PB14 = @"INSERT INTO NBABets (IDHKluba, IDAKluba, IDOklade, ImeHKluba, ImeAKluba, Total,Handicap, IsOver,IsUnder, HomeHendi, AwayHendi, Datum, ToBetOn, BetProcessed, Edited)
                                                      VALUES ( '" + IDH + "','" + IDA + "','" + IDO + "','" + value2 + "','" + value + "','" + TotalBox.Text.ToString().Replace(",", ".") + "','" +
                                                       HandicapBox.Text.ToString().Replace(",", ".") + "', '" + isOver + "', '" + isUnder + "', '" + hHome + "','" + hAway + "', '" +
                                                                              timenoww + "', 0 , 0, 0);";
                    }
                    else if (radioBtnMLB.Checked == true)
                    {
                        query4PB14 = @"INSERT INTO MLBBets (IDHKluba,HomeStarter, IDAKluba, AwayStarter, IDOklade, ImeHKluba, ImeAKluba, Total,Handicap, IsOver,IsUnder, HomeHendi, AwayHendi, Datum, ParkFactor, ToBetOn, BetProcessed, Edited)
                                                      VALUES ( '" + IDH + "','" + homeStarterCB.Text + "','" + IDA + "','" + awayStarterCB.Text + "','" + IDO + "','" + value2 + "','" + value + "','" +
                                                                   TotalBox.Text.ToString().Replace(",", ".") + "','" + HandicapBox.Text.ToString().Replace(",", ".") +
                                                                   "', '" + isOver + "', '" + isUnder + "', '" + hHome + "','" + hAway + "', '" +
                                                                              timenoww + "', '" + ParkFactor.ToString().Replace(",", ".") + "', 0 , 0, 0);";
                    }

                    SqlCommand cmd112 = new SqlCommand(query4PB14, connection);

                    cmd112.ExecuteNonQuery();

                    dgv2(sender, e);

                    // linkk(sender, e);

                    LoadGameBtn_Click(sender, e);

                    isOver = 0;
                    isUnder = 0;
                    hAway = 0;
                    hHome = 0;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void pictureBox14_MouseHover(object sender, EventArgs e)
        {
            finishandCalcPB.Cursor = Cursors.Hand;
            finishandCalcPB.Image = Oklade.Properties.Resources.finishcalc2;

        }

        private void pictureBox14_MouseLeave(object sender, EventArgs e)
        {
            finishandCalcPB.Image = Oklade.Properties.Resources.finishcalc1;

        }

        private void pictureBox15_Click(object sender, EventArgs e)
        {
            //String str1 = "http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/injury/injuries.html";
            //System.Diagnostics.Process.Start(str1);


        }


        private void pictureBox15_MouseHover(object sender, EventArgs e)
        {
            pictureBox15.Cursor = Cursors.Hand;
            pictureBox15.Image = Oklade.Properties.Resources.personal_injury_icon2;

        }

        private void pictureBox15_MouseLeave(object sender, EventArgs e)
        {
            pictureBox15.Image = Oklade.Properties.Resources.personal_injury_icon1;
        }

        private void cleanitup(object sender, EventArgs e)
        {
            getGamesBtn.Enabled = false;
            PickGamesBtn.Enabled = false;

            StartersStatsDGV.DataSource = null;
            DGVAnalysisResults.DataSource = null;
            dataGridView2.DataSource = null;
            ATeamL10DGV.DataSource = null;
            HTeamL10DGV.DataSource = null;
            dataGridView5.DataSource = null;
            TeamsStatsDGV.DataSource = null;
            teamsDetailedStatsDGV.DataSource = null;
            AStarterL10DGV.DataSource = null;
            TeamsStatsDGV.Rows.Clear();
            teamsDetailedStatsDGV.Rows.Clear();

            if (ToBetOnCB.Checked == true)
            {
                ToBetOnCB.Checked = false;
            }
            if (BetProcessedCB.Checked == true)
            {
                BetProcessedCB.Checked = false;
            }
            if (editedCB.Checked == true)
            {
                editedCB.Checked = false;
            }

            TotalBox.Enabled = false;
            HandicapBox.Enabled = false;

            if (AwayTeamCB.Text.Length > 1)
            {
                AwayTeamCB.Text = "-";
                HomeTeamCB.Text = "-";
            }

            //ImeHKluba = "";
            //ImeAKluba = "";
            AStarterHandLabel.Text = "";
            HStarterHandLabel.Text = "";
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            value = "";
            value2 = "";
            TotalBox.Text = "";
            HandicapBox.Text = "";
            teamBox.Text = "";
            lineBox1.Text = "";
            lineBox2.Text = "";
            pictureBox2.Hide();
            pictureBox3.Hide();
            label45.Text = "";
            label46.Text = "";
            label19.Text = "";
            label21.Text = "";
            label22.Text = "";
            label23.Text = "";
            label24.Text = "";
            label25.Text = "";
            label27.Text = "";
            label28.Text = "";
            label33.Text = "";
            label34.Text = "";
            label35.Text = "";
            label36.Text = "";
            label38.Text = "";
            label12.Text = "";
            label42.Text = "";
            label43.Text = "";
            label45.Text = "";
            label46.Text = "";
            L47.Text = "";
            richTextBox1.Text = "";
            richTextBox3.Text = "";

            if (radioBtnMLB.Checked == true)
            {
                awayStarterCB.Text = "";
                awayStarterCB.Enabled = false;
                homeStarterCB.Text = "";
                homeStarterCB.Enabled = false;
            }
            if (richTextBox3.Controls.Contains(linkLabel1))
            {
                richTextBox3.Controls.Remove(linkLabel1);
            }
            if (richTextBox3.Controls.Contains(linkLabel2))
            {
                richTextBox3.Controls.Remove(linkLabel2);
            }
            if (richTextBox3.Controls.Contains(linkLabel3))
            {
                richTextBox3.Controls.Remove(linkLabel3);
            }
            if (richTextBox3.Controls.Contains(linkLabel4))
            {
                richTextBox3.Controls.Remove(linkLabel4);
            }
            if (richTextBox3.Controls.Contains(linkLabel5))
            {
                richTextBox3.Controls.Remove(linkLabel5);
            }
            if (richTextBox3.Controls.Contains(linkLabel6))
            {
                richTextBox3.Controls.Remove(linkLabel6);
            }
            if (richTextBox3.Controls.Contains(linkLabel7))
            {
                richTextBox3.Controls.Remove(linkLabel7);
            }
            if (richTextBox1.Controls.Contains(linkLabel8))
            {
                richTextBox1.Controls.Remove(linkLabel8);
            }
            if (richTextBox1.Controls.Contains(linkLabel9))
            {
                richTextBox1.Controls.Remove(linkLabel9);
            }
            if (richTextBox1.Controls.Contains(linkLabel10))
            {
                richTextBox1.Controls.Remove(linkLabel10);
            }
            if (richTextBox1.Controls.Contains(linkLabel11))
            {
                richTextBox1.Controls.Remove(linkLabel11);
            }
            if (richTextBox1.Controls.Contains(linkLabel12))
            {
                richTextBox1.Controls.Remove(linkLabel12);
            }
            if (richTextBox1.Controls.Contains(linkLabel13))
            {
                richTextBox1.Controls.Remove(linkLabel13);
            }
            if (richTextBox1.Controls.Contains(linkLabel14))
            {
                richTextBox1.Controls.Remove(linkLabel14);
            }


        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            cleanitup(sender, e);

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    { DGVAnalysisResults.Columns.Remove("chk2"); }

                }

                string Datum2 = dateTimePicker1.Value.Date.AddDays(-1).ToShortDateString();
                dateTimePicker1.Text = Datum2;

            }
        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            cleanitup(sender, e);

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    { DGVAnalysisResults.Columns.Remove("chk2"); }

                }

                string Datum2 = dateTimePicker1.Value.Date.AddDays(+1).ToShortDateString();
                dateTimePicker1.Text = Datum2;


            }
        }

        private void pictureBox16_MouseHover(object sender, EventArgs e)
        {
            prevPB.Cursor = Cursors.Hand;
            prevPB.Image = Oklade.Properties.Resources.back_arrow2;
        }

        private void pictureBox16_MouseLeave(object sender, EventArgs e)
        {
            prevPB.Image = Oklade.Properties.Resources.back_arrow1;
        }

        private void pictureBox17_MouseHover(object sender, EventArgs e)
        {
            nextPB.Cursor = Cursors.Hand;
            nextPB.Image = Oklade.Properties.Resources.arrow_right2;
        }

        private void pictureBox17_MouseLeave(object sender, EventArgs e)
        {
            nextPB.Image = Oklade.Properties.Resources.arrow_right1;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            string linkawayteam = "http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/teams/team" + linkat + ".html";
            Process.Start(linkawayteam);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            string linkhometeam = "http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/teams/team" + linkht + ".html";
            Process.Start(linkhometeam);
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            pictureBox2.Cursor = Cursors.Hand;
            pictureBox2.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            pictureBox3.Cursor = Cursors.Hand;
            pictureBox3.BorderStyle = BorderStyle.Fixed3D;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            pictureBox2.BorderStyle = BorderStyle.None;
        }

        private void pictureBox3_MouseLeave(object sender, EventArgs e)
        {
            pictureBox3.BorderStyle = BorderStyle.None;
        }
        private void InjuryAway(object sender, EventArgs e)
        {
            if (radioBtnNBA.Checked == true)
            {

                string teamname = AwayTeamCB.Text;

                if (teamname == "LA Lakers")
                {
                    teamname = "Los Angeles Lakers";
                }
                else if (teamname == "LA Clippers")
                {
                    teamname = "Los Angeles Clippers";
                }
                else if (teamname == "OKC Thunder")
                {
                    teamname = "Oklahoma City";
                }
                else if (teamname == "GS Warriors")
                {
                    teamname = "Golden State";
                }
                else if (teamname == "NewOrleans Pelicans")
                {
                    teamname = "New Orleans";
                }
                else if (teamname == "NewYork Knicks")
                {
                    teamname = "New York";
                }
                else if (teamname == "SanAntonio Spurs")
                {
                    teamname = "San Antonio";
                }
                else if (teamname == "Min Timberwolves")
                {
                    teamname = "Minnesota";
                }
                else if (teamname == "Phi Seventysixers")
                {
                    teamname = "Philadelphia";
                }
                else if (teamname == "Por Trailblazers")
                {
                    teamname = "Portland";
                }


                string teamname2 = "";

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();


                    String queryy339 = "Select ID from KluboviForInjuries where ImeKluba = '" + AwayTeamCB.Text + "'";
                    SqlCommand cmdd339 = new SqlCommand(queryy339, connection);
                    SqlDataReader readd339 = cmdd339.ExecuteReader();
                    while (readd339.Read())
                    {
                        IDKlubaInj = (readd339["ID"].ToString());
                    }
                    readd339.Close();

                    int IDKlubaInj2 = Convert.ToInt16(IDKlubaInj);
                    IDKlubaInj2 = IDKlubaInj2 + 1;

                    String queryy33 = "Select ImeKluba from KluboviForInjuries where ID = '" + IDKlubaInj2 + "'";
                    SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                    SqlDataReader readd33 = cmdd33.ExecuteReader();
                    while (readd33.Read())
                    {
                        teamname2 = (readd33["ImeKluba"].ToString());
                    }
                    readd33.Close();

                    if (IDKlubaInj2 == 31)
                    {
                        teamname2 = "Washington Wizards";
                    }

                    //  teamname2 = teamname2.Split(' ')[1];
                    if (teamname2 == "LA Lakers")
                    {
                        teamname2 = "Los Angeles Lakers";
                    }
                    else if (teamname2 == "LA Clippers")
                    {
                        teamname2 = "Los Angeles Clippers";
                    }
                    else if (teamname2 == "OKC Thunder")
                    {
                        teamname2 = "Oklahoma City";
                    }
                    else if (teamname2 == "GS Warriors")
                    {
                        teamname2 = "Golden State";
                    }
                    else if (teamname2 == "NewOrleans Pelicans")
                    {
                        teamname2 = "New Orleans";
                    }
                    else if (teamname2 == "NewYork Knicks")
                    {
                        teamname2 = "New York";
                    }
                    else if (teamname2 == "SanAntonio Spurs")
                    {
                        teamname2 = "San Antonio";
                    }
                    else if (teamname2 == "Min Timberwolves")
                    {
                        teamname2 = "Minnesota";
                    }
                    else if (teamname2 == "Phi Seventysixers")
                    {
                        teamname2 = "Philadelphia";
                    }
                    else if (teamname2 == "Por Trailblazers")
                    {
                        teamname2 = "Portland";
                    }

                    //WebClient wc = new WebClient();
                    //string htmlString = wc.DownloadString("http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/injury/injuries.html");
                    int i = 1;
                    string team = "";
                    string team2 = "";

                    if (htmlString.Length > 1)
                    {
                        Match mTeam = Regex.Match(htmlString, @"" + teamname + "(.*?)</td>", RegexOptions.Singleline);
                        if (mTeam.Success)
                        {
                            team = mTeam.Groups[i].Value;
                            //   //Console.WriteLine(team);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        // team = Regex.Replace(team, "(.*?)>", "").Trim();
                        index = mTeam.Index;
                        //Console.WriteLine("Index of match: " + index);
                        //Console.WriteLine("Team 1: " + team);

                        int duzinastringa = htmlString.Length;
                        duzinastringa = duzinastringa - index;

                        if (IDKlubaInj2 != 31)
                        {
                            Match mTeam2 = Regex.Match(htmlString.Substring(index, duzinastringa), @"" + teamname2 + "(.*?)</tr>", RegexOptions.Singleline);
                            {
                                team2 = mTeam2.Groups[i].Value;
                                //   //Console.WriteLine(team);
                            }
                            //Console.WriteLine("***************************************************************");
                            //manicure the pattern string
                            //Replace everything before the last (greater than) sign with and empty string
                            //  team2 = Regex.Replace(team2, "(.*?)>", "").Trim();
                            index2 = mTeam2.Index;
                            //Console.WriteLine("Index of match: " + index2);
                            //Console.WriteLine("Team 2: " + team2);
                            //Console.WriteLine("***************************************************************");
                        }

                        if (IDKlubaInj2 == 31)
                        {
                            index2 = duzinastringa;
                        }

                        string playername = "";
                        string playername2 = "";
                        string playername3 = "";
                        string playername4 = "";
                        string playername5 = "";
                        string playername6 = "";
                        string playername7 = "";
                        Match mPlayer = Regex.Match(htmlString.Substring(index, index2), @".html"">(.*?)</a>", RegexOptions.Singleline);
                        if (mPlayer.Success)
                        {
                            playername = mPlayer.Groups[1].Value;
                            // //Console.WriteLine(playername);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        playername = Regex.Replace(playername, "(.*?)>", "").Trim();
                        int index3 = mPlayer.Index;
                        //Console.WriteLine("Index of match: " + index3);
                        //Console.WriteLine("Player 1:" + playername);


                        ////////////////////////////////////////////////////////////////

                        if (playername.Length > 1)
                        {
                            mPlayer = mPlayer.NextMatch();
                            if (mPlayer.Success)
                            {
                                playername2 = mPlayer.Groups[1].Value;
                                playername2 = Regex.Replace(playername2, "(.*?)>", "").Trim();
                                index4 = mPlayer.Index;
                                ////Console.WriteLine("Index of match: " + index4);
                                ////Console.WriteLine("Player 2:" + playername2);
                            }
                            if (playername2.Length > 1)
                            {
                                mPlayer = mPlayer.NextMatch();
                                if (mPlayer.Success)
                                {
                                    playername3 = mPlayer.Groups[1].Value;
                                    playername3 = Regex.Replace(playername3, "(.*?)>", "").Trim();
                                    index5 = mPlayer.Index;
                                    ////Console.WriteLine("Index of match: " + index5);
                                    ////Console.WriteLine("Player 3:" + playername3);
                                }
                                if (playername3.Length > 1)
                                {
                                    mPlayer = mPlayer.NextMatch();
                                    if (mPlayer.Success)
                                    {
                                        playername4 = mPlayer.Groups[1].Value;
                                        playername4 = Regex.Replace(playername4, "(.*?)>", "").Trim();

                                        index6 = mPlayer.Index;
                                        ////Console.WriteLine("Index of match: " + index6);
                                        ////Console.WriteLine("Player 4:" + playername4);
                                    }
                                    if (playername4.Length > 1)
                                    {
                                        mPlayer = mPlayer.NextMatch();
                                        if (mPlayer.Success)
                                        {
                                            playername5 = mPlayer.Groups[1].Value;
                                            playername5 = Regex.Replace(playername5, "(.*?)>", "").Trim();

                                            index7 = mPlayer.Index;
                                            //Console.WriteLine("Index of match: " + index7);
                                            //Console.WriteLine("Player 5:" + playername5);
                                        }
                                        if (playername5.Length > 1)
                                        {
                                            mPlayer = mPlayer.NextMatch();
                                            if (mPlayer.Success)
                                            {
                                                playername6 = mPlayer.Groups[1].Value;
                                                playername6 = Regex.Replace(playername6, "(.*?)>", "").Trim();

                                                index8 = mPlayer.Index;
                                                //Console.WriteLine("Index of match: " + index8);
                                                //Console.WriteLine("Player 6:" + playername6);
                                            }

                                            if (playername6.Length > 1)
                                            {
                                                mPlayer = mPlayer.NextMatch();
                                                if (mPlayer.Success)
                                                {
                                                    playername7 = mPlayer.Groups[1].Value;
                                                    playername7 = Regex.Replace(playername7, "(.*?)>", "").Trim();

                                                    int index9 = mPlayer.Index;
                                                    //Console.WriteLine("Index of match: " + index9);
                                                    //Console.WriteLine("Player 7:" + playername7);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        if (IDKlubaInj2 == 31)
                        {
                            index2 = duzinastringa - index4;
                        }

                        if (playername.Length > 1)
                        {
                            Match mLink = Regex.Match(htmlString.Substring(index, index2), @"<a href=""(.*?)"">" + playername + "", RegexOptions.Singleline);
                            if (mLink.Success)
                            {
                                link11 = mLink.Groups[1].Value;
                                //Console.WriteLine(link11);
                            }
                            link11 = "http://www.covers.com" + link11;

                            if (playername2.Length > 1)
                            {
                                if (IDKlubaInj2 == 31)
                                {
                                    index2 = duzinastringa - index4;
                                }
                                position = index + index4 - 90;
                                Match mLink2 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername2 + "", RegexOptions.Singleline);
                                if (mLink2.Success)
                                {
                                    link12 = mLink2.Groups[1].Value;
                                    //Console.WriteLine(link12);
                                }
                                link12 = "http://www.covers.com" + link12;

                                if (playername3.Length > 1)
                                {
                                    if (IDKlubaInj2 == 31)
                                    {
                                        index2 = duzinastringa - index5;
                                    }
                                    position = index + index5 - 90;
                                    Match mLink3 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername3 + "", RegexOptions.Singleline);
                                    if (mLink3.Success)
                                    {
                                        link13 = mLink3.Groups[1].Value;
                                        //Console.WriteLine(link13);
                                    }
                                    link13 = "http://www.covers.com" + link13;

                                    if (playername4.Length > 1)
                                    {
                                        if (IDKlubaInj2 == 31)
                                        {
                                            index2 = duzinastringa - index6;
                                        }
                                        position = index + index6 - 90;
                                        Match mLink4 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername4 + "", RegexOptions.Singleline);
                                        if (mLink4.Success)
                                        {
                                            link14 = mLink4.Groups[1].Value;
                                            //Console.WriteLine(link14);
                                        }
                                        link14 = "http://www.covers.com" + link14;

                                        if (playername5.Length > 1)
                                        {
                                            if (IDKlubaInj2 == 31)
                                            {
                                                index2 = duzinastringa - index7;
                                            }
                                            position = index + index7 - 90;
                                            Match mLink5 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername5 + "", RegexOptions.Singleline);
                                            if (mLink5.Success)
                                            {
                                                link15 = mLink5.Groups[1].Value;
                                                //Console.WriteLine(link15);
                                            }
                                            link15 = "http://www.covers.com" + link15;

                                            if (playername6.Length > 1)
                                            {
                                                if (IDKlubaInj2 == 31)
                                                {
                                                    index2 = duzinastringa - index8;
                                                }
                                                position = index + index8 - 90;
                                                Match mLink6 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername6 + "", RegexOptions.Singleline);
                                                if (mLink6.Success)
                                                {
                                                    link16 = mLink6.Groups[1].Value;
                                                    //Console.WriteLine(link16);
                                                }
                                                link16 = "http://www.covers.com" + link16;

                                                if (playername7.Length > 1)
                                                {
                                                    if (IDKlubaInj2 == 31)
                                                    {
                                                        index2 = duzinastringa - index9;
                                                    }
                                                    position = index + index9 - 90;
                                                    Match mLink7 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername7 + "", RegexOptions.Singleline);
                                                    if (mLink7.Success)
                                                    {
                                                        link17 = mLink7.Groups[1].Value;
                                                        //Console.WriteLine(link17);
                                                    }
                                                    link17 = "http://www.covers.com" + link17;
                                                }

                                            }
                                        }
                                    }
                                }
                            }

                        }


                        /////////////////////////////////////////////////////////////////////////
                        string injuryreason = "";
                        string injuryreason2 = "";
                        string injuryreason3 = "";
                        string injuryreason4 = "";
                        string injuryreason5 = "";
                        string injuryreason6 = "";
                        string injuryreason7 = "";
                        Match mInjury = Regex.Match(htmlString.Substring(index, index2), @"<strong>(.*?)</td>", RegexOptions.Singleline);
                        if (mInjury.Success)
                        {
                            injuryreason = mInjury.Groups[1].Value;
                            // //Console.WriteLine(playername);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        injuryreason = Regex.Replace(injuryreason, "</strong>", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, "/+", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, @"[\d]", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, @"&#;", "'");
                        if (injuryreason.Length > 1)
                        {
                            int duzina = injuryreason.Length - 2;
                            injuryreason = injuryreason.Substring(0, duzina);
                        }
                        //Console.WriteLine("Injury reason of player 1:" + injuryreason);

                        if (injuryreason.Length > 1)
                        {
                            mInjury = mInjury.NextMatch();
                            if (mInjury.Success)
                            {
                                injuryreason2 = mInjury.Groups[1].Value;
                                injuryreason2 = Regex.Replace(injuryreason2, "</strong>", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, "/+", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, @"[\d]", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, @"&#;", "'");
                                int duzina2 = injuryreason2.Length - 2;
                                injuryreason2 = injuryreason2.Substring(0, duzina2);
                                //Console.WriteLine("Injury reason of player 2:" + injuryreason2);
                            }
                            if (injuryreason2.Length > 1)
                            {
                                mInjury = mInjury.NextMatch();
                                if (mInjury.Success)
                                {
                                    injuryreason3 = mInjury.Groups[1].Value;
                                    injuryreason3 = Regex.Replace(injuryreason3, "</strong>", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, "/+", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, @"[\d]", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, @"&#;", "'");
                                    int duzina3 = injuryreason3.Length - 2;
                                    injuryreason3 = injuryreason3.Substring(0, duzina3);

                                    //Console.WriteLine("Injury reason of player 3:" + injuryreason3);
                                }

                                if (injuryreason3.Length > 1)
                                {
                                    mInjury = mInjury.NextMatch();
                                    if (mInjury.Success)
                                    {
                                        injuryreason4 = mInjury.Groups[1].Value;
                                        injuryreason4 = Regex.Replace(injuryreason4, "</strong>", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, "/+", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, @"[\d]", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, @"&#;", "'");
                                        int duzina4 = injuryreason4.Length - 2;
                                        injuryreason4 = injuryreason4.Substring(0, duzina4);

                                        //Console.WriteLine("Injury reason of player 4:" + injuryreason4);
                                    }

                                    if (injuryreason4.Length > 1)
                                    {
                                        mInjury = mInjury.NextMatch();
                                        if (mInjury.Success)
                                        {
                                            injuryreason5 = mInjury.Groups[1].Value;
                                            injuryreason5 = Regex.Replace(injuryreason5, "</strong>", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, "/+", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, @"[\d]", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, @"&#;", "'");
                                            int duzina5 = injuryreason5.Length - 2;
                                            injuryreason5 = injuryreason5.Substring(0, duzina5);

                                            //Console.WriteLine("Injury reason of player 5:" + injuryreason5);
                                        }

                                        if (injuryreason5.Length > 1)
                                        {
                                            mInjury = mInjury.NextMatch();
                                            if (mInjury.Success)
                                            {
                                                injuryreason6 = mInjury.Groups[1].Value;
                                                injuryreason6 = Regex.Replace(injuryreason6, "</strong>", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, "/+", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, @"[\d]", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, @"&#;", "'");
                                                int duzina6 = injuryreason6.Length - 2;
                                                injuryreason6 = injuryreason6.Substring(0, duzina6);

                                                //Console.WriteLine("Injury reason of player 4:" + injuryreason6);
                                            }

                                            if (injuryreason6.Length > 1)
                                            {
                                                mInjury = mInjury.NextMatch();
                                                if (mInjury.Success)
                                                {
                                                    injuryreason7 = mInjury.Groups[1].Value;
                                                    injuryreason7 = Regex.Replace(injuryreason7, "</strong>", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, "/+", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, @"[\d]", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, @"&#;", "'");
                                                    int duzina7 = injuryreason7.Length - 2;
                                                    injuryreason7 = injuryreason7.Substring(0, duzina7);

                                                    //Console.WriteLine("Injury reason of player 7:" + injuryreason7);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }





                        if (richTextBox1.Controls.Contains(linkLabel8))
                        {
                            richTextBox1.Controls.Remove(linkLabel8);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel9))
                        {
                            richTextBox1.Controls.Remove(linkLabel9);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel10))
                        {
                            richTextBox1.Controls.Remove(linkLabel10);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel11))
                        {
                            richTextBox1.Controls.Remove(linkLabel11);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel12))
                        {
                            richTextBox1.Controls.Remove(linkLabel12);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel13))
                        {
                            richTextBox1.Controls.Remove(linkLabel13);
                        }
                        if (richTextBox1.Controls.Contains(linkLabel14))
                        {
                            richTextBox1.Controls.Remove(linkLabel14);
                        }

                        if (playername.Length < 1)
                        {
                            richTextBox1.Text = "No injuries to report.";
                        }

                        if (playername.Length > 1 && playername != "Advertising")
                        {
                            richTextBox1.Text = "    " + AwayTeamCB.Text.Split(' ')[1] + " Injuries:\n=>";
                            linkLabel8.Text = playername;
                            linkLabel8.AutoSize = true;
                            linkLabel8.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                            richTextBox1.Controls.Add(linkLabel8);
                            richTextBox1.AppendText(linkLabel8.Text + "\n  " + injuryreason + "\n");
                            richTextBox1.SelectionStart = richTextBox1.TextLength;

                            if (playername2.Length > 1 && playername2 != "Advertising")
                            {
                                linkLabel9.Text = playername2;
                                richTextBox1.AppendText("=>");
                                linkLabel9.AutoSize = true;
                                linkLabel9.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                richTextBox1.Controls.Add(linkLabel9);
                                richTextBox1.AppendText("\n  " + injuryreason2 + "\n");
                                richTextBox1.SelectionStart = richTextBox1.TextLength;

                                if (playername3.Length > 1 && playername3 != "Advertising")
                                {
                                    linkLabel10.Text = playername3;
                                    richTextBox1.AppendText("=>");
                                    linkLabel10.AutoSize = true;
                                    linkLabel10.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                    richTextBox1.Controls.Add(linkLabel10);
                                    richTextBox1.AppendText("\n  " + injuryreason3 + "\n");
                                    richTextBox1.SelectionStart = richTextBox1.TextLength;

                                    if (playername4.Length > 1 && playername4 != "Advertising")
                                    {
                                        linkLabel11.Text = playername4;
                                        richTextBox1.AppendText("=>");
                                        linkLabel11.AutoSize = true;
                                        linkLabel11.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                        richTextBox1.Controls.Add(linkLabel11);
                                        richTextBox1.AppendText("\n  " + injuryreason4 + "\n");
                                        richTextBox1.SelectionStart = richTextBox1.TextLength;

                                        if (playername5.Length > 1 && playername5 != "Advertising")
                                        {
                                            linkLabel12.Text = playername5;
                                            richTextBox1.AppendText("=>");
                                            linkLabel12.AutoSize = true;
                                            linkLabel12.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                            richTextBox1.Controls.Add(linkLabel12);
                                            richTextBox1.AppendText("\n  " + injuryreason5 + "\n");
                                            richTextBox1.SelectionStart = richTextBox1.TextLength;

                                            if (playername6.Length > 1 && playername6 != "Advertising")
                                            {
                                                linkLabel13.Text = playername6;
                                                richTextBox1.AppendText("=>");
                                                linkLabel13.AutoSize = true;
                                                linkLabel13.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                                richTextBox1.Controls.Add(linkLabel13);
                                                richTextBox1.AppendText("\n  " + injuryreason6 + "\n");
                                                richTextBox1.SelectionStart = richTextBox1.TextLength;

                                                if (playername7.Length > 1 && playername7 != "Advertising")
                                                {
                                                    linkLabel14.Text = playername7;
                                                    richTextBox1.AppendText("=>");
                                                    linkLabel14.AutoSize = true;
                                                    linkLabel14.Location = richTextBox1.GetPositionFromCharIndex(richTextBox1.TextLength);
                                                    richTextBox1.Controls.Add(linkLabel14);
                                                    richTextBox1.AppendText("\n  " + injuryreason7 + "\n");
                                                    richTextBox1.SelectionStart = richTextBox1.TextLength;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        richTextBox1.Text = Regex.Replace(richTextBox1.Text, @"=>Advertising", "");

                    }

                    else
                    {
                        richTextBox1.Text = "Can't retreve injuries information.\nCheck your internet connection!";
                    }

                }
            }
        }
        private void InjuryHome(object sender, EventArgs e)
        {
            if (radioBtnNBA.Checked == true)
            {
                string teamname = HomeTeamCB.Text;
                if (teamname == "LA Lakers")
                {
                    teamname = "Los Angeles Lakers";
                }
                else if (teamname == "LA Clippers")
                {
                    teamname = "Los Angeles Clippers";
                }
                else if (teamname == "OKC Thunder")
                {
                    teamname = "Oklahoma City";
                }
                else if (teamname == "GS Warriors")
                {
                    teamname = "Golden State";
                }
                else if (teamname == "NewOrleans Pelicans")
                {
                    teamname = "New Orleans";
                }
                else if (teamname == "NewYork Knicks")
                {
                    teamname = "New York";
                }
                else if (teamname == "SanAntonio Spurs")
                {
                    teamname = "San Antonio";
                }
                else if (teamname == "Min Timberwolves")
                {
                    teamname = "Minnesota";
                }
                else if (teamname == "Phi Seventysixers")
                {
                    teamname = "Philadelphia";
                }
                else if (teamname == "Por Trailblazers")
                {
                    teamname = "Portland";
                }

                string teamname2 = "";

                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();


                    String queryy339 = "Select ID from KluboviForInjuries where ImeKluba = '" + HomeTeamCB.Text + "'";
                    SqlCommand cmdd339 = new SqlCommand(queryy339, connection);
                    SqlDataReader readd339 = cmdd339.ExecuteReader();
                    while (readd339.Read())
                    {
                        IDKlubaInj = (readd339["ID"].ToString());
                    }
                    readd339.Close();

                    int IDKlubaInj2 = Convert.ToInt16(IDKlubaInj);
                    IDKlubaInj2 = IDKlubaInj2 + 1;

                    String queryy33 = "Select ImeKluba from KluboviForInjuries where ID = '" + IDKlubaInj2 + "'";
                    SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                    SqlDataReader readd33 = cmdd33.ExecuteReader();
                    while (readd33.Read())
                    {
                        teamname2 = (readd33["ImeKluba"].ToString());
                    }
                    readd33.Close();

                    if (IDKlubaInj2 == 31)
                    {
                        teamname2 = "Washington Wizards";
                    }

                    //  teamname2 = teamname2.Split(' ')[0];
                    if (teamname2 == "LA Lakers")
                    {
                        teamname2 = "Los Angeles Lakers";
                    }
                    else if (teamname2 == "LA Clippers")
                    {
                        teamname2 = "Los Angeles Clippers";
                    }
                    else if (teamname2 == "OKC Thunder")
                    {
                        teamname2 = "Oklahoma City";
                    }
                    else if (teamname2 == "GS Warriors")
                    {
                        teamname2 = "Golden State";
                    }
                    else if (teamname2 == "NewOrleans Pelicans")
                    {
                        teamname2 = "New Orleans";
                    }
                    else if (teamname2 == "NewYork Knicks")
                    {
                        teamname2 = "New York";
                    }
                    else if (teamname2 == "SanAntonio Spurs")
                    {
                        teamname2 = "San Antonio";
                    }
                    else if (teamname2 == "Min Timberwolves")
                    {
                        teamname2 = "Minnesota";
                    }
                    else if (teamname2 == "Phi Seventysixers")
                    {
                        teamname2 = "Philadelphia";
                    }
                    else if (teamname2 == "Por Trailblazers")
                    {
                        teamname2 = "Portland";
                    }

                    //WebClient wc = new WebClient();
                    //string htmlString = wc.DownloadString("http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/injury/injuries.html");
                    int i = 1;
                    string team = "";
                    string team2 = "";


                    if (htmlString.Length > 1)
                    {
                        Match mTeam = Regex.Match(htmlString, @"" + teamname + "(.*?)</td>", RegexOptions.Singleline);
                        if (mTeam.Success)
                        {
                            team = mTeam.Groups[i].Value;
                            //   //Console.WriteLine(team);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        team = Regex.Replace(team, "(.*?)>", "").Trim();
                        index = mTeam.Index;
                        //Console.WriteLine("Index of match: " + index);
                        //Console.WriteLine("Team 1: " + team);



                        int duzinastringa = htmlString.Length;
                        duzinastringa = duzinastringa - index;

                        if (IDKlubaInj2 != 31)
                        {
                            Match mTeam2 = Regex.Match(htmlString.Substring(index, duzinastringa), @"" + teamname2 + "(.*?)</tr>", RegexOptions.Singleline);
                            if (mTeam2.Success)
                            {
                                team2 = mTeam2.Groups[i].Value;
                                //   //Console.WriteLine(team);
                            }
                            //Console.WriteLine("***************************************************************");
                            //manicure the pattern string
                            //Replace everything before the last (greater than) sign with and empty string
                            team2 = Regex.Replace(team2, "(.*?)>", "").Trim();
                            index2 = mTeam2.Index;
                            //Console.WriteLine("Index of match: " + index2);
                            //Console.WriteLine("Team 2: " + team2);
                            //Console.WriteLine("***************************************************************");
                        }

                        if (IDKlubaInj2 == 31)
                        {
                            index2 = duzinastringa;
                        }
                        string playername = "";
                        string playername2 = "";
                        string playername3 = "";
                        string playername4 = "";
                        string playername5 = "";
                        string playername6 = "";
                        string playername7 = "";
                        link1 = "";

                        Match mPlayer = Regex.Match(htmlString.Substring(index, index2), @".html"">(.*?)</a>", RegexOptions.Singleline);
                        if (mPlayer.Success)
                        {
                            playername = mPlayer.Groups[1].Value;
                            // //Console.WriteLine(playername);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        playername = Regex.Replace(playername, "(.*?)>", "").Trim();
                        index3 = mPlayer.Index;
                        ////Console.WriteLine("Index of match: " + index3);
                        ////Console.WriteLine("Player 1:" + playername);

                        //---------------------------------------------------------------------------------------------------------------///



                        if (playername.Length > 1)
                        {
                            mPlayer = mPlayer.NextMatch();
                            if (mPlayer.Success)
                            {
                                playername2 = mPlayer.Groups[1].Value;
                                playername2 = Regex.Replace(playername2, "(.*?)>", "").Trim();
                                index4 = mPlayer.Index;
                                ////Console.WriteLine("Index of match: " + index4);
                                ////Console.WriteLine("Player 2:" + playername2);
                            }
                            if (playername2.Length > 1)
                            {
                                mPlayer = mPlayer.NextMatch();
                                if (mPlayer.Success)
                                {
                                    playername3 = mPlayer.Groups[1].Value;
                                    playername3 = Regex.Replace(playername3, "(.*?)>", "").Trim();
                                    index5 = mPlayer.Index;
                                    ////Console.WriteLine("Index of match: " + index5);
                                    ////Console.WriteLine("Player 3:" + playername3);
                                }
                                if (playername3.Length > 1)
                                {
                                    mPlayer = mPlayer.NextMatch();
                                    if (mPlayer.Success)
                                    {
                                        playername4 = mPlayer.Groups[1].Value;
                                        playername4 = Regex.Replace(playername4, "(.*?)>", "").Trim();

                                        index6 = mPlayer.Index;
                                        ////Console.WriteLine("Index of match: " + index6);
                                        ////Console.WriteLine("Player 4:" + playername4);
                                    }
                                    if (playername4.Length > 1)
                                    {
                                        mPlayer = mPlayer.NextMatch();
                                        if (mPlayer.Success)
                                        {
                                            playername5 = mPlayer.Groups[1].Value;
                                            playername5 = Regex.Replace(playername5, "(.*?)>", "").Trim();

                                            index7 = mPlayer.Index;
                                            //Console.WriteLine("Index of match: " + index7);
                                            //Console.WriteLine("Player 5:" + playername5);
                                        }
                                        if (playername5.Length > 1)
                                        {
                                            mPlayer = mPlayer.NextMatch();
                                            if (mPlayer.Success)
                                            {
                                                playername6 = mPlayer.Groups[1].Value;
                                                playername6 = Regex.Replace(playername6, "(.*?)>", "").Trim();

                                                index8 = mPlayer.Index;
                                                //Console.WriteLine("Index of match: " + index8);
                                                //Console.WriteLine("Player 6:" + playername6);
                                            }

                                            if (playername6.Length > 1)
                                            {
                                                mPlayer = mPlayer.NextMatch();
                                                if (mPlayer.Success)
                                                {
                                                    playername7 = mPlayer.Groups[1].Value;
                                                    playername7 = Regex.Replace(playername7, "(.*?)>", "").Trim();

                                                    int index9 = mPlayer.Index;
                                                    //Console.WriteLine("Index of match: " + index9);
                                                    //Console.WriteLine("Player 7:" + playername7);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        //---------------------------------------------------------------------------------------------------------------///

                        if (playername.Length > 1)
                        {
                            Match mLink = Regex.Match(htmlString.Substring(index, index2), @"href=""(.*?)"">" + playername + "", RegexOptions.Singleline);
                            if (mLink.Success)
                            {
                                link1 = mLink.Groups[1].Value;
                                //Console.WriteLine(link1);
                            }
                            link1 = "http://www.covers.com" + link1;

                            if (playername2.Length > 1)
                            {
                                if (IDKlubaInj2 == 31)
                                {
                                    index2 = duzinastringa - index4;
                                }

                                position = index + index4 - 90;
                                Match mLink2 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername2 + "", RegexOptions.Singleline);
                                if (mLink2.Success)
                                {
                                    link2 = mLink2.Groups[1].Value;
                                    //Console.WriteLine(link2);
                                }
                                link2 = "http://www.covers.com" + link2;

                                if (playername3.Length > 1)
                                {
                                    if (IDKlubaInj2 == 31)
                                    {
                                        index2 = duzinastringa - index5;
                                    }

                                    position = index + index5 - 90;
                                    Match mLink3 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername3 + "", RegexOptions.Singleline);
                                    if (mLink3.Success)
                                    {
                                        link3 = mLink3.Groups[1].Value;
                                        //Console.WriteLine(link3);
                                    }
                                    link3 = "http://www.covers.com" + link3;

                                    if (playername4.Length > 1)
                                    {
                                        if (IDKlubaInj2 == 31)
                                        {
                                            index2 = duzinastringa - index6;
                                        }

                                        position = index + index6 - 90;
                                        Match mLink4 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername4 + "", RegexOptions.Singleline);
                                        if (mLink4.Success)
                                        {
                                            link4 = mLink4.Groups[1].Value;
                                            //Console.WriteLine(link4);
                                        }
                                        link4 = "http://www.covers.com" + link4;

                                        if (playername5.Length > 1)
                                        {
                                            if (IDKlubaInj2 == 31)
                                            {
                                                index2 = duzinastringa - index7;
                                            }

                                            position = index + index7 - 90;
                                            Match mLink5 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername5 + "", RegexOptions.Singleline);
                                            if (mLink5.Success)
                                            {
                                                link5 = mLink5.Groups[1].Value;
                                                //Console.WriteLine(link5);
                                            }
                                            link5 = "http://www.covers.com" + link5;

                                            if (playername6.Length > 1)
                                            {
                                                if (IDKlubaInj2 == 31)
                                                {
                                                    index2 = duzinastringa - index8;
                                                }

                                                position = index + index8 - 90;
                                                Match mLink6 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername6 + "", RegexOptions.Singleline);
                                                if (mLink6.Success)
                                                {
                                                    link6 = mLink6.Groups[1].Value;
                                                    //Console.WriteLine(link6);
                                                }
                                                link6 = "http://www.covers.com" + link6;

                                                if (playername7.Length > 1)
                                                {
                                                    if (IDKlubaInj2 == 31)
                                                    {
                                                        index2 = duzinastringa - index9;
                                                    }

                                                    position = index + index9 - 90;
                                                    Match mLink7 = Regex.Match(htmlString.Substring(position, index2), @"<a href=""(.*?)"">" + playername7 + "", RegexOptions.Singleline);
                                                    if (mLink7.Success)
                                                    {
                                                        link7 = mLink7.Groups[1].Value;
                                                        //Console.WriteLine(link7);
                                                    }
                                                    link7 = "http://www.covers.com" + link7;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        /////////////////////////////////////////////////////////////////////////
                        string injuryreason = "";
                        string injuryreason2 = "";
                        string injuryreason3 = "";
                        string injuryreason4 = "";
                        string injuryreason5 = "";
                        string injuryreason6 = "";
                        string injuryreason7 = "";
                        Match mInjury = Regex.Match(htmlString.Substring(index, index2), @"<strong>(.*?)</td>", RegexOptions.Singleline);
                        if (mInjury.Success)
                        {
                            injuryreason = mInjury.Groups[1].Value;
                            // //Console.WriteLine(playername);
                        }
                        //Console.WriteLine("***************************************************************");
                        //manicure the pattern string
                        //Replace everything before the last (greater than) sign with and empty string
                        injuryreason = Regex.Replace(injuryreason, "</strong>", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, "/+", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, @"[\d]", "").Trim();
                        injuryreason = Regex.Replace(injuryreason, @"&#;", "'");
                        if (injuryreason.Length > 1)
                        {
                            int duzina = injuryreason.Length - 2;
                            injuryreason = injuryreason.Substring(0, duzina);
                        }
                        index7 = mInjury.Index;
                        //Console.WriteLine("Index of match: " + index7);
                        //Console.WriteLine("Injury reason of player 1:" + injuryreason);


                        if (injuryreason.Length > 1)
                        {
                            mInjury = mInjury.NextMatch();
                            if (mInjury.Success)
                            {
                                injuryreason2 = mInjury.Groups[1].Value;
                                injuryreason2 = Regex.Replace(injuryreason2, "</strong>", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, "/+", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, @"[\d]", "").Trim();
                                injuryreason2 = Regex.Replace(injuryreason2, @"&#;", "'");
                                int duzina2 = injuryreason2.Length - 2;
                                injuryreason2 = injuryreason2.Substring(0, duzina2);
                                int index8 = mInjury.Index;
                                //Console.WriteLine("Index of match: " + index8);
                                //Console.WriteLine("Injury reason of player 2:" + injuryreason2);
                            }
                            if (injuryreason2.Length > 1)
                            {
                                mInjury = mInjury.NextMatch();
                                if (mInjury.Success)
                                {
                                    injuryreason3 = mInjury.Groups[1].Value;
                                    injuryreason3 = Regex.Replace(injuryreason3, "</strong>", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, "/+", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, @"[\d]", "").Trim();
                                    injuryreason3 = Regex.Replace(injuryreason3, @"&#;", "'");
                                    int duzina3 = injuryreason3.Length - 2;
                                    injuryreason3 = injuryreason3.Substring(0, duzina3);

                                    //Console.WriteLine("Injury reason of player 3:" + injuryreason3);
                                }

                                if (injuryreason3.Length > 1)
                                {
                                    mInjury = mInjury.NextMatch();
                                    if (mInjury.Success)
                                    {
                                        injuryreason4 = mInjury.Groups[1].Value;
                                        injuryreason4 = Regex.Replace(injuryreason4, "</strong>", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, "/+", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, @"[\d]", "").Trim();
                                        injuryreason4 = Regex.Replace(injuryreason4, @"&#;", "'");
                                        int duzina4 = injuryreason4.Length - 2;
                                        injuryreason4 = injuryreason4.Substring(0, duzina4);

                                        //Console.WriteLine("Injury reason of player 4:" + injuryreason4);
                                    }

                                    if (injuryreason4.Length > 1)
                                    {
                                        mInjury = mInjury.NextMatch();
                                        if (mInjury.Success)
                                        {
                                            injuryreason5 = mInjury.Groups[1].Value;
                                            injuryreason5 = Regex.Replace(injuryreason5, "</strong>", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, "/+", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, @"[\d]", "").Trim();
                                            injuryreason5 = Regex.Replace(injuryreason5, @"&#;", "'");
                                            int duzina5 = injuryreason5.Length - 2;
                                            injuryreason5 = injuryreason5.Substring(0, duzina5);

                                            //Console.WriteLine("Injury reason of player 5:" + injuryreason5);
                                        }
                                        if (injuryreason5.Length > 1)
                                        {
                                            mInjury = mInjury.NextMatch();
                                            if (mInjury.Success)
                                            {
                                                injuryreason6 = mInjury.Groups[1].Value;
                                                injuryreason6 = Regex.Replace(injuryreason6, "</strong>", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, "/+", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, @"[\d]", "").Trim();
                                                injuryreason6 = Regex.Replace(injuryreason6, @"&#;", "'");
                                                int duzina6 = injuryreason6.Length - 2;
                                                injuryreason6 = injuryreason6.Substring(0, duzina6);

                                                //Console.WriteLine("Injury reason of player 4:" + injuryreason6);
                                            }
                                            if (injuryreason6.Length > 1)
                                            {
                                                mInjury = mInjury.NextMatch();
                                                if (mInjury.Success)
                                                {
                                                    injuryreason7 = mInjury.Groups[1].Value;
                                                    injuryreason7 = Regex.Replace(injuryreason7, "</strong>", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, "/+", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, @"[\d]", "").Trim();
                                                    injuryreason7 = Regex.Replace(injuryreason7, @"&#;", "'");
                                                    int duzina7 = injuryreason7.Length - 2;
                                                    injuryreason7 = injuryreason7.Substring(0, duzina7);

                                                    //Console.WriteLine("Injury reason of player 7:" + injuryreason7);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (playername.Length < 1)
                        {
                            richTextBox3.Text = "No injuries to report.";
                        }

                        if (richTextBox3.Controls.Contains(linkLabel1))
                        {
                            richTextBox3.Controls.Remove(linkLabel1);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel2))
                        {
                            richTextBox3.Controls.Remove(linkLabel2);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel3))
                        {
                            richTextBox3.Controls.Remove(linkLabel3);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel4))
                        {
                            richTextBox3.Controls.Remove(linkLabel4);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel5))
                        {
                            richTextBox3.Controls.Remove(linkLabel5);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel6))
                        {
                            richTextBox3.Controls.Remove(linkLabel6);
                        }
                        if (richTextBox3.Controls.Contains(linkLabel7))
                        {
                            richTextBox3.Controls.Remove(linkLabel7);
                        }

                        richTextBox3.Text = "";

                        if (playername.Length < 1)
                        {
                            richTextBox3.Text = "No injuries to report.";
                        }

                        if (playername.Length > 1 && playername != "Advertising")
                        {
                            richTextBox3.Text = "    " + HomeTeamCB.Text.Split(' ')[1] + " Injuries:\n=>";
                            linkLabel1.Text = playername;
                            linkLabel1.AutoSize = true;
                            linkLabel1.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                            richTextBox3.Controls.Add(linkLabel1);
                            richTextBox3.AppendText(linkLabel1.Text + "\n  " + injuryreason + "\n");
                            richTextBox3.SelectionStart = richTextBox3.TextLength;

                            if (playername2.Length > 1 && playername2 != "Advertising")
                            {
                                linkLabel2.Text = playername2;
                                richTextBox3.AppendText("=>");
                                linkLabel2.AutoSize = true;
                                linkLabel2.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                richTextBox3.Controls.Add(linkLabel2);
                                richTextBox3.AppendText("\n  " + injuryreason2 + "\n");
                                richTextBox3.SelectionStart = richTextBox3.TextLength;

                                if (playername3.Length > 1 && playername3 != "Advertising")
                                {
                                    linkLabel3.Text = playername3;
                                    richTextBox3.AppendText("=>");
                                    linkLabel3.AutoSize = true;
                                    linkLabel3.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                    richTextBox3.Controls.Add(linkLabel3);
                                    richTextBox3.AppendText("\n  " + injuryreason3 + "\n");
                                    richTextBox3.SelectionStart = richTextBox3.TextLength;
                                    if (playername4.Length > 1 && playername4 != "Advertising")
                                    {
                                        linkLabel4.Text = playername4;
                                        richTextBox3.AppendText("=>");
                                        linkLabel4.AutoSize = true;
                                        linkLabel4.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                        richTextBox3.Controls.Add(linkLabel4);
                                        richTextBox3.AppendText("\n  " + injuryreason4 + "\n");
                                        richTextBox3.SelectionStart = richTextBox3.TextLength;

                                        if (playername5.Length > 1 && playername5 != "Advertising")
                                        {
                                            linkLabel5.Text = playername5;
                                            richTextBox3.AppendText("=>");
                                            linkLabel5.AutoSize = true;
                                            linkLabel5.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                            richTextBox3.Controls.Add(linkLabel5);
                                            richTextBox3.AppendText("\n  " + injuryreason5 + "\n");
                                            richTextBox3.SelectionStart = richTextBox3.TextLength;

                                            if (playername6.Length > 1 && playername6 != "Advertising")
                                            {
                                                linkLabel6.Text = playername6;
                                                richTextBox3.AppendText("=>");
                                                linkLabel6.AutoSize = true;
                                                linkLabel6.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                                richTextBox3.Controls.Add(linkLabel6);
                                                richTextBox3.AppendText("\n  " + injuryreason6 + "\n");
                                                richTextBox3.SelectionStart = richTextBox3.TextLength;
                                                if (playername7.Length > 1 && playername7 != "Advertising")
                                                {
                                                    linkLabel7.Text = playername7;
                                                    richTextBox3.AppendText("=>");
                                                    linkLabel7.AutoSize = true;
                                                    linkLabel7.Location = richTextBox3.GetPositionFromCharIndex(richTextBox3.TextLength);
                                                    richTextBox3.Controls.Add(linkLabel7);
                                                    richTextBox3.AppendText("\n  " + injuryreason7 + "\n");
                                                    richTextBox3.SelectionStart = richTextBox3.TextLength;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        richTextBox3.Text = Regex.Replace(richTextBox3.Text, @"=> Advertising", "");
                    }
                    else
                    {
                        richTextBox3.Text = "Can't retreve injuries information.\nCheck your internet connection!";
                    }
                }
            }
        }
        private void ll_LinkClicked(object sender, MouseEventArgs e)
        {
            MessageBox.Show("Press here!");
        }

        private void TotalBox_MouseClick(object sender, MouseEventArgs e)
        {
            TotalBox.Text = "";
        }

        private void HandicapBox_MouseClick(object sender, MouseEventArgs e)
        {
            HandicapBox.Text = "";
        }
        private void dgv5(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (dataGridView5.Columns.Contains("Total Points"))
                {
                    dataGridView5.Columns.Remove("Total Points");
                    if (dataGridView5.Columns.Contains("Hendicap Points"))
                    { dataGridView5.Columns.Remove("Hendicap Points"); }

                }
                if (HomeTeamCB.Text.Length > 3)
                {

                    if (radioBtnNBA.Checked == true)
                    {
                        Sql43Last5 = "select TOP 5 Dayofmonth,Month,Year,Team,Opp,Line,Total,Hscore,Ascore, ATSr, OUr from " + AllNBAMLBResultsVar + " where Team = '" + HomeTeamCB.Text.Split(' ')[1] +
                             "' and Opp = '" + AwayTeamCB.Text.Split(' ')[1] + "' or Opp = '" + HomeTeamCB.Text.Split(' ')[1] +
                             "' and Team = '" + AwayTeamCB.Text.Split(' ')[1] + "' order by ID desc";
                    }
                    else
                    {
                        Sql43Last5 = "select TOP 5 Dayofmonth,Month,Season,Team,StarterHLastName as 'HS', Opp,StarterALastName as 'AS',Line,Total,Hscore,Ascore, Hits, OUr from " + AllNBAMLBResultsVar + " where Team = '" + HomeTeamCB.Text.Split(' ')[1] +
                                "' and Opp = '" + AwayTeamCB.Text.Split(' ')[1] + "' or Opp = '" + HomeTeamCB.Text.Split(' ')[1] +
                                "' and Team = '" + AwayTeamCB.Text.Split(' ')[1] + "' order by ID desc";
                    }

                    ds5 = new DataSet();
                    adapterMain = new SqlDataAdapter(Sql43Last5, connection);
                    ds5.Reset();
                    adapterMain.Fill(ds5);
                    dataGridView5.DataSource = ds5.Tables[0];
                    dataGridView5.Columns[0].Width = 53;

                    dataGridView5.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                    dataGridView5.Columns[0].HeaderText = "Day";
                    dataGridView5.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                    dataGridView5.Columns[1].HeaderText = "Month";
                    dataGridView5.Columns[1].Width = 50;
                    dataGridView5.Columns[2].DefaultCellStyle.BackColor = Color.DarkGray;
                    dataGridView5.Columns[2].HeaderText = "Year";
                    dataGridView5.Columns[2].Width = 42;
                    dataGridView5.Columns["Team"].DefaultCellStyle.BackColor = Color.LightGray;
                    dataGridView5.Columns["Team"].HeaderText = "Home Team";
                    dataGridView5.Columns["Team"].Width = 130;
                    dataGridView5.Columns["Opp"].DefaultCellStyle.BackColor = Color.LightGray;
                    dataGridView5.Columns["Opp"].Width = 130;
                    dataGridView5.Columns["Opp"].HeaderText = "Away Team";

                    dataGridView5.Columns["Line"].DefaultCellStyle.BackColor = Color.Silver;
                    dataGridView5.Columns["Line"].HeaderText = "Line";
                    dataGridView5.Columns["Total"].DefaultCellStyle.BackColor = Color.Silver;
                    dataGridView5.Columns["Total"].HeaderText = "Total";
                    dataGridView5.Columns["Hscore"].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    dataGridView5.Columns["Hscore"].HeaderText = "Hs";
                    dataGridView5.Columns["Ascore"].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    dataGridView5.Columns["Ascore"].HeaderText = "As";
                    if (radioBtnMLB.Checked == true)
                    {
                        dataGridView5.Columns["HS"].Width = 110;
                        dataGridView5.Columns["AS"].Width = 110;
                        DGVAnalysisResults.Columns[7].Width = 111;
                        DGVAnalysisResults.Columns[8].Width = 25;
                        DGVAnalysisResults.Columns[9].Width = 111;
                        DGVAnalysisResults.Columns[10].Width = 25;
                    }

                    System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                    boldStyle.Font = new System.Drawing.Font("Ariel", 9.25F, System.Drawing.FontStyle.Bold);


                    var col3 = new DataGridViewTextBoxColumn();
                    var col4 = new DataGridViewTextBoxColumn();

                    col3.HeaderText = "Total Points";
                    col3.Name = "Total Points";

                    col4.HeaderText = "Hand. Points";
                    col4.Name = "Hendicap Points";

                    dataGridView5.Columns.AddRange(new DataGridViewColumn[] { col3, col4 });

                    int totall;
                    double hendikk;
                    DataGridViewRow row = new DataGridViewRow();
                    for (int i = 0; i < dataGridView5.Rows.Count - 1; i++)
                    {
                        if (dataGridView5.Rows[i].Cells["Team"].Value.ToString() == HomeTeamCB.Text.Split(' ')[1])
                        {
                            dataGridView5.Rows[i].Cells["Team"].Style = boldStyle;
                        }
                        if (dataGridView5.Rows[i].Cells["Opp"].Value.ToString() == AwayTeamCB.Text.Split(' ')[1])
                        {
                            dataGridView5.Rows[i].Cells["Opp"].Style = boldStyle;
                        }
                        if (dataGridView5.Rows[i].Cells["Team"].Value.ToString() == AwayTeamCB.Text.Split(' ')[1])
                        {
                            dataGridView5.Rows[i].Cells["Team"].Style = boldStyle;
                        }
                        if (dataGridView5.Rows[i].Cells["Opp"].Value.ToString() == HomeTeamCB.Text.Split(' ')[1])
                        {
                            dataGridView5.Rows[i].Cells["Opp"].Style = boldStyle;
                        }
                        if (radioBtnMLB.Checked == true)
                        {
                            if (DGVAnalysisResults.Rows[i].Cells[9].Value.ToString() == awayStarterCB.Text.Split(' ')[1])
                            {
                                DGVAnalysisResults.Rows[i].Cells[9].Style = boldStyle;
                            }
                            if (DGVAnalysisResults.Rows[i].Cells[7].Value.ToString() == homeStarterCB.Text.Split(' ')[1])
                            {
                                DGVAnalysisResults.Rows[i].Cells[7].Style = boldStyle;
                            }
                        }
                        totall = Convert.ToInt16(dataGridView5.Rows[i].Cells["Hscore"].Value) + Convert.ToInt16(dataGridView5.Rows[i].Cells["Ascore"].Value);
                        dataGridView5.Rows[i].Cells["Total Points"].Value = totall;
                        hendikk = Convert.ToInt16(dataGridView5.Rows[i].Cells["Ascore"].Value) - Convert.ToInt16(dataGridView5.Rows[i].Cells["Hscore"].Value);
                        dataGridView5.Rows[i].Cells["Hendicap Points"].Value = hendikk;
                    }
                    dataGridView5.Rows[0].Cells[0].Selected = false;

                    label43.Text = "Last 5 games between " + AwayTeamCB.Text.Split(' ')[1] + " and " + HomeTeamCB.Text.Split(' ')[1] + "";
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form5 frm5 = new Form5();
            frm5.Show();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link2);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link1);
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link3);
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link4);
        }

        private void richTextBox3_VScroll(object sender, EventArgs e)
        {

        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link5);
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link6);
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link7);
        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link11);
        }

        private void linkLabel9_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link12);
        }
        private void linkLabel10_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link13);
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link14);
        }

        private void linkLabel12_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link15);
        }

        private void linkLabel13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link16);
        }

        private void linkLabel14_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(link17);
        }

        private void HandicapBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                //enter key is down
            }
        }

        private void GetTodaysGames(object sender, EventArgs e)
        {
            //try
            //{

            //    bool stats;
            //    if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
            //    {
            //        stats = true;
            //    }
            //    else
            //    {
            //        stats = false;
            //    }


            //    if (stats == true)
            //    {
            //        WebClient wc = new WebClient();
            //        pinnacleString = wc.DownloadString("http://www.pinnaclesports.com/League/Basketball/NBA/1/Lines.aspx");

            //    }


            //    if (pinnacleString.Length > 1)
            //    {

            //        string today = "";
            //        string tommorrow = "";
            //        Match mToday = Regex.Match(pinnacleString, @"<h4>NBA Basketball(.*?)</h4>", RegexOptions.Singleline);
            //        if (mToday.Success)
            //        {
            //            today = mToday.Groups[0].Value;
            //            pinnindex1 = mToday.Index;
            //            Console.WriteLine(today);
            //            Console.WriteLine(pinnindex1);
            //        }

            //        mToday = mToday.NextMatch();
            //        if (mToday.Success)
            //        {
            //            tommorrow = mToday.Groups[1].Value;
            //            pinnindex2 = mToday.Index;
            //            Console.WriteLine(tommorrow);
            //            Console.WriteLine(pinnindex2);
            //        }


            //        if (pinnindex2 > 0)
            //        {
            //            pinnlenght = pinnindex2 - pinnindex1;
            //        }
            //        else
            //        {
            //            pinnlenght = pinnacleString.Length - pinnindex1 - 5;
            //        }
            //        Match mTeam = Regex.Match(pinnacleString.Substring(pinnindex1, pinnlenght), @"<td class=""linesTeam"">(.*?)</td>", RegexOptions.Singleline);
            //        if (mTeam.Success)
            //        {
            //            pinnteam[0] = mTeam.Groups[0].Value;
            //            Console.WriteLine(pinnteam[0]);
            //        }

            //        //Replace everything before the last (greater than) sign with and empty string
            //        pinnteam[0] = Regex.Replace(pinnteam[0], @"<td class=""linesTeam"">", "");
            //        pinnteam[0] = Regex.Replace(pinnteam[0], @"</td>", "");
            //        pinnteam[0] = Regex.Replace(pinnteam[0], @"Â ", "");
            //        Console.WriteLine(pinnteam[0]);


            //        int i = 1;
            //        while (pinnteam[i - 1].Length > 1)
            //        {
            //            mTeam = mTeam.NextMatch();
            //            if (mTeam.Success)
            //            {
            //                pinnteam[i] = mTeam.Groups[1].Value;
            //                pinnteam[i] = Regex.Replace(pinnteam[i], @"<td class=""linesTeam"">", "");
            //                pinnteam[i] = Regex.Replace(pinnteam[i], @"</td>", "");
            //                pinnteam[i] = Regex.Replace(pinnteam[i], @"Â ", "");

            //                Console.WriteLine("Teams:" + pinnteam[i]);
            //                i++;
            //            }
            //            else
            //            {
            //                break;
            //            }

            //        }

            //        Match mHandicap = Regex.Match(pinnacleString.Substring(pinnindex1, pinnlenght), @"<td class=""linesSpread"">(.*?)</td>", RegexOptions.Singleline);
            //        if (mHandicap.Success)
            //        {
            //            pinnhand[0] = mHandicap.Groups[0].Value;
            //            pinnhand[0] = Regex.Replace(pinnhand[0], @"<td class=""linesSpread"">", "");
            //            pinnhand[0] = Regex.Replace(pinnhand[0], @"</td>", "");
            //            pinnhand[0] = Regex.Replace(pinnhand[0], @"&nbsp;&nbsp;&nbsp;&nbsp;", " ");


            //            if (pinnhand[0] != "Offline")
            //            {
            //                pinnhandodds[0] = pinnhand[0].Split(' ')[1];
            //                pinnhand[0] = pinnhand[0].Split(' ')[0];
            //            }
            //            else
            //            {
            //                pinnhandodds[0] = "N/A";
            //                pinnhand[0] = "N/A";
            //            }

            //        }

            //        i = 1;
            //        while (pinnhand[i - 1].Length > 1)
            //        {
            //            mHandicap = mHandicap.NextMatch();
            //            if (mHandicap.Success)
            //            {
            //                pinnhand[i] = mHandicap.Groups[0].Value;
            //                pinnhand[i] = Regex.Replace(pinnhand[i], @"<td class=""linesSpread"">", "");
            //                pinnhand[i] = Regex.Replace(pinnhand[i], @"</td>", "");
            //                pinnhand[i] = Regex.Replace(pinnhand[i], @"&nbsp;&nbsp;&nbsp;&nbsp;", " ");

            //                if (pinnhand[i] != "Offline")
            //                {
            //                    pinnhandodds[i] = pinnhand[i].Split(' ')[1];
            //                    pinnhand[i] = pinnhand[i].Split(' ')[0];
            //                }
            //                else
            //                {
            //                    pinnhandodds[i] = "N/A";
            //                    pinnhand[i] = "N/A";
            //                }
            //                i++;
            //            }

            //            else
            //            {
            //                break;
            //            }

            //        }

            //        Match mTotal = Regex.Match(pinnacleString.Substring(pinnindex1, pinnlenght), @"<td class=""linesTotals"">(.*?)</td>", RegexOptions.Singleline);
            //        if (mTotal.Success)
            //        {
            //            pinntotal[0] = mTotal.Groups[0].Value;
            //            pinntotal[0] = Regex.Replace(pinntotal[0], @"<td class=""linesTotals"">", "");
            //            pinntotal[0] = Regex.Replace(pinntotal[0], @"</td>", "");
            //            pinntotal[0] = Regex.Replace(pinntotal[0], @"&nbsp;&nbsp;&nbsp;&nbsp;", " ");
            //            pinntotal[0] = Regex.Replace(pinntotal[0], @"ver", "");
            //            pinntotal[0] = Regex.Replace(pinntotal[0], @"nder", "");
            //            int indexx4 = mTeam.Index;

            //            if (pinntotal[0] != "Offline")
            //            {
            //                pinntotalodds[0] = pinntotal[0].Split(' ')[2];
            //                pinntotal[0] = pinntotal[0].Split(' ')[0] + " " + pinntotal[0].Split(' ')[1];
            //            }
            //            else
            //            {
            //                pinntotalodds[0] = "N/A";
            //                pinntotal[0] = "N/A";
            //            }
            //        }

            //        i = 1;
            //        while (pinntotal[i - 1].Length > 1)
            //        {
            //            mTotal = mTotal.NextMatch();
            //            if (mTotal.Success)
            //            {
            //                pinntotal[i] = mTotal.Groups[0].Value;
            //                if (pinntotal[i].Length > 30)
            //                {
            //                    pinntotal[i] = Regex.Replace(pinntotal[i], @"<td class=""linesTotals"">", "");
            //                    pinntotal[i] = Regex.Replace(pinntotal[i], @"</td>", "");
            //                    pinntotal[i] = Regex.Replace(pinntotal[i], @"&nbsp;&nbsp;&nbsp;&nbsp;", " ");
            //                    pinntotal[i] = Regex.Replace(pinntotal[i], @"ver", "");
            //                    pinntotal[i] = Regex.Replace(pinntotal[i], @"nder", "");

            //                    if (pinntotal[i] != "Offline")
            //                    {
            //                        pinntotalodds[i] = pinntotal[i].Split(' ')[2];
            //                        pinntotal[i] = pinntotal[i].Split(' ')[0] + " " + pinntotal[i].Split(' ')[1];
            //                    }
            //                    else
            //                    {
            //                        pinntotalodds[i] = "N/A";
            //                        pinntotal[i] = "N/A";
            //                    }

            //                }
            //                else
            //                {
            //                    pinntotal[i] = "N/A";
            //                    pinntotalodds[i] = "N/A";
            //                }
            //                i++;
            //            }
            //            else
            //            {
            //                break;
            //            }

            //        }

            //        int saveRow = 0;
            //        if (dataGridView6.Rows.Count > 0)
            //            saveRow = dataGridView6.FirstDisplayedCell.RowIndex;



            //        if (dataGridView6.DataSource != null)
            //        {
            //            dataGridView6.DataSource = null;
            //        }
            //        else
            //        {
            //            dataGridView6.Rows.Clear();
            //        }


            //        dataGridView6.Refresh();

            //        dataGridView6.ColumnCount = 5;
            //        dataGridView6.Columns[0].Name = "Teams";
            //        dataGridView6.Columns[0].Width = 165;
            //        dataGridView6.Columns[1].Name = "Hand.";
            //        dataGridView6.Columns[1].Width = 55;
            //        dataGridView6.Columns[2].Name = "Odds";
            //        dataGridView6.Columns[1].Width = 60;
            //        dataGridView6.Columns[3].Name = "Total";
            //        dataGridView6.Columns[3].Width = 70;
            //        dataGridView6.Columns[4].Name = "Odds";


            //        i = 0;
            //        while (pinnteam[i] != null)
            //        {
            //            pinnrow = new string[] { "" + pinnteam[i] + "", "" + pinnhand[i] + "", "" + pinnhandodds[i] + "", "" + pinntotal[i] + "", "" + pinntotalodds[i] + "" };
            //            dataGridView6.Rows.Add(pinnrow);
            //            i++;
            //        }

            //        int rowcount = dataGridView6.Rows.Count - 1;


            //        if (rowcount >= 1)
            //        {
            //            dataGridView6.Rows[0].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 2)
            //        {
            //            dataGridView6.Rows[1].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 4)
            //        {
            //            dataGridView6.Rows[4].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 5)
            //        {
            //            dataGridView6.Rows[5].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 8)
            //        {
            //            dataGridView6.Rows[8].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 9)
            //        {
            //            dataGridView6.Rows[9].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 12)
            //        {
            //            dataGridView6.Rows[12].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 13)
            //        {
            //            dataGridView6.Rows[13].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 16)
            //        {
            //            dataGridView6.Rows[16].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 17)
            //        {
            //            dataGridView6.Rows[17].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 20)
            //        {
            //            dataGridView6.Rows[20].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 21)
            //        {
            //            dataGridView6.Rows[21].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 24)
            //        {
            //            dataGridView6.Rows[24].DefaultCellStyle.BackColor = Color.LightGray;
            //        }
            //        if (rowcount >= 25)
            //        {
            //            dataGridView6.Rows[25].DefaultCellStyle.BackColor = Color.LightGray;
            //        }




            //        dataGridView6.Rows[0].Cells[0].Selected = false;

            //        if (saveRow != 0 && saveRow < dataGridView6.Rows.Count)
            //            dataGridView6.FirstDisplayedScrollingRowIndex = saveRow;
            //    }
            //}

            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.ToString());
            //    //   MessageBox.Show("Cant connect to pinnaclesports.com", "Warning!", MessageBoxButtons.OK);
            //}
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //  GetTodaysGames(sender, e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (tabControlInjuries.SelectedIndex == 2)
            {
                GetTodaysGames(sender, e);
            }
            if (tabControlInjuries.SelectedIndex == 3 && dataGridView8.Rows[0].Cells[3].Value != "")
            {
                if (coversbsno > 0)
                {
                    Scores(sender, e);
                }
            }
        }

        private void tabControlInjuries_TabIndexChanged(object sender, EventArgs e)
        {
            if (tabControlInjuries.SelectedIndex == 2)
            {
                GetTodaysGames(sender, e);
            }
        }

        private void tabControlInjuries_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControlInjuries.SelectedIndex == 2)
            {
                GetTodaysGames(sender, e);
            }
            if (tabControlInjuries.SelectedIndex == 4)
            {
                //  cleanitup(sender,e);

                connection.Open();

                if (DGVAnalysisResults.Columns.Contains("Col0"))
                {
                    DGVAnalysisResults.Columns.Remove("Col0");
                }

                dataGridView7.DataSource = null;

                string sql16 = @"SELECT
       [TeamName]
      ,[SUTotWin]
      ,[SUTotLoss]
      ,[ATSTotWin]
      ,[ATSTotLoss]
      ,[OTotal]
      ,[UTotal]
       FROM [NBATeamsStatistics] order by Conference,SUTotWin desc";


                adapterStandings = new SqlDataAdapter(sql16, connection);
                ds.Tables.Add("Table2");
                adapterStandings.Fill(ds.Tables["Table2"]);

                var col0 = new DataGridViewTextBoxColumn();
                col0.HeaderText = "NO.";
                col0.Name = "Col0";
                dataGridView7.Columns.AddRange(new DataGridViewColumn[] { col0 });

                dataGridView7.DataSource = ds.Tables["Table2"];

                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                boldStyle.Font = new System.Drawing.Font("Ariel", 9.75F, System.Drawing.FontStyle.Bold);

                dataGridView7.Columns[1].DefaultCellStyle = boldStyle;

                dataGridView7.Columns[0].Width = 30;
                dataGridView7.Columns[1].Width = 110;
                dataGridView7.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                dataGridView7.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;

                dataGridView7.Columns[2].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView7.Columns[3].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView7.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView7.Columns[5].DefaultCellStyle.BackColor = Color.LightGray;
                dataGridView7.Columns[6].DefaultCellStyle.BackColor = Color.Silver;
                dataGridView7.Columns[7].DefaultCellStyle.BackColor = Color.Silver;

                dataGridView7.Columns[1].HeaderText = "Team";
                dataGridView7.Columns[2].HeaderText = "SU Win";
                dataGridView7.Columns[3].HeaderText = "SU Loss";
                dataGridView7.Columns[4].HeaderText = "ATS Win";
                dataGridView7.Columns[5].HeaderText = "ATS Loss";
                dataGridView7.Columns[6].HeaderText = "O's";
                dataGridView7.Columns[7].HeaderText = "U's";

                numbering = 1;
                DataGridViewRow row = new DataGridViewRow();
                for (int i = 0; i < 15; i++)
                {
                    dataGridView7.Rows[numbering - 1].Cells[0].Value = numbering;
                    if (numbering < 9)
                    {
                        dataGridView7.Rows[numbering - 1].Cells[0].Style.ForeColor = Color.Green;
                    }
                    numbering++;
                }
                numbering = 1;

                for (int i = 15; i < 30; i++)
                {
                    dataGridView7.Rows[numbering + 14].Cells[0].Value = numbering;
                    if (numbering < 9)
                    {
                        dataGridView7.Rows[numbering + 14].Cells[0].Style.ForeColor = Color.Green;
                    }
                    numbering++;
                }

                connection.Close();

                dataGridView7.CurrentCell.Selected = false;
            }
            if (tabControlInjuries.SelectedIndex == 3)
            {
                dateTimePicker2.Format = DateTimePickerFormat.Custom;
                dateTimePicker2.CustomFormat = "yyyy-MM-dd";
                Scores(sender, e);
            }
        }

        private void dataGridView6_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void renameteams()
        {
            switch (PinnHomeTeam)
            {
                case "Philadelphia 76ers":
                    {
                        PinnHomeTeam = "Phi Seventysixers";
                        break;
                    }
                case "Los Angeles Clippers":
                    {
                        PinnHomeTeam = "LA Clippers";
                        break;
                    }
                case "San Antonio Spurs":
                    {
                        PinnHomeTeam = "SanAntonio Spurs";
                        break;
                    }
                case "Golden State Warriors":
                    {
                        PinnHomeTeam = "GS Warriors";
                        break;
                    }
                case "New Orleans Pelicans":
                    {
                        PinnHomeTeam = "NewOrleans Pelicans";
                        break;
                    }
                case "Portland Trail Blazers":
                    {
                        PinnHomeTeam = "Por Trailblazers";
                        break;
                    }
            }

            switch (PinnAwayTeam)
            {
                case "Philadelphia 76ers":
                    {
                        PinnAwayTeam = "Phi Seventysixers";
                        break;
                    }
                case "Los Angeles Clippers":
                    {
                        PinnAwayTeam = "LA Clippers";
                        break;
                    }
                case "San Antonio Spurs":
                    {
                        PinnAwayTeam = "SanAntonio Spurs";
                        break;
                    }
                case "Golden State Warriors":
                    {
                        PinnAwayTeam = "GS Warriors";
                        break;
                    }
                case "New Orleans Pelicans":
                    {
                        PinnAwayTeam = "NewOrleans Pelicans";
                        break;
                    }
                case "Portland Trail Blazers":
                    {
                        PinnAwayTeam = "Por Trailblazers";
                        break;
                    }
            }
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            PastResults(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Scores(sender, e);
        }

        private void PastResults(object sender, EventArgs e)
        {
            try
            {
                bool stats;
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                {
                    stats = true;
                }
                else
                {
                    stats = false;
                }


                if (stats == true)
                {
                    DateTime dt = DateTime.Today.AddDays(-1);
                    String datum1 = dt.ToString("yyyy-MM-dd");
                    WebClient wc = new WebClient();
                    if (radioBtnNBA.Checked == true)
                    {
                        coversstring = wc.DownloadString("http://www.covers.com/Sports/NBA/Matchups?selectedDate=" + datum1 + "");
                    }
                    else if (radioBtnMLB.Checked == true)
                    {
                        coversstring = wc.DownloadString("http://www.covers.com/Sports/MLB/Matchups?selectedDate=" + datum1 + "");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            if (radioBtnNBA.Checked == true)
            {
                LiveScoresNBA(sender, e);
            }
            else if (radioBtnMLB.Checked == true)
            {
                LiveScoresMLB(sender, e);
            }
        }

        public void Scores(object sender, EventArgs e)
        {
            try
            {
                bool stats;
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                {
                    stats = true;
                }
                else
                {
                    stats = false;
                }
                if (stats == true)
                {
                    WebClient wc = new WebClient();
                    if (radioBtnNBA.Checked == true)
                    {
                        coversstring = wc.DownloadString("http://www.covers.com/Sports/NBA/Matchups");
                    }
                    else
                    {
                        coversstring = wc.DownloadString("http://www.covers.com/Sports/MLB/Matchups");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            if (radioBtnNBA.Checked == true)
            {
                LiveScoresNBA(sender, e);
            }
            else if (radioBtnMLB.Checked == true)
            {
                LiveScoresMLB(sender, e);
            }
        }
        public void LiveScoresNBA(object sender, EventArgs e)
        {
            try
            {
                if (coversstring.Length > 1)
                {
                    for (i = 0; i < 32; i++)
                    {
                        coversteam[i] = null;
                        coversgamestatus[i] = "";
                        coversgamestatus2[i] = "";
                        covershpoints[i] = "";
                        covershpoints2[i] = "";
                        coversapoints[i] = "";
                        coversapoints2[i] = "";
                        coversboxscore[i] = "";
                        coversTotal[i] = null;
                        coversLine[i] = null;

                    }

                    Match mTeamh = Regex.Match(coversstring, @"<div class=""cmg_team_name"">(.*?)</div>", RegexOptions.Singleline);
                    if (mTeamh.Success)
                    {
                        coversteam[0] = mTeamh.Groups[0].Value;
                        coversteam[0] = Regex.Replace(coversteam[0], @"<div class=""cmg_team_name"">", "").Trim();
                        coversteam[0] = Regex.Replace(coversteam[0], @"<span>(.*?)</span>", "").Trim();
                        coversteam[0] = Regex.Replace(coversteam[0], @"</div>", "").Trim();
                        coversteam[0] = Regex.Replace(coversteam[0], @"\n", "").Trim();
                        coversindex[0] = mTeamh.Index;
                    }

                    i = 1;
                    while (coversteam[i - 1].Length > 1)
                    {
                        mTeamh = mTeamh.NextMatch();
                        if (mTeamh.Success)
                        {
                            coversteam[i] = mTeamh.Groups[0].Value;
                            coversteam[i] = Regex.Replace(coversteam[i], @"<div class=""cmg_team_name"">", "").Trim();
                            coversteam[i] = Regex.Replace(coversteam[i], @"<span>(.*?)</span>", "").Trim();
                            coversteam[i] = Regex.Replace(coversteam[i], @"</div>", "").Trim();
                            coversteam[i] = Regex.Replace(coversteam[i], @"\n", "").Trim();
                            coversindex[i] = mTeamh.Index;
                            i++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    int noofteams2 = i;
                    noofteams2 = noofteams2 / 2;


                    j = 0;
                    for (i = 0; i <= noofteams2; i++)
                    {
                        Match mGameStatus = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_game_time"">(.*?)</div>", RegexOptions.Singleline);
                        if (mGameStatus.Success)
                        {
                            coversgamestatus[i] = mGameStatus.Groups[0].Value;
                            coversgamestatus[i] = Regex.Replace(coversgamestatus[i], @"<div class=""cmg_game_time"">", "").Trim();
                            coversgamestatus[i] = Regex.Replace(coversgamestatus[i], @"</div>", "").Trim();
                            coversgamestatus[i] = Regex.Replace(coversgamestatus[i], @"ET", "").Trim();
                            //   coversgamestatus[0] = coversgamestatus2[0].Substring(0,10);

                        }
                        j++;
                        j++;
                    }



                    j = 0;
                    for (i = 0; i <= noofteams2; i++)
                    {
                        Match mGameStatus2 = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_matchup_list_status"">(.*?)</div>", RegexOptions.Singleline);
                        if (mGameStatus2.Success)
                        {
                            coversgamestatus2[i] = mGameStatus2.Groups[0].Value;
                            coversgamestatus2[i] = Regex.Replace(coversgamestatus2[i], @"<div class=""cmg_matchup_list_status"">", "").Trim();
                            coversgamestatus2[i] = Regex.Replace(coversgamestatus2[i], @"</div>", "").Trim();
                            // coversgamestatus2[0] = Regex.Replace(coversgamestatus2[0], @"  ", "").Trim();
                        }
                        j++;
                        j++;
                    }


                    j = 0;
                    for (i = 0; i <= noofteams2; i++)
                    {
                        Match mResultHome = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class='cmg_matchup_list_score_home(.*?)</div>", RegexOptions.Singleline);
                        if (mResultHome.Success)
                        {
                            covershpoints[i] = mResultHome.Groups[0].Value;
                            Match m = Regex.Match(covershpoints[i], @"\d+");
                            covershpoints[i] = m.Value;
                            //covershpoints[i] = Regex.Replace(covershpoints[i], @"<div class='cmg_matchup_list_score_home", "").Trim();
                            //covershpoints[i] = Regex.Replace(covershpoints[i], @"</div>", "").Trim();
                            //covershpoints[i] = Regex.Replace(covershpoints[i], @"cmg_matchup_list_winner'>", "").Trim();
                            //covershpoints[i] = Regex.Replace(covershpoints[i], @">", "").Trim();
                            //covershpoints[i] = Regex.Replace(covershpoints[i], @"'", "").Trim();
                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i <= noofteams2; i++)
                    {
                        Match mResultHome2 = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_matchup_list_score_home"">(.*?)</div>", RegexOptions.Singleline);
                        if (mResultHome2.Success)
                        {
                            covershpoints2[i] = mResultHome2.Groups[0].Value;
                            Match m = Regex.Match(covershpoints2[i], @"\d+");
                            covershpoints2[i] = m.Value;
                            //covershpoints2[i] = Regex.Replace(covershpoints2[i], @"<div class=""cmg_matchup_list_score_home"">", "").Trim();
                            //covershpoints2[i] = Regex.Replace(covershpoints2[i], @"</div>", "").Trim();
                            //covershpoints2[i] = Regex.Replace(covershpoints2[i], @"cmg_matchup_list_winner'>", "").Trim();
                            //covershpoints2[i] = Regex.Replace(covershpoints2[i], @">", "").Trim();

                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i <= noofteams2; i++)
                    {
                        Match mResultAway = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class='cmg_matchup_list_score_away(.*?)</div>", RegexOptions.Singleline);
                        if (mResultAway.Success)
                        {
                            coversapoints[i] = mResultAway.Groups[0].Value;
                            Match m = Regex.Match(coversapoints[i], @"\d+");
                            coversapoints[i] = m.Value;
                            //coversapoints[i] = Regex.Replace(coversapoints[i], @"<div class='cmg_matchup_list_score_away", "").Trim();
                            //coversapoints[i] = Regex.Replace(coversapoints[i], @"</div>", "").Trim();
                            //coversapoints[i] = Regex.Replace(coversapoints[i], @"cmg_matchup_list_winner'>", "").Trim();
                            //coversapoints[i] = Regex.Replace(coversapoints[i], @">", "").Trim();
                            //coversapoints[i] = Regex.Replace(coversapoints[i], @"'", "").Trim();

                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i <= noofteams2; i++)
                    {
                        Match mResultAway2 = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_matchup_list_score_away"">(.*?)</div>", RegexOptions.Singleline);
                        if (mResultAway2.Success)
                        {
                            coversapoints2[i] = mResultAway2.Groups[0].Value;
                            Match m = Regex.Match(coversapoints2[i], @"\d+");
                            coversapoints2[i] = m.Value;
                            //coversapoints2[i] = Regex.Replace(coversapoints2[i], @"<div class=""cmg_matchup_list_score_away"">", "").Trim();
                            //coversapoints2[i] = Regex.Replace(coversapoints2[i], @"</div>", "").Trim();
                            //coversapoints2[i] = Regex.Replace(coversapoints2[i], @">", "").Trim();
                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mCoversTotal = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"LIVE:</span>(.*?)</span>", RegexOptions.Singleline);
                        if (mCoversTotal.Success)
                        {
                            coversTotal[i] = mCoversTotal.Groups[0].Value;

                            Match m = Regex.Match(coversTotal[i], @"\d+\.?\d*");
                            coversTotal[i] = m.Value;

                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"\d*\.\d*", String.Empty).Trim();
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"^[0-9.]*$", String.Empty).Trim();
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"\r\n", String.Empty).Trim();
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"LIVE:<span>O/U:", "").Trim();
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @" | </span>", "");
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"</span>", "");
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"\|", "");
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"LIVE:<span>O/U:", "").Trim();

                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mcoversLine = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"LIVE:</span>(.*?)</div>", RegexOptions.Singleline);
                        if (mcoversLine.Success)
                        {
                            coversLine[i] = mcoversLine.Groups[0].Value;

                            int lenght = coversLine[i].Length;
                            coversLine[i] = coversLine[i].Substring((lenght / 2), (lenght / 2));

                            Match m = Regex.Match(coversLine[i], @"\-?\+?\d+\.?\d*");

                            coversLine[i] = m.Value;

                            //coversLine[i] = Regex.Replace(coversLine[i], @"\r\n", String.Empty).Trim();
                            //coversLine[i] = Regex.Replace(coversLine[i], @"LIVE:<span>O/U:", "").Trim();
                            //coversLine[i] = Regex.Replace(coversLine[i], @" | </span>", "");
                            //coversLine[i] = Regex.Replace(coversLine[i], @"</span>", "");
                            //coversLine[i] = Regex.Replace(coversLine[i], @"\|", "");
                            //coversLine[i] = Regex.Replace(coversLine[i], @"LIVE:<span>O/U:", "").Trim();
                            //coversLine[i] = Regex.Replace(coversLine[i], @"(.*?)<span>", "").Trim();
                            //coversLine[i] = Regex.Replace(coversLine[i], @"</div>", "").Trim();
                            //coversLine[i] = Regex.Replace(coversLine[i], @"[^-+.0-9]", "").Trim();

                        }
                        j++;
                        j++;
                    }



                    Match mLineHistory = Regex.Match(coversstring, @">Line History</a>", RegexOptions.Multiline);
                    if (mLineHistory.Success)
                    {
                        LineHistory[0] = mLineHistory.Groups[0].Value;
                        coversindex2[0] = mLineHistory.Index;
                    }
                    i = 1;
                    while (LineHistory[i - 1].Length >= 1)
                    {
                        mLineHistory = mLineHistory.NextMatch();
                        if (mLineHistory.Success)
                        {
                            LineHistory[i] = mLineHistory.Groups[0].Value;
                            coversindex2[i] = mLineHistory.Index;

                            i++;
                        }
                        else
                        {
                            coversindex2[i] = coversstring.Length - 1;
                            break;
                        }
                    }

                    j = 1;
                    for (i = 0; i <= noofteams2; i++)
                    {
                        Match mBoxScore = Regex.Match(coversstring.Substring(coversindex[j], (coversindex2[i] - coversindex[j])), @"results(.*?)"">Boxscore", RegexOptions.Singleline);
                        if (mBoxScore.Success)
                        {
                            coversboxscore[i] = mBoxScore.Groups[0].Value;
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @"<div class=""cmg_l_row cmg_matchup_list_gamebox"">", "").Trim();
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @">", "").Trim();
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @"\\", "").Trim();
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @"Boxscore", "").Trim();
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @"<a href=", "").Trim();
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @"""", "").Trim();
                            coversboxscore[i] = "http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/" + coversboxscore[i];
                        }
                        j++;
                        j++;
                    }

                    j = 1;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mMatchup = Regex.Match(coversstring.Substring(coversindex[j], (coversindex2[i] - coversindex[j])), @"matchups(.*?)"">Matchup", RegexOptions.Singleline);
                        if (mMatchup.Success)
                        {
                            coversMatchup[i] = mMatchup.Groups[0].Value;
                            //  coversMatchup[i] = Regex.Replace(coversMatchup[i], @"<div class=""cmg_l_row cmg_matchup_list_gamebox"">", "").Trim();
                            coversMatchup[i] = Regex.Replace(coversMatchup[i], @">", "").Trim();
                            coversMatchup[i] = Regex.Replace(coversMatchup[i], @"\\", "").Trim();
                            coversMatchup[i] = Regex.Replace(coversMatchup[i], @"Matchup", "").Trim();
                            coversMatchup[i] = Regex.Replace(coversMatchup[i], @"<a href=", "").Trim();
                            coversMatchup[i] = Regex.Replace(coversMatchup[i], @"""", "").Trim();
                            coversMatchup[i] = "http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/nba/" + coversMatchup[i];
                        }
                        j++;
                        j++;
                    }


                    if (dataGridView8.DataSource != null)
                    {
                        dataGridView8.DataSource = null;
                    }
                    else
                    {
                        dataGridView8.Rows.Clear();
                    }


                    dataGridView8.Refresh();

                    dataGridView8.ColumnCount = 8;
                    dataGridView8.Columns[0].Name = "Away Team";
                    dataGridView8.Columns[0].Width = 70;
                    dataGridView8.Columns[1].Name = "Home Team";
                    dataGridView8.Columns[1].Width = 70;
                    dataGridView8.Columns[2].Name = "Game Status";
                    dataGridView8.Columns[2].Width = 70;
                    dataGridView8.Columns[3].Name = "Away Score";
                    dataGridView8.Columns[3].Width = 70;
                    dataGridView8.Columns[4].Name = "Home Score";
                    dataGridView8.Columns[4].Width = 70;
                    dataGridView8.Columns[5].Name = "Total";
                    dataGridView8.Columns[5].Width = 70;
                    dataGridView8.Columns[6].Name = "Line";
                    dataGridView8.Columns[6].Width = 70;
                    dataGridView8.Columns[7].Name = "Box Score / Matchup";
                    dataGridView8.Columns[7].Width = 100;



                    i = 0;
                    j = 1;
                    k = 0;
                    while (coversteam[i] != null)
                    {
                        coversrow = new string[] { "" + coversteam[i] + "", "" + coversteam[j] + "", "" + coversgamestatus2[k] + "", "" +
                                                   coversapoints2[k] + "", "" + covershpoints2[k] + "",""+coversTotal[k]+"",""+coversLine[k]+"" };
                        dataGridView8.Rows.Add(coversrow);
                        i++;
                        i++;
                        j++;
                        j++;
                        k++;
                    }


                    noofteams = 0;
                    i = 0;
                    while (coversteam[i] != null)
                    {
                        noofteams++;
                        i++;

                    }

                    noofteams = noofteams / 2;

                    j = 0;
                    for (i = 0; i < noofteams; i++)
                    {
                        if (dataGridView8.Rows[i].Cells[2].Value.ToString().Contains("Final"))
                        {
                            // dataGridView8.Rows[i].Cells[2].Value = coversgamestatus2[j];
                            dataGridView8.Rows[i].Cells[3].Value = coversapoints[i];
                            dataGridView8.Rows[i].Cells[4].Value = covershpoints[i];
                            j++;
                        }
                    }

                    j = 0;
                    for (i = 0; i < noofteams; i++)
                    {
                        if (dataGridView8.Rows[i].Cells[2].Value == "")
                        {
                            dataGridView8.Rows[i].Cells[2].Value = coversgamestatus[i];
                            dataGridView8.Rows[i].Cells[3].Value = coversapoints[i];
                            dataGridView8.Rows[i].Cells[4].Value = covershpoints[i];
                            j++;
                        }
                    }
                    coversbsno = 0;   //////////////////////////////////////////////////////////// /
                    for (i = 0; i < noofteams; i++)
                    {
                        if (dataGridView8.Rows[i].Cells[3].Value.ToString() != "")
                        {
                            dataGridView8.Rows[i].Cells[7].Value = "Box Score";
                            dataGridView8.Rows[i].Cells[7].Style.ForeColor = Color.Blue;
                            dataGridView8.Rows[i].Cells[7].Style.Font = new System.Drawing.Font("Ariel", 9, FontStyle.Underline);
                        }
                        else
                        {
                            dataGridView8.Rows[i].Cells[7].Value = "Matchup";
                            dataGridView8.Rows[i].Cells[7].Style.ForeColor = Color.Blue;
                            dataGridView8.Rows[i].Cells[7].Style.Font = new System.Drawing.Font("Ariel", 9, FontStyle.Underline);
                            //  dataGridView8.Columns[5].Name = "Matchup";
                        }
                        if (dataGridView8.Rows[i].Cells[3].Value.ToString() != "" && dataGridView8.Rows[i].Cells[2].Value.ToString().Contains("Final") != true)
                        {
                            coversbsno++;
                        }
                    }

                    dataGridView8.Rows[0].Cells[0].Selected = false;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                //   MessageBox.Show("Cant connect to pinnaclesports.com", "Warning!", MessageBoxButtons.OK);
            }
        }

        private void dataGridView8_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            var dataGridView = (sender as DataGridView);
            //Check the condition as per the requirement casting the cell value to the appropriate type
            if (e.ColumnIndex == 7)
                dataGridView.Cursor = Cursors.Hand;
            else
                dataGridView.Cursor = Cursors.Arrow;
        }

        private void dataGridView8_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView8.Rows[e.RowIndex].Cells[7].Selected)
            {
                //if (dataGridView8.Rows[e.RowIndex].Cells[3].Value.ToString().Length > 1)
                //{
                int clicked = e.RowIndex;
                if (dataGridView8.Rows[clicked].Cells[7].Value.ToString() == "Box Score")
                {
                    System.Diagnostics.Process.Start(coversboxscore[clicked]);
                }
                else if (dataGridView8.Rows[clicked].Cells[7].Value.ToString() == "Matchup")
                {
                    System.Diagnostics.Process.Start(coversMatchup[clicked]);
                }

                // }
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                bool stats;
                if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable() == true)
                {
                    stats = true;
                }
                else
                {
                    stats = false;
                }


                if (stats == true)
                {
                    string datum1 = dateTimePicker2.Text;
                    WebClient wc = new WebClient();

                    if (radioBtnNBA.Checked == true)
                    {
                        coversstring = wc.DownloadString("http://www.covers.com/Sports/NBA/Matchups?selectedDate=" + datum1 + "");
                    }
                    else
                    {
                        coversstring = wc.DownloadString("http://www.covers.com/Sports/MLB/Matchups?selectedDate=" + datum1 + "");
                    }
                }

                //string datum2 = dateTimePicker2.Text;
                //var webClient = new WebClient();
                //string html = webClient.DownloadString("http://www.covers.com/Sports/NBA/Matchups?selectedDate=" + datum2 + "");
                //var doc = new HtmlDocument();
                //doc.LoadHtml(html);

                //HtmlNodeCollection hnc = doc.DocumentNode.SelectNodes(".//div[@class='cmg_team_name']");


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            if (radioBtnNBA.Checked == true)
            {
                LiveScoresNBA(sender, e);
            }
            else if (radioBtnMLB.Checked == true)
            {
                LiveScoresMLB(sender, e);
            }
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            DateTime dt2 = dateTimePicker2.Value.Date.AddDays(-1);
            string Datum2 = dt2.ToString("yyyy-MM-dd");
            dateTimePicker2.Text = Datum2;
        }

        private void pictureBox18_Click(object sender, EventArgs e)
        {
            DateTime dt2 = dateTimePicker2.Value.Date.AddDays(+1);
            string Datum2 = dt2.ToString("yyyy-MM-dd");
            dateTimePicker2.Text = Datum2;
        }

        private void pictureBox1_MouseHover_2(object sender, EventArgs e)
        {
            pictureBox1.Cursor = Cursors.Hand;
            pictureBox1.Image = Oklade.Properties.Resources.back_arrow2;
        }

        private void pictureBox1_MouseLeave_2(object sender, EventArgs e)
        {
            pictureBox1.Image = Oklade.Properties.Resources.back_arrow1;
        }

        private void pictureBox18_MouseHover(object sender, EventArgs e)
        {
            pictureBox18.Cursor = Cursors.Hand;
            pictureBox18.Image = Oklade.Properties.Resources.arrow_right2;
        }

        private void pictureBox18_MouseLeave(object sender, EventArgs e)
        {
            pictureBox18.Image = Oklade.Properties.Resources.arrow_right1;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                DateTime dt3 = dateTimePicker2.Value.Date;
                dateforupdate = dt3.ToString("dd.M.yyyy.");

                for (int i = 0; i < dataGridView8.Rows.Count; i++)
                {
                    if (dataGridView8.Rows[i].Cells[2].Value.ToString().Contains("Final") == true)
                    {
                        int totalscore = Convert.ToInt16(dataGridView8.Rows[i].Cells["Home Score"].Value) + Convert.ToInt16(dataGridView8.Rows[i].Cells["Away Score"].Value);

                        string gettotal = @"SELECT [Total] FROM [LiveScoresUpdates] where Date = '" + dateforupdate + "' " +
                      " and AwayTeam = '" + dataGridView8.Rows[i].Cells["Away Team"].Value + "'";

                        SqlCommand cmdtotal = new SqlCommand(gettotal, connection);
                        SqlDataReader readtotal = cmdtotal.ExecuteReader();

                        while (readtotal.Read())
                        {
                            totaloffer = (Convert.ToDouble(readtotal["Total"]));
                        }
                        readtotal.Close();

                        if (totalscore > totaloffer)
                        {
                            updateOUr = "O";
                        }
                        else if (totalscore == totaloffer)
                        {
                            updateOUr = "P";
                        }
                        else if (totalscore < totaloffer)
                        {
                            updateOUr = "U";
                        }

                        if (Convert.ToInt16(dataGridView8.Rows[i].Cells["Home Score"].Value) > Convert.ToInt16(dataGridView8.Rows[i].Cells["Away Score"].Value))
                        {
                            updateSUr = "W";
                        }
                        else if (Convert.ToInt16(dataGridView8.Rows[i].Cells["Home Score"].Value) < Convert.ToInt16(dataGridView8.Rows[i].Cells["Away Score"].Value))
                        {
                            updateSUr = "L";
                        }

                        string query4 = @"UPDATE LiveScoresUpdates set HomeScore = " + dataGridView8.Rows[i].Cells["Home Score"].Value + ", " +
                      "  AwayScore = " + dataGridView8.Rows[i].Cells["Away Score"].Value + ", OUr = '" + updateOUr + "', SUr = '" + updateSUr + "' where Date = '" + dateforupdate + "' " +
                      " and AwayTeam = '" + dataGridView8.Rows[i].Cells["Away Team"].Value + "' ";
                        SqlCommand cmd114 = new SqlCommand(query4, connection);
                        cmd114.ExecuteNonQuery();
                    }
                }
                MessageBox.Show("Game Results Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void radioBtnNBA_CheckedChanged(object sender, EventArgs e)
        {
            //ManualChange = 1;
            DateTime dtCheck = DateTime.Today;
            DateTime dtPlayoffsStart = new DateTime(dtCheck.Year, 4, 14);
            DateTime dtPlayoffsEnd = new DateTime(dtCheck.Year, 6, 14);

            if (dtCheck.Date >= dtPlayoffsStart && dtCheck.Date < dtPlayoffsEnd)
            {
                pictureBox4.ImageLocation = "http://2.bp.blogspot.com/-fUNKupJA9bE/VJyYeZh3e3I/AAAAAAAAH1A/iRCcaHioIbE/w1200-h630-p-nu/Logo%2BNBA_Playoffs.png";

            }
            else
            {
                pictureBox4.ImageLocation = "https://s3.amazonaws.com/piktochartv2-dev/v2/uploads/f4626f5a-9ce6-4cf6-ae5d-a18b459a2737/59aae3d53b6f595d86feddd505467d1dd0e60e51_original.png";
            }

            label6.Hide();
            label7.Hide();
            awayStarterCB.Hide();
            homeStarterCB.Hide();

            Form2_Load(sender, e);
            NewGameBtn_Click(sender, e);
        }

        private void radioBtnMLB_CheckedChanged(object sender, EventArgs e)
        {
            //ManualChange = 1;
            pictureBox4.ImageLocation = "http://logonoid.com/images/mlb-logo.png";
            awayStarterCB.Show();
            homeStarterCB.Show();
            label6.Show();
            label7.Show();

            Form2_Load(sender, e);
            NewGameBtn_Click(sender, e);
        }

        private void awayStarterCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            //    connection.Open();

            //    String query133 = "Select Hand from MLBStarters where FirstName = '" + awayStarterCB.Text.Split(' ')[0] + "' and LastName = '" + awayStarterCB.Text.Split(' ')[1] + "'";
            //    SqlCommand cmd133 = new SqlCommand(query133, connection);
            //    SqlDataReader read133 = cmd133.ExecuteReader();
            //    while (read133.Read())
            //    {
            //        AwayStarterHand = (read133["Hand"].ToString());
            //    }
            //    read133.Close();

            //    if (AwayStarterHand.Length < 1)
            //    {
            //        AwayStarterHand = dataGridView8.Rows[dataGridView8.CurrentCell.RowIndex].Cells[5].Value.ToString().Split(' ')[2];
            //    }

            //    label11.Text = "(" + AwayStarterHand + ")";

            //    if (AwayTeamTabControl.SelectedIndex == 1)
            //    {
            //        AwayStarterL10();
            //    }
            //}
        }

        private void HomeStarterL10()
        {
            // -------------------------------------------home STARTER LAST 10-------------------------------------
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (radioBtnMLB.Checked == true)
                {
                    //  awayStarterCB.Enabled = true;
                    Sql33Last10 = "select TOP 10 Dayofmonth,Month,StarterHLastName,StarterALastName, Hscore, Ascore, Line, Total, SUr, Hits, OUr,Team, Opp from " + AllNBAMLBResultsVar + " where StarterHFirstName like '" + homeStarterCB.Text.Split(' ')[0] +
                            "%' and StarterHLastName = '" + homeStarterCB.Text.Split(' ')[1] + "' or StarterAFirstName like '" + homeStarterCB.Text.Split(' ')[0] +
                            "%' and StarterALastName = '" + homeStarterCB.Text.Split(' ')[1] + "' order by ID desc";

                    HStarterL10DGV.DataSource = null;
                    ds4 = new DataSet();
                    adapter4 = new SqlDataAdapter(Sql33Last10, connection);
                    ds4.Reset();
                    adapter4.Fill(ds4);
                    HStarterL10DGV.DataSource = ds4.Tables[0];

                    // HStarterL10DGV.CurrentCell.Selected = false;

                    System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                    boldStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);

                    HStarterL10DGV.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                    HStarterL10DGV.Columns[0].HeaderText = "Day";
                    HStarterL10DGV.Columns[0].Width = 35;
                    HStarterL10DGV.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                    HStarterL10DGV.Columns[1].HeaderText = "Month";
                    HStarterL10DGV.Columns[1].Width = 40;
                    HStarterL10DGV.Columns[2].DefaultCellStyle.BackColor = Color.DarkGray;
                    HStarterL10DGV.Columns[2].Width = 107;
                    HStarterL10DGV.Columns[2].HeaderText = "Home Starter";
                    HStarterL10DGV.Columns[3].DefaultCellStyle.BackColor = Color.DarkGray;
                    HStarterL10DGV.Columns[3].Width = 107;
                    HStarterL10DGV.Columns[3].HeaderText = "Away Starter";
                    HStarterL10DGV.Columns[4].DefaultCellStyle.BackColor = Color.Silver;
                    HStarterL10DGV.Columns[4].HeaderText = "Home Points";
                    HStarterL10DGV.Columns[5].DefaultCellStyle.BackColor = Color.Silver;
                    HStarterL10DGV.Columns[5].HeaderText = "Away Points";
                    HStarterL10DGV.Columns[6].DefaultCellStyle.BackColor = Color.LightGray;
                    HStarterL10DGV.Columns[7].DefaultCellStyle.BackColor = Color.LightGray;
                    HStarterL10DGV.Columns[8].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    HStarterL10DGV.Columns[8].DefaultCellStyle = boldStyle;
                    HStarterL10DGV.Columns[9].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    HStarterL10DGV.Columns[9].DefaultCellStyle = boldStyle;
                    HStarterL10DGV.Columns[10].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    HStarterL10DGV.Columns[10].DefaultCellStyle = boldStyle;
                    HStarterL10DGV.Columns[11].HeaderText = "Home Team";
                    HStarterL10DGV.Columns[12].HeaderText = "Away Team";
                    HStarterL10DGV.Columns[11].Width = 107;
                    HStarterL10DGV.Columns[12].Width = 107;
                    HStarterL10DGV.Columns[11].Width = 60;
                    HStarterL10DGV.Columns[12].Width = 60;
                    double line1;

                    DataGridViewRow row = new DataGridViewRow();
                    for (int i = 0; i < HStarterL10DGV.Rows.Count - 1; i++)
                    {

                        if (HStarterL10DGV.Rows[i].Cells[10].Value.ToString() == "U")
                        {
                            HStarterL10DGV.Rows[i].Cells[10].Style.ForeColor = Color.ForestGreen;
                        }
                        else if (HStarterL10DGV.Rows[i].Cells[10].Value.ToString() == "O")
                        {
                            HStarterL10DGV.Rows[i].Cells[10].Style.ForeColor = Color.DarkRed;
                        }

                        ////////////////////////////////////////////////////////////////////////////////////////////

                        if (homeStarterCB.Text.Split(' ')[1] == HStarterL10DGV.Rows[i].Cells[2].Value.ToString())
                        {
                            HStarterL10DGV.Rows[i].Cells[2].Style = boldStyle;
                            if (HStarterL10DGV.Rows[i].Cells[8].Value.ToString() == "L")
                            {
                                HStarterL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.DarkRed;
                            }
                            if (HStarterL10DGV.Rows[i].Cells[8].Value.ToString() == "W")
                            {
                                HStarterL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.ForestGreen;
                            }
                            if (HStarterL10DGV.Rows[i].Cells[9].Value.ToString() == "L")
                            {
                                HStarterL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.DarkRed;
                            }
                            if (HStarterL10DGV.Rows[i].Cells[9].Value.ToString() == "W")
                            {
                                HStarterL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.ForestGreen;
                            }
                        }
                        if (homeStarterCB.Text.Split(' ')[1] == HStarterL10DGV.Rows[i].Cells[3].Value.ToString())
                        {
                            HStarterL10DGV.Rows[i].Cells[3].Style = boldStyle;
                            if (HStarterL10DGV.Rows[i].Cells[8].Value.ToString() == "L")
                            {
                                HStarterL10DGV.Rows[i].Cells[8].Value = "W";
                                HStarterL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.ForestGreen;
                            }
                            else if (HStarterL10DGV.Rows[i].Cells[8].Value.ToString() == "W")
                            {
                                HStarterL10DGV.Rows[i].Cells[8].Value = "L";
                                HStarterL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.DarkRed;
                            }
                            if (HStarterL10DGV.Rows[i].Cells[9].Value.ToString() == "L")
                            {
                                HStarterL10DGV.Rows[i].Cells[9].Value = "W";
                                HStarterL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.ForestGreen;
                            }
                            else if (HStarterL10DGV.Rows[i].Cells[9].Value.ToString() == "W")
                            {
                                HStarterL10DGV.Rows[i].Cells[9].Value = "L";
                                HStarterL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.DarkRed;
                            }
                            if (HStarterL10DGV.Rows[i].Cells[6].Value.ToString() != "")
                            {
                                line1 = Convert.ToDouble(HStarterL10DGV.Rows[i].Cells[6].Value.ToString().Replace(".", ","));
                                line1 = line1 * (-1);
                                HStarterL10DGV.Rows[i].Cells[6].Value = line1.ToString();
                            }
                        }
                    }
                }

                HStarterL10DGV.Rows[0].Cells[0].Selected = false;
                // -------------------------------------------home STARTER LAST 10-------------------------------------
            }
        }

        private void AwayStarterL10()
        {
            // -------------------------------------------AWAY STARTER LAST 10-------------------------------------
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (radioBtnMLB.Checked == true)
                {
                    //  awayStarterCB.Enabled = true;
                    Sql33Last10 = "select TOP 10 Dayofmonth,Month,StarterHLastName,StarterALastName, Hscore, Ascore, Line, Total, SUr, Hits, OUr,Team, Opp from " + AllNBAMLBResultsVar + " where StarterHFirstName like '" + awayStarterCB.Text.Split(' ')[0] +
                            "%' and StarterHLastName = '" + awayStarterCB.Text.Split(' ')[1] + "' or StarterAFirstName like '" + awayStarterCB.Text.Split(' ')[0] +
                            "%' and StarterALastName = '" + awayStarterCB.Text.Split(' ')[1] + "' order by ID desc";

                    AStarterL10DGV.DataSource = null;
                    ds4 = new DataSet();
                    adapter4 = new SqlDataAdapter(Sql33Last10, connection);
                    ds4.Reset();
                    adapter4.Fill(ds4);
                    AStarterL10DGV.DataSource = ds4.Tables[0];

                    // AStarterL10DGV.CurrentCell.Selected = false;

                    System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                    boldStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);

                    AStarterL10DGV.Columns[0].DefaultCellStyle.BackColor = Color.DarkGray;
                    AStarterL10DGV.Columns[0].HeaderText = "Day";
                    AStarterL10DGV.Columns[0].Width = 35;
                    AStarterL10DGV.Columns[1].DefaultCellStyle.BackColor = Color.DarkGray;
                    AStarterL10DGV.Columns[1].HeaderText = "Month";
                    AStarterL10DGV.Columns[1].Width = 40;
                    AStarterL10DGV.Columns[2].DefaultCellStyle.BackColor = Color.DarkGray;
                    AStarterL10DGV.Columns[2].Width = 107;
                    AStarterL10DGV.Columns[2].HeaderText = "Home Starter";
                    AStarterL10DGV.Columns[3].DefaultCellStyle.BackColor = Color.DarkGray;
                    AStarterL10DGV.Columns[3].Width = 107;
                    AStarterL10DGV.Columns[3].HeaderText = "Away Starter";
                    AStarterL10DGV.Columns[4].DefaultCellStyle.BackColor = Color.Silver;
                    AStarterL10DGV.Columns[4].HeaderText = "Home Points";
                    AStarterL10DGV.Columns[5].DefaultCellStyle.BackColor = Color.Silver;
                    AStarterL10DGV.Columns[5].HeaderText = "Away Points";
                    AStarterL10DGV.Columns[6].DefaultCellStyle.BackColor = Color.LightGray;
                    AStarterL10DGV.Columns[7].DefaultCellStyle.BackColor = Color.LightGray;
                    AStarterL10DGV.Columns[8].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    AStarterL10DGV.Columns[8].DefaultCellStyle = boldStyle;
                    AStarterL10DGV.Columns[9].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    AStarterL10DGV.Columns[9].DefaultCellStyle = boldStyle;
                    AStarterL10DGV.Columns[10].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                    AStarterL10DGV.Columns[10].DefaultCellStyle = boldStyle;
                    AStarterL10DGV.Columns[11].HeaderText = "Home Team";
                    AStarterL10DGV.Columns[12].HeaderText = "Away Team";
                    AStarterL10DGV.Columns[11].Width = 107;
                    AStarterL10DGV.Columns[12].Width = 107;
                    AStarterL10DGV.Columns[11].Width = 60;
                    AStarterL10DGV.Columns[12].Width = 60;
                    double line1;

                    DataGridViewRow row = new DataGridViewRow();
                    for (int i = 0; i < AStarterL10DGV.Rows.Count - 1; i++)
                    {

                        if (AStarterL10DGV.Rows[i].Cells[10].Value.ToString() == "U")
                        {
                            AStarterL10DGV.Rows[i].Cells[10].Style.ForeColor = Color.ForestGreen;
                        }
                        else if (AStarterL10DGV.Rows[i].Cells[10].Value.ToString() == "O")
                        {
                            AStarterL10DGV.Rows[i].Cells[10].Style.ForeColor = Color.DarkRed;
                        }

                        ////////////////////////////////////////////////////////////////////////////////////////////

                        if (awayStarterCB.Text.Split(' ')[1] == AStarterL10DGV.Rows[i].Cells[2].Value.ToString())
                        {
                            AStarterL10DGV.Rows[i].Cells[2].Style = boldStyle;
                            if (AStarterL10DGV.Rows[i].Cells[8].Value.ToString() == "L")
                            {
                                AStarterL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.DarkRed;
                            }
                            if (AStarterL10DGV.Rows[i].Cells[8].Value.ToString() == "W")
                            {
                                AStarterL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.ForestGreen;
                            }
                            if (AStarterL10DGV.Rows[i].Cells[9].Value.ToString() == "L")
                            {
                                AStarterL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.DarkRed;
                            }
                            if (AStarterL10DGV.Rows[i].Cells[9].Value.ToString() == "W")
                            {
                                AStarterL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.ForestGreen;
                            }
                        }
                        if (awayStarterCB.Text.Split(' ')[1] == AStarterL10DGV.Rows[i].Cells[3].Value.ToString())
                        {
                            AStarterL10DGV.Rows[i].Cells[3].Style = boldStyle;
                            if (AStarterL10DGV.Rows[i].Cells[8].Value.ToString() == "L")
                            {
                                AStarterL10DGV.Rows[i].Cells[8].Value = "W";
                                AStarterL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.ForestGreen;
                            }
                            else if (AStarterL10DGV.Rows[i].Cells[8].Value.ToString() == "W")
                            {
                                AStarterL10DGV.Rows[i].Cells[8].Value = "L";
                                AStarterL10DGV.Rows[i].Cells[8].Style.ForeColor = Color.DarkRed;
                            }
                            if (AStarterL10DGV.Rows[i].Cells[9].Value.ToString() == "L")
                            {
                                AStarterL10DGV.Rows[i].Cells[9].Value = "W";
                                AStarterL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.ForestGreen;
                            }
                            else if (AStarterL10DGV.Rows[i].Cells[9].Value.ToString() == "W")
                            {
                                AStarterL10DGV.Rows[i].Cells[9].Value = "L";
                                AStarterL10DGV.Rows[i].Cells[9].Style.ForeColor = Color.DarkRed;
                            }
                            if (AStarterL10DGV.Rows[i].Cells[6].Value.ToString() != "")
                            {
                                line1 = Convert.ToDouble(AStarterL10DGV.Rows[i].Cells[6].Value.ToString().Replace(".", ","));
                                line1 = line1 * (-1);
                                AStarterL10DGV.Rows[i].Cells[6].Value = line1.ToString();
                            }
                        }
                    }
                }

                AStarterL10DGV.Rows[0].Cells[0].Selected = false;
                // -------------------------------------------AWAY STARTER LAST 10-------------------------------------
            }
        }

        private void homeStarterCB_SelectedValueChanged(object sender, EventArgs e)
        {
            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            //    connection.Open();

            //    String query134 = "Select Hand from MLBStarters where FirstName = '" + homeStarterCB.Text.Split(' ')[0] + "' and LastName = '" + homeStarterCB.Text.Split(' ')[1] + "'";
            //    SqlCommand cmd134 = new SqlCommand(query134, connection);
            //    SqlDataReader read134 = cmd134.ExecuteReader();
            //    while (read134.Read())
            //    {
            //        HomeStarterHand = (read134["Hand"].ToString());
            //    }
            //    read134.Close();

            //    if (radioBtnMLB.Checked == true)
            //    {
            //        calculateAveragesSUOUHomeTeam();
            //        calculateAveragesSUOUAwayTeam();
            //        showAveragesinDGV();
            //    }

            //    if (HomeStarterHand == "")
            //    {
            //        HomeStarterHand = dataGridView8.Rows[dataGridView8.CurrentCell.RowIndex].Cells[6].Value.ToString().Split(' ')[2];
            //    }

            //    label44.Text = "(" + HomeStarterHand + ")";

            //    if (HomeTeamTabControl.SelectedIndex == 1)
            //    {
            //        HomeStarterL10();
            //    }
            //}
        }

        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                if (radioBtnMLB.Checked == true)
                {
                    awayStarterCB.Focus();
                }
                else
                {
                    HomeTeamCB.Focus();
                }
                AwayTeamCB.SelectionStart = 0;
                AwayTeamCB.SelectionLength = 0;
            }
        }

        private void awayStarterCB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;

                HomeTeamCB.Focus();

                awayStarterCB.SelectionLength = 0;
            }
        }

        private void comboBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                if (radioBtnMLB.Checked == true)
                {
                    homeStarterCB.Focus();
                }
                else
                {
                    TotalBox.Focus();
                }
                HomeTeamCB.SelectionLength = 0;
            }
        }

        private void homeStarterCB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;

                TotalBox.Focus();

                homeStarterCB.SelectionLength = 0;
            }
        }

        private void TotalBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;

                HandicapBox.Focus();

                if (radioBtnMLB.Checked == true)
                {
                    HandicapBox.Text = "0";
                }

                AutoProcessBtn.Focus();

                TotalBox.SelectionLength = 0;
            }
        }

        private void Analyzer_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Control && e.KeyCode == Keys.A)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                AutoProcess_Click_1(sender, e);
            }
            else if (e.Control && e.KeyCode == Keys.D)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                deleteBtn_Click(sender, e);
            }
            else if (e.Control && e.KeyCode == Keys.C)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                NewGameBtn_Click(sender, e);
            }
        }
        public void LiveScoresMLB(object sender, EventArgs e)
        {
            try
            {
                //coversInnings = null;


                if (coversstring.Length > 1)
                {
                    for (i = 0; i < 36; i++)
                    {
                        coversteam[i] = null;
                        coversgamestatus[i] = "";
                        coversgamestatus2[i] = "";
                        covershpoints[i] = "";
                        covershpoints2[i] = "";
                        coversapoints[i] = "";
                        coversapoints2[i] = "";
                        coversboxscore[i] = "";
                        coversInnings[i] = null;
                        coversPitcher[i] = null;
                        coversdiamond[i] = null;
                        coversballs[i] = null;
                        coversstrikes[i] = null;
                        coversouts[i] = null;
                        coversTotal[i] = null;
                        coversLine[i] = null;

                    }

                    Match mTeamh = Regex.Match(coversstring, @"<div class=""cmg_team_name"">(.*?)</div>", RegexOptions.Singleline);
                    if (mTeamh.Success)
                    {
                        coversteam[0] = mTeamh.Groups[0].Value;
                        coversteam[0] = Regex.Replace(coversteam[0], @"<div class=""cmg_team_name"">", "").Trim();
                        coversteam[0] = Regex.Replace(coversteam[0], @"<span>(.*?)</span>", "").Trim();
                        coversteam[0] = Regex.Replace(coversteam[0], @"</div>", "").Trim();
                        coversteam[0] = Regex.Replace(coversteam[0], @"\n", "").Trim();
                        coversindex[0] = mTeamh.Index;
                    }

                    i = 1;
                    while (coversteam[i - 1].Length > 1)
                    {
                        mTeamh = mTeamh.NextMatch();
                        if (mTeamh.Success)
                        {
                            coversteam[i] = mTeamh.Groups[0].Value;
                            coversteam[i] = Regex.Replace(coversteam[i], @"<div class=""cmg_team_name"">", "").Trim();
                            coversteam[i] = Regex.Replace(coversteam[i], @"<span>(.*?)</span>", "").Trim();
                            coversteam[i] = Regex.Replace(coversteam[i], @"</div>", "").Trim();
                            coversteam[i] = Regex.Replace(coversteam[i], @"\n", "").Trim();
                            coversindex[i] = mTeamh.Index;
                            i++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    int noofteams2 = i;
                    noofteams2 = noofteams2 / 2;

                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mGameStatus = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_game_time"">(.*?)</div>", RegexOptions.Singleline);
                        if (mGameStatus.Success)
                        {
                            coversgamestatus[i] = mGameStatus.Groups[0].Value;
                            coversgamestatus[i] = Regex.Replace(coversgamestatus[i], @"<div class=""cmg_game_time"">", "").Trim();
                            coversgamestatus[i] = Regex.Replace(coversgamestatus[i], @"</div>", "").Trim();
                            coversgamestatus[i] = Regex.Replace(coversgamestatus[i], @"ET", "").Trim();
                            //   coversgamestatus[0] = coversgamestatus2[0].Substring(0,10);

                        }
                        j++;
                        j++;
                    }


                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mGameStatus2 = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_matchup_list_status"">(.*?)</div>", RegexOptions.Singleline);
                        if (mGameStatus2.Success)
                        {
                            coversgamestatus2[i] = mGameStatus2.Groups[0].Value;
                            coversgamestatus2[i] = Regex.Replace(coversgamestatus2[i], @"<div class=""cmg_matchup_list_status"">", "").Trim();
                            coversgamestatus2[i] = Regex.Replace(coversgamestatus2[i], @"</div>", "").Trim();
                            // coversgamestatus2[0] = Regex.Replace(coversgamestatus2[0], @"  ", "").Trim();
                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mResultHome = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class='cmg_matchup_list_score_home(.*?)</div>", RegexOptions.Singleline);
                        if (mResultHome.Success)
                        {
                            covershpoints[i] = mResultHome.Groups[0].Value;
                            covershpoints[i] = Regex.Replace(covershpoints[i], @"<div class='cmg_matchup_list_score_home", "").Trim();
                            covershpoints[i] = Regex.Replace(covershpoints[i], @"</div>", "").Trim();
                            covershpoints[i] = Regex.Replace(covershpoints[i], @"cmg_matchup_list_winner'>", "").Trim();
                            covershpoints[i] = Regex.Replace(covershpoints[i], @">", "").Trim();
                            covershpoints[i] = Regex.Replace(covershpoints[i], @"'", "").Trim();
                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mResultHome2 = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_matchup_list_score_home"">(.*?)</div>", RegexOptions.Singleline);
                        if (mResultHome2.Success)
                        {
                            covershpoints2[i] = mResultHome2.Groups[0].Value;
                            covershpoints2[i] = Regex.Replace(covershpoints2[i], @"<div class=""cmg_matchup_list_score_home"">", "").Trim();
                            covershpoints2[i] = Regex.Replace(covershpoints2[i], @"</div>", "").Trim();
                            covershpoints2[i] = Regex.Replace(covershpoints2[i], @"cmg_matchup_list_winner'>", "").Trim();
                            covershpoints2[i] = Regex.Replace(covershpoints2[i], @">", "").Trim();

                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mResultAway = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class='cmg_matchup_list_score_away(.*?)</div>", RegexOptions.Singleline);
                        if (mResultAway.Success)
                        {
                            coversapoints[i] = mResultAway.Groups[0].Value;
                            coversapoints[i] = Regex.Replace(coversapoints[i], @"<div class='cmg_matchup_list_score_away", "").Trim();
                            coversapoints[i] = Regex.Replace(coversapoints[i], @"</div>", "").Trim();
                            coversapoints[i] = Regex.Replace(coversapoints[i], @"cmg_matchup_list_winner'>", "").Trim();
                            coversapoints[i] = Regex.Replace(coversapoints[i], @">", "").Trim();
                            coversapoints[i] = Regex.Replace(coversapoints[i], @"'", "").Trim();

                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mResultAway2 = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_matchup_list_score_away"">(.*?)</div>", RegexOptions.Singleline);
                        if (mResultAway2.Success)
                        {
                            coversapoints2[i] = mResultAway2.Groups[0].Value;
                            coversapoints2[i] = Regex.Replace(coversapoints2[i], @"<div class=""cmg_matchup_list_score_away"">", "").Trim();
                            coversapoints2[i] = Regex.Replace(coversapoints2[i], @"</div>", "").Trim();
                            coversapoints2[i] = Regex.Replace(coversapoints2[i], @">", "").Trim();
                        }
                        j++;
                        j++;
                    }

                    // <div class="cmg_baseball_diamond"><img src="http://images.covers.com/scores/baseball/0.png"></div>


                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mCoversDiamond = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_baseball_diamond"">(.*?)</div>", RegexOptions.Singleline);
                        if (mCoversDiamond.Success)
                        {
                            coversdiamond[i] = mCoversDiamond.Groups[0].Value;
                            coversdiamond[i] = Regex.Replace(coversdiamond[i], @"<div class=""cmg_baseball_diamond""><img src =", "").Trim();
                            coversdiamond[i] = Regex.Replace(coversdiamond[i], @"/></div>", "").Trim();
                            coversdiamond[i] = Regex.Replace(coversdiamond[i], @"""", "").Trim();
                            // coversgamestatus2[0] = Regex.Replace(coversgamestatus2[0], @"  ", "").Trim();
                        }
                        j++;
                        j++;
                    }

                    // <div class="cmg_baseball_strikes">S: <img src="http://images.covers.com/scores/baseball/1s.png"></div>
                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mCoversBalls = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_baseball_balls"">(.*?)</div>", RegexOptions.Singleline);
                        if (mCoversBalls.Success)
                        {
                            coversballs[i] = mCoversBalls.Groups[0].Value;
                            coversballs[i] = Regex.Replace(coversballs[i], @"<div class=""cmg_baseball_balls"">B: <img src =", "").Trim();
                            coversballs[i] = Regex.Replace(coversballs[i], @"/></div>", "").Trim();
                            //    coversballs[i] = Regex.Replace(coversballs[i], @"<div class=""cmg_baseball_strikes"">", "").Trim();
                            coversballs[i] = Regex.Replace(coversballs[i], @"<img src =", "").Trim();
                            coversballs[i] = Regex.Replace(coversballs[i], @"""", "").Trim();
                            // coversgamestatus2[0] = Regex.Replace(coversgamestatus2[0], @"  ", "").Trim();
                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mCoversStrikes = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_baseball_strikes"">(.*?)</div>", RegexOptions.Singleline);
                        if (mCoversStrikes.Success)
                        {
                            coversstrikes[i] = mCoversStrikes.Groups[0].Value;
                            coversstrikes[i] = Regex.Replace(coversstrikes[i], @"<div class=""cmg_baseball_strikes"">S: <img src =", "").Trim();
                            coversstrikes[i] = Regex.Replace(coversstrikes[i], @"/></div>", "").Trim();
                            //    coversstrikes[i] = Regex.Replace(coversstrikes[i], @"<div class=""cmg_baseball_strikes"">", "").Trim();
                            coversstrikes[i] = Regex.Replace(coversstrikes[i], @"<img src =", "").Trim();
                            coversstrikes[i] = Regex.Replace(coversstrikes[i], @"""", "").Trim();
                            // coversgamestatus2[0] = Regex.Replace(coversgamestatus2[0], @"  ", "").Trim();
                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mCoversOuts = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"<div class=""cmg_baseball_outs"">(.*?)</div>", RegexOptions.Singleline);
                        if (mCoversOuts.Success)
                        {
                            coversouts[i] = mCoversOuts.Groups[0].Value;
                            coversouts[i] = Regex.Replace(coversouts[i], @"<div class=""cmg_baseball_outs"">O: <img src =", "").Trim();
                            coversouts[i] = Regex.Replace(coversouts[i], @"/></div>", "").Trim();
                            //    coversouts[i] = Regex.Replace(coversouts[i], @"<div class=""cmg_baseball_strikes"">", "").Trim();
                            coversouts[i] = Regex.Replace(coversouts[i], @"<img src =", "").Trim();
                            coversouts[i] = Regex.Replace(coversouts[i], @"""", "").Trim();
                            // coversgamestatus2[0] = Regex.Replace(coversgamestatus2[0], @"  ", "").Trim();
                        }
                        j++;
                        j++;
                    }


                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mCoversTotal = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"LIVE:</span>(.*?)</span>", RegexOptions.Singleline);
                        if (mCoversTotal.Success)
                        {
                            coversTotal[i] = mCoversTotal.Groups[0].Value;

                            coversTotal[i] = Regex.Replace(coversTotal[i], @"[^-\d*\.?\d+$]", String.Empty).Trim();

                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"\r\n", String.Empty).Trim();
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"LIVE:<span>O/U:", "").Trim();
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @" | </span>", "");
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"</span>", "");
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"\|", "");
                            //coversTotal[i] = Regex.Replace(coversTotal[i], @"LIVE:<span>O/U:", "").Trim();

                        }
                        j++;
                        j++;
                    }

                    j = 0;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mcoversLine = Regex.Match(coversstring.Substring(coversindex[j], (coversindex[j + 1] - coversindex[j])), @"LIVE:</span>(.*?)</div>", RegexOptions.Singleline);
                        if (mcoversLine.Success)
                        {
                            coversLine[i] = mcoversLine.Groups[0].Value;
                            coversLine[i] = Regex.Replace(coversLine[i], @"[^-\d*\.?\d+$]", String.Empty).Trim();

                            //coversLine[i] = Regex.Replace(coversLine[i], @"\r\n", String.Empty).Trim();
                            //coversLine[i] = Regex.Replace(coversLine[i], @"LIVE:<span>O/U:", "").Trim();
                            //coversLine[i] = Regex.Replace(coversLine[i], @" | </span>", "");
                            //coversLine[i] = Regex.Replace(coversLine[i], @"</span>", "");
                            //coversLine[i] = Regex.Replace(coversLine[i], @"\|", "");
                            //coversLine[i] = Regex.Replace(coversLine[i], @"LIVE:<span>O/U:", "").Trim();
                            //coversLine[i] = Regex.Replace(coversLine[i], @"(.*?)<span>", "").Trim();
                            //coversLine[i] = Regex.Replace(coversLine[i], @"</div>", "").Trim();
                            coversLine[i] = Regex.Replace(coversLine[i], @"^[^-+]*", "").Trim();
                        }
                        j++;
                        j++;
                    }



                    Match mLineHistory = Regex.Match(coversstring, @">Line History</a>", RegexOptions.Multiline);
                    if (mLineHistory.Success)
                    {
                        LineHistory[0] = mLineHistory.Groups[0].Value;
                        coversindex2[0] = mLineHistory.Index;
                    }
                    i = 1;
                    while (LineHistory[i - 1].Length >= 1)
                    {
                        mLineHistory = mLineHistory.NextMatch();
                        if (mLineHistory.Success)
                        {
                            LineHistory[i] = mLineHistory.Groups[0].Value;
                            coversindex2[i] = mLineHistory.Index;

                            i++;
                        }
                        else
                        {
                            coversindex2[i] = coversstring.Length - 1;
                            break;
                        }
                    }

                    j = 1;
                    for (i = 0; i < noofteams2; i++)
                    {
                        //   Match mBoxScore = Regex.Match(coversstring.Substring(coversindex[j], (coversindex2[i]-coversindex[j])), @"<div class=""cmg_l_row cmg_matchup_list_gamebox"">(.*?)"">Boxscore", RegexOptions.Singleline);
                        Match mBoxScore = Regex.Match(coversstring.Substring(coversindex[j], (coversindex2[i] - coversindex[j])), @"results(.*?)"">Boxscore", RegexOptions.Singleline);

                        if (mBoxScore.Success)
                        {
                            coversboxscore[i] = mBoxScore.Groups[0].Value;
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @"cmg_matchup_list_gamebox"">", "").Trim();
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @">", "").Trim();
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @"\\", "").Trim();
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @"Boxscore", "").Trim();
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @"<a href=", "").Trim();
                            coversboxscore[i] = Regex.Replace(coversboxscore[i], @"""", "").Trim();
                            coversboxscore[i] = "http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/mlb/" + coversboxscore[i];

                        }
                        j++;
                        j++;
                    }

                    j = 1;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mMatchup = Regex.Match(coversstring.Substring(coversindex[j], (coversindex2[i] - coversindex[j])), @"matchups(.*?)"">Matchup", RegexOptions.Singleline);
                        if (mMatchup.Success)
                        {
                            coversMatchup[i] = mMatchup.Groups[0].Value;
                            //  coversMatchup[i] = Regex.Replace(coversMatchup[i], @"<div class=""cmg_l_row cmg_matchup_list_gamebox"">", "").Trim();
                            coversMatchup[i] = Regex.Replace(coversMatchup[i], @">", "").Trim();
                            coversMatchup[i] = Regex.Replace(coversMatchup[i], @"\\", "").Trim();
                            coversMatchup[i] = Regex.Replace(coversMatchup[i], @"Matchup", "").Trim();
                            coversMatchup[i] = Regex.Replace(coversMatchup[i], @"<a href=", "").Trim();
                            coversMatchup[i] = Regex.Replace(coversMatchup[i], @"""", "").Trim();
                            coversMatchup[i] = "http://www.covers.com/pageLoader/pageLoader.aspx?page=/data/mlb/" + coversMatchup[i];
                        }
                        j++;
                        j++;
                    }


                    j = 1;
                    k = 0;
                    for (i = 0; i < (noofteams2 + noofteams2); i++)
                    {
                        Match mPitcher = Regex.Match(coversstring.Substring(coversindex[j], (coversindex2[k] - coversindex[j])), @"<span>Probable:</span>(.*?)</div>", RegexOptions.Singleline);
                        if (mPitcher.Success)
                        {
                            coversPitcher[i] = mPitcher.Groups[0].Value;
                            coversPitcher[i] = Regex.Replace(coversPitcher[i], @"<span>Probable:</span>", "").Trim();
                            coversPitcher[i] = Regex.Replace(coversPitcher[i], @"</div>", "").Trim();
                            coversPitcher[i] = Regex.Replace(coversPitcher[i], @"""", "").Trim();
                            // coversPitcher[i] = Regex.Replace(coversPitcher[i], @" ", "\n");
                            i++;
                        }


                        mPitcher = mPitcher.NextMatch();
                        if (mPitcher.Success)
                        {
                            coversPitcher[i] = mPitcher.Groups[0].Value;
                            coversPitcher[i] = Regex.Replace(coversPitcher[i], @"<span>Probable:</span>", "").Trim();
                            coversPitcher[i] = Regex.Replace(coversPitcher[i], @"</div>", "").Trim();
                            coversPitcher[i] = Regex.Replace(coversPitcher[i], @"""", "").Trim();
                            //   coversPitcher[i] = Regex.Replace(coversPitcher[i], @" ", "\n");
                        }


                        if (coversPitcher[i] == null)
                        {
                            i++;
                        }
                        k++;
                        j++;
                        j++;
                    }


                    j = 1;
                    for (i = 0; i < noofteams2; i++)
                    {
                        Match mInnings = Regex.Match(coversstring.Substring(coversindex[j], (coversindex2[i] - coversindex[j])), @"<div class=""cmg_matchup_line_score"">(.*?)</div>", RegexOptions.Singleline);
                        if (mInnings.Success)
                        {
                            coversInnings[i] = mInnings.Groups[0].Value;
                            //  coversInnings[i] = Regex.Replace(coversInnings[i], @"<div class=""cmg_l_row cmg_matchup_list_gamebox"">", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"<(.*?)>", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"<div class=""cmg_matchup_line_score"">", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"                                ", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"                            ", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"                        ", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"           ", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"          ", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"        ", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"         ", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"  ", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @" ", "").Trim();
                            coversInnings[i] = Regex.Replace(coversInnings[i], @"\r", "").Trim();
                            // coversInnings[i] = Regex.Replace(coversInnings[i], @"\n", "").Trim();
                            //      coversInnings[i] = Regex.Replace(coversInnings[i], @"\s+", "").Trim();


                        }
                        j++;
                        j++;
                    }
                }

                int saveRow = 0;
                if (dataGridView8.Rows.Count > 0)
                    saveRow = dataGridView8.FirstDisplayedCell.RowIndex;

                if (dataGridView8.DataSource != null)
                {
                    dataGridView8.DataSource = null;
                }
                else
                {
                    dataGridView8.Rows.Clear();
                }

                dataGridView8.Refresh();

                dataGridView8.ColumnCount = 10;
                dataGridView8.Columns[0].Name = "Away Team";
                dataGridView8.Columns[0].Width = 43;
                dataGridView8.Columns[1].Name = "Home Team";
                dataGridView8.Columns[1].Width = 43;
                dataGridView8.Columns[2].Name = "Game Status";
                dataGridView8.Columns[2].Width = 66;
                dataGridView8.Columns[3].Name = "Away Score";
                dataGridView8.Columns[3].Width = 43;
                dataGridView8.Columns[4].Name = "Home Score";
                dataGridView8.Columns[4].Width = 43;
                dataGridView8.Columns[5].Name = "Away Starter";
                dataGridView8.Columns[5].Width = 154;
                dataGridView8.Columns[6].Name = "Home Starter";
                dataGridView8.Columns[6].Width = 154;
                dataGridView8.Columns[7].Name = "Total";
                dataGridView8.Columns[7].Width = 43;
                dataGridView8.Columns[8].Name = "Line";
                dataGridView8.Columns[8].Width = 43;
                dataGridView8.Columns[9].Name = "Box Score / Matchup";

                i = 0;
                j = 1;
                k = 0;
                while (coversteam[i] != null)
                {
                    coversrow = new string[] { "" + coversteam[i] + "", "" + coversteam[j] + "", "" + coversgamestatus2[k] + "", "" +
                                                   coversapoints2[k] + "", "" + covershpoints2[k] + "",""+coversPitcher[i]
                                                   +"",""+coversPitcher[j]+"",""+coversTotal[k]+"",""+coversLine[k]+"" };
                    dataGridView8.Rows.Add(coversrow);

                    i++;
                    i++;
                    j++;
                    j++;
                    k++;
                }


                int noofteams = 0;
                i = 0;
                while (coversteam[i] != null)
                {
                    noofteams++;
                    i++;

                }

                noofteams = noofteams / 2;


                j = 0;
                for (i = 0; i < noofteams; i++)
                {
                    if (dataGridView8.Rows[i].Cells[2].Value.ToString().Contains("Final"))
                    {
                        // dataGridView8.Rows[i].Cells[2].Value = coversgamestatus2[j];
                        dataGridView8.Rows[i].Cells[3].Value = coversapoints[i];
                        dataGridView8.Rows[i].Cells[4].Value = covershpoints[i];

                        j++;
                    }
                }

                j = 0;
                for (i = 0; i < noofteams; i++)
                {
                    if (dataGridView8.Rows[i].Cells[2].Value == "")
                    {
                        dataGridView8.Rows[i].Cells[2].Value = coversgamestatus[i];
                        dataGridView8.Rows[i].Cells[3].Value = coversapoints[i];
                        dataGridView8.Rows[i].Cells[4].Value = covershpoints[i];

                        j++;
                    }
                }
                coversbsno = 0;
                for (i = 0; i < noofteams; i++)
                {
                    if (dataGridView8.Rows[i].Cells[3].Value.ToString() != "")
                    {
                        dataGridView8.Rows[i].Cells["Box Score / Matchup"].Value = "Box Score";
                        dataGridView8.Rows[i].Cells["Box Score / Matchup"].Style.ForeColor = Color.Blue;
                        dataGridView8.Rows[i].Cells["Box Score / Matchup"].Style.Font = new System.Drawing.Font("Ariel", 9, FontStyle.Underline);
                    }
                    else
                    {
                        dataGridView8.Rows[i].Cells["Box Score / Matchup"].Value = "Matchup";
                        dataGridView8.Rows[i].Cells["Box Score / Matchup"].Style.ForeColor = Color.Blue;
                        dataGridView8.Rows[i].Cells["Box Score / Matchup"].Style.Font = new System.Drawing.Font("Ariel", 9, FontStyle.Underline);

                    }
                    if (dataGridView8.Rows[i].Cells[2].Value.ToString().Equals("Postponed") == true)
                    {
                        dataGridView8.Rows[i].Cells[3].Value = "N/A";
                        dataGridView8.Rows[i].Cells[4].Value = "N/A";
                        dataGridView8.Rows[i].Cells["Box Score / Matchup"].Value = "N/A";
                    }

                    if (dataGridView8.Rows[i].Cells["Box Score / Matchup"].Value.ToString() == "Matchup")
                    {
                        coversbsno++;
                    }
                }

                counterstarter = 0;
                foreach (DataGridViewRow row in dataGridView8.Rows)
                {
                    if (row.Cells[5].Value.ToString().Length > 2)
                    {
                        counterstarter++;
                    }
                }

                if (counterstarter == 0)
                {
                    dataGridView8.Columns.Remove("Away Starter");
                    dataGridView8.Columns.Remove("Home Starter");
                    dataGridView8.Columns.Remove("Total");
                }

                if (saveRow != 0 && saveRow < dataGridView8.Rows.Count)
                    dataGridView8.FirstDisplayedScrollingRowIndex = saveRow;

                dataGridView8.Rows[0].Cells[0].Selected = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void dataGridView8_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //   try
            //  {
            if (radioBtnMLB.Checked == true)
            {
                NewGameBtn_Click(sender, e);

                if (dataGridView8.Rows[e.RowIndex].Cells[0].Selected)
                {
                    CoversAwayStarter = dataGridView8.Rows[e.RowIndex].Cells[5].Value.ToString().Replace(" (R)", "");
                    CoversHomeStarter = dataGridView8.Rows[e.RowIndex].Cells[6].Value.ToString().Replace(" (R)", "");
                    PinnAwayTeam = dataGridView8.Rows[e.RowIndex].Cells[0].Value.ToString();
                    PinnHomeTeam = dataGridView8.Rows[e.RowIndex].Cells[1].Value.ToString();

                    PinnHandi = dataGridView8.Rows[e.RowIndex].Cells["Line"].Value.ToString();
                    PinnTotal = dataGridView8.Rows[e.RowIndex].Cells["Total"].Value.ToString().Replace(".", ",");

                    if (PinnHandi == "Off" || PinnHandi == "" || PinnHandi == null)
                    {
                        PinnHandi = "0";
                        MessageBox.Show("There was an 'Off' or empty offer, Hendi is set to 0!", "Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    if (PinnTotal == "Off" || PinnHandi == "" || PinnHandi == null)
                    {
                        PinnTotal = "8";
                        MessageBox.Show("There was an 'Off' or empty offer, Total is set to 8!", "Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        connection.Open();
                        String queryy33 = "Select ImeKluba from Klubovi where ShortForCovers = '" + PinnAwayTeam + "' and League = '" + LeagueNBAMLB + "'";
                        SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                        SqlDataReader readd33 = cmdd33.ExecuteReader();
                        while (readd33.Read())
                        {
                            PinnAwayTeam = (readd33["ImeKluba"].ToString());
                        }
                        readd33.Close();

                        String queryy34 = "Select ImeKluba from Klubovi where ShortForCovers = '" + PinnHomeTeam + "' and League = '" + LeagueNBAMLB + "'";
                        SqlCommand cmdd34 = new SqlCommand(queryy34, connection);
                        SqlDataReader readd34 = cmdd34.ExecuteReader();
                        while (readd34.Read())
                        {
                            PinnHomeTeam = (readd34["ImeKluba"].ToString());
                        }
                        readd34.Close();
                    }
                    renameteams();

                    double PinnH = Convert.ToDouble(PinnHandi);
                    if (PinnH >= 130 && PinnH <= 170)
                    {
                        PinnHandi = "+1";
                    }
                    else if (PinnH < -125 && PinnH >= -170)
                    {
                        PinnHandi = "-1";
                    }
                    else if ((PinnH >= -125 && PinnH < 130))
                    {
                        PinnHandi = "0";
                    }
                    else if (PinnH > 170 && PinnH < 220)
                    {
                        PinnHandi = "+1,5";
                    }
                    else if (PinnH < -170 && PinnH > -220)
                    {
                        PinnHandi = "-1,5";
                    }
                    else if (PinnH >= 220)
                    {
                        PinnHandi = "+2";
                    }
                    else if (PinnH <= -220)
                    {
                        PinnHandi = "-2";
                    }


                    CoversAwayStarter = CoversAwayStarter.Replace("O&#39;Sullivan", "OSullivan");
                    CoversHomeStarter = CoversHomeStarter.Replace("O&#39;Sullivan", "OSullivan");
                    CoversAwayStarter = CoversAwayStarter.Replace("De La Rosa", "DeLaRosa");
                    CoversHomeStarter = CoversHomeStarter.Replace("De La Rosa", "DeLaRosa");
                    CoversAwayStarter = CoversAwayStarter.Replace("Jonathon Niese", "Jon Niese");
                    CoversHomeStarter = CoversHomeStarter.Replace("Jonathon Niese", "Jon Niese");
                    CoversAwayStarter = CoversAwayStarter.Replace("R.A.", "RA");
                    CoversHomeStarter = CoversHomeStarter.Replace("R.A.", "RA");
                    CoversAwayStarter = CoversAwayStarter.Replace("J.A.", "JA");
                    CoversHomeStarter = CoversHomeStarter.Replace("J.A.", "JA");
                    CoversAwayStarter = CoversAwayStarter.Replace("T.J.", "TJ");
                    CoversHomeStarter = CoversHomeStarter.Replace("T.J.", "TJ");
                    CoversAwayStarter = CoversAwayStarter.Replace("A.J.", "AJ");
                    CoversHomeStarter = CoversHomeStarter.Replace("A.J.", "AJ");
                    CoversAwayStarter = CoversAwayStarter.Replace("Chi Chi", "Chi-Chi");
                    CoversHomeStarter = CoversHomeStarter.Replace("Chi Chi", "Chi-Chi");
                    CoversAwayStarter = CoversAwayStarter.Replace(" (L)", "");
                    CoversHomeStarter = CoversHomeStarter.Replace(" (L)", "");

                    HomeStarterHand = dataGridView8.Rows[dataGridView8.CurrentCell.RowIndex].Cells[6].Value.ToString().Split(' ')[2].Replace("(", "");
                    HomeStarterHand = HomeStarterHand.Replace(")", "");

                    AwayStarterHand = dataGridView8.Rows[dataGridView8.CurrentCell.RowIndex].Cells[5].Value.ToString().Split(' ')[2].Replace("(", "");
                    AwayStarterHand = HomeStarterHand.Replace(")", "");
                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        connection.Open();
                        string queryawaystarter = @"IF NOT EXISTS (Select ID from MLBStarters where FirstName = '" + CoversAwayStarter.Split(' ')[0] + "' and LastName = '" + CoversAwayStarter.Split(' ')[1] + "') " +
                                                  " BEGIN " +
                                                  " INSERT INTO MLBStarters (FirstName, LastName, Hand,Team) " +
                                                  " VALUES('" + CoversAwayStarter.Split(' ')[0] + "','" + CoversAwayStarter.Split(' ')[1] + "' " +
                                                  " , '" + AwayStarterHand + "', (Select KratkoIme from Klubovi where ImeKluba = '" + PinnAwayTeam + "')) end";

                        SqlCommand cmdawaystarter = new SqlCommand(queryawaystarter, connection);
                        cmdawaystarter.ExecuteNonQuery();

                        string queryhomestarter = @"IF NOT EXISTS (Select ID from MLBStarters where FirstName = '" + CoversHomeStarter.Split(' ')[0] + "' and LastName = '" + CoversHomeStarter.Split(' ')[1] + "') " +
                                                  " BEGIN " +
                                                  " INSERT INTO MLBStarters (FirstName, LastName, Hand,Team) " +
                                                  " VALUES('" + CoversHomeStarter.Split(' ')[0] + "','" + CoversHomeStarter.Split(' ')[1] + "' " +
                                                  " , '" + HomeStarterHand + "', (Select KratkoIme from Klubovi where ImeKluba = '" + PinnHomeTeam + "')) end";

                        SqlCommand cmdhomestarter = new SqlCommand(queryhomestarter, connection);
                        cmdhomestarter.ExecuteNonQuery();
                    }
                    AwayTeamCB.Text = PinnAwayTeam;
                    HomeTeamCB.Text = PinnHomeTeam;
                    awayStarterCB.Text = CoversAwayStarter;
                    homeStarterCB.Text = CoversHomeStarter;
                    TotalBox.Text = PinnTotal;
                    HandicapBox.Text = PinnHandi;
                    TotalBox.Focus();
                    TotalBox.Refresh();
                    HandicapBox.Focus();
                    HandicapBox.Refresh();

                    awayStarterCB.Enabled = true;
                    homeStarterCB.Enabled = true;
                    TotalBox.Enabled = true;
                    HandicapBox.Enabled = true;
                    AutoProcessBtn.Focus();
                }
            }
            else if (radioBtnNBA.Checked == true)
            {
                NewGameBtn_Click(sender, e);

                if (dataGridView8.Rows[e.RowIndex].Cells[0].Selected)
                {

                    PinnAwayTeam = dataGridView8.Rows[e.RowIndex].Cells[0].Value.ToString();
                    PinnHomeTeam = dataGridView8.Rows[e.RowIndex].Cells[1].Value.ToString();
                    PinnHandi = dataGridView8.Rows[e.RowIndex].Cells[6].Value.ToString();
                    PinnTotal = dataGridView8.Rows[e.RowIndex].Cells[5].Value.ToString().Replace(".", ",");

                    if (PinnHandi == "")
                    {
                        PinnHandi = "0";
                        MessageBox.Show("There was an 'Off' or empty offer, Hendi is set to 0!", "Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    if (PinnTotal == "")
                    {
                        PinnTotal = "200";
                        MessageBox.Show("There was an 'Off' or empty offer, Total is set to 200!", "Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        connection.Open();
                        String queryy33 = "Select ImeKluba from Klubovi where ShortForCovers = '" + PinnAwayTeam + "' and League = '" + LeagueNBAMLB + "'";
                        SqlCommand cmdd33 = new SqlCommand(queryy33, connection);
                        SqlDataReader readd33 = cmdd33.ExecuteReader();
                        while (readd33.Read())
                        {
                            PinnAwayTeam = (readd33["ImeKluba"].ToString());
                        }
                        readd33.Close();

                        String queryy34 = "Select ImeKluba from Klubovi where ShortForCovers = '" + PinnHomeTeam + "' and League = '" + LeagueNBAMLB + "'";
                        SqlCommand cmdd34 = new SqlCommand(queryy34, connection);
                        SqlDataReader readd34 = cmdd34.ExecuteReader();
                        while (readd34.Read())
                        {
                            PinnHomeTeam = (readd34["ImeKluba"].ToString());
                        }
                        readd34.Close();
                    }

                    renameteams();

                    double PinnH = Convert.ToDouble(PinnHandi.Replace(".", ","));
                    PinnHandi = PinnH.ToString();

                    AwayTeamCB.Text = PinnAwayTeam;
                    HomeTeamCB.Text = PinnHomeTeam;
                    TotalBox.Text = PinnTotal;
                    HandicapBox.Text = PinnHandi;
                    TotalBox.Focus();
                    TotalBox.Refresh();
                    HandicapBox.Focus();
                    HandicapBox.Refresh();
                    AutoProcessBtn.Focus();
                }
            }
            // }
            //   catch
            //  {
            //MessageBox.Show("Enter offer for off value!", "Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    }
        }


        private void UpdMLBStrBtn_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand UpdMLBStr = new SqlCommand("UPDATEMLBSTARTERS", connection);
                UpdMLBStr.CommandType = CommandType.StoredProcedure;
                UpdMLBStr.ExecuteNonQuery();
                MessageBox.Show("Starters Updated!", "Important message!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void LoadGameBtn_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                if (DGVAnalysisResults.Columns.Contains("chk"))
                {
                    DGVAnalysisResults.Columns.Remove("chk");
                    if (DGVAnalysisResults.Columns.Contains("chk2"))
                    { DGVAnalysisResults.Columns.Remove("chk2"); }

                }

                DGVAnalysisResults.DataSource = null;

                connection.Open();
                IDO = IDoklBox.Text;
                IDO2 = Convert.ToInt32(IDO);

                if (radioBtnNBA.Checked == true)
                {
                    Sql3 = "select ID,ImeHKluba,ImeAKluba, HResult,AResult,TotalResult,HandicapResult,Season,Line,Total, HomePace, AwayPace from " + NBAMLBResultsVar + " where IDOklade = '" + IDO2 + "'";
                }
                else
                {
                    Sql3 = "select ID,ImeHKluba,ImeAKluba, HResult,AResult,TotalResult,HandicapResult,HomeStarter,HomeStarterHand as HH,AwayStarter,AwayStarterHand as AH, ParkFactor as PF from " + NBAMLBResultsVar + " where IDOklade = '" + IDO2 + "'";
                }
                ds = new DataSet();
                adapter = new SqlDataAdapter(Sql3, connection);
                ds.Reset();
                adapter.Fill(ds);
                //   BindingSource bs1 = new BindingSource();
                //   bs1.DataSource = ds;

                DGVAnalysisResults.DataSource = ds.Tables[0];
                //  dataGridView1.DataSource = bs1;

                DGVAnalysisResults.Columns[0].Width = 80;
                DGVAnalysisResults.Rows[0].Cells[0].Selected = false;

                // Format DGVAnalysisResults
                dgva.DGV1Actions(DGVAnalysisResults);


                if (radioBtnMLB.Checked == true)
                {
                    DGVAnalysisResults.Columns[7].Width = 111;
                    DGVAnalysisResults.Columns[8].Width = 25;
                    DGVAnalysisResults.Columns[9].Width = 111;
                    DGVAnalysisResults.Columns[10].Width = 25;
                }

                System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                boldStyle.Font = new System.Drawing.Font("Ariel", 9.25F, System.Drawing.FontStyle.Bold);


                DataGridViewRow row = new DataGridViewRow();
                for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
                {
                    if (DGVAnalysisResults.Rows[i].Cells[1].Value.ToString() == HomeTeamCB.Text.Split(' ')[1])
                    {
                        DGVAnalysisResults.Rows[i].Cells[1].Style = boldStyle;
                    }
                    if (DGVAnalysisResults.Rows[i].Cells[2].Value.ToString() == AwayTeamCB.Text.Split(' ')[1])
                    {
                        DGVAnalysisResults.Rows[i].Cells[2].Style = boldStyle;
                    }
                    if (radioBtnMLB.Checked == true)
                    {
                        if (DGVAnalysisResults.Rows[i].Cells[9].Value.ToString() == awayStarterCB.Text.Split(' ')[1])
                        {
                            DGVAnalysisResults.Rows[i].Cells[9].Style = boldStyle;
                        }
                        if (DGVAnalysisResults.Rows[i].Cells[7].Value.ToString() == homeStarterCB.Text.Split(' ')[1])
                        {
                            DGVAnalysisResults.Rows[i].Cells[7].Style = boldStyle;
                        }
                    }

                    if (radioBtnNBA.Checked == true)
                    {
                        if (DGVAnalysisResults.Rows[i].Cells["HomePace"].Value.ToString() == "")
                        {
                            string gethomepace = "Select PaceHome from Klubovi where KratkoIme = '" + DGVAnalysisResults.Rows[i].Cells[1].Value.ToString() + "'";
                            string getawaypace = "Select PaceAway from Klubovi where KratkoIme = '" + DGVAnalysisResults.Rows[i].Cells[2].Value.ToString() + "'";

                            SqlCommand cmdhomepace = new SqlCommand(gethomepace, connection);
                            SqlDataReader readhomepace = cmdhomepace.ExecuteReader();
                            while (readhomepace.Read())
                            {
                                dgvHomePace = (readhomepace["PaceHome"].ToString());
                            }
                            readhomepace.Close();

                            SqlCommand cmdawaypace = new SqlCommand(getawaypace, connection);
                            SqlDataReader read4awaypace = cmdawaypace.ExecuteReader();
                            while (read4awaypace.Read())
                            {
                                dgvAwayPace = (read4awaypace["PaceAway"].ToString());
                            }
                            read4awaypace.Close();
                            DGVAnalysisResults.Rows[i].Cells["HomePace"].Value = dgvHomePace;
                            DGVAnalysisResults.Rows[i].Cells["AwayPace"].Value = dgvAwayPace;
                        }
                        SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapter);


                        //     adapter.Update((System.Data.DataTable)bs1.DataSource);
                    }
                    else if (radioBtnMLB.Checked == true)
                    {
                        if (DGVAnalysisResults.Rows[i].Cells["PF"].Value.ToString() == "")
                        {
                            string getparkFactor = "Select ParkFactor from Klubovi where KratkoIme like '" + DGVAnalysisResults.Rows[i].Cells[1].Value.ToString() + "%'";

                            SqlCommand cmdparkFactor = new SqlCommand(getparkFactor, connection);
                            SqlDataReader readparkFactor = cmdparkFactor.ExecuteReader();
                            while (readparkFactor.Read())
                            {
                                dgvHomePace = (readparkFactor["ParkFactor"].ToString());
                            }
                            readparkFactor.Close();

                            DGVAnalysisResults.Rows[i].Cells["PF"].Value = dgvHomePace;
                        }
                        SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapter);
                    }
                }

                queryy2Total = "Select Total from " + NBAMLBBetsVar + " where IDOklade = '" + IDoklBox.Text + "'";
                queryy3Hand = "Select Handicap from " + NBAMLBBetsVar + " where IDOklade = '" + IDoklBox.Text + "'";


                SqlCommand cmdd2 = new SqlCommand(queryy2Total, connection);
                SqlDataReader readd2 = cmdd2.ExecuteReader();
                while (readd2.Read())
                {
                    tot = (readd2["Total"].ToString());
                }
                readd2.Close();
                SqlCommand cmdd3 = new SqlCommand(queryy3Hand, connection);
                SqlDataReader readd3 = cmdd3.ExecuteReader();
                while (readd3.Read())
                {
                    hand = (readd3["Handicap"].ToString());
                }
                readd3.Close();

                //   TotalBox.Text = tot;
                //   HandicapBox.Text = hand;
                for (Int32 i = 0; i <= duplikati; i++)
                {
                    RemoveDuplicate(sender, e);
                }

                adapter.Update(ds);

                colorrows(sender, e);
                dgv2(sender, e);
                dgv5(sender, e);
                finishandCalcPB.Enabled = false;
            }
        }

        private void NewGameBtn_Click(object sender, EventArgs e)
        {
            cleanitup(sender, e);

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                String queryy = "Select TOP 1 IDOklade from " + NBAMLBResultsVar + " order by IDOklade desc";

                SqlCommand cmdd = new SqlCommand(queryy, connection);
                SqlDataReader readd = cmdd.ExecuteReader();

                while (readd.Read())
                {
                    IDOkl2 = (readd["IDOklade"].ToString());
                }
                readd.Close();
                int IDOkl3 = Convert.ToInt32(IDOkl2);
                IDoklBox.Text = (IDOkl3 + 1).ToString();

            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //EXIT BUTTON//
            DialogResult d = MessageBox.Show("Are you sure you want to exit, did you check if all bets are entered?", "Exit Program", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            try
            {

                if (d == DialogResult.Yes)
                {

                    System.Windows.Forms.Application.Exit();

                }
                else if (d == DialogResult.No)
                {

                    return;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SQDLTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    connection.Open();

                    Sql3 = "select Month,Dayofmonth,Team,Opp,Hscore,Ascore,Total from " + AllNBAMLBResultsVar + " where " + SQDLTB.Text + " order by ID desc";

                    ds = new DataSet();
                    adapter = new SqlDataAdapter(Sql3, connection);
                    ds.Reset();
                    adapter.Fill(ds);
                    dataGridView6.DataSource = ds.Tables[0];
                    dataGridView6.Columns[0].Width = 40;
                    dataGridView6.Columns[1].Width = 40;
                }
            }
        }

        private void results1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void results1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && (ModifierKeys & Keys.Control) == Keys.Control)
            {
                BetsTracker BT = new BetsTracker();


                BT.TeamslistBox1.Focus();
                BT.TeamslistBox1.SelectedValue = AwayTeamCB.Text;

                ////int searchIndex = 0;
                //foreach (var item in BT.TeamslistBox1.Items)
                //{
                //    if (item.ToString() == AwayTeamCB.Text)
                //    {
                //        BT.TeamslistBox1.SelectedIndex = searchIndex;
                //        BT.TeamslistBox1.Focus();
                //    }
                //    searchIndex++;
                //}

            }
            else
            {
                if (AwayTeamCB.Text.Length > 1)
                {
                    ResultUpdater frm3 = new ResultUpdater();
                    frm3.value = AwayTeamCB.Text.Split(' ')[1];
                    frm3.Show();
                    //frm3.tabControl1.SelectedIndex = 1;
                    frm3.comboBox1.Text = AwayTeamCB.Text;
                    frm3.button7_Click(sender, e);
                }
            }
        }

        private void AwayTeamTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AwayTeamTabControl.SelectedIndex == 1)
            {
                AwayStarterL10();
            }
        }

        private void HomeTeamTabControl_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (HomeTeamTabControl.SelectedIndex == 1)
            {
                HomeStarterL10();
            }
        }

        private void starterstats()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                //                string Sqlstartersstats = @"SELECT [PlayerName] as Starter
                //                                          ,[GP]
                //                                          ,[GS]
                //                                          ,[IP]
                //                                          ,[W]
                //                                          ,[L]
                //                                          ,[H]
                //                                          ,[R]
                //                                          ,[HR]
                //                                          ,[ER]
                //                                          ,[ERA]
                //                                          ,[BB]
                //                                          ,[K]
                //                                          ,[WHIP]
                //                   FROM [dbo].[MLBStartersStats] where PlayerName IN ('" + awayStarterCB.Text + "','" + homeStarterCB.Text + "')";

                string Sqlstartersstatsall = @"SELECT [PlayerName] as Starter
                                          ,[W]
                                          ,[L]
                                          ,[ERA]
                                          ,[G]
                                          ,[GS]
                                          ,[IP]
                                          ,[H]
                                          ,[R]
                                          ,[ER]
                                          ,[HR]
                                          ,[BB]
                                          ,[SO]
                                          ,[AVG]
                                          ,[WHIP]
                    FROM [dbo].[MLBStartersStatsAll] where PlayerName like ('%" + awayStarterCB.Text.Split(' ')[1] + ", " + awayStarterCB.Text.Split(' ')[0].Substring(0, 1) + "%') and Venue = 'A' or PlayerName like ('%" + homeStarterCB.Text.Split(' ')[1] + ", " + homeStarterCB.Text.Split(' ')[0].Substring(0, 1) + "%') and Venue = 'H' order by Venue asc";

                dsstartersstats = new DataSet();
                adapter = new SqlDataAdapter(Sqlstartersstatsall, connection);
                dsstartersstats.Reset();
                adapter.Fill(dsstartersstats);
                StartersStatsDGV.DataSource = dsstartersstats.Tables[0];

                if (dsstartersstats.Tables[0].Rows.Count > 0)
                {
                    System.Windows.Forms.DataGridViewCellStyle boldStyle = new System.Windows.Forms.DataGridViewCellStyle();
                    boldStyle.Font = new System.Drawing.Font("Ariel Narrow", 9.5F, System.Drawing.FontStyle.Bold);
                    StartersStatsDGV.Columns[0].DefaultCellStyle = boldStyle;
                    StartersStatsDGV.Columns[3].DefaultCellStyle = boldStyle;
                    StartersStatsDGV.Columns[6].DefaultCellStyle = boldStyle;
                    StartersStatsDGV.Columns[14].DefaultCellStyle = boldStyle;
                    StartersStatsDGV.Columns[10].DefaultCellStyle = boldStyle;
                    StartersStatsDGV.Columns[0].Width = 130;
                    StartersStatsDGV.Columns[1].Width = 30;
                    StartersStatsDGV.Columns[2].Width = 30;
                    StartersStatsDGV.Columns[3].Width = 58;
                    StartersStatsDGV.Columns[6].Width = 58;
                    StartersStatsDGV.Columns[13].Width = 50;
                    StartersStatsDGV.Columns[14].Width = 58;

                    StartersStatsDGV.Rows[0].Cells[0].Selected = false;
                }
            }
        }


        private void homeStarterCB_TextChanged(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                if (homeStarterCB.Text.Length > 1)
                {
                    HomeStarterHand = null;
                    String query134 = "Select Hand from MLBStarters where FirstName = '" + homeStarterCB.Text.Split(' ')[0] + "' and LastName = '" + homeStarterCB.Text.Split(' ')[1] + "'";
                    SqlCommand cmd134 = new SqlCommand(query134, connection);
                    SqlDataReader read134 = cmd134.ExecuteReader();
                    while (read134.Read())
                    {
                        HomeStarterHand = (read134["Hand"].ToString());
                    }
                    read134.Close();

                    if (HomeStarterHand == null || HomeStarterHand.Contains('?'))
                    {
                        HomeStarterHand = dataGridView8.Rows[dataGridView8.CurrentCell.RowIndex].Cells[6].Value.ToString().Split(' ')[2].Replace("(", "");
                        HomeStarterHand = HomeStarterHand.Replace(")", "");
                    }

                    HStarterHandLabel.Text = "(" + HomeStarterHand + ")";
                    if (radioBtnMLB.Checked == true)
                    {
                        calculateAveragesSUOUHomeTeam();
                        calculateAveragesSUOUAwayTeam();
                        showAveragesinDGV();
                    }

                    if (HomeTeamTabControl.SelectedIndex == 1)
                    {
                        HomeStarterL10();
                    }
                    starterstats();
                }

            }
        }

        private void ViewGamesAwayBtn_Click(object sender, EventArgs e)
        {
            tabControlInjuries.SelectedIndex = 2;

            SQDLTB.Text = "Opp = '" + AwayTeamCB.Text.Split(' ')[1] + "' and StarterHHand like '" + HomeStarterHand + "'";
            SQDLTB.Text = SQDLTB.Text.Replace("L ", "L");
            SQDLTB.Text = SQDLTB.Text.Replace("R ", "R");
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                Sql3 = "select Month,Dayofmonth,Team,Opp,Hscore,Ascore,Total from " + AllNBAMLBResultsVar + " where " + SQDLTB.Text + " order by ID desc";

                ds = new DataSet();
                adapter = new SqlDataAdapter(Sql3, connection);
                ds.Reset();
                adapter.Fill(ds);
                dataGridView6.DataSource = ds.Tables[0];
                dataGridView6.Columns[0].Width = 40;
                dataGridView6.Columns[1].Width = 40;
                dataGridView6.Rows[0].Cells[0].Selected = false;
            }
        }

        private void ViewGamesHomeBtn_Click(object sender, EventArgs e)
        {
            tabControlInjuries.SelectedIndex = 2;

            SQDLTB.Text = "Team = '" + HomeTeamCB.Text.Split(' ')[1] + "' and StarterAHand like '" + AwayStarterHand + "'";
            SQDLTB.Text = SQDLTB.Text.Replace("L ", "L");
            SQDLTB.Text = SQDLTB.Text.Replace("R ", "R");
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                Sql3 = "select Month,Dayofmonth,Team,Opp,Hscore,Ascore,Total from " + AllNBAMLBResultsVar + " where " + SQDLTB.Text + " order by ID desc";

                ds = new DataSet();
                adapter = new SqlDataAdapter(Sql3, connection);
                ds.Reset();
                adapter.Fill(ds);
                dataGridView6.DataSource = ds.Tables[0];
                dataGridView6.Columns[0].Width = 40;
                dataGridView6.Columns[1].Width = 40;
                dataGridView6.Rows[0].Cells[0].Selected = false;
            }
        }

        private void awayStarterCB_TextChanged(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                if (awayStarterCB.Text.Length > 1)
                {
                    AwayStarterHand = null;
                    String query133 = "Select Hand from MLBStarters where FirstName = '" + awayStarterCB.Text.Split(' ')[0] + "' and LastName = '" + awayStarterCB.Text.Split(' ')[1] + "'";
                    SqlCommand cmd133 = new SqlCommand(query133, connection);
                    SqlDataReader read133 = cmd133.ExecuteReader();
                    while (read133.Read())
                    {
                        AwayStarterHand = (read133["Hand"].ToString());
                    }
                    read133.Close();

                    if (AwayStarterHand == null || AwayStarterHand.Contains('?'))
                    {
                        AwayStarterHand = dataGridView8.Rows[dataGridView8.CurrentCell.RowIndex].Cells[5].Value.ToString().Split(' ')[2].Replace("(", "");
                        AwayStarterHand = AwayStarterHand.Replace(")", "");
                    }

                    AStarterHandLabel.Text = "(" + AwayStarterHand + ")";

                    if (AwayTeamTabControl.SelectedIndex == 1)
                    {
                        AwayStarterL10();
                    }
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                for (int i = 0; i < dataGridView8.Rows.Count; i++)
                {
                    if (dataGridView8.Rows[i].Cells["Total"].Value.ToString() == "Off")
                    {
                        if (dataGridView8.Rows[i].Cells["Line"].Value.ToString() == "")
                        {
                            dataGridView8.Rows[i].Cells["Line"].Value = 0;
                        }
                        dataGridView8.Rows[i].Cells["Total"].Value = 0;
                    }
                    //                        String query3 = @"INSERT INTO LiveScoresUpdates (Date, AwayTeam,AwayStarter,HomeTeam,HomeStarter,Total,Line,GameStatus)
                    //              VALUES ( '" + DateTime.Now.ToShortDateString() + "','" + dataGridView8.Rows[i].Cells["Away Team"].Value + "','" + dataGridView8.Rows[i].Cells["Away Starter"].Value +
                    //                          "','" + dataGridView8.Rows[i].Cells["Home Team"].Value + "','" + dataGridView8.Rows[i].Cells["Home Starter"].Value + "'," + dataGridView8.Rows[i].Cells["Total"].Value +
                    //                          "," + dataGridView8.Rows[i].Cells["Line"].Value + ",'" + dataGridView8.Rows[i].Cells["Game Status"].Value + "') ON DUPLICATE KEY UPDATE Total = " +
                    //                          dataGridView8.Rows[i].Cells["Total"].Value + ", Line = " + dataGridView8.Rows[i].Cells["Line"].Value + ";";

                    string query3 = @"if not exists " +
                                      "(select top 1 ID from LiveScoresUpdates where Date = '" + DateTime.Now.ToShortDateString() + "' and AwayTeam = '" + dataGridView8.Rows[i].Cells["Away Team"].Value + "')" +
                                      " begin " +
                                       " INSERT INTO LiveScoresUpdates (Date, AwayTeam,AwayStarter,HomeTeam,HomeStarter,Total,Line,GameStatus) " +
                                      " VALUES ( '" + DateTime.Now.ToShortDateString() + "','" + dataGridView8.Rows[i].Cells["Away Team"].Value + "','" + dataGridView8.Rows[i].Cells["Away Starter"].Value + "', " +
                                       " '" + dataGridView8.Rows[i].Cells["Home Team"].Value + "','" + dataGridView8.Rows[i].Cells["Home Starter"].Value + "'," + dataGridView8.Rows[i].Cells["Total"].Value + ", " +
                                       " " + dataGridView8.Rows[i].Cells["Line"].Value + ",'" + dataGridView8.Rows[i].Cells["Game Status"].Value + "') " +
                                      " end " +
                                      " else " +
                                      " begin " +
                                      " UPDATE LiveScoresUpdates set Total = " + dataGridView8.Rows[i].Cells["Total"].Value + ", " +
                                      "  Line = " + dataGridView8.Rows[i].Cells["Line"].Value + " where  Date = '" + DateTime.Now.ToShortDateString() + "' " +
                                      " and AwayTeam = '" + dataGridView8.Rows[i].Cells["Away Team"].Value + "' " +
                                      "end";

                    SqlCommand cmd111 = new SqlCommand(query3, connection);
                    cmd111.ExecuteNonQuery();



                }

                MessageBox.Show("Games Inserted/Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void SaveandRecalcbtn_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (radioBtnNBA.Checked.Equals(true))
                {
                    Sql3 = "select ID,ImeHKluba,ImeAKluba, HResult,AResult,TotalResult,HandicapResult,Season,Line,Total, HomePace, AwayPace from " + NBAMLBResultsVar + " where IDOklade = '" + IDO2 + "'";
                }
                else if (radioBtnMLB.Checked.Equals(true))
                {
                    Sql3 = "select ID,ImeHKluba,ImeAKluba, HResult,AResult,TotalResult,HandicapResult,HomeStarter,HomeStarterHand as HH,AwayStarter,AwayStarterHand as AH from " + NBAMLBResultsVar + " where IDOklade = '" + IDO2 + "'";
                }
                int j = 0;
                int k = 0;
                for (int i = 0; i < DGVAnalysisResults.Rows.Count - 1; i++)
                {
                    String rez1 = DGVAnalysisResults.Rows[i].Cells[3].Value.ToString();
                    String rez2 = DGVAnalysisResults.Rows[i].Cells[4].Value.ToString();
                    Double rezz1 = Convert.ToDouble(rez1);
                    Double rezz2 = Convert.ToDouble(rez2);
                    Double tot1 = rezz1 + rezz2;
                    Double hen1 = rezz2 - rezz1;
                    if (ds.Tables[0].Rows[i].RowState != DataRowState.Deleted)
                    {
                        ds.Tables[0].Rows[j + k][5] = tot1.ToString();
                        ds.Tables[0].Rows[j + k][6] = hen1.ToString();
                    }
                    else
                    {
                        k++;
                        ds.Tables[0].Rows[j + k][5] = tot1.ToString();
                        ds.Tables[0].Rows[j + k][6] = hen1.ToString();
                    }
                    j++;
                }

                try
                {
                    adapterMain = new SqlDataAdapter(Sql3, connection);
                    SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapterMain);
                    adapterMain.Update(ds);
                    MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                updateResultsPB_Click(sender, e);
            }
        }
        private void pace_eff()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (radioBtnNBA.Checked == true)
                {
                    if (teamsDetailedStatsDGV.DataSource != null)
                    {
                        teamsDetailedStatsDGV.DataSource = null;
                    }
                    else
                    {
                        teamsDetailedStatsDGV.Rows.Clear();
                    }

                    teamsDetailedStatsDGV.ColumnCount = 3;

                    string sqlquery_pace_away = @"select PaceAway from klubovi where KratkoIme = '" + AwayTeamCB.Text.Split(' ')[1] + "'";
                    SqlCommand sqlcmd_pace_away = new SqlCommand(sqlquery_pace_away, connection);
                    SqlDataReader readd33 = sqlcmd_pace_away.ExecuteReader();
                    while (readd33.Read())
                    {
                        paceaway = (readd33["PaceAway"].ToString());
                    }
                    readd33.Close();

                    string sqlquery_pace_home = @"select PaceHome from klubovi where KratkoIme = '" + HomeTeamCB.Text.Split(' ')[1] + "'";
                    SqlCommand sqlcmd_pace_home = new SqlCommand(sqlquery_pace_home, connection);
                    SqlDataReader readd34 = sqlcmd_pace_home.ExecuteReader();
                    while (readd34.Read())
                    {
                        pacehome = (readd34["PaceHome"].ToString());
                    }
                    readd34.Close();

                    string sqlquery_oeff_away = @"select OeffAway from klubovi where KratkoIme = '" + AwayTeamCB.Text.Split(' ')[1] + "'";
                    SqlCommand sqlcmd_oeff_away = new SqlCommand(sqlquery_oeff_away, connection);
                    SqlDataReader readd35 = sqlcmd_oeff_away.ExecuteReader();
                    while (readd35.Read())
                    {
                        offeffaway = (readd35["OeffAway"].ToString());
                    }
                    readd35.Close();

                    string sqlquery_oeff_home = @"select OeffHome from klubovi where KratkoIme = '" + HomeTeamCB.Text.Split(' ')[1] + "'";
                    SqlCommand sqlcmd_oeff_home = new SqlCommand(sqlquery_oeff_home, connection);
                    SqlDataReader readd36 = sqlcmd_oeff_home.ExecuteReader();
                    while (readd36.Read())
                    {
                        offeffhome = (readd36["OeffHome"].ToString());
                    }
                    readd36.Close();

                    string sqlquery_deff_away = @"select DeffAway from klubovi where KratkoIme = '" + AwayTeamCB.Text.Split(' ')[1] + "'";
                    SqlCommand sqlcmd_deff_away = new SqlCommand(sqlquery_deff_away, connection);
                    SqlDataReader readd37 = sqlcmd_deff_away.ExecuteReader();
                    while (readd37.Read())
                    {
                        deffeffaway = (readd37["DeffAway"].ToString());
                    }
                    readd37.Close();

                    string sqlquery_deff_home = @"select DeffHome from klubovi where KratkoIme = '" + HomeTeamCB.Text.Split(' ')[1] + "'";
                    SqlCommand sqlcmd_deff_home = new SqlCommand(sqlquery_deff_home, connection);
                    SqlDataReader readd38 = sqlcmd_deff_home.ExecuteReader();
                    while (readd38.Read())
                    {
                        deffeffhome = (readd38["DeffHome"].ToString());
                    }
                    readd38.Close();


                    coversrow = new string[] { paceaway, "Pace", pacehome };
                    teamsDetailedStatsDGV.Rows.Add(coversrow);

                    coversrow2 = new string[] { offeffaway, "Off Eff", offeffhome };
                    teamsDetailedStatsDGV.Rows.Add(coversrow2);

                    coversrow3 = new string[] { deffeffaway, "Deff Eff", deffeffhome };
                    teamsDetailedStatsDGV.Rows.Add(coversrow3);

                    for (i = 0; i < 3; i++)
                    {
                        teamsDetailedStatsDGV.Rows[i].Cells[1].Style.ForeColor = Color.White;
                    }

                    teamsDetailedStatsDGV.Rows[0].Cells[0].Selected = false;

                    label45.Text = AwayTeamCB.Text.Split(' ')[1];
                    label46.Text = HomeTeamCB.Text.Split(' ')[1];
                }
            }
        }

        private void TotalPlusButton_Click(object sender, EventArgs e)
        {
            double totalNEW = Convert.ToDouble(TotalBox.Text);
            totalNEW += 0.5;
            TotalBox.Text = totalNEW.ToString();
        }

        private void TotalMinusButton_Click(object sender, EventArgs e)
        {
            double totalNEW = Convert.ToDouble(TotalBox.Text);
            totalNEW -= 0.5;
            TotalBox.Text = totalNEW.ToString();
        }

        private void HandicapPlusButton_Click(object sender, EventArgs e)
        {
            double totalNEW = Convert.ToDouble(HandicapBox.Text);
            totalNEW += 0.5;
            HandicapBox.Text = totalNEW.ToString();
        }

        private void HandicapMinusButton_Click(object sender, EventArgs e)
        {
            double totalNEW = Convert.ToDouble(HandicapBox.Text);
            totalNEW -= 0.5;
            HandicapBox.Text = totalNEW.ToString();
        }

        private void Checkboxes()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                ToBetOnCB.Checked = false;
                BetProcessedCB.Checked = false;
                editedCB.Checked = false;
                ToBetOn = 0;
                BetProcessed = 0;
                Edited = 0;


                string sqlqcheck = @"SELECT ToBetOn, BetProcessed, Edited from " + NBAMLBBetsVar + " where IDOklade = " + IDoklBox.Text + "";

                SqlCommand cmdsqlqcheck = new SqlCommand(sqlqcheck, connection);
                SqlDataReader readsqlqcheck = cmdsqlqcheck.ExecuteReader();

                while (readsqlqcheck.Read())
                {
                    ToBetOn = Convert.ToInt32(readsqlqcheck["ToBetOn"]);
                    BetProcessed = Convert.ToInt32(readsqlqcheck["BetProcessed"]);
                    Edited = Convert.ToInt32(readsqlqcheck["Edited"]);
                }
                readsqlqcheck.Close();

                if (ToBetOn == 1)
                {
                    ToBetOnCB.Checked = true;
                }
                else
                {
                    ToBetOnCB.Checked = false;
                }
                if (BetProcessed == 1)
                {
                    BetProcessedCB.Checked = true;
                }
                else
                {
                    BetProcessedCB.Checked = false;
                }
                if (Edited == 1)
                {
                    editedCB.Checked = true;
                }
                else
                {
                    editedCB.Checked = false;
                }
            }
        }

        private void IDoklBox_TextChanged(object sender, EventArgs e)
        {
            if (IDoklBox.Text != "")
            {
                Checkboxes();
            }
        }

        private void editedCB_MouseClick(object sender, MouseEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (editedCB.Checked == true)
                {
                    seton = 1;
                }
                else
                {
                    seton = 0;
                }
                string sqlqEdited = @"UPDATE " + NBAMLBBetsVar + " SET Edited = " + seton + " where IDOklade = " + IDoklBox.Text + "";

                SqlCommand cmd112 = new SqlCommand(sqlqEdited, connection);

                cmd112.ExecuteNonQuery();
            }
        }

        private void ToBetOnCB_MouseClick(object sender, MouseEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (ToBetOnCB.Checked == true)
                {
                    seton = 1;
                }
                else
                {
                    seton = 0;
                }
                string sqlqToBetOn = @"UPDATE " + NBAMLBBetsVar + " SET ToBetOn = " + seton + " where IDOklade = " + IDoklBox.Text + "";

                SqlCommand cmd112 = new SqlCommand(sqlqToBetOn, connection);

                cmd112.ExecuteNonQuery();
            }
        }

        private void BetProcessedCB_MouseClick(object sender, MouseEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                if (BetProcessedCB.Checked == true)
                {
                    seton = 1;
                }
                else
                {
                    seton = 0;
                }

                string sqlqBetProcessed = @"UPDATE " + NBAMLBBetsVar + " SET BetProcessed = " + seton + " where IDOklade = " + IDoklBox.Text + "";

                SqlCommand cmd112 = new SqlCommand(sqlqBetProcessed, connection);

                cmd112.ExecuteNonQuery();
            }
        }

        private void ateamradio_CheckedChanged(object sender, EventArgs e)
        {
            if (ateamradio.Checked == true)
            {
                homecheckBox1.Checked = false;
                awaycheckBox2.Checked = true;
                button3_Click(sender, e);
            }
            else
            {
                homecheckBox1.Checked = true;
                awaycheckBox2.Checked = false;
                button3_Click(sender, e);
            }
        }

        private void TotalBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
            (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == ',') && ((sender as System.Windows.Forms.TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
            if (Regex.IsMatch(TotalBox.Text, @"\,\d"))
            {
                e.Handled = true;
            }
        }

        private void HandicapBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
          (e.KeyChar != ',') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == ',') && ((sender as System.Windows.Forms.TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
            // only allow one minus
            if ((e.KeyChar == '-') && ((sender as System.Windows.Forms.TextBox).Text.IndexOf('-') > -1))
            {
                e.Handled = true;
            }
            if (Regex.IsMatch(HandicapBox.Text, @"\,\d"))
            {
                e.Handled = true;
            }
        }

        private void DelRowBtn_Click(object sender, EventArgs e)
        {

            DialogResult d = MessageBox.Show("Are you sure you want to delete this game?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            try
            {

                if (d == DialogResult.Yes)
                {

                    using (SqlConnection connection = new SqlConnection(_connectionString))
                    {
                        connection.Open();

                        Sql3 = "select ID,ImeHKluba,ImeAKluba, HResult,AResult,TotalResult,HandicapResult,Season,Line,Total, HomePace, AwayPace from " + NBAMLBResultsVar + " where IDOklade = '" + IDO2 + "'";

                        foreach (DataGridViewCell oneCell in DGVAnalysisResults.SelectedCells)
                        {
                            if (oneCell.Selected)
                                DGVAnalysisResults.Rows.RemoveAt(oneCell.RowIndex);
                        }

                        try
                        {
                            adapterMain = new SqlDataAdapter(Sql3, connection);
                            SqlCommandBuilder cmd223 = new SqlCommandBuilder(adapterMain);
                            adapterMain.Update(ds);
                            MessageBox.Show("Updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }

                        updateResultsPB_Click(sender, e);
                    }
                }
                else if (d == DialogResult.No)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void AvsDivBTN_Click(object sender, EventArgs e)
        {
            string vsDIVvar = "";
            if (ateamradio.Checked == true)
            {
                vsDIVvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+o%3Adivision%3Ddivision&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                vsDIVvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+o%3Adivision%3Ddivision&submit=++S+D+Q+L+%21++";
            }
            Process.Start(vsDIVvar);
        }

        private void AafterLBTN_Click(object sender, EventArgs e)
        {
            string afterLvar = "";
            if (ateamradio.Checked == true)
            {
                afterLvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+p%3AL&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                afterLvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+p%3AL&submit=++S+D+Q+L+%21++";
            }
            Process.Start(afterLvar);
        }

        private void vsNODIVBTN_Click(object sender, EventArgs e)
        {
            string vsNODIVvar = "";
            if (ateamradio.Checked == true)
            {
                vsNODIVvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+o%3Adivision%21%3Ddivision&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                vsNODIVvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+o%3Adivision%21%3Ddivision&submit=++S+D+Q+L+%21++";
            }
            Process.Start(vsNODIVvar);
        }

        private void AfterWBTN_Click(object sender, EventArgs e)
        {
            string afterWvar = "";
            if (ateamradio.Checked == true)
            {
                afterWvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+p%3AW&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                afterWvar = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+p%3AW&submit=++S+D+Q+L+%21++";
            }
            Process.Start(afterWvar);
        }

        private void firstBtoBbtn_Click(object sender, EventArgs e)
        {
            string firstndofBtoB = "";
            if (ateamradio.Checked == true)
            {
                firstndofBtoB = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+n%3Arest%3D0&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                firstndofBtoB = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+n%3Arest%3D0&submit=++S+D+Q+L+%21++";
            }
            Process.Start(firstndofBtoB);
        }

        private void secondBtoBbtn_Click(object sender, EventArgs e)
        {
            string secondndofBtoB = "";
            if (ateamradio.Checked == true)
            {
                secondndofBtoB = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + AwayTeamCB.Text.Split(' ')[1] + "+and+A+and+rest%3D0&submit=++S+D+Q+L+%21++";
            }
            else if (hteamradio.Checked == true)
            {
                secondndofBtoB = "http://sportsdatabase.com/" + LeagueNBAMLB.ToLower() + "/query?output=default&sdql=season+%3D+" + SeasonNBAMLB + "+and+team+%3D+" + HomeTeamCB.Text.Split(' ')[1] + "+and+H+and+rest%3D0&submit=++S+D+Q+L+%21++";
            }
            Process.Start(secondndofBtoB);
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void lastXGamesSetBtn_Click(object sender, EventArgs e)
        {
            sharedmethods.UpdateSetting("LastXGames", LastXgamesCB.Text);
        }
    }
}
